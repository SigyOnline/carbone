<?php

require_once MYCORE. '/Cadastro/Imovel.php';
require_once MYCORE. '/Cadastro/Noticia.php';
require_once MYCORE. '/Cadastro/Popup.php';



Class IndexController extends My_Controller
{
	public function ini()
	{
		$this->imovel = new Cadastro_Imovel($this->db);
		$this->popup = new Cadastro_Popup($this->db);

	}
	
	public function indexAction ()
	{
		$this->view->fase = $this->cfiltro->ListaStatus();
		$this->view->bairros = $this->imovel->getBairroPorZona();
		$this->view->zonas = $this->imovel->getZonas();
		$noticias = new Cadastro_Noticia($this->db);
		$this->view->noticia = $noticias->lista(NULL,array('data desc'));
		$this->view->popup = $this->popup->lista();
		$this->view->banner = $this->imovel->listaBannerHome();

	}
	
	public function abasAction ()
	{
		$this->view->fase = (array)$this->cfiltro->ListaStatus();
		
		if ( isset($_GET['aba']) )
			$filtro = $_GET['aba']; 
		else 
			$_GET['aba'] = $filtro = 'VENDA';
			
		$this->view->dados = $this->imovel->ListaAba($filtro);
	}
	
	public function erroAction ()
	{
		$this->view->fase = $this->imovel->getFase();
		$this->view->bairros = $this->imovel->getBairroPorZona();
		$this->view->zonas = $this->imovel->getZonas();
	}
}