<?php

require_once MYCORE. '/Cadastro/Index.php';
require_once MYCORE. '/Cadastro/FavCom.php';
require_once  MYCORE.'/Cadastro/Newsletter.php';

Class ContatoController extends My_Controller
{
	public function ini()
	{
		$this->view->title = 'Contato';
	}
	public function indexAction ()
	{
		
	}
	
	public function criticasAction()
	{
		
	}
	
	public function sacAction ()
	{
		
	}

	public function diretoriaAction()
	{
		
	}

	public function cadastreAction()
	{
		$categoria = new Cadastro_Consultafiltro($this->db);
		$this->view->categoria = $categoria->ListaCategoria();
	}

	public function emailenviadoAction ()
	{
		$this->view->title = 'Confirmação de Envio';
	}
	
	public function trabalheconoscoAction ()
	{
		$this->view->title = 'Trabalhe Conosco';
	}
	
	public function enviadoAction ()
	{
		//Defino a Chave do meu site
		$secret_key = '6LezSGIUAAAAAP1cw0OyBQ6WudarG3tGf8QKSbuh';

		//Pego a validação do Captcha feita pelo usuário
		$recaptcha_response = $_POST['g-recaptcha-response'];

		// Verifico se foi feita a postagem do Captcha 
		if(isset($recaptcha_response)){

			// Valido se a ação do usuário foi correta junto ao google
			$answer = 
			json_decode(
				file_get_contents(
					'https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.
					'&response='.$_POST['g-recaptcha-response']
				)
			);
						// exit($recaptcha_response);

			// Se a ação do usuário foi correta executo o restante do meu formulário
			if($answer->success == 1) {
				Zend_Loader::loadClass('Zend_Mail');
				Zend_Loader::loadClass('Zend_Mail_Transport_Smtp');

				$conteudo = "";
				$assunto = $_POST['Assunto'];
				
				if (isset($_POST['fotos']))
					unset($_POST['fotos']);
				
				foreach($_POST as $chave => $valor) 
				{
					if ($chave != "g-recaptcha-response") {
					$conteudo.= str_replace("_"," ",$chave) . ": <b>" . $valor . "</b><br>";
					}
				}

				$data = date("d/m/y");                                  	
				$ip = $_SERVER['REMOTE_ADDR'];                          	
				$navegador = $_SERVER['HTTP_USER_AGENT'];               	
				$hora = date("H:i");                                    	

				$corpoMensagem = " 
				<html>
					<body>
						<font face=\"Arial\" size=\"2\" color=\"#333333\"><br/>
							Data: <b>$data</b><br/>
							Hora: <b>$hora</b><br/>    
							$conteudo
						</font>
					</body>
				</html>
				";

				$host = 'mail.sigyonline.com.br';
				$from = 'homologar@sigyonline.com.br';
				$name = 'Site - '. $this->config->www->pagetitle;
				//$remetente = 'sac@marc.com.br';
				$remetente = 'homologar@sigyonline.com.br';
				
				$config = array('auth' => 'login',
								'username' 	=> 'homologar@sigyonline.com.br',
								'password' 	=> 'a10b20c30',
								'port' => "587");
				
				$tr   = new Zend_Mail_Transport_Smtp($host,$config);
				$mail = new Zend_Mail('utf-8');
				$mail->setFrom($from, $name);

				if (!empty($_POST['EmailIndicacao']))
				{
					$mail->addCc($_POST['EmailIndicacao']);	
				}

				if (!empty($_POST['Email']))
				{
					$mail->AddBCC($_POST['Email']);	
				}

				$mail->addTo($remetente);
				$mail->setSubject($assunto);
				$mail->setBodyHtml($corpoMensagem);
				
				if(isset($_FILES['file']))
				{
					
					$mail->createAttachment(file_get_contents($_FILES['file']['tmp_name']), $_FILES['file']['type'], Zend_Mime::DISPOSITION_INLINE, Zend_Mime::ENCODING_BASE64, $_FILES['file']['name']);
					
				}
				
				if ( isset($_FILES['fotos']) )
				{
					for($i=0; $i < count($_FILES['fotos']['name']); $i++ )
					{
						if ( $_FILES['fotos']['error'][$i] == 0)
						{
							$content = file_get_contents($_FILES['fotos']['tmp_name'][$i]); 
							$attachment = new Zend_Mime_Part($content);
							$attachment->type = $_FILES['fotos']['type'][$i];
							$attachment->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
							$attachment->encoding = Zend_Mime::ENCODING_BASE64;
							$attachment->filename = $_FILES['fotos']['name'][$i];
							$mail->addAttachment($attachment);
						} 
					}	
				}


				try{
					$mail->send();
					$this->_redirect('contato/emailenviado');
					echo "<script>location.href='emailenviado'</script>";
				} catch(Exception $e){
					die(print_r($e));
				}
			}
			else{
				$_SESSION['back'] = 'falhou';
				//echo "<script>console.log(".$_SESSION['back'].");</script>";
				echo "<script type='text/javascript'>window.history.back();</script>";

				if($_POST['Assunto'] == 'Contato-Por-Email'){
					$this->_redirect('contato');
				}
				elseif($_POST['Assunto'] == 'Trabalhe-Conosco'){
					$this->_redirect('contato/trabalheconosco');
				}
				else{
					$this->_redirect('contato');
				}
				// $this->_redirect('contato');
				//$this->_redirect('contato/emailenviado');
			}
		}
		//ocorre se o formulario nao tiver o captcha do google
		else{
			Zend_Loader::loadClass('Zend_Mail');
			Zend_Loader::loadClass('Zend_Mail_Transport_Smtp');

			$conteudo = "";
			$assunto = $_POST['Assunto'];

			if (isset($_POST['fotos']))
				unset($_POST['fotos']);

			foreach($_POST as $chave => $valor) 
			{
				$conteudo.= str_replace("_"," ",$chave) . ": <b>" . $valor . "</b><br>";
			}
			
			$data = date("d/m/y");                                  	
			$ip = $_SERVER['REMOTE_ADDR'];                          	
			$navegador = $_SERVER['HTTP_USER_AGENT'];               	
			$hora = date("H:i");                                    	

			$corpoMensagem = " 
			<html>
				<body>
					<font face=\"Arial\" size=\"2\" color=\"#333333\"><br/>
						Data: <b>$data</b><br/>
						Hora: <b>$hora</b><br/>    
						$conteudo
					</font>
				</body>
			</html>
			";

			$host = 'smtp.carboneimoveis.com.br';
			$from = 'contato@carboneimoveis.com.br';
			$name = 'Site - '. $this->config->www->pagetitle;
			$remetente = 'contato@carboneimoveis.com.br';

			$config = array('auth' => 'login',
							'username' 	=> 'contato@carboneimoveis.com.br',
							'password' 	=> 'a10b20c30',
							'port' => "587");

			$tr   = new Zend_Mail_Transport_Smtp($host,$config);
			$mail = new Zend_Mail('utf-8');
			$mail->setFrom($from, $name);

			if (!empty($_POST['EmailIndicacao']))
			{
				$mail->addCc($_POST['EmailIndicacao']);	
			}

			if (!empty($_POST['Email']))
			{
				$mail->AddBCC($_POST['Email']);	
			}

			$mail->addTo($remetente);
			$mail->setSubject($assunto);
			$mail->setBodyHtml($corpoMensagem);
			
			if($_FILES['file']['type'] == 'application/octet-stream'){
				$_FILES['file']['type'] = 'application/docx';
			}
			if(isset($_FILES['file']))
			{

				$mail->createAttachment(file_get_contents($_FILES['file']['tmp_name']), $_FILES['file']['type'], Zend_Mime::DISPOSITION_INLINE, Zend_Mime::ENCODING_BASE64, $_FILES['file']['name']);

			}

			if ( isset($_FILES['fotos']) )
			{
				for($i=0; $i < count($_FILES['fotos']['name']); $i++ )
				{
					if ( $_FILES['fotos']['error'][$i] == 0)
					{
						$content = file_get_contents($_FILES['fotos']['tmp_name'][$i]); 
						$attachment = new Zend_Mime_Part($content);
						$attachment->type = $_FILES['fotos']['type'][$i];
						$attachment->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
						$attachment->encoding = Zend_Mime::ENCODING_BASE64;
						$attachment->filename = $_FILES['fotos']['name'][$i];
						$mail->addAttachment($attachment);
					} 
				}	
			}

			try{
				$mail->send();
				$this->_redirect('contato/emailenviado');
			} catch(Exception $e){
				die($e);
			}
		}
	}
	
	public function newsletterAction()
	{
		header('Content-Type: application/json');
		
		$obj = new Cadastro_Newsletter($this->db);
		
		if( $_POST )
		{

			if (strtoupper($_SESSION['securimage_code_value']['default']) == strtoupper($_POST['code']))
			{
				$verifica = $obj->GetNewsletter($_POST['email']);
				
				if ( empty($verifica) )
				{
					$res = $obj->novo($_POST);
					
					if ($res > 0)
					{
						$resultado = array('error' => 0, 'msg' => 'Cadastro Realizado');
					}else{
						$resultado = array('error' => 1, 'msg' => 'Erro ao Cadastrar'); 	
					} 
				}else{
					$resultado = array('error' => 1, 'msg' => 'Email já cadastrado'); 	
				} 
			} else 
			{
				$resultado = array('error' => 1, 'msg' => 'Código Inválido');
			}
		} else 
		{
			$resultado = array('error' => 1, 'msg' => 'Dados inválidos');
		}
		
		exit(json_encode($resultado));
	}

	public function loginAction()
	{

	}
	
}