<?php

require_once MYCORE. '/Cadastro/Index.php';

Class ContatoController extends My_Controller
{
	public function indexAction ()
	{
		
	}
	
	public function cadastreAction ()
	{
		
	}
	
	public function trabalheconoscoAction ()
	{
		
	}
	
	public function ligamosAction ()
	{
	}
	
	public function contatoemailAction ()
	{
		
	}
	
	public function emailenviadoAction ()
	{
		
	}
	
	public function newsletterAction ()
	{
		if ( $_POST['nome'] &&  $_POST['email'] )
		{
			require_once MYCORE. '/Cadastro/Newsletter.php';
			$dados = array(
							'nome' => strip_tags($_POST['nome']),
							'email' => strip_tags($_POST['email'])
							);
			
			$newsletter = new Cadastro_Newsletter($this->db);
			$id = $newsletter->novo($dados);
			
			if ( $id )
			{
				$this->noCaptcha = true;
				Zend_Registry::set('noCaptcha', $this->noCaptcha);
				$this->forward('enviado');
			} else 
			{
				$resultado = array('error' => 1, 'msg' => 'Não foi possível cadastrar o email');
				$this->_helper->json->sendJson(($resultado));
			} 	
		}
	}
	
	public function enviadoAction ()
	{
		$this->noCaptcha = false;
		
		if ( Zend_Registry::isRegistered('noCaptcha') )
			$this->noCaptcha = Zend_Registry::get('noCaptcha'); 
		
		if ( $this->noCaptcha || strtoupper($_SESSION['captcha']) == strtoupper($_POST['code']))
		{
			unset($_POST['code']);
			Zend_Loader::loadClass('Zend_Mail');
			Zend_Loader::loadClass('Zend_Mail_Transport_Smtp');
	
			$conteudo = "";
			$assunto = $_POST['Assunto'];
				
			foreach($_POST as $chave => $valor) 
			{
				$conteudo.= str_replace("_"," ",$chave) . ": <b>" . $valor . "</b><br>";
			}
		
				$data = date("d/m/y");                                  	
				$ip = $_SERVER['REMOTE_ADDR'];                          	
				$navegador = $_SERVER['HTTP_USER_AGENT'];               	
				$hora = date("H:i");                                    	
				
				$corpoMensagem = " 
					<html>
						<body>
							<font face=\"Arial\" size=\"2\" color=\"#333333\"><br/>
								Data: <b>$data</b><br/>
								Hora: <b>$hora</b><br/>    
								$conteudo
							</font>
						</body>
					</html>
				";

			$host = 'mail.sigyonline.com.br' ;
			$from = 'homologar@sigyonline.com.br';
			$name = 'Site - '. $this->config->www->pagetitle;
			$remetente = 'site@carboneimoveis.com.br';
			$copiaoculta = 'andre@carboneimoveis.com.br';
           	$copia = 'contato@carboneimoveis.com.br';
			
			$config = array('auth' => 'login',
							'username' 	=> 'homologar@sigyonline.com.br',
	                   		'password' 	=> 'a10b20c30',
	                   		'ssl'		=> "tls" );
			
			$config['port'] = 587;
			
			$tr   = new Zend_Mail_Transport_Smtp($host,$config);
		    $mail = new Zend_Mail('utf-8');
		    $mail->setFrom($from, $name);
		    
				if (!empty($_POST['EmailIndicacao']))
				{
					$mail->addCc($_POST['EmailIndicacao']);	
				}
				
			$mail->addTo($remetente);
			$mail->addCC($copia);
            $mail->addBCC($copiaoculta);
			$mail->setSubject($assunto);
			$mail->setBodyHtml($corpoMensagem);

			//print_r($_FILES);
			//exit();
			if(isset($_FILES['file']))
   			  {
 
				$mail->createAttachment(file_get_contents($_FILES['file']['tmp_name']), $_FILES['file']['type'], Zend_Mime::DISPOSITION_INLINE, Zend_Mime::ENCODING_BASE64, $_FILES['file']['name']);
   			  }
			
   			  if ( isset($_FILES['fotos']) )
   			  {
   			  	for($i=0; $i < count($_FILES['fotos']['name']); $i++ )
   			  	{
   			  		if ( $_FILES['fotos']['error'][$i] == 0)
   			  		{
   			  			$content = file_get_contents($_FILES['fotos']['tmp_name'][$i]);
   			  			$attachment = new Zend_Mime_Part($content);
   			  			$attachment->type = $_FILES['fotos']['type'][$i];
   			  			$attachment->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
   			  			$attachment->encoding = Zend_Mime::ENCODING_BASE64;
   			  			$attachment->filename = $_FILES['fotos']['name'][$i];
   			  			$mail->addAttachment($attachment);
   			  		}
   			  	}
   			  }
   			  
			$mail->send($tr);
			
			$resultado = array('error' => 0, 
// 								'msg' => 'Enviado',
								'redir' => 'contato/emailenviado');
			
		}else{
			$resultado = array('error' => 1, 'msg' => 'Código inválido'); 		
		} 
		
			$this->_helper->json->sendJson($resultado);
	}
	
}