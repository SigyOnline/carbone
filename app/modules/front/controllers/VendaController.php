<?php
require_once MYCORE. '/Cadastro/Venda.php';
class VendaController extends My_Controller 
{
	public function ini(){
		$this->obj = new Cadastro_Venda($this->db);
	}
	
	public function indexAction(){
		
		if($_GET['json_cliente']!=''){
			
			echo  json_encode($this->obj->clientes(urldecode($_GET['json_cliente'])));
			exit;
		}
		$venda =  new Zend_Session_Namespace('venda');
		if($venda->dialog == true){
			$venda->dialog = false;
			$this->view->dialog_cliente = $venda->dados;
		}		
				
		if($_POST){
			
			$post = $_POST;	
			if($post['acao'] == 'cadastrar'){

				$exp = explode('/',  $_POST['data_retroativa']);

				$post['data_retroativa'] = $exp[2].'-'.$exp[1].'-'.$exp[0];
				
				$a = explode('/', $_POST['inicio_vigencia']);
			
			    $post['inicio'] = $a[2].'-'.$a[1].'-'.$a[0];
			    
				$b = explode('/', $_POST['fim_vigencia']);
			
			    $post['fim'] = $b[2].'-'.$b[1].'-'.$b[0];
			
				if($post['id_status'] == 'Selecione')
				{
					$post['id_status'] = 1;
				}else{
 					$post['id_status'] = $post['id_status'];
				}
				
				//$post['premio_liquido'] = str_replace(',', '.', $_POST['premio_liquido']);
				//$post['premio_bruto'] = str_replace(',', '.', $_POST['premio_bruto']);
				
				$res = $this->obj->novo($post);
				
				$historico['id_venda'] = $res;
				$historico['id_login'] = 1;
 				$historico['status'] = $post['id_status'];
				$historico['obs'] = $post['obs_status'];
				
				$this->obj->nova_interacao($historico);
				
				if ( $res )
				{
					$this->view->e = "Cadastro realizado com sucesso.";
				} else
				{
					$this->view->e = "Erro ao Cadastrar.";
				}	
			}	
			
			if($post['acao'] == 'editar'){
				$id = $post['id'];
				unset($post['id']);
				
				$exp = explode('/',  $_POST['data_retroativa']);

				$post['data_retroativa'] = $exp[2].'-'.$exp[1].'-'.$exp[0];
				
				$a = explode('/', $_POST['inicio_vigencia']);
			
			    $post['inicio'] = $a[2].'-'.$a[1].'-'.$a[0];
			    
				$b = explode('/', $_POST['fim_vigencia']);
			
			    $post['fim'] = $b[2].'-'.$b[1].'-'.$b[0];
				
				if($post['id_status'] == 'Selecione')
				{
					$post['id_status'] = 1;
				}else{
 					$post['id_status'] = $post['id_status'];
				}
				
				$res = $this->obj->edita($id,$post);
				
				if ( $res )
				{
					$this->view->e = "Atualização realizada com sucesso.";
				} else
				{
					$this->view->e = "Erro ao Editar";
				}
			}
			
			if($post['acao'] == 'excluir'){
				$return = $this->obj->apaga($post['id']);
					
				if($return > 0)
				{
					$this->view->e = "Registro excluido com sucesso!";
				}else{
					$this->view->e = "Erro ao excluir";
				}
			}
			if($post['acao'] == 'editar_cliente'){
				require_once MYCORE. '/Cadastro/Cliente.php';
				$cliente = new Cadastro_Cliente($this->db);
				
				$id= $post['id'];
				$res = $cliente->edita($id, $post);
				
				if ( $res )
				{
					$this->view->e = "Cadastro realizado com sucesso.";
				} else
				{
					$this->view->e = "Erro ao Cadastrar.";
				}	
			}
		}
		
		$this->view->grupo = $this->obj->grupos();
		$this->view->produtor = $this->obj->produtores();
		$this->view->seguradora = $this->obj->seguradoras();
		$this->view->produto = $this->obj->produtos();
		$this->view->cliente = $this->obj->clientes();
		$this->view->entidade = $this->obj->entidades();
		$this->view->status = $this->obj->status(1);
		$this->view->liststatus = $this->obj->liststatus();
		
		$this->view->filtro = null;
		if($_POST['filtro'] == 1){
			$this->view->filtro = $_POST;
		}
		$this->view->result = $this->obj->lista($this->view->filtro);
	}
	public function historicoAction(){
		if($_POST){
			$post = $_POST;
			$post['id_venda'] = $this->id;
			$post['id_login'] = $this->me->id_login;
			$status['id_status'] = $_POST['status'];
			$this->obj->nova_interacao($post);
			$res = $this->obj->edita($this->id,$status);
		}
		
		if($post['acao'] == 'editar'){
				$id = $post['id'];
				unset($post['id']);
				
				$exp = explode('/',  $_POST['data_retroativa']);

				$post['data_retroativa'] = $exp[2].'-'.$exp[1].'-'.$exp[0];
				
				if($post['id_status'] == 'Selecione')
				{
					$post['id_status'] = 1;
				}else{
 					$post['id_status'] = $post['id_status'];
				}
				
				$res = $this->obj->edita($id,$post);
				
				if ( $res )
				{
					$this->view->e = "Atualização realizada com sucesso.";
				} else
				{
					$this->view->e = "Erro ao Editar";
				}
			}
		
		$this->view->venda = $this->obj->dados_venda($this->id);
		$this->view->info = $this->obj->infocontato($this->id);
		$this->view->status = $this->obj->status(1);
		$this->view->result = $this->obj->lista_historico($this->id);
				
	}
	public function ajaxAction(){
		$this->view->render('html.func.php');  
		/*
		 * Editar Vendas
		 */
		if($_POST['editar']=='venda' && $this->id>0){
			$this->view->grupo = $this->obj->grupos();
			$this->view->produtor = $this->obj->produtores();
			$this->view->seguradora = $this->obj->seguradoras();
			$this->view->produto = $this->obj->produtos();
			$this->view->cliente = $this->obj->clientes();
			$this->view->entidade = $this->obj->entidades();
			$this->view->status = $this->obj->status(1);
			$this->view->liststatus = $this->obj->liststatus();
			$this->view->editar = $this->obj->dados_venda($this->id);
			echo $this->view->render('venda/form_venda.php');
		}
		if($_POST['editar']=='cliente' && $this->id>0){
			$this->view->grupo = $this->obj->grupos();
			$this->view->entidade = $this->obj->entidades();
			$this->view->produtor = $this->obj->produtores();
			$this->view->editar = $this->obj->getCliente($this->id);
			echo $this->view->render('venda/form_cliente.php');
		}
		exit;
	}
}