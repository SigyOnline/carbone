<?php echo $this->render("header.php"); ?>
		<main class="pr border_top">
		<section class="center">
			<!-- titulo -->
            <h1 class="pa noMobile ttl_internas">Administração de Imóveis</h1>
			<section class="box_img_padrao">
				<h2 class="sub_ttl"></h2>
                
                <div class="adm_img"></div>
				<p class="paragrafo_internas">Garantia de rentabilidade e tranquilidade, ao deixar seu imóvel para alugar, você conta com a experiência da <strong>Xavier Camargo Imobiliária</strong> e serviços personalizados.</p>      
                
                <li> Plano de marketing para promoção dos imóveis com divulgação no site da <strong>Xavier Camargo Imobiliária</strong>, líder de busca, jornal local e nos principais portais imobiliários;</li>
                <li> Velocidade e técnica na busca do inquilino ideal;</li>
                <li> Completa avaliação da documentação do potencial inquilino e garantias locatícias;</li>
                <li> Possibilidade de aluguel garantido;</li>
                <li> Assessoria jurídica para confecção do contrato de locação;</li>
                <li> Acompanhamento na entrega de chaves, com vistoria detalhada;</li> 
                <li> Análise da documentação do comprador, imóvel e vendedor;</li>
                <li> Representação do proprietário junto ao inquilino;</li>
                <li> Suporte durante toda a vigência do contrato.</li>        
                
			</section>
            
		</section>
	
<?php echo $this->render("footer.php"); ?>
	</main><!-- /main -->

