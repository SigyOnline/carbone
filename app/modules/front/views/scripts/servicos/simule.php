<?php echo $this->render("header.php"); ?>
		<main class="pr border_top">
		<section class="center">
			<!-- titulo -->
            <h1 class="pa noMobile ttl_internas ttl_extra">Simulador de Financiamento Imobiliário</h1>

<!--		<figure class="box_img_padrao simule"><img src="src/bg-simule-financiamento.jpg" alt="Imagem referente a simular um financiamento"></figure>-->

                <p class="paragrafo_internas1">Tire seus planos do papel. Faça uma simulação do seu financiamento com um de nossos consultores.</p>
                
                <p class="paragrafo_internas1 marg_top10">Este é um serviço direcionado a pessoas que desejam encontrar a melhor condição de financiamento Imobiliário.</p></br>
            
                <p class="paragrafo_internas1 marg_top10 marg_bot10"><strong>Correspondente Bancário</strong></p>

			<ul id="lista_bancosC">

				<li><a href="quemsomos/correspondente" title="Correspondente Bancário" rel="external" class="opacity"><img src="src/bancos/caixa.jpg" alt="Imagem referente ao banco caixa"></a></li>

			</ul><br>
            
                <p class="paragrafo_internas1 marg_top10"><strong>Instituições Financeiras</strong></p>
            
			<ul id="lista_bancos">

<!--		  <li><a href="quemsomos/correspondente" title="Simule com a Caixa" rel="external" class="opacity"><img src="src/bancos/caixa.jpg" alt="Imagem referente ao banco caixa"></a></li>-->

				<li><img src="src/bancos/itau.jpg" alt="Imagem referente ao banco itau"></li>

				<li><img src="src/bancos/santander.jpg" alt="Imagem referente ao banco santander"></li>

				<li><img src="src/bancos/bradesco.jpg" alt="Imagem referente ao banco bradesco"></li>

				<li><img src="src/bancos/bb.jpg" alt="Imagem referente ao banco"></li>

			</ul>



		</section>
<?php echo $this->render("footer.php"); ?>
	</main><!-- /main -->
</body>
</html>