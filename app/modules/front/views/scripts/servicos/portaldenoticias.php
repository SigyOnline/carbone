<?php echo $this->render("header.php"); ?>
        <main class="pr border_top">
        <section class="center">
            <!-- titulo -->
            <h1 class="pa noMobile ttl_internas">JM FOX / Noticias</h1>
             
            <?php 
             	if ( $this->noticia ):?>
                <ul>
             <ul id="portal_noticias" class="fl w100">
             <?php 
             		foreach ( $this->noticia as $n ): 
					 $limitTitulo = @strpos($n->noticia, " ",100) ;
					 if(!$limitTitulo) 
					 	$limitTitulo = 110;
			?>
                <li>
                    <div class="fl comp_img_news"><a href="servicos/noticia/id/<?php echo $n->id_noticia; ?>" class="fl" title=""><img src="<?php echo ($n->imagem)? $n->imagem : 'src/foto-indisponivel.jpg'; ?>" alt="Foto  destaque" /></a></div>
                    <h2 class="ttl_nome fnt_treb"><?php echo $n->titulo; ?></h2>
                    <p class="txt_noticia fnt_myriad"><?php echo substr( $n->noticia,0, $limitTitulo).'...'; ?>...</p><!--máx.carac.120-->
                    <a href="servicos/noticia/id/<?php echo $n->id_noticia; ?>" class="fl radius2 fnt_center fnt_arial link_consulte">LEIA +</a>
               </li> 
			<?php endforeach;?>
             </ul>    
           <?php endif;?>

            <nav id="paginacao">

                <div class="pag_number">

                <?php echo $this->noticia;?>

                </div>

            </nav>  

        </section>
    </main>
<?php echo $this->render("footer.php"); ?>
    </main><!-- /main -->
</body>
</html>