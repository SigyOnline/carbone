<?php echo $this->render("header.php");
?>
</section>
<section id="banner">
	<!-- BANNER ROTATIVO -->
	<div class="JB_FWS">
		<div class="JB_Container">
			<!-- ITEM BANNER -->          
			<?php foreach ($this->banner as $banner) :?>
				<!-- <a href="imoveis/detalhe/id/<?php echo $banner->codigo; ?>"> -->
					<div class="JB_Slide pr" >
						<div class="container-banner">
							<div class="img_content">
								<div>
									<?php echo $banner->status; ?>
									<span>Ref.:<?php echo $banner->codigo; ?></span>
								</div>
								<a href="<?php echo 'imoveis/detalhe/id/'.$banner->codigo ?>">
									<img src="<?php echo $banner->foto_destaque; ?>">
								</a>
							</div>
							<section class="conteudo_content">
								<!-- <h2>REF.: <?php echo $banner->codigo; ?></h2>
								<h1><?php echo $banner->categoria; ?> - <?php echo $banner->bairro; ?></h1>
								<div class="banner-dataimv">
									<span><i class="fa fa-caret-right" aria-hidden="true"></i> <?php echo (!empty($banner->dormitorios))? $banner->dormitorios : ""; ?> Dorm(s)</span>
									<span><i class="fa fa-caret-right" aria-hidden="true"></i> <?php echo (!empty($banner->suites))? $banner->suites : ""; ?> Suíte(s)</span>
									<span><i class="fa fa-caret-right" aria-hidden="true"></i> <?php echo (!empty($banner->vagas))? $banner->vagas : ""; ?> Vaga(s)</span>
									<span><i class="fa fa-caret-right" aria-hidden="true"></i> <?php echo (!empty($banner->banheiro_social_qtd))? $banner->banheiro_social_qtd : ""; ?> Banheiro(s)</span>
								</div>
								<div class="banner_price">
									<strong>R$ <?php echo number_format(($banner->status == 'VENDA')? $banner->valor_venda : $banner->valor_locacao, 2, ',', '.'); ?></strong>
								</div> -->
							<div class="textbanner_title">
								<h2><?php echo $banner->categoria; ?></h2>
								<h3><?php echo $banner->bairro; ?> / <?php echo $banner->cidade; ?></h3>
							</div>
							<div class="textbanner_body">
								<div class="banner_item" id="iconDorm"><?php echo $banner->dormitorios; ?> Dormitório(s)</div>
								<div class="banner_item" id="iconVaga"><?php echo $banner->vagas; ?> Vaga(s)</div>
								<div class="banner_item" id="iconSuite"><?php echo $banner->suites; ?> Suíte(s)</div>
								<div class="banner_item" id="iconArea">Área: <?php echo $banner->area_total; ?>m²</div>
								<div class="description">
									<?php if(!empty($banner->descricao_web)){ ?>
										<span>Descrição:</span>
										<p><?php echo $banner->descricao_web; ?></p>
									<?php } ?>
								</div>
								<?php 	if ( $banner->status == 'VENDA' ): ?>
									<p class="price">Venda: R$ <?php echo number_format($banner->valor_venda, 2, ',', '.'); ?></p>
								<?php endif; ?>
								<?php 	if ( $banner->status == 'ALUGUEL' ): ?>
									<p class="price">Aluguel: R$ <?php echo number_format($banner->valor_locacao, 2, ',', '.'); ?></p>
								<?php endif; ?>
								<?php 	if ( $banner->status == 'VENDA E ALUGUEL' ): ?>
									<p class="price">Venda: R$ <?php echo number_format($banner->valor_venda, 2, ',', '.'); ?></p>
									<p class="price">Aluguel: R$ <?php echo number_format($banner->valor_locacao, 2, ',', '.'); ?></p>
								<?php endif; ?>
								<a class="btn_consultar" href="<?php echo 'imoveis/detalhe/id/'.$banner->codigo ?>">Consultar</a>
							</div>
							</section>
						</div>
					</div>
				<!-- </a> -->
				<!-- <a href="<?php echo (!empty($banner->link))? $banner->link : '#'; ?>">
					<div class="JB_Slide pr">
						<img src="<?php echo $this->baseUrl . $banner->imagem;?>" alt="<?php echo $banner->titulo?>" name="background" /> -->
						<!--aside class="comporta_texto_banner radius4">
							<h1><?php echo $banner->titulo?></h1>
							<h2><?php echo nl2br($banner->descricao);?></h2>
							<a href="<?php echo $banner->link;?>" class="btn_banner fnt_center radius4 bold opacity">CONSULTE</a>
							php
						</aside-->
					<!-- </div>
				</a> -->
			<?php endforeach;?>
		</div>
		<!-- BANNER NAV SETAS -->
		<div class="JB_Button_Left_BG"></div>
		<div class="JB_Button_Left"></div>
		<div class="JB_Button_Right_BG"></div>
		<div class="JB_Button_Right"></div>
	</div>
</section>
<!-- destaques -->
<section id="destaques">
	<div class="center">
		<!--ttl -->
		<h1 class="ttl pa noMobile">
			Imóveis em <strong>Destaque</strong>
		</h1>
		<!--desktop/mobile -->
		<div class="abashome">
			<ul id="abas_menu noMobile">
				<?php foreach ($this->fase as $row) :?>
					<li><a class="ativabtn" href="javascript:carrega('<?php echo $row->status?>');"><?php echo $row->status;?></a></li>
				<?php endforeach;?>
			</ul>
		</div>
		<div id="carrega_abas"></div>
	</div>
</section>
<!-- compramos seu terreno / trabalhe conosco -->
<section id="row" class="noMobile marg_top50">
	<section class="center">
		<aside class="blocos_servicos pr">
			<h1 class="ttl">Cadastre seu Imóvel</h1>
			<img src="src/imagem-compramos-seu-terreno.jpg"
			alt="Imagem compramos seu terreno" />
			<p class="pa">Em breve entraremos em contato.</p>
			<!--<span class="pa bg_compramos"></span>--> <a href="contato/cadastre" class="pa">CONSULTE</a>
		</aside>
		<aside class="blocos_servicos pr noOnlyTablet">
			<h1 class="ttl">Curta nossa Fanpage no Facebook</h1>
			<div class="box_face">
				<div class="fb-like-box" data-href="https://www.facebook.com/carbone.imoveis" data-width="500" data-height="300" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="false"></div>
			</div>
		</aside>
	</section>
</section>

<!-- Mapa titulo -->
<section id="ttl_mapa">
	<div class="center">
		<h1 class="ttl pa noMobile noDesktop">
			Mapa de <strong>Atuação</strong>
		</h1>
	</div>
</section>


<!-- contato -->
<section id="box_contatos" class="noMobile marg_top50">
	<section class="center">
		<h1 class="ttl pa">
			Fale <strong>Conosco</strong>
		</h1>
		<ul>
			<li> <span class="icon_telefone"></span>
				<h1>Central de Atendimento</h1>

				<p>
					<img src="" alt="" class="hm-home-tel"/>
					(11) 2606-4000<br>
				</p>
			</li>
			<li><a href="contato/ligamos<?php echo $this->id ? "?ref={$this->id}":NULL;?>" class="" data-fancybox data-src="#ligamos_lightbox"> <span class="icon_celular"></span>
				<h1>
					Ligamos <br />Para você
				</h1>
				<p>Solicite nosso contato.</p>
			</a></li>
			<li><a href="contato/contatoemail<?php echo $this->id ? "?ref={$this->id}":NULL;?>" class="" data-fancybox data-src="#contato_lightbox"> <span class="icon_email"></span>
				<h1>
					Contato <br />por e-mail
				</h1>
				<p>Fale conosco de forma rápida</p>
			</a></li>
			<li><span class="icon_email"></span>
				<h1>
					Nossos <br />Simuladores
				</h1>
				<a href="https://ww3.itau.com.br/imobline/pre/simuladores_new/fichaProposta/index.aspx?IMOB_TipoBKL=&amp;ident_bkl=pre" target="_blank"><img src="src/bancos/itau.png" alt="Itau"></a>
				<a href="http://www.bradesco.com.br/html/classic/produtos-servicos/emprestimo-e-financiamento/index.shtm" target="_blank"><img src="src/bancos/bradesco.png" alt="bradesco"></a>
				<a href="http://www.santander.com.br/portal/wps/script/templates/GCMRequest.do?page=5516" target="_blank"><img src="src/bancos/santander.png" alt="santander"></a>
				<a href="http://www8.caixa.gov.br/siopiinternet/simulaOperacaoInternet.do?method=inicializarCasoUso" target="_blank"><img src="src/bancos/caixa.png" alt="caixa"></a>
			</li>

		</ul>
	</section>
</section>

<?php foreach($this->popup as $p) {
	if ($p->ativo != 1) {

	}else{ ?>
		<a href="#popup_entrada" class="fancybox clickhref" style="display: none;"></a>
		<div style="display: none;">
			<div id="popup_entrada">
				<header class="lightbox_padrao">
					<span class="closeFancyBox fecha_lightbox" title="Fechar">X</span>
				</header>
				<section class="centraliza_lightbox">
					<a href="<?php echo $p->link ?>">
						<img title="<?php echo $p->titulo ?>" src="<?php echo $p->imagem ?>">
					</a>
				</section>
			</div>
		</div>
		<script type="text/javascript">
			window.onload = function () {
				$('.clickhref').click();
			}

			$(document).ready(function() {
				$(".fancybox").fancybox({
					openEffect	: 'none',
					closeEffect	: 'none'
				});
			});
		</script>

	<?php } 
} 
?>
<?php echo $this->render("footer.php"); ?>

<!-- likebox -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.0";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Scripts Banner Full Width -->
<script type="text/javascript"
src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="js/jb_fws.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		JBFWS = new JBFWS();
		JBFWS.init({
			/* Slider Width - Height */
			width            : "100%",
			height           : "675px",

			/* Buttons settings */
			showBigButtons   : 1,  /* 0 - Hide, 1 - Show */
			showSmallButtons : 1,  /* 0 - Hide, 1 - Show */

			/* Main Slide */
			slideSpeed       : 1000, /* miliseconds */
			slideEffect      : "easeInOutExpo",

			/* Secondary Slide */
			slideDelay       : 600,  /* miliseconds. 0 - Off */
			slideSpeed2      : 1000, /* miliseconds. 0 - Off */
			slideEffect2     : "easeOutExpo",

			/* Drag Slide */
			dragSlide        : 1, /* 0 - Off, 1 - On */

			/* Auto Slide */
			autoSlide        : 1,   /* 0 - Off, 1 - On */
			autoSlideDelay   : 4000 /* miliseconds */   
		});
	});
</script>
<!-- Script menu aba -->
<script type="text/javascript">
	$("#abas_menu").addClass("js");
    //$("#abas_menu").addClass("js").before('<div id="menu_carousel">☰ DESTAQUES: <strong>LANÇAMENTOS</strong></div>');
    $("#menu_carousel").click(function(){
    	$("#abas_menu").toggle("slow");
    });

    $(".ativabtn").click(function(){
    	$(".ativabtn").removeClass("active");
    	$(this).addClass("active");
    });


    $(window).resize(function(){
    	if(window.innerWidth > 730) {
    		$("#abas_menu").removeAttr("style");
    	}
    });

    var carrega = function (aba) { 
    	$.get('<?php echo $this->baseUrl; ?>index/abas', {'aba':aba}, function(data){
    		
//     		if ($(ob).hasClass('active')) {
//     			$(ob).removeClass("active");
//     		} else {
//     			$(ob).addClass("active");
//     		}
$('#carrega_abas').html(data);
});
    };
    
    $( ".select_regioes" ).change(function() {
    	if ( $(this).val() != '' && $(this).val() != undefined )
    	{
    		document.location.href = "/imoveis/?bairro[]="+ $(this).val()
    	}
    });
    carrega()
</script>
