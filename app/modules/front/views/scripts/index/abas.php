<?php
$dado = array_shift($this->dados);

//echo "<pre>";

//print_r($this->dados);


?>

<section class="col_destaques">
    <div class="imovel_destaque">
        <img src="<?php echo (empty($dado->foto_destaque))?'src/foto-indisponivel.jpg':$dado->foto_destaque; ?>"
            alt="Foto do imóvel em destaque"/>
        <div class="referencia_destaque pa">REF.: <?php echo $dado->codigo; ?></div>
        <div class="pos_info">
            <h1><?php echo $dado->bairro; ?> / <?php echo $dado->cidade; ?></h1>
            <h2><?php echo $dado->categoria; ?></h2>
        </div>
        <div class="infos">
            <div class="favorito_destaque pa" onclick="favoritos(this, '<?php echo $dado->codigo; ?>');" title="Adicionar aos Favoritos"><span></span></div>
            <div class="comparar_destaque pa" onclick="comparar(this, '<?php echo $dado->codigo; ?>');" title="Comparar Imóvel"><span></span></div>
            <h1 class="local"><?php echo $dado->bairro; ?> /  <?php echo $dado->cidade; ?></h1>
            <h2 class="tipo"><?php echo $dado->categoria; ?></h2>
            <span class="acabamento"></span>
            <h2 class="itens"><?php echo $dado->dormitorios; ?> Dorm(s)  | <?php echo $dado->vagas; ?> Vaga(s)
                                | <?php echo $dado->suites; ?> Suíte(s)</h2>
            <?php if( $dado->valor_venda > 0) : ?>
            <h2 class="valor">R$ <?php echo number_format($dado->valor_venda,2,',','.'); ?></h2>         
            <?php endif; ?>
            <?php if( $dado->valor_locacao > 0) : ?>
            <h2 class="valor">R$ <?php echo number_format($dado->valor_locacao,2,',','.'); ?></h2>
            <?php endif; ?>
            <a href="<?php echo $this->seo->getLink((array)$dado); ?>"
                class="">CONSULTE</a>
        </div>
    </div>
</section>
<section class="col_destaques_middle">
    <ul>
    <?php
        $i = 0;
        $dados = array_slice($this->dados, 0, 2);
        foreach ( $dados as $dado ):
        ?>
        <li class="imovel_destaque">
            <img src="<?php echo (empty($dado->foto_destaque))?'src/foto-indisponivel.jpg':$dado->foto_destaque; ?>" alt="Foto do imóvel em destaque" style="width:307px; height:150px;"/>
            <div class="referencia_destaque pa">REF.: <?php echo $dado->codigo; ?></div>

            <div class="pos_info">
                <h1> <?php echo $dado->bairro; ?> /  <?php echo $dado->cidade; ?></h1>
                <h2> <?php echo $dado->categoria; ?></h2>
            </div>
            <div class="infos">
                <div class="favorito_destaque pa" onclick="favoritos(this, '<?php echo $dado->codigo; ?>');" title="Adicionar aos Favoritos"><span></span></div>
                <div class="comparar_destaque pa" onclick="comparar(this, '<?php echo $dado->codigo; ?>');" title="Comparar Imóvel"><span></span></div>
                <h1 class="local"><?php echo $dado->bairro; ?> /  <?php echo $dado->cidade; ?></h1>
                <h2 class="tipo"><?php echo $dado->categoria; ?></h2>
                <span class="acabamento"></span>
                <h2 class="itens"><?php echo $dado->dormitorios; ?> Dorm(s)  | <?php echo $dado->vagas; ?> Vaga(s) | <?php echo $dado->suites; ?> Suíte(s)</h2>
                    <?php
                    $conteudo = "";
                    if( $dado->valor_venda > 0 && strtolower($_GET['aba']) != 'aluguel') {
                        
                        $conteudo.= "<h2 class='valor'>R$ " . number_format($dado->valor_venda,2,',','.') . "</h2>";
                        
                    }	
                    if ($dado->valor_locacao > 0 ) {

                        $conteudo.= "<h2 class='valor'>R$ " . number_format($dado->valor_locacao,2,',','.') . "</h2>";
                    }
                    echo $conteudo;
                ?>
                <a href="<?php echo $this->seo->getLink((array)$dado); ?>"
                    class="">CONSULTE</a>
            </div>
        </li>
        <?php
        endforeach; ?>
    </ul>
</section>
<section class="col_destaques_last">
    <ul>
    <?php
    $dados = array_slice($this->dados, 2, 4);
    foreach ( $dados as $dado ): ?>
        <li class="imovel_destaque">
            <img src="<?php echo (empty($dado->foto_destaque))?'src/foto-indisponivel.jpg':$dado->foto_destaque; ?>" alt="Foto do imóvel em destaque" style="width:307px; height:150px;"/>
            <div class="referencia_destaque pa">REF.: <?php echo $dado->codigo; ?></div>

            <div class="pos_info">
                <h1> <?php echo $dado->bairro; ?> /  <?php echo $dado->cidade; ?></h1>
                <h2> <?php echo $dado->categoria; ?></h2>
            </div>
            <div class="infos">
                <div class="favorito_destaque pa" onclick="favoritos(this, '<?php echo $dado->codigo; ?>');" title="Adicionar aos Favoritos"><span></span></div>
                <div class="comparar_destaque pa" onclick="comparar(this, '<?php echo $dado->codigo; ?>');" title="Comparar Imóvel"><span></span></div>
                <h1 class="local"><?php echo $dado->bairro; ?> /  <?php echo $dado->cidade; ?></h1>
                <h2 class="tipo"><?php echo $dado->categoria; ?></h2>
                <span class="acabamento"></span>
                <h2 class="itens"><?php echo $dado->dormitorios; ?> Dorm(s)  | <?php echo $dado->vagas; ?> Vaga(s) | <?php echo $dado->suites; ?> Suíte(s)</h2>
                <?php if( $dado->valor_venda > 0) : ?>
                <h2 class="valor">R$ <?php echo number_format($dado->valor_venda,2,',','.'); ?></h2>
                <?php endif; ?>
                <?php if( $dado->valor_locacao > 0) : ?>
                <h2 class="valor">R$ <?php echo number_format($dado->valor_locacao,2,',','.'); ?></h2>
                <?php endif; ?>
                <a href="<?php echo $this->seo->getLink((array)$dado); ?>"
                    class="">CONSULTE</a>
            </div>
        </li>
        <?php endforeach; ?>
    </ul>
</section>
