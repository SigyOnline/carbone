<?php
$result = $this->result;
?>
                        <!-- item -->
                        <li class="imovel_destaque22" id="fav-<?php echo $result['codigo']; ?>">
                            <div class="ttl_imagem"><h2 class="ttl fl"><?php echo $result['status']; ?></h2><h3 class="item_ref fr">Ref.: <?php echo $result['codigo']; ?></h3></div>
                            <a href="<?php echo Zend_Registry::get('seo')->getLink($result); ?>" class="lista-imovel-link" style="background-image:url(<?php echo ($result['foto_destaque'])? $result['foto_destaque'] : 'src/foto-indisponivel.jpg'; ?>);">
                            	<?php /*<img src="<?php echo ($result['foto_destaque'])? $result['foto_destaque'] : 'src/foto-indisponivel.jpg'; ?>" alt="Foto do Imóvel"/> */?>
                            </a>
                            <div class="infos">
                                <h1 class="tipo fnt_opensans"><?php echo (is_numeric($result['categoria'])? $result['cat'] : $result['categoria']); ?></h1>
                                <h2 class="local fnt_opensans"><?php echo $result['cidade']; ?> - <?php echo $result['uf']; ?></h2>
							<?php if ( strtoupper($result['status']) == 'VENDA' ) 
										$valor = number_format($result['valor_venda'], 2, ',', '.');
									  elseif ( strtoupper($result['status']) == 'ALUGUEL' ) 
										$valor = number_format($result['valor_locacao'], 2, ',', '.');	
									  else 
									  	$valor = NULL;
									  
									  if ( $valor ) : 
								?>
                                <h2 class="valor fnt_opensans">R$ <?php echo $valor;?></h2>
                                <?php endif;?>
                            </div>
                            <ul class="itens_destaque">
                                <li>
                                    <p>Dorm(s)</p>
                                    <p class="marg_top10"><?php echo (int) $result['dormitorios']; ?></p>
                                </li>
                                <li>
                                    <p>Vaga(s)</p>
                                    <p class="marg_top10"><?php echo (int) $result['vagas']; ?></p>
                                </li>
                                <li>
                                    <p>Área</p>
                                    <p class="marg_top10"><?php echo (int) $result['area_privativa']; ?>m²</p>
                                </li>
                                <li>
                                    <div class="box_check">
                                        <a href="javascript:;" name="favorito" id="favsem" onclick="<?php echo $this->type == 'favoritos'? 'favoritosRemove' : 'favoritos';?>(this, '<?php echo $result['codigo']; ?>');">
                                        <label for="favsem" class="icon_favorito <?php echo Cadastro_FavCom::getInstance()->getFavClass($result['codigo']); ?>">Favorito</label></a>
                                    </div>
                                    <div class="box_check">
                                        <a href="javascript:;" name="favorito" id="comparesem" onclick="comparar(this, '<?php echo $result['codigo']; ?>');" />
                                        <label for="comparesem" class="icon_compare <?php echo Cadastro_FavCom::getInstance()->getComClass($result['codigo']); ?>">Compare</label></a>
                                    </div> 
                                </li>
                            </ul>
                        </li>
