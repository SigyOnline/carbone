<?php echo $this->render("header.php"); ?>
		<main class="pr border_top">
		<section class="center">
			<!-- titulo -->
            <h1 class="pa noMobile ttl_internas">Assessoria Jurídica</h1>
			<section class="box_img_padrao">
<!--				<h2 class="sub_ttl"></h2>-->
                <div class="assessoria_img"></div>
                
				<p class="paragrafo_internas entre_2">A <strong>Xavier Camargo Imobiliária</strong> conta com uma assessoria jurídica experiente na área contratual e operacional imobiliária. Todos os processos de vendas são assessorados até sua conclusão, bem como durante toda vigência dos contratos de locação, com destaque para a análise acurada de documentos e certidões particulares e públicas. 
 </p>
                
                
                                  

			</section>
				
		</section>
	
<?php echo $this->render("footer.php"); ?>
	</main><!-- /main -->

