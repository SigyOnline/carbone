<?php echo $this->render("header.php"); ?>
	<main class="pr border_top">

		<section class="center">

			<!-- titulo -->

            <h1 class="pa noMobile ttl_internas">Confirmação de Envio</h1>

			<!-- no results -->

			<section id="no_result">

                <div class="pa imagem_casal"><img src="src/imagem-casal.png" alt="Imagem referente a não retorno de sua pesquisa" /></div>

                <div class="bg_faixa">

                    <div class="center">

                        <h1 class="bold">RECEBEMOS SUA MENSAGEM!</h1>

                        <p class="">Obrigado por entrar em contato. Em breve um consultor entrará em contato.</p>

                        <div class="comporta_links"><a href="" class="bold opacity">IR PARA HOME</a><a href="javascript: window.history.go(-1)" class="bold opacity">RETORNAR</a></div>

                    </div>

                </div>

            </section>	

		</section>

	</main>
<?php echo $this->render("footer.php"); ?>
