<div class="lightbox_padrao">
	<h1>Contato por e-mail</h1>
    <span class="closeFancyBox fecha_lightbox" title="Fechar">X</span>
</div>
<div class="centraliza_lightbox">
	<form action="contato/enviado" name="contatoImovel" data-validade="true" novalidate="novalidate" id="contatoImovel" method="post">
        <input type="hidden" value="Contato-Por-Email" name="Assunto" />
<?php if ( $_GET['ref'] ):?>
	<input type="hidden" name="ref" value="<?php echo $_GET['ref']?>">
<?php endif;?>
        <div class="linha_form">
            <div class="box_input_light">
                <label for="nomeContato" class="label01">Nome <span>(*)</span></label>
                <input type="text" name="Nome" class="input01" id="nomeContato" required="required"/>
            </div>
            <div class="box_input_light">
                <label for="emailContato" class="label01">E-mail <span>(*)</span></label>
                <input type="email" name="Email" class="input01" id="emailContato" required="required"/>
            </div>
        </div>
        
            <div class="fl w100 marg_top15">
                <label for="assuntoContato" class="label01">Assunto <span>(*)</span></label>
                <input type="text" name="Celular" class="input01" id="assuntoContato"/>
            </div>
        
        <div class="fl w100 marg_top15">
            <label for="txtContato" class="label01">Mensagem <span>(*)</span></label>
            <textarea class="txtarea01" id="txtContato" name="Mensagem" rows="2" cols="2" required="required"></textarea>
        </div>
        
        <!-- captcha 
        <div class="box_captcha_light marg_top15">
            <div class="g-recaptcha" data-sitekey="6LeSM1sUAAAAAPj0pDQiXjin0d2D83TrvXSFT32W"></div>
             <img class="fl" src="capsigy/captcha.php" alt="código captcha" id="captchaLight">
            <input type="button" class="captcha_loader_detal captcha_loader_light" name="Limpar" value="Atualizar Código" onclick="document.getElementById('captchaLight').src='capsigy/captcha.php?ID=SigyOnline' + Math.random();">            
            <input type="text" name="code" class="input_cap_detalhes input_capt_lightbox" placeholder="Insira o codigo"  required="required"/>       
            <p id="retornoCapLight" class="error_captcha fonte_red"></p>
        </div>-->
        
        <div class="box_texto marg_top15"> 
            <p class="sendText">Campos com <span class="font_red">(*)</span> devem ser preenchidos</p>
            <input type="submit" class="btn_enviar_form btn_padrao" value="ENVIAR"/>
        </div>
        
    </form>
    
</div>

<script type="text/javascript">
app.init();
// jQuery(function(){
// 			jQuery( '#contatoImovel' ).ajaxForm( {
// 				 beforeSend: function() { 
// 						$('#contatoImovel input[type=submit]').attr({'value': 'ENVIANDO...', 'disabled':true});
// 			        },
// 		        success: function( data ) {
// 			        if(data)
// 			        {
// 			            if(parseInt(data.error) > 0 )
// 				        {
// 			            	$('#contatoImovel input[type=submit]').attr({'value': 'ENVIAR', 'disabled':false});
// 			            	$("#retornoCapLight").html('*Código inválido');
// 							alert(data.msg);
// 			            }
// 			            else
// 			            {
// 			            	$('#contatoImovel input[type=submit]').attr({'value': 'ENVIADO', 'disabled':true});
// 			            	window.location = 'contato/emailenviado';
// 			            	//alert(data.msg);					            
// 			            }
// 			        }
// 		        }
// 		    } );

// 		});
			
</script>
