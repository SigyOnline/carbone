<?php echo $this->render("header.php"); ?>
<?php if(!empty($_SESSION['back'])){
	echo "<script>alert('Preencha o código Captcha.');</script>";
}
unset($_SESSION['back']);
?>
<main class="pr border_top">
		<section class="center">
			<!-- titulo -->
            <h1 class="pa noMobile ttl_internas">Trabalhe Conosco</h1>
			<form action="contato/enviado" id="form_trabalhe" method="post" enctype="multipart/form-data">
			<input type="hidden" value="Trabalhe-Conosco" name="Assunto" />
				<aside>
					<h2 class="ttl_form fl w100">Responda ao questionário abaixo escolhendo qual área tem interesse em atuar. Nosso departamento de Recursos Humanos entrará em contato em momento oportuno.</h2>
					<!--Linha Formulario-->
	                <div class="linha_form">
	                    <div class="box_input">
	                        <label for="trab01" class="label01">Nome Completo <span>(*)</span></label>
	                        <input type="text" id="trab01" class="input01" name="Nome" maxlength="30" required="required"/>
	                    </div>
	                    <div class="box_input">
	                        <label for="trab05" class="label01">E-mail <span>(*)</span></label>
	                        <input type="email" id="trab05" class="input01" name="Email" maxlength="30" required="required"/>
	                    </div>


	                    <div class="box_input_DDDt">
	                        <label for="cad03" class="label01">DDD <span>(*)</span></label>
	                        <input type="number" id="cad03" class="input01" name="Telefone" maxlength="2" required="required"/>
                        </div>
	                    <div class="box_input_Telt">
	                        <label for="cad03" class="label01">Telefone 0000-0000 <span>(*)</span></label>
	                        <input type="tel" pattern="\[0-9]{4}-[0-9]{4}$" id="cad03" class="input01" name="Telefone" maxlength="9" required="required"/>
	                    </div>
<!--
	                    <div class="box_inputDDD">
	                        <label for="trab2" class="label01">Telefone <span>(*)</span></label>
	                        <input type="tel" pattern="\([0-9]{2}\) [0-9]{4,5}-[0-9]{4}$" id="trab2" class="input01" name="Telefone" maxlength="30" required="required"/>
	                    </div>
-->

	                    <div class="box_input marg_right0">
                            <label for="areaInter" class="label01">Área de Interesse <span>(*)</span></label>
                            <select name="Areainter" id="areaInter" class="select01">
                                <option value="Selecione">Selecione</option>
                                <option value="Admin">Administrativo</option>
                                <option value="CorLocacao">Corretor de Locação</option>
                                <option value="CorVendas">Corretor de Vendas</option>
                                <option value="Finance">Financeiro</option>
                                <option value="Mkt">Marketing</option>
                                <option value="Recepcao">Recepção</option>
                            </select>
	                    </div>
	                    <div class="box_input fl">
	                        <label for="trab4" class="fl label01 w100">Anexe seu Currículo (Somente em PDF) <span>(*)</span></label>
	                        <input type="file" name="file" id="" class=""/>
	                    </div>
	                    
	                    <div class="fl w100 marg_top15">
	                    	<label for="trab3" class="label01">Mensagem (Opcional)</label>
	                    	<textarea id="codObs" class="txtarea01 w100" cols="2" rows="2" name="Mensagem" maxlength="150"></textarea>
	                    </div>
	                    <div class="box_captcha_internas marg_top15">
		                    <div class="alinha">
								<div class="g-recaptcha" data-sitekey="6LeSM1sUAAAAAPj0pDQiXjin0d2D83TrvXSFT32W"></div>
		                    </div>
		                    <div class="box_sendText"> 
		                        <p class="sendText">Campos com <span>(*)</span> devem ser preenchidos</p>
		                        <input type="submit" class="btn_enviar_form btn_padrao" value="ENVIAR CURRÍCULO"/>
		                    </div>
		                </div>
	                </div>	
				</aside>
			</form>	
		</section>
	</main>

<?php echo $this->render("footer.php"); ?>	
