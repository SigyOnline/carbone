<?php echo $this->render("header.php"); ?>
<?php if(!empty($_SESSION['back'])){
	echo "<script>alert('Preencha o código Captcha.');</script>";
}
unset($_SESSION['back']);
?>



    <?php foreach($this->popup as $p) {
        if ($p->ativo != 1) {

        }else{ ?>

        	
            <a class="inenter" style="display: none;" href="#popup_entrada"></a>
            <div style="display: none;">
                <div id="popup_entrada">
                    <header class="lightbox_padrao">
                        <span class="closeFancyBox fecha_lightbox" title="Fechar">X</span>
                    </header>
                    <section class="centraliza_lightbox">
                        <a href="<?php echo $p->link ?>">
                            <img title="<?php echo $p->titulo ?>" src="<?php echo $p->imagem ?>">
                        </a>
                    </section>
                </div>
            </div>
            <script type="text/javascript">
                window.onload = function () {
                    $('.inenter').click();
                }

                $("#popup_entrada").fancybox({
                    'padding'   :0,
                    'margin'    :0,
                    'autoDimensions'    :true
                });
            </script>
    <?php } 
    } 
    ?>
    <a class="hidden-link pop-up" href="/assets/images/banner-aviso.png">&nbsp;</a>
		<main class="pr border_top">
		<section class="center">
			<!-- titulo -->
            <h1 class="pa noMobile ttl_internas outro_position41">Contato</h1>
			<form action="contato/enviado" id="form_contato" method="post">
			<input type="hidden" value="Contato-Por-Email" name="Assunto" />
				<aside class="form_contato">
					<h2 class="ttl_form fl w100">Envie sua mensagem / Fale conosco.</h2>
					<!--Linha Formulario-->
	                <div class="linha_form">
	                    <div class="box_input">
	                        <label for="cad01" class="label01">Nome <span>(*)</span></label>
	                        <input type="text" id="cad01" class="input01" name="Nome" maxlength="30" required="required"/>
	                    </div>
	                    <div class="box_input">
	                        <label for="cad02" class="label01">E-mail <span>(*)</span></label>
	                        <input type="email" id="cad02" class="input01" name="Email" maxlength="30" required="required"/>
	                    </div>
                    </div>
	                <div class="linha_form">
	                    <div class="box_input_DDD">
	                        <label for="cad03" class="label01">DDD <span>(*)</span></label>
	                        <input type="tel"  id="cad03" class="input01" name="DDD" maxlength="2" required="required"/>
                        </div>
	                    <div class="box_input_Tel">
	                        <label for="cad03" class="label01">Telefone 0000-0000 <span>(*)</span></label>
	                        <input type="tel"  id="cad03" class="input01" name="Telefone" maxlength="10" required="required"/>
	                    </div>
	                    <div class="box_input_DDD">
	                        <label for="cad03" class="label01">DDD <span>(*)</span></label>
	                        <input type="tel"  id="cad03" class="input01" name="DDD1" maxlength="2" required="required"/>
                        </div>
	                    <div class="box_input_TelR">
	                        <label for="cad04" class="label01">Celular 00000-0000 <span>(*)</span></label>
	                        <input type="tel" id="cad04" class="input01" name="Celular" maxlength="10" required="required"/>
	                    </div>
	                    <div class="box_input">
	                        <label for="cad05" class="label01">Assunto</label>
	                        <input type="text" id="cad05" class="input01" name="Assunto" maxlength="50" required="required"/>
	                    </div>
	                </div>
	                <!-- linha formulario-->
	                <div class="fl w100 marg_top15">
	                    <label for="cod16" class="label01">Mensagem</label>
	                    <textarea id="cod16" class="txtarea01 w100" cols="2" rows="2" name="Descricao" maxlength="50"></textarea>
	                </div>
	                <div class="box_captcha_internas marg_top15">
	                    <div class="alinha">
	                       <!-- <div class="g-recaptcha" data-sitekey="6LfNpnYUAAAAAAken3nqDvWt3ei-mFGQB3UgisF_"></div>-->

	                        <!-- <input type="text" name="code" class="input_capt_internas" value="Digite o código" onClick="apagaCampo(this)" onBlur="preencheCampo(this)"/> -->
	                        <!-- <p id="retornoCap" class="marg_cod fonte_red"></p> -->
	                    </div>
	                    <div class="box_sendText"> 
	                        <p class="sendText">Campos com <span>(*)</span> devem ser preenchidos</p>
	                        <input type="submit" class="btn_enviar_form btn_padrao" value="ENVIAR"/>
	                    </div>
	                </div>		
				</aside>
				<aside class="box_filiais">
					<ul>
						<li>
							<h2 class="ico_phone">Central de Atendimento</h2>
							<h3><span>(11) 2606-4000</span></h3>
						</li>
						<li>
							<h2 class="ico_whats">Whatsapp</h2>
                            <h3><a target="_blank" href="https://api.whatsapp.com/send?phone=5511962004000" class="opacity">(11) 96200-4000</a></h3>
						</li>
							<!--<li>
							<h2 class="ico_phone">Ligamos para você</h2>
							<a href="contato/ligamos<?php echo $this->id ? "?ref={$this->id}":NULL;?>"
									class="" data-fancybox data-src="#ligamos_lightbox">Solicite nossa ligação</a>
						</li>
						<li>
							<h2 class="ico_corretor">Fale com um corretor</h2>
							<a class="">Estamos ONLINE</a>
							href="#" onclick="window.open(this.href,'galeria','width=680,height=470'); return false;"
						</li>-->
						<li>
							<h2 class="ico_mapa">Localização</h2>
							<a href="localizacao" class="">Veja o MAPA</a>
						</li>
					</ul>			
				</aside>
			</form>	
		</section>
	</main>
<!--<script src='https://www.google.com/recaptcha/api.js?hl=pt-BR'></script>-->
	
<?php echo $this->render("footer.php"); ?>
