<div class="lightbox_padrao">
	<h1>Ligamos para você1</h1>
    <span class="closeFancyBox fecha_lightbox" title="Fechar">X</span>
</div>
<div class="centraliza_lightbox">
	<form action="contato/enviado" name="ligamosImovel" id="ligamosImovel" data-validade="true" novalidate="novalidate" method="post">
        <input type="hidden" value="Ligamos-Para-Você" name="Assunto" />
<?php if ( $_GET['ref'] ):?>
	<input type="hidden" name="ref" value="<?php echo $_GET['ref']?>">
<?php endif;?>
        <div class="linha_form">
            <div class="box_input_light">
                <label for="nome-call" class="label01">Nome <span>(*)</span></label>
                <input type="text" name="Nome" class="input01" id="nome-call" required="required"/>
            </div>
            <div class="box_input_light">
                <label for="email-call" class="label01">E-mail</label>
                <input type="email" name="Email" class="input01" id="email-call" required="required"/>
            </div>
        </div>
        <div class="linha_form">
            <div class="box_input_lightDDD">
                <label for="tel-call" class="label01">DDD <span>(*)</span></label>
                <input type="tel" name="TelDDD" class="input01" id="tel-call" required="required" maxlength="2"/>
            </div>
            <div class="box_input_lightTel">
                <label for="tel-call" class="label01">Telefone 0005555500-0000 <span>(*)</span></label>
                <input type="tel" pattern="\[0-9]{4,5}-[0-9]{4}$" name="Telefone" class="input01" id="tel-call" required="required" maxlength="10"/>
            </div>
            <div class="box_input_lightDDD">
                <label for="cel-call" class="label01">DDD</label>
                <input type="tel" name="CelDDD" class="input01" id="cel-call" maxlength="2"/>
            </div>
            <div class="box_input_lightCel">
                <label for="cel-call" class="label01">Telefone 00000-0000</label>
                <input type="tel" maxlength="9" pattern="\[0-9]{4,5}-[0-9]{4}$" name="Celular" class="input01" id="cel-call" maxlength="10"/>
            </div>
<!--
            <div class="box_input_light">
                <label for="cel-call" class="label01">Celular (00) 00000-0000</label>
                <input type="tel" pattern="\([0-9]{2}\) [0-9]{5}-[0-9]{4}$" name="Celular" class="input01" id="cel-call"/>
            </div>
-->
        </div>   
        <div class="fl w100 marg_top15">
            <label for="txt-prop" class="label01">Assunto (Opcional)</label>
            <textarea class="txtarea01" id="txt-prop" name="Mensagem" rows="2" cols="2"></textarea>
        </div>
        <!-- captcha 
        <div class="box_captcha_light marg_top15">
            <img class="fl" src="capsigy/captcha.php" alt="código captcha" id="captchaLight">
            <input type="button" class="captcha_loader_detal captcha_loader_light" name="Limpar" value="Atualizar Código" onclick="document.getElementById('captchaLight').src='capsigy/captcha.php?ID=SigyOnline' + Math.random();">            
            <input type="text" id="code" name="code" class="input_cap_detalhes input_capt_lightbox" placeholder="Insira o codigo" required="required"/>       
            <p id="retornoCapLight" class="error_captcha fonte_red"></p>
        </div>-->
        <div class="box_texto marg_top15"> 
            <p class="sendText">Campos com <span class="font_red">(*)</span> devem ser preenchidos</p>
            <input type="submit" class="btn_enviar_form btn_padrao" value="ENVIAR"/>
        </div>
    </form>
</div>
<script type="text/javascript">
app.init();
// jQuery(function(){
// 			jQuery( '#ligamosImovel' ).ajaxForm( {
// 				 beforeSend: function() { 
// 						$('#ligamosImovel input[type=submit]').attr({'value': 'ENVIANDO...', 'disabled':true});
// 			        },
// 		        success: function( data ) {
// 			        if(data)
// 			        {
// 			            if(parseInt(data.error) != ''  )
// 				        {
// 			            	$('#ligamosImovel input[type=submit]').attr({'value': 'ENVIAR', 'disabled':false});
// 			            	$("#retornoCapLight").html('*Código inválido');
// 							alert(data.msg);
// 			            }
// 			            else
// 			            {
// 			            	$('#ligamosImovel input[type=submit]').attr({'value': 'ENVIADO', 'disabled':true});
// 			            	window.location = 'contato/emailenviado';
// 			            	//alert(data.msg);					            
// 			            }
// 			        }
// 		        }
// 		    } );

// 		});
			
</script>
