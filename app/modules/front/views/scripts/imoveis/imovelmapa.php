<?php echo $this->render("header.php"); ?>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyD1TJ8Xnva73RmWptFhmWffv5IP71MuzhU"></script>
<script src="js/jquery.gmap.js"></script>
<script src="js/google_maps/ModuloMapa.js"></script>
<div id="box_ttlMobile">
		<div class="center">
			<h1>MAPA DO IMÓVEL</h1>
		</div>
	</div>
    <!--conteudo-->
	<article class="content pad010">
        <div class="maps gmap load-gmap" id="maps" style="width: 100%; height: 420px;">
             <?php /*<script type="text/javascript">initialize('<?php echo $this->dados->latitude; ?>','<?php echo $this->dados->longitude; ?>');</script> */ ?>
             <input type="hidden" class="gmaps-cor" value="#000000" />
							<input type="hidden" class="gmaps-km" value="600" />
							<input type="hidden" class="gmaps-icon" value="" />
							<input type="hidden" class="gmaps-zoom" value="15" />
							<ul><li><?php echo $this->dados->latitude; ?>||<?php echo $this->dados->longitude; ?>||circulo||</li></ul>
		</div>
		<div id="gmap-nearby" class="gmap-nearby">
                         <h3 class="sub-titulo">Estabelecimentos próximos:</h3>
                               <ul class="lista-estabelecimentos">
                                  <ul>
                                    <li><a href="#" rel="gym|gym"> <span class="off"></span> Academias</a></li>
                                    <li><a href="#" rel="bank|bank"><span class="off"></span> Bancos </a></li>
                                    
                                    <li><a href="#" rel="bar|bar"><span class="off"></span> Bares </a></li>
                                    
                                    <li><a href="#" rel="fire_station|firemen"><span class="off"></span> Bombeiros</a></li>
                                    
                                    <li><a href="#" rel="movie_theater|cinema"><span class="off"></span> Cinemas</a></li>
                                    
                                    <li><a href="#" rel="post_office|postal"><span class="off"></span> Correios</a></li>
                                    
                                    <li><a href="#" rel="school|school"><span class="off"></span> Escolas</a></li>
                                    
                                    <li><a href="#" rel="parking|parking"><span class="off"></span> Estacionamentos</a></li>
                                    
                                    <li><a href="#" rel="pharmacy|drugs"><span class="off"></span> Farmácias</a></li>
                                    
                                    <li><a href="#" rel="hospital|hospital"><span class="off"></span> Hospitais</a></li>
                                    
                                    <li><a href="#" rel="hotel|hotel"><span class="off"></span> Hoteis</a></li>
                                    
                                    <li><a href="#" rel="doctor|doctor"><span class="off"></span> Médicos</a></li>
                                    
                                    <li><a href="#" rel="bakery|bread"><span class="off"></span> Padarias</a></li>
                                    
                                    <li><a href="#" rel="park|park"><span class="off"></span> Parques</a></li>
                                    
                                    <li><a href="#" rel="police|police2"><span class="off"></span> Polícia</a></li>
                                    
                                    <li><a href="#" rel="gas_station|gazstation"><span class="off"></span> Postos de Gas.</a></li>
                                    
                                    <li><a href="#" rel="restaurant|restaurant"><span class="off"></span> Restaurantes</a></li>
                                    
                                    <li><a href="#" rel="shopping_mall|shoppingmall"><span class="off"></span> Shoppings</a></li>
                                    <li><a href="#" rel="taxi_stand|taxi"><span class="off"></span> Taxi</a></li>
                                    
                                    <li><a href="#" rel="grocery_or_supermarket|market"><span class="off"></span> Supermercados</a></li>
                                </ul>
                               </ul>
                                
                            </div>
        <a href="javascript:window.history.go(-1)" class="btn_back">VOLTAR PARA PÁGINA ANTERIOR</a>
    </article>
<?php echo $this->render("footer.php"); ?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
<script type="text/javascript" src="js/fullbiz.gmaps.js"></script>
<style>
.lista-estabelecimentos .active span {
    background-color: #B41B1E !important;
}
</style>


