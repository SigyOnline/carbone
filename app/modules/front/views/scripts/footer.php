<!--<nav id="barra_fixa" class="dn730">
	<section class="center">
		<ul>
			<li class="ico_phone">(19) 3522-7777</li>
			<li class=""><a target="_blank" href="https://api.whatsapp.com/phone=551998128-0410" class="opacity"><i class="fa fa-whatsapp" aria-hidden="true"></i>(19) 98128-0410</a></li>        
			<li class="ico_phone"><a href="contato/ligamos<?php echo $this->id ? "?ref={$this->id}":NULL;?>" class="ligamos_lightbox opacity">Ligamos para você</a></li>
			<li class="ico_contato"><a href="contato/contatoemail<?php echo $this->id ? "?ref={$this->id}":NULL;?>" class="contato_lightbox opacity">Contato por e-mail</a></li>
            <li class="ico_customer"><a href="http://www.gregorisoft.com.br/imobiliarias/area-restrita/login.php?i=QXAyS0FvOHNuMzNkajRKU1A1M2U1MDAxMjEzV3Fsaw==" target="_blank">Área do Cliente</a></li>
		</ul>-->
		<!--<a href="http://www.uniondata.com.br:8180/unionweb/login.ud;jsessionid=567288FDB2793278B15F4C2C05DDCF02?windowId=9ff&acao=cadastre&adm=15" onclick="window.open(this.href,'galeria','width=789,height=620'); return false;" class="link_corretor fnt_center radius4 opacity">Acesse seu Condomínio</a>-->
	<!--</section>
</nav>-->


<section id="redesSociais" class="row">
	<div class="center">
		<ul>
			<li class="noMobile">
				<h2>Nossas Redes Sociais</h2>
				<p class="">Participe de nossas redes!</p>
			</li>
			
			<li class=""><a href="https://www.facebook.com/carbone.imoveis" class="icones_face opacity" rel="external" target="_blank">Curta nossa fanpage</a></li>
            
			<!-- <li class=""><a href="#" class="icones_instagran opacity" rel="external">Siga nosso Instagram</a></li> -->
            

<!--
			<li class=""><a href="https://www.youtube.com/user/jmfox" class="icones_youtube opacity" rel="external">Inscreva-se em nosso canal</a></li>

			<li class="border"></li>
			<li class="fixo">
				<div class="botoes">
					<div class="addthis_native_toolbox"></div>
				</div>
				<h2 class="fl">Compartilhe</h2>
				<p class="fl">Nossa Página</p>
			</li>
-->
		</ul>
		
	</div>
	
</section>


<footer id="footer">

	<section class="center">

		<div class="col_rodape">

			<div class="nav_rodape">

				<h1 class="ttl">Principal</h1>

				<ul class="nav">

					<li><a href="">Home</a></li>

					<li><a href="quemsomos">Empresa</a></li>

					<li><a href="localizacao">Localização</a></li>

					<li><a href="contato">Contato</a></li>

				</ul>	

			</div>

			<div class="nav_rodape">

				<h1 class="ttl">Imóveis</h1>

				<ul class="nav">

					<li><a href="imoveis?status=VENDA">Venda</a></li>

					<li><a href="imoveis?status=ALUGUEL">Locação</a></li>

				</ul>	

			</div>
			<div class="nav_rodape inform marg_top30m">

				<h1 class="ttl">Informações</h1>

				<ul id="lista_info" class="nav">

					<li class="marg_bot10">

						<p class="icon_endereco"><span>Endereço:</span> Rua Itamaracá, nº 200 - Mooca - São Paulo -SP </p>

					</li>

					<li class="marg_bot10">

						<p class="icon_atendimento"><span>Central de Atendimento:</span> (11) 2606-4000</p>


					</li>

					<li class="marg_bot10">

                        <p class="icon_whats"><span><i class="fa fa-whatsapp fa-lg" aria-hidden="true"></i>WhatsApp:</span><a target="_blank" href="https://api.whatsapp.com/send?phone=5511962004000"> (11) 96200-4000</a></p>


					</li>
				<li class="marg_bot10">

						<p class=""><span>Creci nº: 16669-J</span></p>


					</li>
				</ul>	

			</div>
			<div class="nav_rodape marg_top30m">
				<ul class="nav">
					<li>
					<h1 class="ttl">Newsletter</h1>
					<p class="texto">Receba nossas atualizações</p>
					<form action="contato/newsletter" method="post" id="newsl">
						<input type="hidden" id="news_assunto" class="inputNews" name="Assunto" value="Novo cadastro newsletter">
						<input type="text" id="news_nome" class="inputNews" placeholder="NOME" name="nome">
						<input type="text" id="news_email" class="inputNews" placeholder="EMAIL" name="email">
						<input type="submit" class="btn opacity" value="OK">	
					</form>
					</li>
				</ul>
			</div>

		</div>	

		<div class="col_logo noMobile">
			<img src="src/logo.png" alt="Carbone Imóveis" />
			<p class="whitefletter">Creci nº: 16669-J</p>
			<a href="https://www.facebook.com/carbone.imoveis" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
			<!-- <i class="fa fa-instagram" aria-hidden="true"></i> -->
		</div>
		

<!-- 
		<div class="col_info">

			<h1 class="ttl">Informações</h1>

			<ul id="lista_info" class="nav">

				<li class="marg_bot10">

					<p class="icon_endereco"><span>Endereço:</span> Av. Pe Manuel da Nóbrega, 583 </p>

				</li>

				<li class="marg_bot10">

					<p class="icon_atendimento"><span>Central de Atendimento:</span> (11) 4994-7337</p>


				</li>

				<li class="marg_bot10">

					<p class="icon_whats"><span><i class="fa fa-whatsapp fa-lg" aria-hidden="true"></i>Whatsapp: </span> (11) 93800-4111</p>


				</li>

				<li>

				<p class="icon_email"><span>E-mail:</span> <a href="mailto:imobiliariavilar@imobiliariavilar.com.br">imobiliariavilar@imobiliariavilar.com.br</a></p>

				</li>

			</ul>	

		</div> -->
		<!-- <div class="col_info">
			<ul class="nav">
				<li>
				<h1 class="ttl">Newsletter</h1>
				<p class="texto">Receba nossas atualizações</p>
				<form action="contato/newsletter" method="post" id="newsl">
					<input type="hidden" id="news_assunto" class="inputNews" name="Assunto" value="Novo cadastro newsletter">
					<input type="text" id="news_nome" class="inputNews" placeholder="NOME" name="nome">
					<input type="text" id="news_email" class="inputNews" placeholder="EMAIL" name="email">
					<input type="submit" class="btn opacity" value="OK">	
				</form>
				</li>
			</ul>
		</div> -->
	</section>

</footer>
<!-- geral -->


<!--Script links externos-->

 <script type="text/javascript">
   $(document).ready(function(){

   $('a[rel=external]').attr('target','_blank');
   $(document).on('submit', '#newsl', function(e) {
	      var validation;
	      e.preventDefault();
	      validation = new Validate();
	      validation.text('#news_nome');
	      validation.email('#news_email');
	      if (validation.message.length > 0) {
	        return false;
	      }
	      $.ajax({
	          type: "POST",
	          dataType: 'json',
	          url: "contato/newsletter",
//	          processData: false,
	          data: $(this).serialize(),
	          success: function(data) {
	          	if ( data.error == 1 )
	          	{
	          		alert(data.msg);
	          		console.log('error', data);
	          	} else
	          	{
	          	  $('#newsl input[type=text], #newsl input[type=email], #newsl input[type=tel], #newsl textarea').val("");
	  	           alert("Cadastrado com sucesso!");
	         	}
	          },
	        error: function(data) {
	          console.log('error', data);
	          clear_form();
	        }
	      });
	    });

  });
</script>



<!-- Script limpa conteudo do input no clique -->

<script type="text/javascript">

function apagaCampo(campo){

if (campo.value == campo.defaultValue) {

	campo.value = '';}

}

function preencheCampo(campo){

if (campo.value == "") {

	campo.value = campo.defaultValue;}

}

</script>



<!--script menu-->

<script src="js/script.js"></script>



<!--busca principal-->

<script type="text/javascript" >

	$(document).ready(function()

	{

	$(".search").click(function()

	{

	var X=$(this).attr('id');



	if(X==1)

	{

	$(".submenu").slideDown();

	$(this).attr('id', '0');	

	}

	else

	{



	$(".submenu").slideUp();

	$(this).attr('id', '1');

	}

		

	});



	//Mouseup textarea false

	$(".submenu").mouseup(function()

	{

	return false

	});

	$(".search").mouseup(function()

	{

	return false

	});





	//Textarea without editing.

	$(document).mouseup(function()

	{

	$(".submenu").hide();

	$(".search").attr('id', '');

	});

		

	});

	

</script>
	


<!-- likebox -->

<div id="fb-root"></div>


<!-- Go to www.addthis.com/dashboard to customize your tools -->

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-547fae404a810197" async="async"></script>





<!--[(gte IE 6)&(lte IE 8)] Usado para funcionar last-child -->

<!-- <script type="text/javascript" src="js/selectivizr.js"></script> -->



<!-- Script Busca -->

<script type="text/javascript" src="js/sigy.busca.js"></script>



<!-- Fancybox -->

<script src="js/fancybox/jquery.fancybox.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {

        $('.closeFancyBox').live('click', function(){$.fancybox.close()});

    });

</script>



<!-- Script menu aba 

<script type="text/javascript">

    $("#nav_mobile").addClass("js");

    $("#nav_mobile").addClass("js").before('<li id="links" class="ico_phone">Central de Atendimento</li>');

    $("#links").click(function(){

    $("#nav_mobile").toggle("slow");

    });

    $(window).resize(function(){

    if(window.innerWidth > 730) {

        $("#nav_mobile").removeAttr("style");

        }

    });

</script>

-->

<!-- Scripts Banner Full Width -->     

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<script type="text/javascript" src="js/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="js/jb_fws.js"></script>

<script type="text/javascript" src="js/jquery-ui-1.10.3.custom.min.js"></script>

<script type="text/javascript" src="js/jquery.wt-lightbox.js"></script>

<script type="text/javascript" src="js/jquery.thumb-scroller.js"></script>    

<script type="text/javascript">

    $(document).ready(function(){

        JBFWS = new JBFWS();

        JBFWS.init({

            /* Slider Width - Height */

            width            : "100%",

            height           : "825px",

            

            /* Buttons settings */

            showBigButtons   : 1,  /* 0 - Hide, 1 - Show */

            showSmallButtons : 1,  /* 0 - Hide, 1 - Show */

                

            /* Main Slide */

            slideSpeed       : 1000, /* miliseconds */

            slideEffect      : "easeInOutExpo",

                

            /* Secondary Slide */

            slideDelay       : 600,  /* miliseconds. 0 - Off */

            slideSpeed2      : 1000, /* miliseconds. 0 - Off */

            slideEffect2     : "easeOutExpo",

            

            /* Drag Slide */

            dragSlide        : 1, /* 0 - Off, 1 - On */

            

            /* Auto Slide */

            autoSlide        : 1,   /* 0 - Off, 1 - On */

            autoSlideDelay   : 4000 /* miliseconds */   

        });

    });

</script>

<!-- Script menu aba -->

<script type="text/javascript">

    $("#abas_menu").addClass("js");

    $("#abas_menu").addClass("js").before('<div id="menu_carousel"><strong>LANÇAMENTOS</strong></div>');

    $("#menu_carousel").click(function(){

    $("#abas_menu").toggle("slow");

    });

    $(window).resize(function(){

    if(window.innerWidth > 730) {

        $("#abas_menu").removeAttr("style");

        }

    });

</script> 
<adress id="assinaturas">

	<section class="center">
		
		<a href="#" class="goomark opacity" title="VistaSoft" rel="external"> | VistaSoft</a>	

		<a href="http://www.sigyonline.com.br/" class="sigy opacity" rel="external" title="SigyOnline"></a>

		<p>Site desenvolvido por</p>

	</section>

</adress>
<!--Script links externos-->
 <script type="text/javascript">
   $(document).ready(function(){
   $('a[rel=external]').attr('target','_blank');
  });
</script>

<!-- Script limpa conteudo do input no clique -->
<script type="text/javascript">
function apagaCampo(campo){
if (campo.value == campo.defaultValue) {
	campo.value = '';}
}
function preencheCampo(campo){
if (campo.value == "") {
	campo.value = campo.defaultValue;}
}

function atualizaCom(qtd){
	$(".qtd_com").html(qtd+' de 04 Imóvel(is)');
}

function atualizaFav(qtd){
    if( qtd )
	$(".qtd_fav").html(qtd+' Adicionado(s)');
}

function comparar(ob, id_imovel) {
	if( $(ob).hasClass('com_active') ) {
		var tipo = 'remove';
		$(ob).removeClass('com_active');
	} else {
		var tipo = 'add';
		$(ob).addClass('com_active');
	}
	$.post('imoveis/com', {'item':'com', 'acao':tipo, 'id':id_imovel }, function (data) {
    	if( data == 'LIMITE') {
    		$(ob).removeClass('com_active');
			alert('Limite de imoveis atingido');
    	} else {
			atualizaCom(data);
    	}
	});
}

function removecomp(id)
{
	$.post('imoveis/com', {'item':'com', 'acao':'remove', 'id':id }, function (data) {
		$("#id"+id).hide("slow");
		//atualizaCom(data);
		if (parseInt(data) < 2) {
			$("#resultado_off").fadeIn();
			$("#lista_compara").fadeOut();
		} 
	});
	
}

function favoritos(ob, id_imovel) {
    
	if( $(ob).hasClass('fav_active') ) {
		var tipo = 'remove';
		$(ob).removeClass('fav_active');
	} else {
		var tipo = 'add';
		$(ob).addClass('fav_active');
	}
	$.post('imoveis/favcom', {'item':'fav', 'acao':tipo, 'id':id_imovel }, function (data) {
		atualizaFav(data);
	});
}

function compararRemTodos() {
	$.post('imoveis/com', {'item':'com', 'acao':'removertodos'}, function (data) {
    	setTimeout(function(){
        	location.href = location.href;
        }, 500);
	});
}
</script>



<div id="fb-root"></div>

<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.12';
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<!-- Script do Chat Vista - enviado por email) -->
<!-- <script type="text/javascript">
var LHCChatOptions = {};
LHCChatOptions.opt = {widget_height:340,widget_width:300,popup_height:520,popup_width:500};
(function() {
var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
var referrer = (document.referrer) ? encodeURIComponent(document.referrer.substr(document.referrer.indexOf('://')+1)) : '';
var location  = (document.location) ? encodeURIComponent(window.location.href.substring(window.location.protocol.length)) : '';
po.src = '//chat.novovista.com.br/chat/index.php/chat/getstatus/(click)/internal/(position)/bottom_right/(ma)/br/(check_operator_messages)/true/(top)/350/(units)/pixels/(leaveamessage)/true?r='+referrer+'&l='+location+'&e=9421';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})();
</script> -->

</body>
</html>