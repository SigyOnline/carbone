<?php
class Zend_Controller_Action_Helper_Interacao extends Zend_Controller_Action_Helper_Abstract
{
	public function getQtdDias($id_venda)
	{
		$s = new Zend_Db_Select(Zend_Registry::get('db'));
		$s->from(array('V' => Zend_Registry::get('config')->tb->vendas), array(''));
		$s->joinLeft(array('I' => Zend_Registry::get('config')->tb->interacao_venda), 'V.id_venda = I.id_venda', array('criado as data'));
		$s->order('I.criado DESC');
		$s->where('V.id_venda=?', $id_venda);
		$s->limit(1);	
		//exit($s);	
		$dados = $s->query(Zend_Db::FETCH_OBJ)->fetchObject();
		if($dados)
		{
			
			$diferenca = time() - strtotime($dados->data);
			$dias = (int)floor( $diferenca / (60 * 60 * 24));
			return $dias;	
		}
		
		return 0; 
		
	}
	
}