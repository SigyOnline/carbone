<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<form method="post" action="<?php echo $dados['action'];?>" id="form">
<input type="hidden" name="id_carteira"
value="<?php echo $dados['id_carteira'];?>">

<input type="hidden" name="id_transacao" value="<?php echo $dados['id_transacao'];?>">
<input type="hidden" name="descricao" value="<?php echo utf8_decode($dados['descricao']);?>">
<input type="hidden" name="razao" value="<?php echo $dados['razao'];?>">
<input type="hidden" name="nome" value="<?php echo $dados['nome'];?>">
<input type="hidden" name="valor" value="<?php echo $dados['valor'];?>">
<input type="hidden" name="frete" value="<?php echo $dados['frete'];?>">
<input type="hidden" name="peso_compra" value="<?php echo $dados['peso_compra'];?>">
<input type="hidden" name="url_retorno" value="<?php echo $dados['url_retorno'];?>">

<!-- INÍCIO DOS DADOS DO USUÁRIO -->
<input type="hidden" name="pagador_nome" 
value="<?php echo $dados['pagador_nome'];?>">
<input type="hidden" name="pagador_cep" value="<?php echo $dados['pagador_cep'];?>">
<input type="hidden" name="pagador_logradouro" 
value="<?php echo $dados['pagador_logradouro'];?>">
<input type="hidden" name="pagador_numero" value="<?php echo $dados['pagador_numero'];?>">
<input type="hidden" name="pagador_complemento" value="<?php echo $dados['pagador_complemento'];?>">
<input type="hidden" name="pagador_bairro" 
value="<?php echo $dados['pagador_bairro'];?>">
<input type="hidden" name="pagador_cidade" 
value="<?php echo $dados['pagador_cidade'];?>">
<input type="hidden" name="pagador_estado" value="<?php echo $dados['pagador_estado'];?>">
<input type="hidden" name="pagador_pais" value="<?php echo $dados['pagador_pais'];?>">
<input type="hidden" name="pagador_telefone" value="<?php echo $dados['pagador_telefone'];?>">
<input type="hidden" name="pagador_email" 
value="<?php echo $dados['pagador_email'];?>">
<script type="text/javascript">
document.getElementById('form').submit();
</script>
<!-- FIM DOS DADOS DO USUÁRIO -->
<!--
<input type="image" 
src="https://p.simg.uol.com.br/out/pagseguro/i/botoes/pagamentos/99x61-pagar-assina.gif" 
name="submit" alt="Pague com PagSeguro - é rápido, grátis e seguro!">-->
</form>
</body>
</html>
