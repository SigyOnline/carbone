<?php
/**
 * Action Helper para gerar PDF a partir de HTML usando HTML2PDF
 * 
 * @uses Zend_Pdf
 *
 * @author Fabio Nader
 *
 * @param array $data
 * @param string $filename
 * @param 'I'|'D'|'F'|'S' $dest
 *
 * Saidas:
 * I: Envia para a saída padrão
 * D: Download do arquivo
 */
require_once 'app/models/html.func.php';
require_once 'app/views/helper/ValorExtenso.php';
 
class Helper_Pagamento_Cobranca_NotaDebito_NdPDF extends Zend_Controller_Action_Helper_Abstract
//class Zend_Controller_Action_Helper_NdPDF extends Zend_Controller_Action_Helper_Abstract
{
	const DISPLAY_INLINE = 'I';
	
	const DOWNLOAD = 'D';
    
   /**
     * The default encoding
     * 
     * @var string
     */
    public static $encoding = 'UTF-8';

    /**
     * Align text at left of provided coordinates
     * 
     * @var string
     */
    const TEXT_ALIGN_LEFT = 'left';
    
    /**
     * Align text at right of provided coordinates
     * 
     * @var string
     */
    const TEXT_ALIGN_RIGHT = 'right';
    
    /**
     * Center-text horizontally within provided coordinates
     * 
     * @var string
     */
    const TEXT_ALIGN_CENTER = 'center';	
	
    public function direct(Pagamento_Cobranca_NotaDebito $nd, $filename=NULL, $dest = 'I')
    {
    	try
    	{
			$pdf = new Zend_Pdf(file_get_contents('notasdebito/_template_v1.pdf'));
			
			if ( NULL === $filename )
				$filename = $nd->getId();

			$pdf->pages[0]->setFont(Zend_Pdf_Font::fontWithName('Helvetica'),'9');
			//now we can write:
			$date = new Zend_Date();
			$currency = new Zend_Currency();
			$data = $dados = $nd->getFatura()->getDados() + $nd->getDados();
			$criado = $date->setTimestamp(strtotime($data['criado']))->toString('dd.MM.YY');
			$vencimento = $date->setTimestamp(strtotime($data['vencimento']))->toString('dd/MM/YYYY');
			$subtotal = ( $data['subtotal_simbolico']?$data['subtotal_simbolico']:$data['subtotal'] );
			$total = ( $data['total_simbolico']?$data['total_simbolico']:$data['total'] );
			$path = 'notasdebito/'.rtrim($filename,'.pdf').'.pdf';
			
			$endereco = '';
			if ( $data['endereco'] ) $endereco .= $data['endereco'];
			if ( $data['numero'] ) $endereco .= ', '. $data['numero'];
			if ( $data['comp'] ) $endereco .= $data['comp'];
			if ( $data['bairro'] ) $endereco .= ' - '. $data['bairro'];
			if ( $data['cep'] ) $endereco .= ' - CEP: '. $data['cep'];
			
			if ( $data['assinatura'] )
			{
				$assinatura = Zend_Pdf_Image::imageWithPath($data['assinatura']);
				$h = $assinatura->getPixelHeight();
				$w = $assinatura->getPixelWidth();
				
				$pdf->pages[0]->drawImage($assinatura, 245, 107, $w*(110/$w)+215, $h*(110/$w)+135);
			}
			
			$pdf->pages[0]->drawText($criado, 432, 706);//data de emissão
			$pdf->pages[0]->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD),'9');
			$pdf->pages[0]->drawText($vencimento, 290, 669);//vencimento
			$pdf->pages[0]->drawText($currency->toCurrency($data['total_simbolico']), 375, 669);//valor
			$pdf->pages[0]->drawText($data['numero_nd']?$data['numero_nd']:$data['id_notadebito'], 480, 669);//número
			$pdf->pages[0]->drawText(strtoupper($data['empresa']), 140, 640,'UTF-8');//Cliente		
			$pdf->pages[0]->setFont(Zend_Pdf_Font::fontWithName('Helvetica'),'9');
			$pdf->pages[0]->drawText($endereco, 140, 625,'UTF-8');//Endereço
			$pdf->pages[0]->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD),'9');
			$pdf->pages[0]->drawText($data['cidade'], 140, 610,'UTF-8');//Municipio
			$pdf->pages[0]->drawText($data['uf'], 360, 610,'UTF-8');//UF
			$pdf->pages[0]->setFont(Zend_Pdf_Font::fontWithName('Helvetica'),'9');
			$pdf->pages[0]->drawText(formatarCPF_CNPJ($data['cpf_cnpj']), 140, 593,'UTF-8');//CNPJ		
			$pdf->pages[0]->drawText(strtoupper(Helper_ValorExtenso::escreve($data['total_simbolico'])), 108, 561,'UTF-8');//Valor Extenso
			self::drawTextBox($pdf->pages[0],$data['discriminacao'], 44, 420,350,self::TEXT_ALIGN_LEFT,1.5);//Texto Discriminação
			$pdf->pages[0]->drawText($currency->toCurrency($subtotal), 446, 236,'UTF-8');//subototal		
			$pdf->pages[0]->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD),'9');
			$pdf->pages[0]->drawText($currency->toCurrency($total), 446, 210,'UTF-8');//total
			$pdf->pages[0]->setFont(Zend_Pdf_Font::fontWithName('Helvetica'),'10');
			self::drawTextBox($pdf->pages[0],$data['nome'], 220, 95,480,self::TEXT_ALIGN_CENTER,1.5);//Assinatura
			$out = $pdf->render();
			$pdf->save($path);
			
			if ( $path != $data['arquivopdf'] )
			{
				$nd->arquivopdf = $path;
				if ( ! $nd->atualiza() )
					return (new Error('Não foi possível gerar a nota de débito. Tente novamente!'));
			}
			
				//Download file
			if(isset($HTTP_SERVER_VARS['HTTP_USER_AGENT']) and strpos($HTTP_SERVER_VARS['HTTP_USER_AGENT'],'MSIE'))
				header('Content-Type: application/force-download');
			else
				header('Content-Type: application/octet-stream');
	
			if( headers_sent() )
				throw new Exception('Some data has already been output to browser, can\'t send PDF file');
	
			header('Content-Length: '.strlen($out));
			header('Content-disposition: attachment; filename=NotaDebito.pdf');
	
			exit($out);
    	} catch (Zend_Pdf_Exception $e)
    	{
    		require_once 'app/models/Error/Error.php';
    		return(new Error('Erro ao gerar o PDF.'.$e->getMessage()));
    	}		
    }
    
    /**
     * Extension of basic draw-text function to allow it to vertically center text
     *
     * @param Zend_Pdf_Page $page
     * @param string $text
     * @param int $x1
     * @param int $y1
     * @param int $x2
     * @param int $position
     * @param string $encoding
     * @return Zend_Pdf_Page
     */
    public static function drawText(Zend_Pdf_Page $page, $text, $x1, $y1, $x2 = null, $position = self::TEXT_ALIGN_LEFT, $encoding = null){
        if( $encoding == null ) $encoding = self::$encoding;
        
        $bottom = $y1; // could do the same for vertical-centering
        switch ($position) {
            case self::TEXT_ALIGN_LEFT :
                $left = $x1;
                break;
            case self::TEXT_ALIGN_RIGHT :
            if (null === $x2) {
                    throw new Exception ( "Cannot right-align text horizontally, x2 is not provided" );
                }
                $textWidth = self::getTextWidth ( $text, $page );
                $left = $x2 - $textWidth;
                break;
            case self::TEXT_ALIGN_CENTER :
                if (null === $x2) {
                    throw new Exception ( "Cannot center text horizontally, x2 is not provided" );
                }
                $textWidth = self::getTextWidth ( $text, $page );
                $left = $x1 + $textWidth / 2;
                break;
            default :
                throw new Exception ( "Invalid position value \"$position\"" );
        }
        
        // display multi-line text
        $page->drawText ( $text, $left, $y1, $encoding );
        return $page;
    }    
    
    /**
     * Draw text inside a box using word wrap
     * 
     * @param Zend_Pdf_Page $page
     * @param string $text
     * @param int $x1
     * @param int $y1
     * @param int $x2
     * @param int $position
     * @param float $lineHeight
     * @param string $encoding
     * 
     * @return integer bottomPosition
     */
    public static function drawTextBox(Zend_Pdf_Page $page, $text, $x1, $y1, $x2 = null, $position = self::TEXT_ALIGN_LEFT, $lineHeight = 1.1, $encoding = null)
    {
        if( $encoding == null ) $encoding = self::$encoding;
        
        $lines = explode(PHP_EOL, $text);
        
        $bottom = $y1;
        $lineHeight = $page->getFontSize() * $lineHeight;
        foreach( $lines as $line ){
            preg_match_all('/([^\s]*\s*)/i', $line, $matches);
            
            $words = $matches[1];
            
            $lineText = '';
            $lineWidth = 0;
            foreach( $words as $word ){
                $wordWidth = self::getTextWidth($word, $page);
                
                if( $lineWidth+$wordWidth < $x2-$x1 ){
                    $lineText .= $word;
                    $lineWidth += $wordWidth;
                }else{
                    self::drawText( $page, $lineText, $x1, $bottom, $x2, $position, $encoding );
                    $bottom -= $lineHeight;
                    $lineText = $word;
                    $lineWidth = $wordWidth;
                }
            }
            
            self::drawText( $page, $lineText, $x1, $bottom, $x2, $position, $encoding );
            $bottom -= $lineHeight;
        }
        
        return $bottom;
    }

    /**
     * Return length of generated string in points
     *
     * @param string                     $text
     * @param Zend_Pdf_Resource_Font|Zend_Pdf_Page     $font
     * @param int                         $fontSize
     * @return double
     */
    public static function getTextWidth($text, $resource, $fontSize = null, $encoding = null) {
        if( $encoding == null ) $encoding = self::$encoding;
        
        if( $resource instanceof Zend_Pdf_Page ){
            $font = $resource->getFont();
            $fontSize = $resource->getFontSize();
        }elseif( $resource instanceof Zend_Pdf_Resource_Font ){
            $font = $resource;
            if( $fontSize === null ) throw new Exception('The fontsize is unknown');
        }
        
        if( !$font instanceof Zend_Pdf_Resource_Font ){
            throw new Exception('Invalid resource passed');
        }
        
        $drawingText = iconv ( '', $encoding, $text );
        $characters = array ();
        for($i = 0; $i < strlen ( $drawingText ); $i ++) {
            $characters [] = ord ( $drawingText [$i] );
        }
        $glyphs = $font->glyphNumbersForCharacters ( $characters );
        $widths = $font->widthsForGlyphs ( $glyphs );
        $textWidth = (array_sum ( $widths ) / $font->getUnitsPerEm ()) * $fontSize;
        return $textWidth;
    }    
}