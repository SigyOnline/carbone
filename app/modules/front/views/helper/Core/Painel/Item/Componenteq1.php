      <div style="padding:8px; border:solid; border-color:#DDD">
      <strong>Categoria:</strong> <?php echo $categoria['categoria']; ?> &nbsp;.&nbsp; <strong>Status:</strong> <span><?php echo $status['status']; ?></span><br />
      <br>
      <table id="linhas" class="listagem" cellpadding="5" cellspacing="2" border="0" width="100%">
        <tr class="head">
          <td align="left">Protocolo</td>
          <td align="left">Nome</td>
          <td align="left">Categoria</td>
          <td align="left">Status</td>
          <td width="100" align="center">Data de Abertura</td>
          <td width="100" align="center">Data Última Interação</td>
          <td width="20" align="left">&nbsp;</td>
        </tr>
<?php  
if ($res):
foreach ($res as $chamado):
?>        
        <tr>
          <td align="left"><a href="javascript:;" onclick="consulta(<?php echo $chamado->id_chamado; ?>);"><?php  echo printDadosFicha($chamado->id_chamado,'id');?></a></td>
          <td align="left"><?php echo $chamado->nome; ?></td>
          <td align="left"><?php echo $chamado->categoria; ?></td>
          <td align="left"><?php echo $chamado->status; ?></td>
          <td align="center"><?php echo date ('d-m-Y H\:i\:s',strtotime($chamado->criado)); ?></td>
          <td align="center"><?php echo is_int(strtotime($chamado->modificado))? date ('d-m-Y H\:i\:s',strtotime($chamado->modificado)): '-'; ?></td>
          <td align="center"><a href="chamado/edt/id/<?php echo  $chamado->id_chamado?>"><img src="<?php echo $this->baseUrl;?>/images/icolapis.png" /></a></td>
        </tr>
<?php endforeach;
else:
?>   
<tr>
<td colspan="7">Não há Chamados para está Categoria e Status:</td>
</tr> 
<?php endif; ?>       
      </table>
<?php if ($res):?>      
      <div class="paginas" ><a href="/chamado/pesquisa?id_status=<?php echo $status['id_status']; ?>">Ver todos</a></div>
<?php endif;?>      
      </div>
      <br>
