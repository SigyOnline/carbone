<?php
require_once MYCORE . '/Cadastro/Consultafiltro.php';
$filtro = new Cadastro_Consultafiltro ( Zend_Registry::get ( 'db' ) );
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<base href="<?php echo Zend_Registry::get('config')->www->host; ?>">
	<meta property="og:url" content="<?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>">
	<meta property="og:site_name" content="Carbone Imóveis">
	<meta property="og:type" content="website">
	<meta property="og:image:width" content="600">
	<meta property="og:image:height" content="315">
	<?php $url_ver = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>
	<?php if(strpos($url_ver, "html") || strpos($url_ver, "detalhe")){ ?>
		<meta property="og:description" content="<?php echo $this->dados->descricao_web; ?>">
		<meta property="og:title" content="<?php echo $this->dados->categoria.' em '.$this->dados->bairro; ?>">
		<meta property="og:image" content="<?php echo $this->dados->foto_destaque; ?>">
	<?php }else{ ?>
		<meta property="og:description" content="Imóveis para venda e locação em São Paulo">
		<meta property="og:title" content="Carbone Imóveis">
		<meta property="og:image" content="src/logo.png">
	<?php } ?>
	<script type="text/javascript"src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> -->
	<!-- <script src="//code.jquery.com/jquery-3.2.1.min.js"></script> -->
	<link rel="stylesheet" type="text/css" href="css/style.min.css">
	<link rel="stylesheet" type="text/css" href="css/menu.css">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="css/wt-lightbox.css" />
	<link rel="stylesheet" type="text/css" href="css/thumb-scroller.css" />
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/fancybox/jquery.fancybox.min.css">



	<!-- <script type="text/javascript" src="js/jquery.min.js"></script> -->
	<script type="text/javascript" src="js/jquery.form.min.js"></script>
	<script type="text/javascript" src="js/utils/validate.min.js"></script>
	<script type="text/javascript" src="js/app.min.js"></script>
	<?php $url_ver = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>
	<?php if(isset($_GET['id_noticia'])){ ?>
		<meta property="og:description" content="">
		<meta property="og:title" content="<?php echo $this->noticia->titulo; ?>">
		<meta property="og:image" content="<?php echo $this->noticia->imagem; ?>">
	<?php }elseif(strpos($url_ver, "html") || strpos($url_ver, "detalhe")){ ?>
		<meta property="og:description" content="Imóveis recomendados para você">
		<?php if(strpos($url_ver, "empreendimento")){ ?>
			<?php 

			function compressImage($source_path, $destination_path, $quality) {
				$info = getimagesize($source_path);

				if ($info['mime'] == 'image/jpeg') {
					$image = imagecreatefromjpeg($source_path);
				} elseif ($info['mime'] == 'image/png') {
					$image = imagecreatefrompng($source_path);
				}

				imagejpeg($image, $destination_path, $quality);

				return $destination_path;
			}
			$novonome = str_replace(' ', '-', $this->dados->empreendimento);
			$caminho = "imagens/imoveis/compressed/".$novonome.".jpg";
			$image = compressImage($this->dados->foto_destaque, $caminho, 25);

			?>
			<meta property="og:title" content="<?php echo 'Empreendimento em '.$this->dados->bairro; ?>">
			<meta property="og:image" itemprop="image" content="<?php echo "http://".$_SERVER['HTTP_HOST']."/".$image; ?>">
		<?php }else{ ?>
			<meta property="og:title" content="<?php echo $this->dados->categoria.' em '.$this->dados->bairro; ?>">
			<meta property="og:image" itemprop="image" content="<?php echo $this->dados->foto_destaque; ?>">
		<?php } ?>

	<?php }else{ ?>
		<meta property="og:description" content="">
		<meta property="og:title" content="Cmarqx">
		<meta property="og:image" content="src/logo.png">
	<?php } ?>
<!--[if IE]>

	<link rel="stylesheet" href="css/onlyie.css" type="text/css" />

<![endif]-->

<meta charset="utf-8">
<meta name="viewport"
content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="robots" content="index, follow" />
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
<meta name="author" content="Sigy Online" />
<link rel="stylesheet" type="text/css" href="css/lightSlider.css">

<style type="text/css">
.com_active {
	color: #f4b800 !important;
}
.is-invalid-input:not(:focus) {
	background-color: rgba(236,88,64,.1);
	border-color: #ec5840;
</style>

<script> 

	var $buoop = {vs:{i:8,f:15,o:12.1,s:5.1},c:2}; 

	function $buo_f(){ 

		var e = document.createElement("script"); 

		e.src = "//browser-update.org/update.js"; 

		document.body.appendChild(e);

	};

	try {document.addEventListener("DOMContentLoaded", $buo_f,false)}

	catch(e){window.attachEvent("onload", $buo_f)}

</script>

<title><?php echo Zend_Registry::get('config')->www->pagetitle; ?></title>

<!-- Global site tag (gtag.js) - AdWords: 981908078 --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-981908078"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-981908078'); </script>

<!-- Analytics -->
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-43601829-1', 'xaviercamargo.com.br');
	ga('send', 'pageview');

</script>
<!-- Analytics -->

</head>

<body>
	<!-- Google Tag Manager (noscript) -->

	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5F75D54"

		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

		<!-- End Google Tag Manager (noscript) -->


		<!-- Topo Position -->

		<section id="topo_position">

			<header id="menu" class="">

				<div class="center">

					<nav id="box_menu" class="noDesktop">

						<div class="icons_mobtab">

							<div class="ico_phone">(11) 2606-4000</div>
							<div class="ico_phone"><i class="fa fa-whatsapp fa-lg" aria-hidden="true"></i>(11) 96200-4000</div>

						
						</div>

						<div class="cssmenu">

							<ul class='border_li'>

								<li class='active'><a href=''>HOME</a></li>

								<li class='quemsomos'><a href=''>EMPRESA</a>

								<!-- <ul>

									<li class=''><a href='quemsomos'>Quem Somos</a></li>

									<li class=''><a href='quemsomos/correspondente'>Correspondente Bancário</a></li>

									<li class=''><a href='quemsomos/assessoria'>Assessoria Jurídica</a></li>

									<li class=''><a href='quemsomos/empreendimentos'>Empreendimentos</a></li>

									<li class=''><a href='quemsomos/parceiros'>Parceiro</a></li>

								</ul> -->

							</li>

							<li class=''><a href=''>IMÓVEIS</a> 

								<ul>

									<li class=''><a href='imoveis?status=VENDA'>Venda</a></li>

									<li class=''><a href='imoveis?status=ALUGUEL'>Locação</a></li>

									<!-- <li class=''><a href='imoveis/comparar'>Comparar Imóveis</a></li>

									<li class=''><a href='imoveis/favoritos'>Imóveis favoritos</a></li>
								-->
							</ul>

						</li>

						<li class=''><a href='servicos'>SERVIÇOS</a> 

								<!-- <ul>

									<li class=''><a href='servicos/administracao'>Administração de Imóveis</a></li>

									<li class=''><a href='contato/cadastre'>Cadastre seu imóvel</a></li>

									<li class=''><a href='http://www.gregorisoft.com.br/imobiliarias/area-restrita/login.php?i=QXAyS0FvOHNuMzNkajRKU1A1M2U1MDAxMjEzV3Fsaw==' target="_blank">2ª via do Boleto</a></li>

									<li class=''><a href='servicos/simule'>Crédito Imobiliário</a></li>

									<li class=''><a href='servicos/exclusividade'>Exclusividade</a></li>

								</ul> -->

							</li>

							<li class=' noMobile noTablet'><a href='servicos'>Nosso Serviços</a></li>

							<li class=''><a href='contato'>CONTATO</a></li>


						</ul>

					</div>

				</nav>

			</div>

		</header>

		<article id="box_logo">

			<div class="center">

				<h1 class="logo animated pulse">
					<a href=""><img src="src/logo.png"
						alt="<?php echo Zend_Registry::get('config')->www->pagetitle; ?>" /></a>
						<!--<span>CRECI 23.597-J</span>-->
					</h1>

					<div style="margin-top:20px; margin-left:50px;" class="nodeskgambi icons_mobtab">

							<div style="margin-left:10px;">(11) 2606-4000</div>
							<div class="ico_phone">(11) 96200-4000</div>

						
						</div>


					<nav id="box_menu_desktop" class="noOnlyTablet noMobile">
						


						<div class="cssmenu">

							<ul class=''>

								<li class='active'><a href=''>HOME</a></li>

								<li class=''><a href='quemsomos'>EMPRESA<!--  <span class="ico_seta"></span> --></a>

					<!-- <ul>

						<li class=''><a href='quemsomos'>Quem somos</a></li>
						<li class=''><a href='quemsomos/correspondente'>Correspondente Bancário</a></li>
						<li class=''><a href='quemsomos/assessoria'>Assessoria Jurídica</a></li>
						<li class=''><a href='quemsomos/empreendimentos'>Empreendimentos</a></li>
						<li class=''><a href='quemsomos/parceiros'>Parceiros</a></li>

					</ul>
				-->
			</li>

			<li class=''><a href='javascript:void(0);'>IMÓVEIS <span class="ico_seta"></span></a>

				<ul>

					<li class=''><a href='imoveis?status=VENDA'>Venda</a></li>
					<li class=''><a href='imoveis?status=ALUGUEL'>Locação</a></li>
						<!-- <li class=''><a href="imoveis/comparar">Comparar Imóveis</a></li>
							<li class=''><a href="imoveis/favoritos">Imóveis Favoritos</a></li> -->

						</ul>

					</li>

					<li class=''><a href='servicos'>SERVIÇOS <!-- <span class="ico_seta"></span> --></a>
<!-- 
					<ul>
						<li class=''><a href='servicos/administracao'>Administração de Imóveis</a></li>
						<li class=''><a href='contato/cadastre'>Cadastre seu Imóvel</a></li>
						<li class=''><a href='http://www.gregorisoft.com.br/imobiliarias/area-restrita/login.php?i=QXAyS0FvOHNuMzNkajRKU1A1M2U1MDAxMjEzV3Fsaw==' target="_blank">2ª via de boleto</a></li>
						<li class=''><a href='servicos/simule'>Crédito Imobiliário</a></li>
						<li class=''><a href='servicos/exclusividade'>Exclusividade</a></li>

					</ul>
				-->
			</li>
			<li class=''><a href='localizacao'>LOCALIZAÇÃO <!-- <span class="ico_seta"></span> --></a>
			</li>

			<li class=''><a href='contato'>CONTATO <!-- <span class="ico_seta"></span> --></a>

				<!-- <ul>

					<li class=''><a href='contato'>Fale Conosco</a></li>

					<li class=''><a href='contato/ligamos<?php echo $this->id ? "?ref={$this->id}":NULL;?>'
						class="" data-fancybox data-src="#ligamos_lightbox">Ligamos para você</a></li>
						<li class=''><a href='contato/trabalheconosco'>Trabalhe Conosco</a></li>

						<li class=''><a href='localizacao'>Localização</a></li>

					</ul>
 -->
				</li>
				<li class='central'>
					Central de Atendimento<br>
					<a href="https://wa.me/5511962004000?text=Olá%20seja%20bem%20vindo%20a%20Carbone%20Imovéis%20em%20que%20posso%20ajuda-ló?" target="_blank"><span class="whatsheader"> (11) 2606-4000</span></a>
					<a href="https://wa.me/5511962004000?text=Olá%20seja%20bem%20vindo%20a%20Carbone%20Imovéis%20em%20que%20posso%20ajuda-ló?" target="_blank"><i class="fa fa-whatsapp fa-lg" aria-hidden="true"></i><span class="whatsheader"> (11) 96200-4000</span></a>

				</li>
			</ul>

		</div>

	</nav>

	<ul id="contato_links">

        
		<!--<li class="ico_phone"><a
			href="contato/ligamos<?php echo $this->id ? "?ref={$this->id}":NULL;?>"
			class="ligamos_lightbox">Ligamos para você</a></li>

			<li class="ico_contato"><a
				href="contato/contatoemail<?php echo $this->id ? "?ref={$this->id}":NULL;?>"
				class="contato_lightbox">Contato por e-mail</a></li>-->

			</ul>

		</div>

	</article>

	<!--busca principal-->

	<nav id="busca_principal">

		<div class="center">

			<!--include busca-->

			<div id="busca">

				<form action="imoveis" method="get" name="form1" id="form2">

					<div id="nav_busca">

						<h1>Encontre seu Imóvel:
						</h1>

						<ul id="lista_finalidade">

							<li><input type="radio" id="venda" name="finalidade"
								value="RESIDENCIAL" /> <label for="venda" class="">Residencial</label>
							</li>
							<li><input type="radio" id="locacao" name="finalidade"
								value="COMERCIAL" /> <label for="locacao" class="">Comercial</label>
							</li>

						</ul>

					</div>

					<div id="comporta_busca">
						<div class="campo">
							<div class="select" id="campoStatus">
								<div class="mask">STATUS</div>
							</div>
							<div class="options">
								<?php foreach ( $filtro->ListaStatus() as $s ): 
									if ( !$s->status || $s->status == 'VENDA E ALUGUEL' )
										continue;
									?> 

									<!--Item -->
									<label class="statusitem"> <input type="radio" class="statuscampo" name="status"
										value="<?php echo $s->status; ?>" class="checkbox" <?php echo ($s->status == $_GET['status']) ? 'checked' : '' ?> />
										<?php echo $s->status; ?>
									</label>
								<?php endforeach;?>
							</div>
						</div>
						<!-- Campo -->
						<div class="campo">
							<div class="select">
								<div class="mask">TIPO</div>
							</div>

							<div class="options">
								<?php foreach ( $filtro->ListaCategoria() as $cat ): ?>
									<?php if ($cat->categoria!="Empreendimento") {?>

										<!--Item -->
										<label> <input type="checkbox" name="categoria[]"
											value="<?php echo $cat->id_categoria; ?>" class="checkbox" <?php echo in_array($cat->id_categoria, $_GET['categoria'])? 'checked': '' ?> />
											<?php echo $cat->categoria; ?>
										</label>
									<?php }?>
								<?php endforeach; ?>  
							</div>
						</div>
						<!-- Campo -->
						<div class="campo">
							<div class="select">
								<div class="mask">CIDADE</div>
							</div>
							<div class="options cid">
								<label> <input type="radio" name="cidade"
									value="" />Selecione</label> 
									<!-- Itens --> 
									<?php foreach ( $filtro->ListaCidade() as $c ): ?>     
										<label class="cidadeitem"> <input type="radio" name="cidade"
											value="<?php echo $c->cidade; ?>" <?php echo isset($_GET['cidade']) && $_GET['cidade'] == $c->cidade ? 'checked' : ''; ?> />
											<?php echo $c->cidade; ?>
										</label> 
									<?php endforeach; ?>      
								</div>
							</div>
							<!-- Campo -->
							<div class="campo bair">
								<div class="select">
									<div class="mask">BAIRRO</div>
								</div>
								<div class="options">
									<!--Item -->
									<?php if(isset($_GET['cidade'])) :  ?>
										<?php foreach ( $filtro->ListaBairro($_GET['cidade']) as $b ): 

											?> 

											<label> <input type="checkbox" name="bairro[]"
												value="<?php echo $b->bairro; ?>" class="checkbox" <?php echo in_array($b->bairro, $_GET['bairro']) ? 'checked' : ''; ?> />
												<?php echo $b->bairro; ?>
											</label>
											<?php
										endforeach; ?>  
									<?php endif; ?>   
								</div>
							</div>
							<!-- Campo -->
							<div class="campo" id="valorvenda">
								<div class="select">
									<div class="mask">VALOR</div>
								</div>
								<div class="options">
									<!--Item -->
									<label> <input type="checkbox" name="valor_venda[]"
										value="0a200000" class="checkbox" />Até R$ 200.000,00
									</label> <label> <input type="checkbox" name="valor_venda[]"
										value="200000a400000" class="checkbox" />R$ 200.000,00 a R$ 400.000,00
									</label> <label> <input type="checkbox" name="valor_venda[]"
										value="400000a600000" class="checkbox" />R$ 400.000,00 a R$ 600.000,00
									</label> <label> <input type="checkbox" name="valor_venda[]"
										value="600000a800000" class="checkbox" />R$ 600.000,00 a R$ 800.000,00
									</label> <label> <input type="checkbox" name="valor_venda[]"
										value="800000a1000000" class="checkbox" />R$ 800.000,00 a R$ 1.000.000,00
									</label> <label> <input type="checkbox" name="valor_venda[]"
										value="1000000" class="checkbox" />acima de R$ 1.000.000,00
									</label>
								</div>
							</div>

							<div class="campo" style="display: none;" id="valoraluguel">
								<div class="select">
									<div class="mask">VALOR</div>
								</div>
								<div class="options">
									<!--Item -->
									<label> <input type="checkbox" name="valor_locacao[]"
										value="0a1000" class="checkbox"
										<?php echo (@in_array('0a1000', $_GET['valor_locacao']))?'checked="checked"':'' ?> />Até 1.000,00
									</label> <label> <input type="checkbox" name="valor_locacao[]"
										value="1000a2000" class="checkbox"
										<?php echo (@in_array('1000a2000', $_GET['valor_locacao']))?'checked="checked"':'' ?> />1.000,00
										a 2.000,00
									</label> <label> <input type="checkbox" name="valor_locacao[]"
										value="2000a3000" class="checkbox"
										<?php echo (@in_array('2000a3000', $_GET['valor_locacao']))?'checked="checked"':'' ?> />2.000,00
										a 3.000,00
									</label> <label> <input type="checkbox" name="valor_locacao[]"
										value="3000a4000" class="checkbox"
										<?php echo (@in_array('3000a4000', $_GET['valor_locacao']))?'checked="checked"':'' ?> />3.000,00
										a 4.000,00
									</label>  <label> <input type="checkbox" name="valor_locacao[]"
										value="4000" class="checkbox"
										<?php echo (@in_array('4000', $_GET['valor_locacao']))?'checked="checked"':'' ?> />Acima
										de 4.000,00
									</label>
								</div>
							</div>

							<div class="box_btn">
								<input type="submit" class="btn_padrao" onclick="return buscarImoveis()" value="BUSCAR"
								title="Buscar" />
							</div>
						</div>
					</form>

					<form action="imoveis/detalhe/" method="post"
					class="buscatopreferencia" id="formref">
					<div id="busca_palavra">

						<input type="text" class="palavra_chave" id="ref"
						onclick="apagaCampo(this)" onblur="preencheCampo(this)"
						value="Exemplo: 4558" name="ref" /> <a href="javascript:;"
						class="btn_ok" id="refe" title="Buscar"></a>

						<h1>Busca por Referência:</h1>

					</div>

				</form>

			</div>

		</div>

	</nav>

	<nav id="busca_mobile">
		<div class="busca center">
			<h1 class="search">
				<span>Buscar Imóvel</span>
			</h1>
			<form id="formBuscaMob" action="imoveis" method="get" name="form3">
				<div class="busca_form submenu">
					<ul id="finalidades">
						<li><input type="hidden" value="" name="finalidade"
							id="findalidade" class="" /><label id="resd" class="fin <?php echo ($_GET['finalidade'] == 'RESIDENCIAL') ? 'active':''; ?>"
								for="venda" data-finalidade="RESIDENCIAL">RESIDENCIAL</label></li>
								<li><label id="come" class="fin <?php echo ($_GET['finalidade'] == 'COMERCIAL') ? 'active':''; ?>" data-finalidade="COMERCIAL"
									for="locaÃ§Ã£o">COMERCIAL</label></li>
								</ul>
								<div>
									<span><select class="" name="categoria">
										<option value="">TIPO</option>
										<?php foreach ( $filtro->ListaCategoria() as $cat ): ?>    
											<option <?php echo ($_GET['categoria'] == $cat->id_categoria) ? 'selected':''; ?> value="<?php echo $cat->id_categoria; ?>"><?php echo $cat->categoria; ?></option>
										<?php endforeach; ?>
									</select></span>
								</div>
								<div>
									<span><select class="" name="status" id="stamob">
										<option value="">STATUS</option>
										<option <?php echo ($_GET['status'] == 'VENDA') ? 'selected':''; ?> value="VENDA">Venda</option>
										<option <?php echo ($_GET['status'] == 'ALUGUEL') ? 'selected':''; ?> value="ALUGUEL">Aluguel</option>
									</select></span>
								</div>
								<div>
									<span><select class="selectcid" name="cidade">
										<option value="">CIDADE</option>
										<?php foreach ( $filtro->ListaCidade() as $c ): ?>          
											<option <?php echo ($_GET['cidade'] == $c->cidade) ? 'selected':''; ?> value="<?php echo $c->cidade; ?>"><?php echo $c->cidade; ?></option>
										<?php endforeach;?>    
									</select></span>
								</div>
								<div>
									<!-- <span><select class="" name="bairro"> -->
										<span><select class="retornabairro" name="bairro">
											<option value="">Selecione a cidade</option>
									<!-- <option value="">BAIRRO</option>
	       <?php foreach ( $filtro->ListaBairro() as $b ): ?>        
	            <option <?php echo ($_GET['bairro'] == $b->bairro) ? 'selected':''; ?> value="<?php echo $b->bairro; ?>"><?php echo $b->bairro; ?></option>
	            <?php endforeach; ?>      -->
	        </select></span>
	    </div>
	    <div id="valorvendamobile">
	    	<span><select class="" name="valor_venda">
	    		<option value="">VALOR</option>
	    		<option <?php echo ($_GET['valor_venda'] == '0a200000') ? 'selected':''; ?> value="0a200000">Até 200.000,00</option>
	    		<option <?php echo ($_GET['valor_venda'] == '200000a400000') ? 'selected':''; ?> value="200000a400000">200.000,00 - 400.000,00</option>
	    		<option <?php echo ($_GET['valor_venda'] == '400000a600000') ? 'selected':''; ?> value="400000a600000">400.000,00 - 600.000,00</option>
	    		<option <?php echo ($_GET['valor_venda'] == '600000a800000') ? 'selected':''; ?> value="600000a800000">600.000,00 - 800.000,00</option>
	    		<option <?php echo ($_GET['valor_venda'] == '800000a1000000') ? 'selected':''; ?> value="800000a1000000">800.000,00 - 1.000.000,00</option>
	    		<option <?php echo ($_GET['valor_venda'] == '1000000') ? 'selected':''; ?> value="1000000">Acima de 1.000.000,00</option>
	    	</select></span>
	    </div>

	    <div style="display: none;" id="valoraluguelmobile">
	    	<span><select class="" name="valor_locacao">
	    		<option value="">VALOR</option>
	    		<option <?php echo ($_GET['valor_locacao'] == '0a1000') ? 'selected':''; ?> value="0a1000">Até 1.000,00</option>
	    		<option <?php echo ($_GET['valor_locacao'] == '1000a2000') ? 'selected':''; ?> value="1000a2000">1.000,00 - 2.000,00</option>
	    		<option <?php echo ($_GET['valor_locacao'] == '2000a3000') ? 'selected':''; ?> value="2000a3000">2.000,00 - 3.000,00</option>
	    		<option <?php echo ($_GET['valor_locacao'] == '3000a4000') ? 'selected':''; ?> value="3000a4000">3.000,00 - 4.000,00</option>
	    		<option <?php echo ($_GET['valor_locacao'] == '4000') ? 'selected':''; ?> value="4000">Acima de 4.000,00</option>
	    	</select></span>
	    </div>

	    <form>
	    	<div class="btn_radios">

	    		<p class="marg_top10 marg_bot10">DORMITÓRIOS</p>

	    		<input <?php echo ($_GET['dormitorios'] == '1') ? 'checked':''; ?> type="radio" id="dormit" name="dormitorios" value="1">
	    		<label for="dormitorio" class="marg_right30">1</label>

	    		<input <?php echo ($_GET['dormitorios'] == '2') ? 'checked':''; ?> type="radio" id="dormit" name="dormitorios" value="2">
	    		<label for="dormitorio" class="marg_right30">2</label>

	    		<input <?php echo ($_GET['dormitorios'] == '3') ? 'checked':''; ?> type="radio" id="dormit" name="dormitorios" value="3">
	    		<label for="dormitorio" class="marg_right30">3</label>

	    		<input <?php echo ($_GET['dormitorios'] == '4') ? 'checked':''; ?> type="radio" id="dormit" name="dormitorios" value="4">
	    		<label for="dormitorio" class="marg_right30">4</label>

	    		<input <?php echo ($_GET['dormitorios'] == '5') ? 'checked':''; ?> type="radio" id="dormit" name="dormitorios" value="5">
	    		<label for="dormitorio" class="marg_right30">5</label>

	    	</div> <!-- DORMITÓRIOS -->

	    	<div class="btn_radios">

	    		<p class="marg_top10 marg_bot10">SUÍTES</p>

	    		<input <?php echo ($_GET['suites'] == '1') ? 'checked':''; ?> type="radio" id="suites" name="suites" value="1">
	    		<label for="suites" class="marg_right30">1</label>

	    		<input <?php echo ($_GET['suites'] == '2') ? 'checked':''; ?> type="radio" id="suites" name="suites" value="2">
	    		<label for="suites" class="marg_right30">2</label>

	    		<input <?php echo ($_GET['suites'] == '3') ? 'checked':''; ?> type="radio" id="suites" name="suites" value="3">
	    		<label for="suites" class="marg_right30">3</label>

	    		<input <?php echo ($_GET['suites'] == '4') ? 'checked':''; ?> type="radio" id="suites" name="suites" value="4">
	    		<label for="suites" class="marg_right30">4</label>

	    		<input <?php echo ($_GET['suites'] == '5') ? 'checked':''; ?> type="radio" id="suites" name="suites" value="5">
	    		<label for="suites" class="marg_right30">5</label>

	    	</div> <!-- SUÍTES -->

	    	<div class="btn_radios">

	    		<p class="marg_top10 marg_bot10">VAGAS</p>

	    		<input <?php echo ($_GET['vagas'] == '1') ? 'checked':''; ?> type="radio" id="vagas" name="vagas" value="1">
	    		<label for="vagas" class="marg_right30">1</label>

	    		<input <?php echo ($_GET['vagas'] == '2') ? 'checked':''; ?> type="radio" id="vagas" name="vagas" value="2">
	    		<label for="vagas" class="marg_right30">2</label>

	    		<input <?php echo ($_GET['vagas'] == '3') ? 'checked':''; ?> type="radio" id="vagas" name="vagas" value="3">
	    		<label for="vagas" class="marg_right30">3</label>

	    		<input <?php echo ($_GET['vagas'] == '4') ? 'checked':''; ?> type="radio" id="vagas" name="vagas" value="4">
	    		<label for="vagas" class="marg_right30">4</label>

	    		<input <?php echo ($_GET['vagas'] == '5') ? 'checked':''; ?> type="radio" id="vagas" name="vagas" value="5">
	    		<label for="vagas" class="marg_right30">5</label>

	    	</div> <!-- SUÍTES -->



	    	<div>
	    		<input type="submit" value="BUSCAR" onclick="return buscarImoveis()" class="btn_padrao" />
	    	</div>

	    </form>

	    <form id="formBuscaMob" action="/imoveis/detalhe/" method="post"
	    class="formrefmobile">
	    <div>
	    	<input type="text" class="tipo_input" name="ref"
	    	value="REFERÊNCIA (EX.: 4558)" onclick="apagaCampo(this)"
	    	id="refmobi" /> <a href="javascript:;" id="refmobile"
	    	class="buscarReferencia"></a>
	    </div>
	</form>
</div>
</form>
</div>
</nav>

<script type="text/javascript">
	$(function(){
		$(".statuscampo").click(function(){
			var campo = $(this).val();

			if( campo == 'VENDA')
			{
				$("#valorvenda").show();
				$("#valoraluguel").hide();

			}

			if( campo == 'ALUGUEL')
			{
				$("#valoraluguel").show();
				$("#valorvenda").hide();

			}
		});

		$("#refe").click(function(){
			$("#formref").submit();
		});

		$("#stamob").change(function(){
			var sta = $(this).val();
			if( sta == 'VENDA')
			{
				$("#valorvendamobile").show();
				$("#valoraluguelmobile").hide();

			}

			if( sta == 'ALUGUEL')
			{
				$("#valoraluguelmobile").show();
				$("#valorvendamobile").hide();

			}

			$('select[name=valor_venda]').prop('selectedIndex', 0)
			$('select[name=valor_locacao]').prop('selectedIndex', 0)
		});

		if($("#stamob").val()=='ALUGUEL'){
			$("#valoraluguelmobile").show();
			$("#valorvendamobile").hide();
		}

		$(".fin").click(function(){
			var d =  $(this).data('finalidade');
			if( d == 'RESIDENCIAL')
			{
				$("#findalidade").val(d);
				$("#resd").addClass('active');
				$("#come").removeClass('active');
			}

			if( d == 'COMERCIAL')
			{
				$("#findalidade").val(d);
				$("#come").addClass('active');
				$("#resd").removeClass('active');
			}
		});
		$("#refmobile").click(function(){
			var idrefmob = $("#refmobi").val();
			window.location.href = "<?php echo Zend_Registry::get('config')->www->host; ?>/imoveis/detalhe/id/"+idrefmob;
		});
	});
	function buscarImoveis(){
		var idrefmob = $("#refmobi").val();
		var idref = $("#ref").val();
		if(idrefmob !='REFERÊNCIA (EX.: 4558)'){
			window.location.href = "<?php echo Zend_Registry::get('config')->www->host; ?>/imoveis/detalhe/id/"+idrefmob;
			return false;
		}
		if(idref !='Exemplo: 4558'){
			window.location.href = "<?php echo Zend_Registry::get('config')->www->host; ?>/imoveis/detalhe/id/"+idref;
			return false;
		}

		return true;
	}
	$("#comporta_busca .options.cid label").click(function(){
		var id = $(this).children().val();
		console.log(id);
		$.post("<?php echo Zend_Registry::get('config')->www->host;?>/imoveis/bairro/id/"+id,function(data){
			$(".bair .options").html(data);
			$('.birsel .mask').html("Bairro");
		});	
	});

	$('.selectcid').change(function(){
		var id = $(this).val();
		$.post("<?php echo Zend_Registry::get('config')->www->host;?>/imoveis/bairro/id/"+id,{tipo:"select"},function(data){
			$(".retornabairro").html(data);
		//console.log(data);
	});	
	});

	var cidade = '<?php echo $_GET['cidade'];?>';
	if (cidade !="") {
		$.post("<?php echo Zend_Registry::get('config')->www->host;?>/imoveis/bairro/id/"+cidade,{tipo:"select"},function(data){
			$(".retornabairro").html(data);
		});
	}

	$('.statusitem, .cidadeitem').click(function(){
		$('.options').hide();
	});
</script>

<!-- lightbox ligamos para voce -->

<div style="display: none;" id="ligamos_lightbox">
	<div class="lightbox_padrao">
		<h1>Ligamos para você</h1>
		<span class="closeFancyBox fecha_lightbox" title="Fechar">X</span>
	</div>
	<div class="centraliza_lightbox">
		<form action="contato/enviado" id="ligamosImovel" method="post" enctype="multipart/form-data">
			<input type="hidden" value="Ligamos-Para-Você" name="Assunto" />
			<?php if ( $_GET['ref'] ):?>
				<input type="hidden" name="ref" value="<?php echo $_GET['ref']?>">
			<?php endif;?>
			<div class="linha_form">
				<div class="box_input_light">
					<label for="nome-call" class="label01">Nome <span>(*)</span></label>
					<input type="text" name="Nome" class="input01" id="nome-call" required="required"/>
				</div>
				<div class="box_input_light">
					<label for="email-call" class="label01">E-mail</label>
					<input type="email" name="Email" class="input01" id="email-call" required="required"/>
				</div>
			</div>
			<div class="linha_form">
				<div class="box_input_lightDDD">
					<label for="tel-call" class="label01">DDD <span>(*)</span></label>
					<input type="tel" name="TelDDD" class="input01" id="tel-call" required="required" maxlength="2"/>
				</div>
				<div class="box_input_lightTel">
					<label for="tel-call" class="label01">Telefone (11) 2606-4000 <span>(*)</span></label>
					<input type="tel" pattern="\[0-9]{4,5}-[0-9]{4}$" name="Telefone" class="input01" id="tel-call" required="required" maxlength="10"/>
				</div>
				<div class="box_input_lightDDD">
					<label for="cel-call" class="label01">DDD</label>
					<input type="tel" name="CelDDD" class="input01" id="cel-call" maxlength="2"/>
				</div>
				<div class="box_input_lightCel">
					<label for="cel-call" class="label01">Telefone 96200-4000</label>
					<input type="tel" maxlength="9" pattern="\[0-9]{4,5}-[0-9]{4}$" name="Celular" class="input01" id="cel-call" maxlength="10"/>
				</div>
			</div>   
			<div class="fl w100 marg_top15">
				<label for="txt-prop" class="label01">Assunto (Opcional)</label>
				<textarea class="txtarea01" id="txt-prop" name="Mensagem" rows="2" cols="2"></textarea>
			</div>
			<!-- captcha -->
			<div class="box_captcha_light marg_top15">
				<div class="g-recaptcha" data-sitekey="6LeSM1sUAAAAAPj0pDQiXjin0d2D83TrvXSFT32W"></div>

			</div>
			<div class="box_texto marg_top15"> 
				<p class="sendText">Campos com <span class="font_red">(*)</span> devem ser preenchidos</p>
				<input type="submit" class="btn_enviar_form btn_padrao" value="ENVIAR"/>
			</div>
		</form>
	</div>
</div>

<!-- fim lightbox ligamos para voce -->


<!-- lightbox Contato por e-mail -->

<div style="display: none;" id="contato_lightbox">
	<div class="lightbox_padrao">
		<h1>Contato por e-mail</h1>
		<span class="closeFancyBox fecha_lightbox" title="Fechar">X</span>
	</div>
	<div class="centraliza_lightbox">
		<form action="contato/enviado" name="contatoImovel" data-validade="true" novalidate="novalidate" id="contatoImovel" method="post">
			<input type="hidden" value="Contato-Por-Email" name="Assunto" />
			<?php if ( $_GET['ref'] ):?>
				<input type="hidden" name="ref" value="<?php echo $_GET['ref']?>">
			<?php endif;?>
			<div class="linha_form">
				<div class="box_input_light">
					<label for="nomeContato" class="label01">Nome <span>(*)</span></label>
					<input type="text" name="Nome" class="input01" id="nomeContato" required="required"/>
				</div>
				<div class="box_input_light">
					<label for="emailContato" class="label01">E-mail <span>(*)</span></label>
					<input type="email" name="Email" class="input01" id="emailContato" required="required"/>
				</div>
			</div>
			
			<div class="fl w100 marg_top15">
				<label for="assuntoContato" class="label01">Assunto <span>(*)</span></label>
				<input type="text" name="Celular" class="input01" id="assuntoContato"/>
			</div>
			
			<div class="fl w100 marg_top15">
				<label for="txtContato" class="label01">Mensagem <span>(*)</span></label>
				<textarea class="txtarea01" id="txtContato" name="Mensagem" rows="2" cols="2" required="required"></textarea>
			</div>
			
			<!-- captcha -->
			<div class="box_captcha_light marg_top15">
				<div class="g-recaptcha" data-sitekey="6LeSM1sUAAAAAPj0pDQiXjin0d2D83TrvXSFT32W"></div>
<!--             <img class="fl" src="capsigy/captcha.php" alt="código captcha" id="captchaLight">
            <input type="button" class="captcha_loader_detal captcha_loader_light" name="Limpar" value="Atualizar Código" onclick="document.getElementById('captchaLight').src='capsigy/captcha.php?ID=SigyOnline' + Math.random();">            
            <input type="text" name="code" class="input_cap_detalhes input_capt_lightbox" placeholder="Insira o codigo"  required="required"/>       
            <p id="retornoCapLight" class="error_captcha fonte_red"></p> -->
        </div>
        
        <div class="box_texto marg_top15"> 
        	<p class="sendText">Campos com <span class="font_red">(*)</span> devem ser preenchidos</p>
        	<input type="submit" class="btn_enviar_form btn_padrao" value="ENVIAR"/>
        </div>
        
    </form>
    
</div>

<script type="text/javascript">
	app.init();
// jQuery(function(){
// 			jQuery( '#contatoImovel' ).ajaxForm( {
// 				 beforeSend: function() { 
// 						$('#contatoImovel input[type=submit]').attr({'value': 'ENVIANDO...', 'disabled':true});
// 			        },
// 		        success: function( data ) {
// 			        if(data)
// 			        {
// 			            if(parseInt(data.error) > 0 )
// 				        {
// 			            	$('#contatoImovel input[type=submit]').attr({'value': 'ENVIAR', 'disabled':false});
// 			            	$("#retornoCapLight").html('*Código inválido');
// 							alert(data.msg);
// 			            }
// 			            else
// 			            {
// 			            	$('#contatoImovel input[type=submit]').attr({'value': 'ENVIADO', 'disabled':true});
// 			            	window.location = 'contato/emailenviado';
// 			            	//alert(data.msg);					            
// 			            }
// 			        }
// 		        }
// 		    } );

// 		});

</script>

</div>

<!-- fim lightbox Contato por e-mail -->