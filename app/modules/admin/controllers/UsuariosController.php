<?php
require_once 'app/models/MyCore/Cadastro/Usuarios.php';

class Admin_UsuariosController extends My_Controller
{
	public function ini()
	{}
	
	public function indexAction()
	{
		$usuarios = new Cadastro_Usuarios($this->db);
		$usuarios->paginar(false);
		
		if ( $_GET['excluir'] > 0 )
		{
			$r = $usuarios->apaga($_GET['excluir']);
				
			if ( $r > 0 )
				$this->view->e = new Success('Banner apagado com sucesso.');
			else
				$this->view->e = new Error('Falha ao apagar. Nenhuma alteração foi feita.');
		}
		
		$this->view->result = $usuarios->lista();
	}
	
	public function cadastrarAction()
	{
		$this->view->dados = array();
		if ( $_POST )
		{
			$post = $_POST;
			
			$usuarios = new Cadastro_Usuarios($this->db);
			$post['senha'] = md5(Zend_Registry::get('config')->www->encryptvector.$post['senha']);
			$res = $usuarios->novo($post);
			
			if( $res > 0 )
			{
				$this->_redirect('admin/usuarios/editar/id/' . $res);
			} else 
			{
				new Error('Ocorreu um erro ao cadastrar.');
				$this->view->dados = $post;
				unset($this->view->dados['senha']);
			}
			
			
		}
		echo $this->view->render('usuarios/form.php');
		exit;		
	}
	
	public function editarAction()
	{
		
		if( $this->id < 1 )
			$this->_redirect('usuarios');
		
		$usuarios = new Cadastro_Usuarios($this->db);
		
		if( $_POST ) 
		{
			$post = $_POST;
			$post['senha'] = md5(Zend_Registry::get('config')->www->encryptvector.$post['senha']);
			$res = $usuarios->edita($this->id, $post);
			
			if( $res > 0 )
				$this->view->e = new Success('Usuário editado com sucesso');
			else 
				$this->view->e = new Error('Erro ao editar usuário');
		}
		
		$this->view->dados = (array) $usuarios->getDados($this->id);
		unset($this->view->dados['senha']);
		
		echo $this->view->render('usuarios/form.php');
		exit;
	}
	
}	