<?php
require_once 'app/models/MyCore/Cadastro/Imovel.php';

class Admin_ImportController extends My_Controller
{
	public function ini()
	{
		$frontController = Zend_Controller_Front::getInstance();
		$frontController->setParam('disableOutputBuffering', true);
		ob_implicit_flush(true);
		set_time_limit(0);
		$this->key = $this->config->rest->key;
		$this->host = $this->config->rest->host;
		$this->colImoveis = array(
				'Codigo'=>'codigo',
				'DataCadastro'=>'data_cadastro',
				'Endereco'=>'endereco',
				'BairroComercial'=>'bairro',
				'Cidade'=>'cidade',
				'UF'=>'uf',
				'CEP'=>'cep',
				'Numero'=>'numero',
				'Empreendimento'=>'empreendimento',
				'Referencia'=>'referencia',
				'Dormitorios'=>'dormitorios',
				'Suites'=>'suites',
				'ValorCondominio'=>'valor_condominio',
				'ValorLocacao'=>'valor_locacao',
				'ValorVenda'=>'valor_venda',
				'Exclusivo'=>'exclusivo',
				'Observacoes'=>'observacoes',
				'Lancamento'=>'lancamento',
				'ValorIptu'=>'valor_iptu',
				'PosicaoAndar'=>'posicao_andar',
				'Complemento'=>'complemento',
				'Situacao'  => 'situacao',
				'Bloco'=>'bloco',
				'DataAtualizacao'=>'data_atualizacao',
				'TipoImovel'=>'tipo_imovel',
				'LocacaoAnual'=>'locacao_anual',
				'LocacaoTemporada'=>'locacao_temporada',
				'Venda'=>'venda',
				'ExibirNoSite'=>'exibir_no_site',
				'AptosEdificio'=>'aptos_edificio',
				'AptosAndar'=>'aptos_andar',
				'GaragemTipo'=>'garagem_tipo',
				'Vagas'=>'vagas',
				'AreaTotal'=>'area_total',
				'AreaTerreno'=>'area_terreno',
				'Status'=>'status',
				'TituloSite'=>'titulo_site',
				'Incompleto'=>'incompleto',
				'EmDestaque'=>'em_destaque',
				'AreaPrivativa'=>'area_privativa',
				'DataEngrega'=>'data_entrega',
				'EdificioResidencial'=>'edificio_residencial',
				'TipoEndereco'=>'tipo_endereco',
				'Finalidade'=>'finalidade',
				'AreaConstruida'=>'area_construida',
                'AreaUtil'=>'area_util',
				'Salas'=>'salas',
				'SuperDestaqueWeb'=>'super_destaque_web',
				'BairroComercial'=>'bairro_comercial',
				'TituloAnuncio'=>'titulo_anuncio',
				'TipoOferta'=>'tipo_oferta',
				'Categoria'=>'categoria',
				'FotoDestaque'=>'foto_destaque',
				'FotoDestaquePequena'=>'foto_destaque_pequena',
				'Latitude'=>'latitude',
				'Longitude'=>'longitude',
				'DescricaoWeb'=>'descricao_web',
				'DataHoraAtualizacao'=>'data_hora_atualizacao',
				'Regiao'=>'regiao',
                'EEmpreendimento'=>'e_empreendimento',
				'FaseObra'=>'fase_obra'
		);
		
		$this->data = array();
		$this->imoveis = array();
		
		$this->colFoto = array(
				'id_imovel', 'data', 'descricao', 'destaque', 'foto', 
				'foto_pequena', 'tipo'		);
		
		$this->colVideo = array(
				'id_imovel', 'data', 'descricao', 'destaque', 'video', 'tipo'
		);
	}
	
	public function curlMultiRequest($urls, $options = array())
	{
		$ch = array();
		$results = array();
		$mh = curl_multi_init();
		foreach($urls as $key => $val) {
			$ch[$key] = curl_init();
			if ($options) {
				curl_setopt_array($ch[$key], $options);
			}
			curl_setopt($ch[$key], CURLOPT_URL, $val);
			curl_multi_add_handle($mh, $ch[$key]);
		}
		$running = null;
		do {
			curl_multi_exec($mh, $running);
		}
		while ($running > 0);
		// Get content and remove handles.
		foreach ($ch as $key => $val) {
			$results[$key] = curl_multi_getcontent($val);
			curl_multi_remove_handle($mh, $val);
		}
		curl_multi_close($mh);
		return $results;
	}
	
	
	public function VerificaImoveis()
	{
		$codigo = $this->GetCodigo();
		
		$codigoarray = array();
		
		foreach ( $codigo as $cod )
		{
			$codigoarray[] = $cod->codigo;
		}
		
		try {
			
		$curl = curl_init();
		curl_setopt_array($curl, array(
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_URL => $this->host.'/imoveis/listarcampos?key='.$this->key,
				CURLOPT_HTTPHEADER => array('Accept: application/json'),
				CURLO
				
				
		));
			
			
		$campo = (array)json_decode(curl_exec($curl));
		curl_close($curl);
			
		$colunas = array();
		
		foreach ($campo['imoveis'] as $index => $col) {
			if (isset($this->colImoveis[$col])) $colunas[] = $col;
		}
			
		$campos['fields'] = array('Codigo');			
			
		$pagina = 1;
		$totalImportados = 1;
		
		$pesquisar = array(
				'fields' => $campos['fields'],
				'filter' => array(
						array('ExibirNoSite' => array('Sim','sim'))
				),
				'paginacao' => array(
						'pagina'        => $pagina,
						'quantidade'    => 50
				));
		$tipoBusca = 'listar';
		
		$importados = 0;
		$tentativa = 0;
		$dados = array();
		while (true)
		{
			$urls = array();
			$options = array(
					CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_HTTPHEADER => array('Accept: application/json')
			);
			$curll = curl_init();
			curl_setopt_array($curll, array(
					CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_URL => $this->host .'/imoveis/listar?&key='.$this->key.'&pesquisa='.json_encode($pesquisar),
					CURLOPT_HTTPHEADER => array('Accept: application/json')
			));
		
			$resp = json_decode(curl_exec($curll));
			exit(print_r($resp));
			if ( $resp->status == 200 )
				break;
		
		
			foreach ($resp as $key => $value)
			{
		
				$pesquisaD = array(
						'fields' => $campos['fields'],
						'filter' => array(
								array('ExibirNoSite' => array('Sim','sim'))
						),
				);
					
				$urls[] = $this->host .'/imoveis/detalhes?&key='.$this->key
				.'&imovel='. $value->Codigo .'&pesquisa='.json_encode($pesquisaD);
			}
		
			$imoveis = $this->curlMultiRequest($urls,$options);
			$imoveisCodigo = array();
			
				
			foreach ($imoveis as $imovel)
			{
				$imovel = (array)json_decode($imovel);

				$dados[] = $imovel['Codigo'];
				array_push($imoveisCodigo, $imovel['Codigo']);
				//ob_flush();flush();
		
			}
			
				
				
			$pesquisar['paginacao']['pagina']++;
		}
		
		// $query = $this->GetNotIN($dados);
		$query = $this->NotIn($dados);

		foreach ( $query as $r )
		{
			$this->db->getConnection()->query('DELETE FROM sg_imoveis WHERE codigo = '.$r->codigo);
		}
		
		@curl_close($curl);
		
		}catch (Exception $e)
		{
			exit(print_r($e,1));
		}
		
	}
	
	public function removeImoveis(){
		$db = $this->db->getConnection();
		$res = $db->query('SELECT COUNT(*) as total FROM sg_imoveis');
		$imo = $res->fetchObject();
		$this->db->closeConnection();
		
		$limite = 300;
		$total = ceil($imo->total/$limite);
		$current = 0;
		$codigos = array();
		$codigosExcluidos = array();
		while ($current  < $total) {
			$offset = $current*$limite;
			
			$db = $this->db->getConnection();
			// "SELECT codigo FROM sg_imoveis LIMIT {$limite}, {$offset}<br>";
			$res = $db->query("SELECT codigo FROM sg_imoveis LIMIT {$offset}, {$limite}");
			$imo = $res->fetchAll(PDO::FETCH_OBJ);
			$this->db->closeConnection();
			
			foreach($imo as $i){
				array_push($codigos, $i->codigo);
			}
			//ex. do post: imoveis=[1,2,3,4,5]
			//retorna somente os imoveis excluidos do array 
			$imoveisCodigo = "[" . implode(", ", $codigos) . "]";
			
			$curll = curl_init();
			curl_setopt_array($curll, array(
					CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_URL =>  $this->host.'/imoveis/removidos?key='. $this->key,
					CURLOPT_HTTPHEADER => array('Accept: application/json'),
					CURLOPT_POST => 1,
					CURLOPT_POSTFIELDS => 'imoveis='. $imoveisCodigo,
						
			));
						
			$resp = (array)json_decode(curl_exec($curll));
			//percorre resultado e add ao array de imoveis excluidos
			foreach($resp as $k => $v) {
				array_push($codigosExcluidos,  $v);				
			}
			
			$codigos = array(); //Limpa array de codigos temporarios do select 
			curl_close($curll);
			
			
			$current++;
		}
		foreach ($codigosExcluidos as $cod) {
			$db = $this->db->getConnection();
			$res = $db->query("DELETE FROM sg_imoveis WHERE codigo = '{$cod}'");
			$res = $db->query("DELETE FROM sg_caracteristica_imovel WHERE id_imovel = '{$cod}'"); 
			$res = $db->query("DELETE FROM sg_fotos WHERE id_imovel = '{$cod}'"); 
			echo  utf8_decode('Cód. ' . $cod . ' Imovel excluido.<br>');
		}
	}
	
	public function indexAction()
	{
		$cadImovel = new Cadastro_Imovel($this->db);
		$data_importacao =  $this->GetDataImportacao();
		
		$dataimp = date('Y-m-d H:i:s');
			
		if ( $data_importacao != '' )
		{
// 				$codigo = $this->VerificaImoveis();
				$this->removeImoveis();
		}
		
		try {
			
			$curl = curl_init();
			curl_setopt_array($curl, array(
					CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_URL => $this->host.'/imoveis/listarcampos?key='.$this->key,
					CURLOPT_HTTPHEADER => array('Accept: application/json')
			));
			
			
			$campo = (array)json_decode(curl_exec($curl));
			curl_close($curl);
			
			$colunas = array();

			foreach ($campo['imoveis'] as $index => $col) {
				if (isset($this->colImoveis[$col])) $colunas[] = $col;
			}
			
			$campos['fields'] = array('Codigo');
			$campos['fields'][] = "Caracteristicas";
			$campos['fields'][] = "InfraEstrutura";
			//$campos['fields'][] = "Agencia";
			
			$camposFotos['fields'] = $colunas;
			$camposFotos['fields'][] = array('Foto' => $campo['Foto']);
			$camposFotos['fields'][] = array('Video' => $campo['Video']);
			$camposFotos['fields'][] = "Caracteristicas";
			$camposFotos['fields'][] = "InfraEstrutura";
			$camposFotos['fields'][] = array('Agencia' => array('Nome','DDD','Fone','Fone2'));
			
			
			$pagina = 1;
			$totalImportados = 1;
			$pesquisar = array(
										'fields' => $campos['fields'],
										'filter' => array(
													array('ExibirNoSite' => array('Sim','sim'))
												),
										'paginacao' => array(
						                    'pagina'        => $pagina,
						                    'quantidade'    => 50
				                    	));
			$tipoBusca = 'listar';

			$importados = 0;
			$tentativa = 0;
			
			if( $data_importacao == '' )
			{
				$this->db->getConnection()->query(
						   'truncate table sg_imoveis; 
							truncate table sg_categorias;
							truncate table sg_caracteristica_imovel;
							truncate table sg_fotos;
							truncate table sg_imovel_videos;
							truncate table sg_imoveis_agencia;'
						);
			}
			
			$pagina = 1;
			$totalImportados = 1;
			
			if ( $data_importacao != '' )
			{
				$pesquisar = array(
						'fields' => $campos['fields'],
						'filter' => array(
								array('ExibirNoSite' => array('Sim','sim'),
									  'DataHoraAtualizacao' => array(str_replace(' ', '+', $data_importacao), date('Y-m-d+H:i:s'))
								)
						),
						'paginacao' => array(
								'pagina'        => $pagina,
								'quantidade'    => 50
						));
			}else{
				
				$pesquisar = array(
						'fields' => $campos['fields'],
						'filter' => array(
								array('ExibirNoSite' => array('Sim','sim'))
						),
						'paginacao' => array(
								'pagina'        => $pagina,
								'quantidade'    => 50
						));
			}
			
			while (true)
			{
				$urls = array();
				$options = array(
						CURLOPT_RETURNTRANSFER => 1,
						CURLOPT_HTTPHEADER => array('Accept: application/json')
				);
				$curll = curl_init();
				curl_setopt_array($curll, array(
						CURLOPT_RETURNTRANSFER => 1,
						CURLOPT_URL => $this->host .'/imoveis/listar?&key='.$this->key.'&pesquisa='.json_encode($pesquisar),
						CURLOPT_HTTPHEADER => array('Accept: application/json')
				));
				
				$resp = json_decode(curl_exec($curll));
				
				if ( $resp->status == 200 )
					break;
				
				
				foreach ($resp as $key => $value) 
				{

					$pesquisaD = array(
							'fields' => $camposFotos['fields'],
							'filter' => array(
									array('ExibirNoSite' => array('Sim','sim'))
							),
					);
					
					$urls[] = $this->host .'/imoveis/detalhes?&key='.$this->key
					.'&imovel='. $value->Codigo .'&pesquisa='.json_encode($pesquisaD);
				}
				
				$imoveis = $this->curlMultiRequest($urls,$options);
				
				foreach ($imoveis as $imovel)
				{
					$imovel = (array)json_decode($imovel);
					
					
					$dados = array();
				    
					foreach ($imovel as $k => $v)
					{
						
						$coluna = $this->normalizeCampos($k);
							//if ( !in_array($coluna, $this->colImoveis) )
						//		continue;
							
					switch ($coluna) 
							{
								case 'categoria':
									$cats = $this->getCategorias();
									$c = array_search($v, $cats);
									
									if ( !$c )
									{
										$this->setCategorias($v);
										$cats = $this->getCategorias();
										$c = array_search($v, $cats);
									}
										$dados[$coluna] = $c;
								break;
								case 'descricao_web':
									$dados['descricao'] = base64_encode($v);
									break;
								case 'bairro_comercial':
									$dados['bairro'] = $v;
								break;
								//case 'valor_condominio':
									//$dados['valor_condominio'] = $v;
								//break;
								//case 'posicao_andar':
									//$dados['posicao_andar'] = $v;
								//break;
								default:
									if ( !empty($v) && !is_null($v) )
										$dados[$coluna] = ($coluna == 'descricao' || $coluna == 'observacoes')?base64_encode($v):$v;
								break;
							}
							
							$dados['data_importacao'] = $dataimp;
							//$dados['area_util'] = $dados['area_privativa'];
							
							if (!empty($dados['bairro_comercial']))
							{
								$dados['bairro'] = $dados['bairro_comercial'];
							}else{
								$dados['bairro'] = $dados['bairro'];
							} 
					}
				//	print_r($dados);
					//exit;
				try {
					
					if( $data_importacao == '' )
					{
						$id = $cadImovel->novo($dados);
					}else{
						$vse = $this->VerificaSeExiste($dados['codigo']);
						
						if (empty($vse))
						{
							echo  utf8_decode('Cód. '.$imovel['Codigo'].' Novo Imóvel Inserido na data '.$imovel['DataHoraAtualizacao'].'<br>');
							
							$id = $cadImovel->novo($dados);
						}else{
							echo  utf8_decode('Cód. '.$imovel['Codigo'].' Atualização de Imóvel na data '.$imovel['DataHoraAtualizacao'].'<br>');
							
							$id = $cadImovel->edita($dados['codigo'], $dados);
						}
					}
				
					$id = $this->imoveis[$dados['codigo']] = $dados['codigo'];
					
				} catch (Exception $e)
				{
					if ( $e->getCode() == 23000 )
						echo "<br> Imóvel duplicado: ". $dados['codigo'];
					else
						exit("<br>Falha no código: ".$dados['codigo']." \n\r". print_r($e->getMessage(),1));
				
					continue;
				}
				
				if ( $id < 0 )
					exit("<br>Falha no código: $id \n\r". print_r($dados,1));
				else
					$importados++;
					
				### caracteristicas
				$carac = (array)$imovel['Caracteristicas'];
				
				if ( is_array($carac) )
				{
					$this->db->getConnection()->query('DELETE FROM sg_caracteristica_imovel WHERE id_imovel = '.$id.' AND id_tipo_caracteristica_imovel = 1');
					foreach ($carac as $label => $valor)
						$cadImovel->addCaracteristica($label, $valor);
				}
				
				### infra
				$infra = (array)$imovel['InfraEstrutura'];
				if ( is_array($infra) )
				{
					$this->db->getConnection()->query('DELETE FROM sg_caracteristica_imovel WHERE id_imovel = '.$id.' AND id_tipo_caracteristica_imovel = 2');
					foreach ($infra as $label => $valor)
						$cadImovel->addInfra($label, $valor);
				}
					
				### Fotos
				$pgatual = 1;
				$restFoto = $rest;
				
					$fotonosite = (isset($foto['ExibirNoSite'])?'ExibirNoSite':'ExibirSite');
					
					$this->db->getConnection()->query('DELETE FROM sg_fotos WHERE id_imovel = '.$id);
					
					foreach ($imovel['Foto'] as $foto)
					{
						$foto = (array) $foto;
						if ( $foto[$fotonosite] != 'Sim'  )
							continue;
							
						$dados = array(
								'id_imovel'    => $id,
								'data' 		   => $foto['Data']?$foto['Data']:NULL,
								'descricao'    => $foto['Descricao'],
								'destaque'     => $foto['Destaque'],
								'foto'		   => $foto['Foto'],
								'foto_pequena' => $foto['FotoPequena'],
								'tipo'		   => $foto['Tipo']
						);
							
						try {
							$cadImovel->addFoto($dados);
						} catch (Exception $e)
						{
							echo $e;
							exit(print_r($dados));
						}
					}
				
					foreach ($imovel['Video'] as $video)
					{
						$video = (array) $video;
						if ( $video['ExibirSite'] != 'Sim' )
							continue;
				
						$dados = array(
								'id_imovel'    => $id,
								'data' 		   => $video['Data']?$video['Data']:NULL,
								'descricao'    => $video['Descricao'],
								'destaque'     => $video['Destaque'],
								'video'		   => $video['Video'],
								'tipo'		   => $video['Tipo']
						);
				
						try {
							$cadImovel->addVideo($dados);
						} catch (Exception $e)
						{
							echo $e;
							exit(print_r($dados));
						}
					}
				
					ob_flush();flush();
				
			}
			$pesquisar['paginacao']['pagina']++;
			} 
			
			@curl_close($curl);
			
			
				
			
		} catch (Exception $e)
		{
			exit(print_r($e,1));
		}
		//exit ( "<br>".date("d-m-Y H:i:s")."<br> $importados importado(s)" );
		
		exit ( $importados. ' importado(s)' );
	}
	
	public function getCategorias ()
	{
		if ( !sizeof($this->catImovel) )
		{
			$this->catImovel = array();
			$db = $this->db->getConnection();
			$res = $db->query('SELECT * FROM  sg_categorias');
			$cats = $res->fetchAll(PDO::FETCH_OBJ);
				
			if ( $cats )
			{
				foreach ($cats as $cat)
				{
					$this->catImovel[$cat->id_categoria] = $cat->categoria;
				}
			}
		}
	
		return $this->catImovel;
	}
	
	public function GetDataImportacao()
	{
		$db = $this->db->getConnection();
		$res = $db->query('SELECT data_importacao FROM sg_imoveis ORDER BY data_importacao DESC limit 1');
		$imo = $res->fetchAll(PDO::FETCH_OBJ);
	
		return $imo[0]->data_importacao;
	}
	
	public function GetCodigo()
	{
		$db = $this->db->getConnection();
		$res = $db->query('SELECT codigo FROM sg_imoveis ORDER BY codigo DESC');
		$imo = $res->fetchAll(PDO::FETCH_OBJ);
	
		return $imo;
	}
	public function GetCodigoRemove($offset)
	{
		$db = $this->db->getConnection();
		$res = $db->query("SELECT codigo FROM sg_imoveis ORDER BY codigo DESC LIMIT $offset, 300");
		$imo = $res->fetchAll(PDO::FETCH_OBJ);
	
		return $imo;
	}
	
	public function VerificaSeExiste($id)
	{
		$db = $this->db->getConnection();
		$res = $db->query('SELECT codigo FROM sg_imoveis WHERE codigo = '.$id);
		$imo = $res->fetchAll(PDO::FETCH_OBJ);
	
		return $imo[0]->codigo;
	}

	
	public function GetNotIN($ids)
	{
		$imp = implode(',',$ids);
		$db = $this->db->getConnection();
		$res = $db->query('SELECT codigo FROM sg_imoveis WHERE codigo NOT IN ('.$imp.')');
		$imo = $res->fetchAll(PDO::FETCH_OBJ);
	
		return $imo;
	}

	public function NotIn($ids)
	{
		$this->db->closeConnection();
		$db = $this->db->getConnection();
		$res = $db->query('SELECT COUNT(*) as total FROM sg_imoveis');
		$limite = 100;
		$start = 0;
		$imo = $res->fetchObject();
		$total = ceil($imo->total/$limite);
		$current = 1;

		$notIn = array();
		while ($current  < $total) {
			$offset = $current*$limite;
			$this->db->closeConnection();
			$db = $this->db->getConnection();
			// "SELECT codigo FROM sg_imoveis LIMIT {$limite}, {$offset}<br>";
			$res = $db->query("SELECT codigo FROM sg_imoveis LIMIT {$limite}, {$offset}");
			$imo = $res->fetchAll(PDO::FETCH_OBJ);
			foreach($imo as $i){
				if( !in_array($i->codigo, $ids) )
				{
					array_push($notIn, $i);
				}
			}

			$current++;
		}
		return $notIn;
	}
	
	public function setCategorias ($val)
	{
		$db = $this->db->getConnection();
		$stmt = $db->prepare('INSERT INTO sg_categorias (categoria) VALUE (?)');
	
		try {
			$res = $stmt->execute(array($val));
				
			if ( !$res )
				exit (print_r($stmt->errorInfo(),1));
				
			$this->catImovel = array();
				
		} catch (PDOException $e)
		{
			throw new PDOException($e);
		}
	}
	
	public function loop()
	{
		
	}
	
	public function normalizeCampos($param) 
	{
			$filter = new Zend_Filter_Word_CamelCaseToUnderscore();
		
		return strtolower($filter->filter($param));
	}
}	