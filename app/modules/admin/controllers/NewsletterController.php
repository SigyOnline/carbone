<?php

require_once 'app/models/MyCore/Cadastro/Newsletter.php';
require_once CORE.'/Painel/Usuario.php';

class Admin_NewsletterController extends My_Controller
{
	public function ini()
	{}
	
	public function indexAction()
	{
		$obj = new Cadastro_Newsletter($this->db);
		
	/*	if ( $_GET['excluir'] > 0 )
		{
			$r = $obj->apaga($_GET['excluir']);
				
			if ( $r > 0 )
				$this->view->e = new Success('Noticia apagada com sucesso.');
			else
				$this->view->e = new Error('Falha ao apagar. Nenhuma alteração foi feita.');
		}*/
		
		if ( $_GET['exportar'] == 'sim' )
		{
			$filename ="lista.csv";
			
			$contents .= "Nome;Email;data;\n";
				
			foreach  ($obj->lista() as $row)
			{
				$contents .= $row->nome.";". $row->email .";". date('d-m-Y', strtotime($row->criado)) ."\n";
			}
			$contents = utf8_decode($contents);
				
			header('Content-Type: text/html; charset=utf-8');
			header('Content-type: application/ms-excel');
			header('Content-Disposition: attachment; filename='.$filename);
			echo $contents;
			exit();
		}
		
		if ($_GET['excluir'])
		{
			$obj->apaga($_GET['excluir']);
			$this->_redirect($this->view->baseModule . '/'. $this->view->baseController . '/?ok=s');
		}
		
		if ($_GET['ok'] == 's')
		{
			$this->view->e = new Success('Email Excluído');
		}
		
		$this->view->result = $obj->lista();
		
		
	}
	
	public function geranewsAction()
	{
		$this->view->result = array();
		$obj = new Cadastro_Newsletter($this->db);
		
		$file = file_get_contents('app/modules/admin/views/scripts/template.php');
		
		if ( $_POST )
		{
			$post = $_POST;
			$data = explode('/',$post['data']);
			
			
			$post['data'] = $data[2].'-'.$data[1].'-'.$data[0]; 
			$res = $obj->novonews($post);
			$var =	explode(',',$post['ref']);
			
			foreach ($var as $r)
			{
				$dados['id_imovel'] = $r;
				$dados['id_campanha'] = $res;
				 
				$res2 = $obj->novoflag($dados);
			}
			
			$imo = $obj->RetornaImoveis($res);
			$monta = $obj->MontaCampanha($imo);
			$campanha = $obj->RetornaCampanha($res);
			
			$e = str_replace('#titulo',$campanha->titulo, $file);
			$e = str_replace('#imagem',Zend_Registry::get('config')->www->host.'/'.$campanha->imagem, $e);
			$e = str_replace('#lst_inicio',$monta,$e);
			$e = str_replace('#dominio',Zend_Registry::get('config')->www->host,$e);
			$e = str_replace('#link',Zend_Registry::get('config')->www->host.'/newsletter/campanha'.$res.'.php',$e);
			
			file_put_contents('newsletter/campanha'.$res.'.php', $e);
			
			
			if ( $res > 0 )
			{
				$this->view->e = new Success('Campanha adicionada com sucesso');
			} else
			{
				$this->view->e = new Error('Erro ao cadastrar noticia.');
				$this->view->result = $post;
			}
				
		}
				$this->view->result = $obj->ListaCampanha();
	}
	
	public function gerahtmlAction()
	{
		$this->view->id = $this->id;
	}
}	
