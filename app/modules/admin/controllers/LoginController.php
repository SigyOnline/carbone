<?php
require_once 'app/models/MyCore/Cadastro/Admin.php';

class Admin_LoginController extends My_Controller
{
	
	public function indexAction()
	{
		if ( $_POST )
		{
			//error_reporting(0);
				
			$login = new Cadastro_Admin($this->db);
				
			if ( sizeof($_POST) )
				$res = $login->login($_POST['login'], $_POST['senha']);
		
			
			if ( $res > 0 )
			{
				$usuario = new Cadastro_Admin($this->db);
				$auth = $usuario->getAuth();
				$this->auth = $auth;
				$this->me = $this->view->me = $auth;
				Zend_Registry::Set('me',$this->view->me);
		
				if ( $_GET['r'] ){
					// $this->_redirect('/admin');
					$this->_redirect($this->getRequest()->getModuleName());
				}else{
					$this->_redirect('/admin');
				}
			} else
			{
				require_once 'app/models/Error/Error.php';
		
				switch ($res)
				{
					case Cadastro_Login_Abstract::LOGIN_INATIVO:
						$this->view->e = new Error2("Seu login não está ativo.");
						break;
							
					case Cadastro_Login_Abstract::LOGIN_INVALIDO:
						$this->view->e = new Error2('Login e/ou senha inválidos');
						break;
						
					case Cadastro_Login_Abstract::LOGIN_PERMISSAO:
						$this->view->e = new Error2('Você não possui permissão de acesso.');
						break;
							
					default:
						$this->view->e = new Error2('Erro ao efetuar o login, tente novamente');
						break;
				}
			}
		}
	}
	public function sairAction()
	{
		$usuario = new Cadastro_Admin($this->db);
		$usuario->sair();
		
		$this->_redirect($this->view->baseModule.'/login');
		
	}
	
}	