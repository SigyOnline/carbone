<?php
require_once 'app/models/MyCore/Cadastro/Perfil.php';

class Admin_PerfilbuscaController extends My_Controller
{
	public function ini()
	{
	}
	
	public function indexAction()
	{
		$obj = new Cadastro_Perfil($this->db);
		$this->view->filtro = array(
				'tipo' => 'bairros', 
				'data_inicio' => '',
				'data_fim' => '',
		);
		
		if ( !empty($_POST) )
		{
			$this->view->filtro = $_POST;
		}
		
		$this->view->result = $obj->lista($this->view->filtro);
		
	}
	
	
}	