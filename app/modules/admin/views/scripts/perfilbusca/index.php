<?php echo $this->render('header.php'); ?>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Mais Acessados</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
             <div class="row">
             <div class="col-lg-12">
                <div class="panel panel-default">
                        <div class="panel-heading">
                           Filtros
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        	<form role="form" action="" method="post" enctype="multipart/form-data">
                        	 <div class="row">
								<div class="form-group col-lg-3">
								    <label>Tipo de Relatório</label>
								    <select class="form-control" name="tipo">
								    	<option value="bairros" <?php echo ($this->filtro['tipo'] == 'bairros') ? 'selected="selected"':''; ?>>Bairros Mais Acessados</option>
								    	<option value="categorias" <?php echo ($this->filtro['tipo'] == 'categorias') ? 'selected="selected"':''; ?>>Categorias Mais Acessadas</option>
								    	<option value="cidades" <?php echo ($this->filtro['tipo'] == 'cidades') ? 'selected="selected"':''; ?>>Cidades Mais Acessadas</option>
								    	<option value="categoria_cidade" <?php echo ($this->filtro['tipo'] == 'categoria_cidade') ? 'selected="selected"':''; ?>>Categorias + Cidades Mais Acessadas</option>
								    </select>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-lg-2">
								    <label>Data Inicio</label>
								    <input class="form-control datepicker" name="data_inicio" placeholder="30/01/2000" value="<?php echo $this->filtro['data_inicio']; ?>" >
								</div>
								<div class="form-group col-lg-2">
								    <label>Data Fim</label>
								    <input class="form-control datepicker" name="data_fim" placeholder="30/01/2000" value="<?php echo $this->filtro['data_fim']; ?>" >
								</div>
                        	 </div>
                        	 <div class="row">
                        	 	<div class="form-group col-lg-4">
                        	 		<button class="btn btn-warning">Consultar</button>
                        	 		<!-- <button class="btn btn-warning">Total de Acessos</button>  -->
                        	 	</div>
                        	 </div>
                        	 </form>
                        </div>
                </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                <div class="panel panel-default">
                        <div class="panel-heading">
                           Buscas Realizadas
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <?php if ( $this->e ) echo $this->e;?>
                            <div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables">
                                    <thead>
                                        <tr>
                                            <th>Buscas Realizadas</th>
                                            <th>Visualizações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    	if ( $this->result ): 
                                    		foreach ($this->result as $row) :
                                    		
                                    			switch ( $this->filtro['tipo'] )
                                    			{
                                    				case 'bairros':
                                    					$campo1 = 'bairro';
                                    				break;
                                    				
                                    				case 'categorias':
                                    					$campo1 = 'cat';
                                    				break;
                                    				
                                    				case 'cidades':
                                    					$campo1 = '';
                                    				break;
                                    				
                                    				case 'categoria_cidade':
                                    					$campo1 = 'cat';
                                    				break;
                                    			}
                                    	
                                    	?>
                                        <tr class="odd gradeX">
                                            <td><?php echo ( $campo1 ) ? $row->{$campo1} . ' - ' : ''; ?><?php echo $row->cidade; ?></td>
                                            <td class="center"><?php echo $row->qtd; ?></td>
                                        </tr>
                                    <?php 	endforeach;
                                    	endif; 
                                    ?>
                                    </tbody>
                                </table>
                                <?php //echo $this->result; ?>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
</div>

<?php echo $this->render('footer.php'); ?>