<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>#titulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
<style type="text/css">
/*--------------- Client Style  ---------------*/
/* Force Hotmail/Outlook.com to display emails at full width. */
.ReadMsgBody{width:100%;} .ExternalClass{width:100%;}
.underine{text-decoration:underline;}
/* Force Hotmail/Outlook.com to display line heights normally. */
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;}
/* Remove spacing between tables in Outlook 2007 and up. */
table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}
/* Force Outlook 2007 and up to provide a "view in browser" message. */
#outlook a{padding:0;}
/* Force IE to smoothly render resized images. */
img{-ms-interpolation-mode: bicubic;}
/* Prevent Windows- and Webkit-based mobile platforms from changing declared text sizes. */
body, table, td, p, a, li, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%;}
html { width: 100%; }
body { -webkit-text-size-adjust: none; -ms-text-size-adjust: none; margin: 0; padding: 0; }
table { border-spacing: 0;border-collapse: collapse; }
table td { border-collapse: collapse; }
a { color:#ffffff!important; }
h1, h2, h3, h4, h5, h6, p{margin:0; padding:0;}
/*--------------- Tablet Style  ---------------*/
@media only screen and (max-width: 600px) {
body{width:100% !important; min-width:100% !important;}
table[class="flexibleFull"], table[class="flexibleFullContainer"] {width:100% !important;}
table[class="flexibleHeader1"]{width:100% !important; text-align:center!important;}
table[class="flexibleHeader2"]{width:100% !important;text-align:center!important; height:35px!important;}
table[class="flexibleContainer"] {width:48% !important; padding-bottom:20px!important;}
img[class="flexibleImage"] {width:100% !important; height: auto !important;}
td[class="flexibleCenter"] {text-align: center!important;}
td[class="templateColumnContainer"] {width:170px !important; height: auto !important;}
}
/*--------------- Mobile Style  ---------------*/
@media only screen and (max-width: 480px){
/* Force iOS Mail to render the email at full width. */
body{width:100% !important; min-width:100% !important;}
table[class="flexibleFull"], table[class="flexibleFullContainer"], table[class="flexibleContainer"], table[class="ButtonBg"]{width:100% !important;}
table[class="flexibleHeader1"]{width:100% !important; text-align:center!important;}
table[class="flexibleHeader2"]{width:100% !important;text-align:center!important; height:35px!important;}
img[class="flexibleImage"] {width:100% !important;  height: auto !important;}
table[class="textContentLast"], td[class="padding-bt-20"]{padding-bottom:20px !important;}
td[class="flexibleCenter"] {text-align: center!important;}
td[class="templateColumnContainer"] {display:block !important; width:100% !important;}
}
</style>
</head>
<body>
<!-- TABLE MODULE START // -->
<table style="background: none repeat scroll 0% 0% rgb(42, 42, 42);" modules="preheader" cellpadding="0" cellspacing="0" border="0" width="100%">
<tbody>
<tr>
<td align="center" valign="top">
<!-- MODULE ROW // -->
<table class="flexibleFull" cellpadding="0" cellspacing="0" border="0" width="600">
<tbody>
<tr>
<td align="center" valign="top">
<table class="flexibleFullContainer" cellpadding="0" cellspacing="0" border="0" width="600">
<tbody>
<tr>
<td style="padding:0px 10px; font-size:13px; font-family:Arial;" align="center" valign="top">
<!-- CONTENT TABLE // -->
<table class="textlight" style="color: rgb(255, 255, 255); padding: 10px 0px;" cellpadding="0" cellspacing="0" align="center" border="0" width="100%">
<tbody>
<tr>
<td style="height: 30px; position: relative;" width="200">
Caso não consiga visualizar este e-mail, <a style="color: rgb(197, 47, 51); text-decoration: none; text-decoration:underline" href="#link" title="Acesse" >acesse este link</a>
</td>
</tr>
</tbody>
</table>
<!-- // CONTENT TABLE -->
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<!-- // MODULE ROW -->
</td>
</tr>
</tbody>
</table>
<!-- // TABLE MODULE END -->
<!-- TABLE MODULE START // -->
<table style="background: none repeat scroll 0% 0% rgb(234, 234, 234);" modules="header" cellpadding="0" cellspacing="0" border="0" width="100%">
<tbody>
<tr>
<td align="center" valign="top">
<!-- MODULE ROW // -->
<table class="flexibleFull" cellpadding="0" cellspacing="0" border="0" width="600">
<tbody>
<tr>
<td align="center" valign="top">
<table class="flexibleFullContainer" cellpadding="0" cellspacing="0" border="0" width="600">
<tbody>
<tr>
<td style="padding:10px;  font-size:13px; font-family:Arial; " align="center" valign="top">
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<!-- // MODULE ROW -->
</td>
</tr>
</tbody>
</table>
<!-- // TABLE MODULE END -->
<!-- TABLE MODULE START // -->
<table style="background: none repeat scroll 0% 0% rgb(234, 234, 234);" modules="image" cellpadding="0" cellspacing="0" border="0" width="100%">
<tbody>
<tr>
<td align="center" valign="top">
<!-- MODULE ROW // -->
<table class="flexibleFull" cellpadding="0" cellspacing="0" border="0" width="600">
<tbody>
<tr>
<td align="center" valign="top">
<table class="flexibleFullContainer" cellpadding="0" cellspacing="0" border="0" width="600">
<tbody>
<tr>
<td class="bg-img" style="font-family:Arial;" align="center" valign="top"><!--[if gte mso 9]><v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="mso-width-percent:100;width:600px; height:300px;"><v:fill type="tile" src="http://mos/_mylab/Marketing/cuzto-buiderv1.3/http://www.theapppoint.com/cuzto/img-zip/banner-600.jpg" color="#e8aa9b" ></v:fill><v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0"><![endif]-->
<!-- CONTENT TABLE // -->
<div>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tbody>
<tr>
<td class="txt-imgheadColor" style="padding: 20px; color: rgb(69, 69, 69);" align="left" height="158" valign="middle">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tbody>
<tr>
<td style="color: rgb(69, 69, 69);" align="left" valign="top">
<a href="#link" title="#titulo">
<img src="#imagem" alt="#titulo" width="600">
</a>
</td>
</tr>
<tr>
<td style="color: rgb(69, 69, 69);" align="left" valign="top">
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
<!-- // CONTENT TABLE -->
<!--[if gte mso 9]></v:textbox></v:rect><![endif]--></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<!-- // MODULE ROW -->
</td>
</tr>
</tbody>
</table>
<!-- // TABLE MODULE END -->
<!-- TABLE MODULE START // -->
<table style="background:#f2f2f2;" modules="space" cellpadding="0" cellspacing="0" border="0" width="100%">
<tbody>
<tr>
<td align="center" valign="top">
<!-- MODULE ROW // -->
<table class="flexibleFull" cellpadding="0" cellspacing="0" border="0" width="600">
<tbody>
<tr>
<td align="center" valign="top">
<table class="flexibleFullContainer" cellpadding="0" cellspacing="0" border="0" width="600">
<tbody>
<tr>
<td align="left" height="25" valign="top">
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<!-- // MODULE ROW -->
</td>
</tr>
</tbody>
</table>
<!-- // TABLE MODULE END -->
<!--  LISTA // -->
<table style="background:#f2f2f2;" modules="2cols" cellpadding="0" cellspacing="0" border="0" width="100%">
<tbody>
<tr>
<td align="center" valign="top">
<!-- MODULE ROW // -->
<table class="flexibleFull" style="background:#ffffff; border-radius:3px;" cellpadding="0" cellspacing="0" border="0" width="600">
<tbody>
<tr>
<td align="center" valign="top">
<table class="flexibleFullContainer" cellpadding="0" cellspacing="0" border="0" width="600">
<tbody>
<tr>
<td class="flexibleContainerCell" style="padding:20px;" valign="top">
<!-- CONTENT TABLE // -->
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tbody>
<tr>
<td style="font-family: Arial; padding-bottom:10px;">
<div style="font-size: 18px; color: rgb(197, 47, 51); font-weight: bold; text-align: left; position: relative;" class="txt-headColor">
<p style="padding: 0px; margin: 0px;">
#titulo
</p>
</div>
</td>
</tr>
</tbody>
</table>
#lst_inicio


<!-- // CONTENT TABLE -->
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<!-- // MODULE ROW -->
</td>
</tr>
</tbody>
</table>
<!-- // LISTA -->
<!-- TABLE MODULE START // -->
<table style="background: none repeat scroll 0% 0% rgb(51, 51, 51);" modules="social" cellpadding="0" cellspacing="0" border="0" width="100%">
<tbody>
<tr>
<td align="center" valign="top">
<!-- MODULE ROW // -->
<table class="flexibleFull" cellpadding="0" cellspacing="0" border="0" width="600">
<tbody>
<tr>
<td align="center" valign="top">
<table class="flexibleFullContainer" cellpadding="0" cellspacing="0" border="0" width="600">
<tbody>
<tr>
<td class="myicon-social" align="center" valign="middle">
<!-- CONTENT TABLE // -->
<table cellpadding="0" cellspacing="0" border="0">
<tbody>
<tr>
<td align="center" height="45" width="45">
<a style="color: rgb(255, 255, 255); text-decoration: none;" href="{canalface}"><img src="{dominio}bns-newsletter/facebook.png" alt="" class="dribbble" border="0" height="26" width="26"></a>
</td>
<td align="center" height="45" width="45">
<a style="color: rgb(255, 255, 255); text-decoration: none;" href="{canaltwitter}"><img src="{dominio}bns-newsletter/twitter.png" alt="" class="dribbble" border="0" height="26" width="26"></a>
</td>
<td align="center" height="45" width="45">
<a style="color: rgb(255, 255, 255); text-decoration: none;" href="{canalyoutube}"><img src="{dominio}bns-newsletter/youtube.png" alt="" class="dribbble" border="0" height="26" width="26"></a>
</td>
</tr>
</tbody>
</table>
<!-- // CONTENT TABLE -->
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<!-- // MODULE ROW -->
</td>
</tr>
</tbody>
</table>
<!-- // TABLE MODULE END -->
<!-- TABLE MODULE START // -->
<table style="background: none repeat scroll 0% 0% rgb(91, 91, 91);" modules="footer" cellpadding="0" cellspacing="0" border="0" width="100%">
<tbody>
<tr>
<td align="center" valign="top">
<!-- MODULE ROW // -->
<table class="flexibleFull" cellpadding="0" cellspacing="0" border="0" width="600">
<tbody>
<tr>
<td align="center" valign="top">
<table class="flexibleFullContainer" cellpadding="0" cellspacing="0" border="0" width="600">
<tbody>
<tr>
<td style="padding:10px;" align="center" valign="top">
<!-- CONTENT TABLE // -->
<table cellpadding="0" cellspacing="0" align="center" border="0" width="100%">
<tbody>
<tr>
<td class="textlight" style="font-size: 13px; font-family: Arial; color: rgb(255, 255, 255); position: relative;" align="center">
{enderecodominio}<br />
<a href="#dominio" style="font-weight:bold">(dominio)</a>
</td>
</tr>
<tr>
<td height="10">
</td>
</tr>
</tbody>
</table>
<!-- // CONTENT TABLE -->
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<!-- // MODULE ROW -->
</td>
</tr>
</tbody>
</table>
<!-- // TABLE MODULE END -->
<!-- TABLE MODULE START // -->
<!-- // TABLE MODULE END -->
</body>
</html>