<?php if ($this->pageCount): ?> 
<?php 
if ( sizeof($_GET) > 0 ):
	$g = $_GET; 
	$q = array();
	
	unset($g['p']);
	
	while (list($k,$v) = each($g)) 
	{
		$q[] = $k .'='.$v;
	}
	
	$q = '&' . implode('&',$q);
endif;
?>
    	<table width="100%" cellpadding="0" cellspacing="0" border="0" class="paginacao">
    		<tr>
    			<td>Mostrando&nbsp;<?php echo $this->firstItemNumber; ?> - <?php echo $this->lastItemNumber; ?> 
    				&nbsp;de&nbsp;<?php echo $this->totalItemCount; ?>&nbsp; &nbsp; 
    				<!-- <a href="javascript:submitForm('?viewAll=true')">Ver todos</a>-->
    					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    			</td>
    			<td align="right"> 
					<?php if (isset($this->previous)): ?>
					 &lt; <a href="?p=<?php echo $this->previous . $q; ?>">Anterior</a>					
					<?php else: ?>
					  &lt;&nbsp;Anterior&nbsp;&nbsp;&nbsp;					
					<?php endif; ?>
					
					<?php foreach ($this->pagesInRange as $page): ?> 
					  <?php if ($page != $this->current): ?>
					    <a href="<?php echo '?p='.$page . $q; ?>"><?php echo $page; ?></a> | 
					  <?php else: ?>
					    <a href="<?php echo '?p='.$page . $q; ?>" class="selected"><?php echo $page; ?></a> | 
					  <?php endif; ?>
					<?php endforeach; ?>				
						
    				&nbsp;&nbsp;&nbsp;
					<?php if (isset($this->next)): ?> 
					  <a href="?p=<?php echo $this->next . $q;?>">Próximo </a>&gt;
					<?php else: ?> 
					  <span class="disabled">Próximo &gt;</span>
					<?php endif; ?>    				
    			</td>
    		</tr>
    	</table>
<?php endif; ?>
