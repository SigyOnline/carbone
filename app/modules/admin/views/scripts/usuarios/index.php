<?php echo $this->render('header.php'); ?>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Usuários</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            Usuários Cadastrados
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables">
                                    <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>Login</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($this->result as $result):?>
                                        <tr>
                                            <td><?php echo $result->nome; ?></td>
                                            <td><?php echo $result->login; ?></td>
                                            <td>
                                            	<a href="/admin/usuarios/editar/id/<?php echo $result->id_login; ?>"><span class="fa fa-edit"></span></a>
                                            	<?php if ( $result->id_login > 1 ): ?>
                                            	<a href="admin/usuarios/?excluir=<?php echo $result->id_login; ?>" onclick="return confirm('Deseja realmente excluir este usuário do sistema?');"><span class="fa fa-trash-o"></span></a>
                                            	<?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
</div>

<?php echo $this->render('footer.php'); ?>
