<?php 
$index = $this->index;
$i = 0;
while($i < 4):
?>
<div class="foto-imovel" id="foto-imovel-<?php echo $index ?>">
    <div class="row">
        <div class="form-group col-lg-1 " style="<?php if ($index <= 1) echo 'padding-top: 30px; display: block'; else echo 'display: none;'; ?> text-align: center">
            <label for="cbx-img-principal-<?php echo $index?>">Principal</label>
            <input type="radio" class="op_destaque"
                   id="cbx-img-principal-<?php echo $index?>" 
                   name="destaque"
                   <?php echo ( isset($this->result['foto_destaque']) && $this->result['foto_destaque'] != '' ? 'CHECKED' : '') ?>
                   value="<?PHP echo $index ?>" />
        </div>
        <div class="form-group col-lg-1" style="<?php if ($index <= 1) echo 'padding-top: 30px;' ?>">
            <label for="ordem-imagem-<?php echo $index?>">Ordem</label>
            <input type="text" 
                   class="form-control" 
                   id="ordem-imagem-<?php echo $index?>" 
                   placeholder="1" name="Imagem[<?php echo $index?>][ordem]" 
                   value="<?php echo ( isset( $imagem ) ? $imagem['ordem']: '') ?>" />
        </div>
        <div class="form-group col-lg-4" style="<?php if ($index <= 1) echo 'padding-top: 30px;' ?>">
            <label for="url-imagem-<?php echo $index?>">Imagem</label>
            <input type="file" 
                   class="form-control arquivo" 
                   id="url-imagem-<?php echo $index?>" 
                   name="endfoto[<?php echo $index?>]" 
                   value="<?php echo ( isset( $imagem['ENDFOTO'] ) ? $imagem['ENDFOTO']: '') ?>" />
        </div>
      </div>
      <div class="row">  
        <div class="form-group col-lg-4" style="<?php if ($index <= 1) echo 'padding-top: 30px;' ?>">
            <label for="descr-imagem-<?php echo $index?>">Descrição</label>
            <input type="text" 
                   class="form-control" 
                   id="descr-imagem-<?php echo $index?>" 
                   placeholder="Por exemplo: Quarto do imóvel" name="Imagem[<?php echo $index?>][descricao]" 
                   value="<?php echo ( isset( $imagem ) ? $imagem['DESCRFOTO']: '') ?>" />
        </div>
        <div class="form-group col-lg-4" style="<?php if ($index <= 1) echo 'padding-top: 30px;' ?>; display:">
            <label for="cat-imagem-<?php echo $index?>">Tipo</label>
            <select class="form-control" name="Imagem[<?php echo $index ?>][tipo]" id="cat-imagem-<?php echo $index ?>">
            	<?php foreach ($this->foto_categoria as $categoria_foto):?>
            	<option value="<?php echo $categoria_foto; ?>"><?php echo $categoria_foto; ?></option>
            	<?php endforeach; ?>
            </select>
        </div>
        <div class="col-lg-2 imagens-acoes" style="<?php if ($index <= 1) echo 'padding-top: 30px;' ?>">
            <label style="display: block">Ações</label>
            
            <?php if ( $i == 3 ): ?>
            <button class="btn btn-sm btn-info" 
                    title="Adicionar nova  imagem"
                    type="button"
                    onclick="javascript:addFotoImovel(<?php echo ( $index  + 1 ) ?>, this)">
                <i class="glyphicon glyphicon-plus"></i>
            </button>
            <?php endif; ?>
            <button class="btn btn-xs btn-danger <?php echo ( $i < 3)?'': 'hidden'; ?>" 
                    type="button"
                    onclick="javascript:removerFotoImovel(<?php echo $index ?>)" 
                    title="Remover Foto">
                <i class="glyphicon glyphicon-remove"></i>
            </button>
        </div>
        
    </div>

    <hr/>
</div>
<?php 
$index++;
$i++;
endwhile;
?>      
