<?php
require_once CORE . '/Painel/Item/Interface.php';
require_once 'app/models/MyCore/Carrinho.php';

Class MyCore_Painel_Item_Admin_MeusCursos implements Painel_Item_Interface
{
	/**
	 *@var Cadastro_Item_Abstract
	 */
	protected $usuario;
	
	public function __construct (Cadastro_Item_Abstract $usuario) 
	{
		$this->usuario = $usuario;
	}
	
	public function render()
	{
		$id = $this->usuario->getId();
		$select = new Zend_Db_Select(Zend_Registry::get('db'));
		
		$select->from(array('I' => Zend_Registry::get('config')->tb->inscricao),array('id_inscricao','data_inscr'))
			   ->joinLeft(array('C' => Zend_Registry::get('config')->tb->cliente), 
			   			  'I.id_cliente = C.id_cliente', 
			   			  array('id_cliente'))
			   ->joinLeft(array('FCOB' => Zend_Registry::get('config')->tb->cobranca_flag_cob), 
			   			  'I.id_cliente = FCOB.id_cliente',
			   			  array())
			   ->joinLeft(array('COB' => Zend_Registry::get('config')->tb->cobranca), 
			   			  'FCOB.id_cobranca = COB.id_cobranca',
			   			  array('id_cobranca'))
			   ->joinLeft(array('P' => Zend_Registry::get('config')->tb->pedido), 
			   			  'C.id_pedido = P.id_pedido', array('id_status'))
			   ->joinLeft(array('PI' => Zend_Registry::get('config')->tb->pedido_item), 
			   			  'P.id_pedido = PI.id_pedido',
			   			  array('produto'))
			   ->joinLeft(array('PR' => Zend_Registry::get('config')->tb->produto), 
			   			  'PI.id_produto = PR.id_produto',
			   			  array())
			   ->joinLeft(array('PRT' => Zend_Registry::get('config')->tb->produto_turma), 
			   			  'PR.id_produto = PRT.id_produto',
			   			  array('id_turma'))
			   ->joinLeft(array('T' => Zend_Registry::get('config')->tb->turma), 
			   			  'PRT.id_turma = T.id_turma',
			   			  array())
			   ->joinLeft(array('S' => Zend_Registry::get('config')->tb->status), 
			   			  'P.id_status = S.id_status',
			   			  array('status'))
			   ->joinLeft(array('CUR' => Zend_Registry::get('config')->tb->curso), 
			   			  'T.id_curso = CUR.id_curso',
			   			  array('id_curso','curso'))
				->where('C.id_cadastro = ?',$id)
				->order('I.id_inscricao DESC')
				->having('id_turma > 0')
				->group('I.id_cliente');
				//echo $select;
		$res = $select->query(Zend_Db::FETCH_OBJ)->fetchAll();
		
		require_once CORE_HELPER_DEFAULT.'/Painel/Item/Admin/MeusCursos.php';
	}
}