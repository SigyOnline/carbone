<?php
class ListaFiltros{
	
	protected $filtro;
	
	protected $itens = array();
	
	protected $itensValidos = array(
			'cidade',
			'status',
			'valorMaximo',
			'valorMinimo',
			'dormitorios',
			'suites',
			'vagas',
			'finalidade',
			'organico',
			'bairro',
			'categoria',
			'area_privativa',
			'valor_venda',
			'valor_locacao',
	);
	
	protected $itensIgnorados = array(
			
	);
	
	public function __construct(array $filtro){
		$this->filtro = $filtro;
		
	}

	public function generate(){
		foreach ($this->filtro as $key => $value){
			
			if(empty($value)) 
				continue;
			
			if(is_array($value)) {
				foreach ($value as $k => $v){
					$this->createLink($key, $v, $k);
				}
				
			} else {
				$this->createLink($key, $value);
			}
			
		}
		return $this;
	}
	
	public function createLink($key, $value, $k = null){
		if($this->validaItem($key) && !$this->itemIgnorado($key, $value)){
			$tempFiltro = $this->filtro;
			
			if($k){
				unset($tempFiltro[$key][$k]);
			}else{
				unset($tempFiltro[$key]);
			}
			
			$url = http_build_query($tempFiltro);
			$url = preg_replace('/%5B[0-9]+%5D/simU', '%5B%5D', $url);
			$this->addItem($key, $value, urldecode($url));
		}
	}
	
	public function addItem($key, $value, $link){
		
		$this->itens[] = array(
				'label' => $this->filter($key, $value),
				'link' => $link,
		);
		
	}
	
	public function getItens(){
		return $this->itens;
	}
	
	public function filter($key, $value){
		echo $key . ' - ';
		$label = '';
		
		switch ($key) {
			
			case 'dormitorios':
				$label = 'Dormitórios ' . $value;
				break;
			
			case 'vagas':
				$label = 'Vagas ' . $value;
				break;
			
			case 'suites':
				$label = 'Suítes ' . $value;
				break;
			
			case 'area_privativa':
				$label = 'Metragem '. str_replace('a', '-', $value);
				break;
			
			case 'categoria':
				$filtro = new Cadastro_Consultafiltro(Zend_Registry::get('db'));
				$label = $filtro->GetCategoria($value);
				break;
			
			case 'valor_venda':
				$label = 'Valor Venda R$ '.str_replace('a', '-', $value);
				break;
			
			case 'valor_locacao':
				$label = 'Valor Locação R$ '.str_replace('a', '-', $value);
				break;
			
			case 'valorMaximo':
				$label = 'Valor Máximo R$ '.str_replace('a', '-', $value);
				break;
			
			case 'valorMinimo':
				$label = 'Valor Mínimo R$ '.str_replace('a', '-', $value);
				break;
				
			default:
				$label = $value;
				break;
			
		}
		
		return $label;
	}
	
	public function validaItem($key){
		
		if(in_array($key, $this->itensValidos)){
			return true;
		}
		
		return false;
	}
	
	public function itemIgnorado($key, $valor){
		
		if(!empty($this->itensIgnorados[$key])){
			if($this->itensIgnorados[$key] == urldecode($valor)){
				return true;
			}
		}
		
		return false;
	}
	
	public function addItemIgnorado($key, $value){
		$this->itensIgnorados[$key] = $value;
		
		return $this;
		
		
	}
	
	public function getHtmlItens(){
		$html = '';
		foreach ($this->itens as $item) {
			
			$html .= ' <a class="delete_filter" href="imoveis/?'.$item['link'].'">'. strtoupper($item['label']) .'</a>' . PHP_EOL;
			
		}
		return $html;
	}
}