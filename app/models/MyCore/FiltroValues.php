<?php
class FiltroValues {
	
	/**
	 * 
	 * @param Zend_Db_Select $select
	 */
	public function __construct(Zend_Db_Select $select){
		$this->select = $select;
	}
	
	/**
	 * array(
	 * 	'item',
	 *  'item1'
	 * )
	 * 
	 * @param array $filtro
	 * @param string $campo
	 */
	public function filtro($filtro, $campo ){
		$cont = 1;
		
		if (count ( $filtro ) == 1) {
			$this->select->where ( "{$campo} = '" . $filtro[0] . "'" );
		} else {
			foreach ( $filtro as $value ) {
				if ($cont == 1) {
					$this->select->where ( "({$campo} = '" . $value . "'" );
				} else {
					
					if (count ( $filtro ) == $cont) {
						$this->select->orWhere ( "{$campo} = '" . $value . "')" );
					} else {
						$this->select->orWhere ( "{$campo} = '" . $value . "'" );
					}
				}
				
				$cont ++;
			}
		}
	}
	
	/**
	 * Modelo de array
	 * array('100a150','200a150', '500')
	 * 
	 * @param array|string $filtro
	 * @param string $campo
	 */
	public function filtroBetween($filtro, $campo) {
		
		$cont = 1;
		 
		if ( is_array($filtro) )
		{
			if (count ( $filtro ) == 1) 
			{
				
				$args = explode('a', $filtro[0]);
				
				if (!empty($args[1]))
				{
					$this->select->where("{$campo} >= ". $args[0]." AND {$campo} <= ". $args[1]);
				} else
				{
					$this->select->where("{$campo} >= ". $args[0]);
				}
			} else 
			{
				foreach ($filtro as $value)
				{
					$args = explode('a', $value);
			
					if ( $cont == 1 )
					{
						if (!empty($args[1]))
						{
							$this->select->where("({$campo} >= ". $args[0]." AND {$campo} <= ". $args[1]);
						} else
						{
							$this->select->where("({$campo} >= ". $args[0]);
						}
					} else
					{
						$sep = '';
						if (count ( $filtro ) == $cont) 
						{
							$sep = ')';
						}
						
						if (!empty($args[1]))
						{
							$this->select->orWhere("{$campo} >= ". $args[0]." AND {$campo} <= ". $args[1] . $sep);
						} else
						{
							$this->select->orWhere("{$campo} >= ". $args[0] . $sep);
						}
					}
			
					$cont++;
				}
			}
		} else
		{
			$args = explode('a', $filtro);
				
			if (!empty($args[1]))
			{
				$this->select->where("{$campo} >= ". $args[0]." AND {$campo} <= ". $args[1]);
			} else
			{
				$this->select->where("{$campo} >= ". $args[0]);
			}
		}
		
	}
	
	
}