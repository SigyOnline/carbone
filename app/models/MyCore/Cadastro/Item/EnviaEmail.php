<?php
 
class Cadastro_Item_EnviaEmail
{
	public function envia($post)
	{
		
		Zend_Loader::loadClass('Zend_Mail');
		Zend_Loader::loadClass('Zend_Mail_Transport_Smtp');
		
			$host = HOST_EMAIL;
			$from = EMAIL_CONTATO_SITE;
			$name = $pousada;
			$remetente = EMAIL_CONTATO_SITE;
			
			$config = array('auth' => 'login',
							'port' => 587,
							'username' 	=> USER_EMAIL,
                    		'password' 	=> PASS_EMAIL );
			
		$tr   = new Zend_Mail_Transport_Smtp($host,$config);
	    $mail = new Zend_Mail('utf-8');
	    $m = 'Nome: '.$post['nome']."\n";
	    $m .= 'Email: '.$post['email']."\n";
	    $m .= 'Pousada: '.$post['pousada']."\n\n";
	    
	    $msg = $m.$post['msg'];
	    
	    $mail->setFrom($from, $from);
		$mail->addTo($remetente);
		$mail->setSubject($post['assunto']);
			
		//if ( $opcoes['isHTML'] === true )
			//$mail->setBodyHtml($msg);
		//else
			$mail->setBodyText($msg);
			
		return  $mail->send($tr);
	}
}
