<?php
class Cadastro_FavCom {
	
	static $instance;
	
	protected $itens;
	
	protected $favclass = ' active';
	
	protected $comclass = ' active';
	
	const FAVORITOS = 'fav';

	const COMPARAR = 'com';
	
	//ArrayObject
	private function __construct(){
		
		$this->itens =  new Zend_Session_Namespace('fav_com');
			
		if( !is_array($this->itens->fav) )
		{
			$this->itens->fav = array();
		}
			
		if( !is_array($this->itens->com) )
		{
			$this->itens->com = array();
		}
	}
	
	static function getInstance()
	{
		if ( !(self::$instance instanceof Cadastro_FavCom ) )
		{
			self::$instance = new Cadastro_FavCom();
		}
		return self::$instance;
	}
	
	public function addFav($id_imovel)
	{
		if ( $id_imovel > 0  && !$this->inFavoritos($id_imovel) )
			$this->itens->fav[] = $id_imovel;
		return $this->count(self::FAVORITOS);
	}
	
	public function addCom($id_imovel)
	{
		if( $this->count(self::COMPARAR) >= 4)
			return 'LIMITE';
		
		if ( $id_imovel > 0 && !$this->inComparar($id_imovel) )
			$this->itens->com[] = $id_imovel;
		
		return $this->count(self::COMPARAR);
	}
	
	public function removeFav($id_imovel)
	{
		foreach ($this->itens->fav as $k => $fav)
		{
			if ( $id_imovel == $fav)
			{
				unset($this->itens->fav[$k]);
				break;
			}
		}
		
		return count($this->itens->fav);
	}
	
	public function removeCom($id_imovel)
	{
		foreach ($this->itens->com as $k => $com)
		{
			if ( $id_imovel == $com)
			{
				unset($this->itens->com[$k]);
				break;
			}
		}
		
		return count($this->itens->com);
	}
	
	public function removerTodos($session)
	{
		if( !empty($session) && isset($this->itens->{$session}) )
		{
			$this->itens->{$session} = array();
			return '0';
		}
		return false;
	}
	
	public function inFavoritos($id_imovel)
	{
		if( in_array($id_imovel, $this->itens->fav) )
			return true;
		
		return false;
	}
	
	public function inComparar($id_imovel)
	{
		if( in_array($id_imovel, $this->itens->com) )
			return true;
		
		return false;
	}
	
	public function getItens($session)
	{
		if( !empty($session) && isset($this->itens->{$session}) )
			return $this->itens->{$session};
		
		return false;
	} 
	
	public function count($session)
	{
		if( !empty($session) && isset($this->itens->{$session}) )
			return count( $this->itens->{$session} );
		
		return false;
	}
	
	public function getFavClass($id_imovel)
	{
		if ($this->inFavoritos($id_imovel))
			return $this->favclass;
		
		return '';
	}
	
	public function getComClass($id_imovel)
	{
		if ($this->inComparar($id_imovel))
			return $this->comclass;
		
		return '';
	}
}