<?php
require_once 'app/models/Error/Error.php';
require_once 'app/models/Error/Success.php';
require_once CORE. '/Cadastro/Login/Abstract.php';

class Cadastro_Admin extends Cadastro_Login_Abstract
{
	const NAME_SPACE_LOGIN = 'Administrador';
	
	const TIPO_LOGIN = 1;
	
	/**
	 * Tabela principal do cadastro 
	 * @var string
	 */
	protected $tabela;
	
	/**
	 * Classe do Item
	 * @param Cadastro_Item_Abstract $classeItem
	 */
	static public $classeItem = 'Cadastro_Item_Admin';
	
	public function __construct(Zend_Db_Adapter_Abstract $db)
	{
		if ( Zend_Registry::isRegistered('config') )
		{
			if ( Zend_Registry::get('config')->tb->login )
				$this->tabela = Zend_Registry::get('config')->tb->login;
			else
				throw new Exception('Tabela inválida');
		} else 
		{
			throw new Exception('Config inválido');
		}
		
		parent::__construct($db);
	}
	
	/**
	 * 
	 * Criar um novo usuário
	 * @param array $dados
	 * @throws Exception
	 * @return Cadastro_Item_Usuario
	 */
	
	static public function novo ($dados)
	{
		self::carregaClasseUsuario();

		$class = new self::$classeItem;
		
		if ( $class instanceof Cadastro_Item_Abstract )
		{
			$res = $class->novo($dados);
			if ( $res > 0 )
				return $class;
			else
				return $res;
		} else 
		{
			throw new Exception('A classe do item não é válida');
		}
	}
	
	static public function carregaClasseUsuario() 
	{
		if ( !class_exists(self::$classeItem) )
			return require_once MYCORE.'/'. str_replace('_', '/', self::$classeItem) .'.php';
	}
	
	public function apaga ($id)
	{
		self::carregaClasseUsuario();
		
		$tabela = new My_Table($this->tabela);
		return $tabela->delete($tabela->getAdapter()->quoteInto('id_login = ?', $id));
	}
	
	public function login ($login=NULL,$senha=NULL, $opcoes=NULL) 
	{
		Zend_Loader::autoload('Zend_Auth_Adapter_DbTable');
		Zend_Loader::autoload('Zend_Auth_Storage_Session');
		Zend_Loader::autoload('Zend_Auth_Result');
		Zend_Loader::autoload('Zend_Filter_StripTags');
		
		self::carregaClasseUsuario();
			
		if ( $opcoes['token'] )
		{
			unset($_SESSION['Login'],$_SESSION['CORE_CART']);
			try {
				$authAd = new Zend_Auth_Adapter_DbTable($this->db,$this->tabela,'login','token');
			} catch (Exception $e)
			{
				return Zend_Auth_Result::FAILURE_UNCATEGORIZED;
			}
			$filter = new Zend_Filter_Alnum();
			$filter2 = new Zend_Filter_StripTags();
			
			$token = $filter->filter($opcoes['token']);
			$login = $filter2->filter($login);
			
			if ( !empty($login) && !empty($token) )
			{
			    $authAd->setIdentity($login)
			           ->setCredential($token);
				$ok = true;
			}
		} else
		{
			try {
				$authAd = new Zend_Auth_Adapter_DbTable($this->db,$this->tabela,'login','senha');
			} catch (Exception $e)
			{
				return Zend_Auth_Result::FAILURE_UNCATEGORIZED;
			}
	
			$filter = new Zend_Filter_StripTags();
			    
			$lg = $filter->filter($login);
			$pw = $filter->filter($senha);

			if ( !empty($lg) && !empty($pw) )
			{
			    $authAd->setIdentity($lg)
			           ->setCredential(Cadastro_Item_Admin::cripto($pw));
			           //->setCredentialTreatment('md5(?)');
			     $ok = true;
			}
		}

		if ( $ok )
		{
		    $result = $authAd->authenticate();
		    
		    if ( $result->isValid() ) 
		    {
		    	$res = $authAd->getResultRowObject();
		    	
	    		$auth = Zend_Auth::getInstance();
		        $auth->setStorage(
		                        new Zend_Auth_Storage_Session(self::NAME_SPACE_LOGIN)
		                    );
		
		        // store the identity as an object where the password column has been omitted
		        $auth->getStorage()->write($res);
				
		        if ( $token )
		        {
		        	$tabela = new Cadastro_Item_Admin($login);
		        	if ( !$tabela->edita(array('token' => NULL)) )
		        	{
		        		return self::LOGIN_INATIVO;
		        	}
		        }  else 
		        {
			        $acessos = new Zend_Db_Table(Zend_Registry::get('config')->tb->acessos_login);
			        $acessos->insert(array(
			                        'login' 		=> $lg,
			                        'ip'			=> $_SERVER['REMOTE_ADDR'],
			                        'ultimo_acesso' => new Zend_Db_Expr('Now()'),
			        				'tipo'   		=> self::TIPO_LOGIN
			                       ));
		        }
		        return $result->getCode();
		    }else{
			// exit("asdas");
	        return 1;
		    	
		    }
		} else
		{
			return Zend_Auth_Result::FAILURE;
		}
	}
	
	public function recuperaSenha ($login)
	{
		if ( strpos($login, '@') > 0 || is_int($login) )
		{
			self::carregaClasseUsuario();
				
			$usuario = new self::$classeItem($login);
			return $usuario->novaSenha ();
		} else
		{
			return false;
		}
	}
}