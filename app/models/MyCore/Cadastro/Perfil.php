<?php
require_once CORE.'/Cadastro/Abstract.php';
require_once 'app/models/MyCore/Cadastro/Interface.php';

class Cadastro_Perfil extends Cadastro_Abstract implements Cadastro_Interface
{
	protected $paginacao = true;
	
	protected $_config = array();
	
	protected $porPagina = 10;
	
	protected $linha;
	
	protected $required = array();
	
	protected $notNull = array();
	
	protected $id;
	
	public function ini ()
	{
	
		$this->_config = array('tbPesquisados' => Zend_Registry::get('config')->tb->pesquisados,
				'tbImoveis' => Zend_Registry::get('config')->tb->imoveis,
				'tbCat' => Zend_Registry::get('config')->tb->categorias,
		);		
	}
	
	
	public function lista ($filtro = NULL, $order = NULL ) 
	{
    	$listagem = new Zend_Db_Select($this->db);
    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);
    	
    	$listagem->from(array('A' =>  $this->_config['tbPesquisados']), array(''))
    		->joinLeft(array('I' => $this->_config['tbImoveis']), 'A.id_imovel = I.codigo', array('bairro', 'cidade'));

    	if ( $filtro['tipo'] == 'bairros' )
    	{
    		$listagem->join(array('C' => $this->_config['tbCat']), 'I.categoria = C.id_categoria', 
    				array('categoria AS cat',
    						'(SELECT COUNT(id_imovel) FROM ' . $this->_config['tbPesquisados'] . ' AS A2 LEFT JOIN ' .$this->_config['tbImoveis']. ' AS `I2` ON A2.id_imovel = I2.codigo  WHERE I2.bairro = I.bairro) AS qtd', 
    				)
			);
    		$listagem->distinct(true);
    		$listagem->group('bairro');
    		
    	} elseif ( $filtro['tipo'] == 'categorias' )
    	{
    		
    		$listagem->join(array('C' => $this->_config['tbCat']), 'I.categoria = C.id_categoria',
    				array('categoria AS cat',
    						'(SELECT COUNT(id_imovel) FROM ' . $this->_config['tbPesquisados'] . ' AS A2 LEFT JOIN ' .$this->_config['tbImoveis']. ' AS `I2` ON A2.id_imovel = I2.codigo  WHERE I2.categoria = I.categoria) AS qtd',
    				)
    		);
    		$listagem->distinct(true);
    		$listagem->group('I.categoria');
    		
    		
    	} elseif ( $filtro['tipo'] == 'cidades' )
    	{
    		$listagem->join(array('C' => $this->_config['tbCat']), 'I.categoria = C.id_categoria',
    				array('categoria AS cat',
    						'(SELECT COUNT(id_imovel) FROM ' . $this->_config['tbPesquisados'] . ' AS A2 LEFT JOIN ' .$this->_config['tbImoveis']. ' AS `I2` ON A2.id_imovel = I2.codigo  WHERE I2.cidade = I.cidade) AS qtd',
    				)
    		);
    		$listagem->distinct(true);
    		$listagem->group('I.cidade');
    		
		} elseif ( $filtro['tipo'] == 'categoria_cidade' )
		{
			$listagem->join(array('C' => $this->_config['tbCat']), 'I.categoria = C.id_categoria',
					array('categoria AS cat',
							'(SELECT COUNT(id_imovel) FROM ' . $this->_config['tbPesquisados'] . ' AS A2 LEFT JOIN ' .$this->_config['tbImoveis']. ' AS `I2` ON A2.id_imovel = I2.codigo  WHERE I2.cidade = I.cidade AND I2.categoria = I.categoria) AS qtd',
					)
			);
			$listagem->distinct(true);
			$listagem->group(array('I.categoria', 'I.cidade'));
			
		}
		
		if( $filtro['data_inicio'] &&  $filtro['data_fim'])
		{
			$filtro['data_inicio'] = date('Y-m-d', strtotime(str_replace('/', '-', $filtro['data_inicio'])));
			$filtro['data_fim'] = date('Y-m-d', strtotime(str_replace('/', '-', $filtro['data_fim'])));
			
			$listagem->where("A.criado BETWEEN  '{$filtro['data_inicio']}' AND '{$filtro['data_fim']}'");
			
		}elseif ( $filtro['data_inicio'] )   
		{
			$filtro['data_inicio'] = date('Y-m-d', strtotime(str_replace('/', '-', $filtro['data_inicio'])));
			$listagem->where("A.criado >= ?", $filtro['data_inicio']); 
			
		}elseif ( $filtro['data_fim'] )   
		{
			$filtro['data_fim'] = date('Y-m-d', strtotime(str_replace('/', '-', $filtro['data_fim'])));
			$listagem->where("A.criado <= ?", $filtro['data_fim']);
		}
		 	
    	if ( $this->paginacao === true )
    	{
    		require_once 'Zend/Paginator.php';
    		
			$this->paginator = Zend_Paginator::factory($listagem);
			$this->paginator->setItemCountPerPage($this->porPagina);
			Zend_Paginator::setDefaultScrollingStyle('Sliding');
			Zend_View_Helper_PaginationControl::setDefaultViewPartial('pagina.php');
			$this->paginator->setCurrentPageNumber((int)$_GET['p']);
			$result = $this->paginator;
    	} else
    	{
    		$result = $listagem->query()->fetchAll();
    		//$listagem->query(Zend_Db::FETCH_OBJ)->rowCount();
    	}
    	return $result;
	}
	
	public function paginar ($esp=true) 
	{
		$this->paginacao = (bool) $esp;
	}
	
	public function setId ($id)
	{
		$this->id = $id;
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function getDados($id)
	{
		$pag = $this->paginacao;
		$this->paginar(false);
		$res = current($this->lista(array('id = ?' => $id)));
		$this->paginar($pag);
		
		return $res;
	}
	

	public function novo ($dados) 
	{
		$res = $this->_salva($dados, Model_Data::NOVO);
		
		if ( $res > 0 )
		{
			$this->setId($res);
		}
		
		return $res;
	}
	
	public function edita ($id,$dados) 
	{
	
		$this->setId($id);
		$res = $this->_salva($dados, Model_Data::ATUALIZA,$id);
		
		return $res;
	}
	
	protected function _salva ($dados,$opt,$id=NULL)
	{
		
    	$tabela = new Model_Data(
    						$this->_config['tbPesquisados'],
    						NULL,
    						NULL,
    						$this->required,
    						$this->notNull);
    	
    	$edt = $tabela->edit($id,$dados,NULL,$opt);
		
    	return $edt;
    }
	
	public function apaga ($id) 
	{
		
		$produto = new My_Table($this->_config['tbPesquisados']);
		//$faq = new Cadastro_Faq($this->db);
		return $produto->delete($produto->getAdapter()->quoteInto('id = ?',$id));
	}
}
