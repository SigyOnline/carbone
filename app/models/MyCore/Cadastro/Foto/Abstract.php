<?php
require_once CORE.'/Cadastro/Abstract.php';

abstract class Cadastro_Foto_Abstract extends Cadastro_Abstract
{
	protected $paginacao = false;
	
	protected $_config = array();
	
	protected $porPagina = 20;
	
	protected $linha;
	
	protected $required = array('legenda','foto');
	
	protected $notNull = array('foto');
	
	/**
	 * 
	 * Tabela
	 * @var My_Table
	 */
	protected $tabela;
	
	public function ini ()
	{
		$this->_config['tbFoto'] = Zend_Registry::get('config')->tb->foto;
		$this->tabela = new My_Table($this->_config['tbFoto']);		
	}
	
	public function inserir ($files)
	{
		if ( sizeof($files) )
		{
			if ( $files['size'] < 20 )
				return;
				
				
			$foto = new Model_Data($this->tabela, NULL,NULL,$this->required, $this->notNull);	
			$file = new App_File_Upload ($files,
										 $this->path,
										 array('path' => $this->path,
											   'type' => 'image',
												'extensions' => array('jpg','bmp','gif','png'),
												'root' => '' ));

			if ( !$file->_isError() )
			{
				$data = array('legenda' => '',
							  'foto' => $file->_where());
			} else 
			{
				$this->e .= $file->_messages()."\r";
				return false;
			}
								 
			$res = $foto->edit(NULL,$data,NULL,Model_Data::NOVO);
			
			return $res;
		} else
		{
			return Model_Data::ERRO_GERAL;
		}
	}
	
	public function apaga ($id) 
	{
		$where = $this->tabela->getAdapter()->quoteInto(
										'id_foto = ?',$id
										);
		$res = $this->tabela->fetchRow($where);
									
		if ( $res )
		{
			@unlink($res->foto);
			$this->tabela->delete($where);
		}
		
		return $res;
	}
	
	abstract public function listaFoto (array $where=NULL);
}