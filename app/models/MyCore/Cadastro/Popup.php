<?php
require_once CORE.'/Cadastro/Abstract.php';
require_once 'app/models/MyCore/Cadastro/Interface.php';

class Cadastro_Popup extends Cadastro_Abstract implements Cadastro_Interface
{
	protected $paginacao = false;
	
	protected $_config = array();
	
	protected $porPagina = 10;
	
	protected $linha;
	
	protected $required = array();/*array('id_banner', 'titulo', 'descricao');*/
	
	protected $notNull = array('nome','texto');
	
	protected $id;
	
	public function ini ()
	{
		$this->_config = array(
				'tbPopup' => Zend_Registry::get('config')->tb->popup,
		);
	}
	
	public function lista ($filtro = NULL, $order = NULL ) 
	{
    	$listagem = new Zend_Db_Select($this->db);
    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);
    	
    	$listagem->from($this->_config['tbPopup']);
    	if ( $filtro )
    		$listagem->where(key($filtro),current($filtro));
		
    	if ( $this->paginacao === true )
    	{
    		require_once 'Zend/Paginator.php';
    		
			$this->paginator = Zend_Paginator::factory($listagem);
			$this->paginator->setItemCountPerPage($this->porPagina);
			Zend_Paginator::setDefaultScrollingStyle('Sliding');
			Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginacao.php');
			$this->paginator->setCurrentPageNumber((int)$_GET['p']);
			$result = $this->paginator;
    	} else
    	{
    		
    		$result = $listagem->query()->fetchAll();
    		//$listagem->query(Zend_Db::FETCH_OBJ)->rowCount();

    	}
    	return $result;
	}
	
	public function paginar ($esp=true) 
	{
		$this->paginacao = (bool) $esp;
	}
	
	public function setId ($id)
	{
		$this->id = $id;
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function getDados($id)
	{
		$pag = $this->paginacao;
		$this->paginar(false);
		$res = current($this->lista(array('id_popup = ?' => $id)));
		$this->paginar($pag);
		
		return $res;
	}
	

	public function novo ($dados) 
	{
		$res = $this->_salva($dados, Model_Data::NOVO);
		
		if ( $res > 0 )
		{
			$this->setId($res);
		}
		
		return $res;
	}
	
	public function edita ($id,$dados) 
	{
	
		$this->setId($id);
		$res = $this->_salva($dados, Model_Data::ATUALIZA,$id);
		
		return $res;
	}
	
	protected function _salva ($dados,$opt,$id=NULL)
	{
    	$tabela = new Model_Data(
    						$this->_config['tbPopup'],
    						NULL,
    						NULL,
    						$this->required,
    						$this->notNull);
    							
    	$tabela->load_options('imagem',array('path' => 'imagens/popup',
    										 'type' => 'image',
    	// 									 'size' => 800,
    										 'root' => '' ));
    	
    	$edt = $tabela->edit($id,$dados,NULL,$opt);
		
    	return $edt;
    }
	
	public function apaga ($id) 
	{
		$r = $this->db->getConnection()->query('DELETE FROM sg_popup WHERE id_popup = '.$id);
		return $r; 
	}
	
	public function GetDepoimento($id)
	{
		$listagem = new Zend_Db_Select($this->db);
		$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);
	
		$listagem->from($this->_config['tbdepo']);
		$listagem->where('id_depoimento = ?', $id);
	
		$result = $listagem->query()->fetchObject();
			
		return $result;
	}
}
