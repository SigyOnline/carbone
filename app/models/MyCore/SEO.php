<?php
	require_once MYCORE. '/Cadastro/Imovel.php';

class SEO {
	
	protected $url; 
	
	const EXT = '.html';
	
	//const SEP = 'http://192.168.0.200:8081';
	//const SEP = 'http://www.sigyonline.com.br/Desenvolvimento';
	
	public function __construct()
	{
		$this->url =  Zend_Registry::get('config')->www->baseurl;
		$this->obj = new Cadastro_Imovel(Zend_Registry::get('db'));
		
	}
	
	public function slug($texto)
	{
			$acentos = array('à','á','â','ã','ä','å','ç','è','é','ê','ë','ì','í','î','ï','ñ','ò','ó','ô','õ','ö','ù','ü','ú','ÿ','À','Á','Â','Ã','Ä','Å','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ñ','Ò','Ó','Ô','Õ','Ö','O','Ù','Ü','Ú','Ÿ', ']','[','>','<','}','{',')','(',':',';',',','!','?', '*', '%', '~', '^', '`','&','#','@',' ',"'",);
			$trocar = array('a','a','a','a','a','a','c','e','e','e','e','i','i','i','i','n','o','o','o','o','o','u','u','u','y','A','A','A','A','A','A','C','E','E','E','E','I','I','I','I','N','O','O','O','O','O','O','U','U','U','Y','','','','','','','','','','','','','','','','','','','','','','-','', );
			$slug = str_replace($acentos, $trocar, $texto);
			
			$slug = substr(strtolower($slug), 0, 50) . '/';
			
		return $slug;
	}
	
	public function getLink($imovel)
	{
		
		$url = $this->obj->GetSeo((int)$imovel['codigo']);
		
		if (is_array($imovel))
		{
			if ($url[0]->url == '')
			{	
				$categoria = (empty($imovel['cat']))?$imovel['categoria']:$imovel['cat'];
			    $slug = $this->slug($categoria);
			    $slug .= $this->slug($imovel['bairro']);
			    $r = $this->url . $slug . $imovel['codigo'] . self::EXT;
			}else{
				$r = $url[0]->url.'/'.$imovel['codigo']. self::EXT;
			}     
		}else{
			if ($url[0]->url == '')
			{
				$categoria = (empty($imovel['cat']))?$imovel['categoria']:$imovel['cat'];
			    $slug = $this->slug($categoria);
			    $slug .= $this->slug($imovel->bairro);
			    $r = $this->url . $slug . $imovel->codigo . self::EXT;
			}else{   
				$r = $url[0]->url.'/'.$imovel->codigo . self::EXT;
			}
		} 
		
	   return $r;
	}
	
	public function seoMetaTags($imovel)
	{
		
		/*
		 * TITLE
		*/
		if( !empty($imovel->title) )
		{
			echo PHP_EOL.'<title>'. $imovel->title . ' | Detalhes do Imóvel | ' . Zend_Registry::get('config')->www->pagetitle .'</title>' . PHP_EOL;				
		}
// 		elseif( !empty($imovel->empreendimeto))
// 		{
// 			echo PHP_EOL.'<title>' . $imovel->empreendimento .' | Detalhes do Imóvel</title>.'.PHP_EOL;
// 		}
		elseif( !empty($imovel))
		{
			echo PHP_EOL.'<title>' . Zend_Registry::get('config')->www->pagetitle .' | Detalhes do Imóvel</title>'.PHP_EOL;
		}
		else
		{
			echo PHP_EOL.'<title>Imóvel não encontrado</title>'.PHP_EOL;
		}
		
		/* 
		 * META DESCRIPTION
		 */
		if( !empty($imovel->seo_description))
		{
			echo '<meta name="description" content="' . $imovel->description . '" />'. PHP_EOL;
		}
		else
		{
			echo '<meta name="description" content="' . substr( strip_tags($imovel->descricao), 0, (strlen( strip_tags($imovel->descricao)) > 140?strpos( strip_tags($imovel->descricao), ' ', 140):strlen( strip_tags($imovel->descricao)))) . '" />'. PHP_EOL;
		}
		
		/*
		 * KEYWORDS
		 */
		if( !empty($imovel->keywords))
		{
			echo '<meta name="keywords" content="' . $imovel->keywords . '" />'. PHP_EOL;
		}
		else
		{
			//echo '<meta name="keywords" content="" />'. PHP_EOL;
		}
	}
	
}