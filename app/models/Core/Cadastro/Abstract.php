<?php
require_once 'app/models/Model_Data.php';
require_once 'app/models/My_Table.php';

/**
 * Descrição..
 *
 * @category   Core
 * @package    Cadastro
 * @copyright  Copyright (c) 2010-2011 Nova Era Design (http://www.novaeradesign.com.br)
 */

abstract class Cadastro_Abstract
{
	const LOGIN_INATIVO = -3;
	
	const LOGIN_INVALIDO = 0;
	
	const LOGADO = 1;
	
	const TIPO_TEL_RES = 1;
	
	const TIPO_TEL_CEL = 3;
	
	const TIPO_TEL_COM = 2;
	
	const TIPO_EMAIL_DEFAULT = 'email';
	
	const TIPO_EMAIL_MSN = 'msn';
	
	/**
	 * Conexão com o BD
	 * @var Zend_Db_Adapter_Abstract
	 */	
	protected $db;
	
	public function __construct(Zend_Db_Adapter_Abstract $db)
	{
		$this->db = $db;
		$this->ini();
	}
	
	public function ini ()
	{}
	
// 	abstract static public function novo ($dados);
	
	//abstract public function login ($login,$senha, $opcoes=NULL);
}