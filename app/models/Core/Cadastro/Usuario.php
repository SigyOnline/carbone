<?php
require_once 'app/models/Error/Error.php';
require_once 'app/models/Error/Success.php';
require_once CORE. '/Cadastro/Abstract.php';

class Cadastro_Usuario extends Cadastro_Abstract
{
	const NAME_SPACE_LOGIN = 'Login';
	
	const TIPO_LOGIN = 2;
	
	const ADMINS = 1;
	
	/**
	 * Tabela principal do cadastro 
	 * @var string
	 */
	protected $tabela;
	
	/**
	 * Classe do Item
	 * @param Cadastro_Item_Abstract $classeItem
	 */
	static public $classeItem = 'Cadastro_Item_Usuario';
	
	public function __construct(Zend_Db_Adapter_Abstract $db)
	{
		if ( Zend_Registry::isRegistered('config') )
		{
			if ( Zend_Registry::get('config')->tb->login )
				$this->tabela = Zend_Registry::get('config')->tb->login;
			else
				throw new Exception('Tabela inválida');
		} else 
		{
			throw new Exception('Config inválido');
		}
		
		parent::__construct($db);
	}
	
	/**
	 * 
	 * Criar um novo usuário
	 * @param array $dados
	 * @throws Exception
	 * @return Cadastro_Item_Usuario
	 */
	static public function novo ($dados)
	{
		self::carregaClasseUsuario();

		$class = new self::$classeItem;
		
		if ( $class instanceof Cadastro_Item_Abstract )
		{
			$res = $class->novo($dados);
			if ( $res > 0 )
				return $class;
			else
				return $res;
		} else 
		{
			throw new Exception('A classe do item não é válida');
		}
	}
	
	static public function carregaClasseUsuario() 
	{
		if ( !class_exists(self::$classeItem) )
			return require_once CORE.'/'. str_replace('_', '/', self::$classeItem) .'.php';
	}
	
	public function apaga ($id)
	{
		self::carregaClasseUsuario();
		
		$tabela = new My_Table($this->tabela);
		return $tabela->delete($tabela->getAdapter()->quoteInto($tabela->info(My_Table::PRIMARY).' = ?', $id));
	}
	
	public function login ($login=NULL,$senha=NULL, $opcoes=NULL) 
	{
		Zend_Loader::autoload('Zend_Auth_Adapter_DbTable');
		Zend_Loader::autoload('Zend_Auth_Storage_Session');
		Zend_Loader::autoload('Zend_Auth_Result');
		Zend_Loader::autoload('Zend_Filter_StripTags');
		
		if ( !class_exists(self::$classeItem) )
			require_once CORE.'/'. str_replace('_', '/', self::$classeItem) .'.php';
			
		if ( $opcoes['token'] )
		{
			//unset($_SESSION['Login'],$_SESSION['CORE_CART']);
			try {
				$authAd = new Zend_Auth_Adapter_DbTable($this->db,$this->tabela,'login','token');
			} catch (Exception $e)
			{
				return Zend_Auth_Result::FAILURE_UNCATEGORIZED;
			}
			$filter = new Zend_Filter_Alnum();
			$filter2 = new Zend_Filter_StripTags();
			
			$token = $filter->filter($opcoes['token']);
			$login = $filter2->filter($login);
			
			if ( !empty($login) && !empty($token) )
			{
			    $authAd->setIdentity($login)
			           ->setCredential($token);
				$ok = true;
			}
		} else
		{
			try {
				$authAd = new Zend_Auth_Adapter_DbTable($this->db,$this->tabela,'login','senha');
			} catch (Exception $e)
			{
				return Zend_Auth_Result::FAILURE_UNCATEGORIZED;
			}
	
			$filter = new Zend_Filter_StripTags();
			    
			$lg = $filter->filter($login);
			$pw = $filter->filter($senha);
			if ( !empty($lg) && !empty($pw) )
			{

			    $authAd->setIdentity($lg)
			           ->setCredential(Cadastro_Item_Usuario::cripto($pw));
			     $ok = true;
			}
		}

		if ( $ok )
		{
		    $result = $authAd->authenticate();
		    
		    if ( $result->isValid() ) 
		    {
		    	$res = $authAd->getResultRowObject();
		    	if ( $res->id_status == 3 )
		    	{
		    		$auth = Zend_Auth::getInstance();
		    		
			        $auth->setStorage(
			                        new Zend_Auth_Storage_Session(self::NAME_SPACE_LOGIN)
			                    );
			                    
			       // print_r($res);
			       // exit('aki');
			        			
					//$res->permissoes = $this->_getPermissao($res->id_usuario);

			        // store the identity as an object where the password column has been omitted
					$auth->getStorage()->write($res);
			        
					
			        if ( $token )
			        {
			        	$tabela = new Cadastro_Item_Usuario($login);
			        	if ( !$tabela->edita(array('token' => NULL)) )
			        	{
			        		return self::LOGIN_INATIVO;
			        	}
			        }  else 
			        {
			        	$acessos = new Zend_Db_Table(Zend_Registry::get('config')->tb->acessos_login);
				        $acessos->insert(array(
				                        'login' 		=> $lg,
				                        'ip'			=> $_SERVER['REMOTE_ADDR'],
				        				//'id_login'    => $res->id_login
				                       ));
			        }
			        return $result->getCode();
		    	} else
		    	{
		    		return Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID;
		    	}
		    }
		} else
		{
			return Zend_Auth_Result::FAILURE;
		}
	}
	
	public function getAuth()
	{
		$auth = new Zend_Auth_Storage_Session(self::NAME_SPACE_LOGIN);
		
		if ( !$auth->isEmpty() )
			return $auth->read();
		else
			return;
	}
	
	public function sair ()
	{
    	$auth = new Zend_Auth_Storage_Session(Cadastro_Usuario::NAME_SPACE_LOGIN);
    	$auth->clear();
    	unset(Zend_Registry::get('config')->session);
	}
	
	protected function _getPermissao($id) 
	{
		$tabela = new Zend_Db_Select(Zend_Registry::get('db'));
		$permissao = new Zend_Acl();
		
		$tabela->from(array('U' => $this->tabela),array('id_grupo'))
			   ->joinLeft(array('FLG' => Zend_Registry::get('config')->tb->fl_grupo_permissao), 
			   			 'U.id_grupo = FLG.id_grupo', array())
			   ->joinLeft(array('P' => Zend_Registry::get('config')->tb->permissoes), 
			   			  'FLG.id_permissoes = P.id_permissoes',
			   			  array('id_permissoes','permissoes'))
			   ->where('U.id_usuario = ?',$id);
			   
		$result = $tabela->query(Zend_Db::FETCH_OBJ)->fetchAll();

		if ( $result )
		{
			foreach ($result as $item) 
			{
				if ( !$permissao->hasRole($item->id_grupo) === true )
					$permissao->addRole(new Zend_Acl_Role($item->id_grupo));
					
				if ( !$permissao->has($item->id_permissoes) === true && NULL !== $item->id_permissoes )
					$permissao->addResource(new Zend_Acl_Resource($item->id_permissoes));
				
				if ( $item->id_grupo === self::ADMINS )
					$permissao->allow($item->id_grupo);
				else
					$permissao->allow($item->id_grupo,$item->id_permissoes);
			}
		}
		
		return $permissao;
	}
	
	public function recuperaSenha ($login)
	{
		if ( strpos($login, '@') > 0 || is_int($login) )
		{
			self::carregaClasseUsuario();
				
			$usuario = new self::$classeItem($login);
			return $usuario->novaSenha ();
		} else
		{
			return false;
		}
	}
}