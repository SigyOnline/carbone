<?php
require_once CORE. '/Cart/Storage/Abstract.php';

Class Cart_Storage_Session extends Cart_Storage_Abstract
{
	public function init()
	{
		if ( !Zend_Session::sessionExists() )
			Zend_Session::start();
			
		if ( is_array($_SESSION['CORE_CART']) )
			$this->restoreInstanceItems($_SESSION['CORE_CART']);
			
		$_SESSION['CORE_CART']['instance'] = $this; 
	}
	
	static public function getInstance ($space=NULL)
	{
		if ( NULL === self::$instance )
		{
			//$session = Zend_Registry::get('session');
			
			if ( !self::$instance )
			{
				self::$instance = new self($space);
				
				//if ( $session->CORE_CART )
				//	self::$instance->restoreInstanceItems($session->CORE_CART);
			}
		}
		
		return self::$instance;
	}
	
	public function getPaymentGateways() 
	{
	}
	
	public function processCheckout($paymentId)
	{
	}
	
	public function restoreInstanceItems (array $items)
	{
		if ( $items['instance'] )
		{
			$this->addItemsSpace($is);
		} else 
		{
			foreach ( $items as $k => $v ) 
				$this->{$k} = $v;
		}
	}
	
	public function getInstanceItems ()
	{
		$rfl = new ReflectionClass($this);
		$pp = $rfl->getDefaultProperties();
		
		return $pp;
	}
	
	public function clean($itemSpace)
	{
		parent::clean($itemSpace);
		unset($_SESSION['CORE_CART']);
	}
	
	public function __destruct()
	{
		//$_SESSION['CORE_CART'] = $this->getInstanceItems();
	}
}