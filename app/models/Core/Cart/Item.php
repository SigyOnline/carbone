<?php

require_once CORE. '/Cart/Interface/Item.php';


abstract class Cart_Item  implements Cart_Interface_Item
{
	/**
	 * Item adicionado
	 *
	 * @var array
	 */
	protected $item = array();
	
	/**
	 * Produto adicionado
	 *
	 * @var Produto_Abstract
	 */
	protected $product;
	
	/**
	 * ID do item, pode ser o ID do banco de dados
	 *
	 * @var string|int
	 */
	protected $id;
	
	/**
	 * quantidade do item
	 *
	 * @var numeric
	 */
	protected $qty;
	
	/**
	 * valor unitário do item
	 *
	 * @var float
	 */
	protected $price;
	
	/**
	 * Construtor
	 *
	 * @param id|string $id
	 * @param int $qty
	 * @param array $item
	 */
	public function __construct($id,$price,$qty=1,array $item=NULL) 
	{
		$this->id = $id;
		$this->setPrice($price);
		$this->addQty($qty);
		
		if ( NULL !== $item ) 
			$this->setItem($item);
	}

	/**
	 * adiciona quantidade
	 *
	 * @param numeric $qty
	 * @return Cart_Item object $this Cart_Item
	 */
	public function addQty ($qty=NULL) 
	{
		if ( is_numeric($qty) )
		{
			 $this->qty = $qty;
		} else
		{
			$this->qty++;
		}
		
		return $this;
	}
	
	/**
	 * retorna a quantidade do item
	 *
	 * @return numeric
	 */
	public function getQty () 
	{
		return $this->qty;
	}
	
	/**
	 * define o valor do item
	 *
	 * @param float $price
	 * @return Cart_Item object $this Cart_Item
	 */
	public function setPrice ($price)
	{
		$this->price = (float) $price;
		return $this;
	}
	
	/**
	 * pega o valor do item
	 *
	 * @return float
	 */
	public function getPrice () 
	{
		return (float) $this->price;
	}
	
	/**
	 * retorna o Array() do item
	 *
	 * @return array;
	 */
	public function getItem ()
	{
		return (array) $this->item;
	}
	
	/**
	 * define o conteúdo do item
	 *
	 * @param mixed $item
	 * @return Cart_Item object $this Cart_Item
	 */
	public function setItem ($item) 
	{
		if ( $item instanceof Cart_Interface_Item )
			$i = $item->getItem();
		else 
			$i = (array) $item;
		$this->item = array_merge($this->item,$i);
		
		return $this;
	}
		
	abstract public function checkout ($cartId,$paymentId);
	
	abstract public function refreshPrice ();
	
	/**
	 *
	 * @see Cart_Interface_Item::total()
	 */
	public function total() 
	{
		$this->refreshPrice();
		return (float) $this->getPrice() * $this->getQty();

	}
	
}