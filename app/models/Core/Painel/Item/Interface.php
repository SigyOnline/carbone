<?php
interface Painel_Item_Interface
{
	public function __construct (Cadastro_Item_Abstract $usuario,$flag);
	
	public function render();
	
	public function edit();
}