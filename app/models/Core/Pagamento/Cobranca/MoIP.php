<?php
require_once CORE.'/Pagamento/Cobranca/Abstract.php';

class Pagamento_Cobranca_MoIP extends Pagamento_Cobranca_Abstract
{
	const TIPO = 6;
	
	const STATUS_PAGO = 7;
	
	const STATUS_NAOPAGO = 4;
	
	const STATUS_CANCELADO = 6;	
	
	protected $_modificados = array();
	
	protected $_required = array('transacao','pago','valor','valor_bol','linha_1',
								 'linha_2','linha_3','data_emissao','data_venc',
								 'data_pagto','data_disp','id_formapagto','id_status');
	
	protected $_notNull = array('id_status','id_formapagto','transacao',
								'data_venc','valor');
	
	protected $_tabela;	
	
	public $tipo_titulo = 'MoIP';
	
	public function getTabela ()
	{
		if ( NULL === $this->_tabela )
		{
			$tabela = Zend_Registry::get('config')->tb->cobranca;
			$this->_tabela = new My_Table($tabela);
		}
		return $this->_tabela;
	}	
	
	public function insere ($dados=NULL)
	{
		if ( !$dados['id_status'] )
			$dados['id_status'] = self::STATUS_NAOPAGO;

		if ( $id = parent::insere($dados) )
		{ 
			$this->transacao = $this->unico_id();
			if ( !parent::atualiza() )
				throw new Exception('Erro ao definir o nosso número');
		}
		return $id;
	}

	protected function unico_id ()
	{
		return $this->_dados['transacao'] = '';
	}
	
	public function preparaDados ($dados)
	{
		$valores = array ('valor');
		
		foreach ( $valores as $i )
		{
			if ( $dados[$i] )
			{
				$dados[$i] = preg_replace('/[^0-9,.]/', '', (string) $dados[$i]);
				$dados[$i] = (float) str_replace(',','.',$dados[$i]);
			}
		}
		$dados['id_formapagto'] = self::TIPO;
		
		if ( !$dados['data_emissao'] )
			$dados['data_emissao'] = date('Y-m-d');
			
		if ( !$dados['data_venc'] && !$this->_dados['data_venc'] )
		{
			switch (date('D')) 
			{
				case 'Fri':
					$data_v 	= date('Y-m-d',strtotime('+4 day'));
				break;
				
				case 'Sat':
					$data_v 	= date('Y-m-d',strtotime('+3 day'));
				break;
				
				default:
					$data_v 	= date('Y-m-d',strtotime('+2 day'));
				break;
			}
			
			$dados['data_venc'] = $data_v;
		} elseif ( $dados['data_venc'] )
		{
			$dados['data_venc'] = date('Y-m-d',strtotime($dados['data_venc']));
		} 
		
		if ( $dados['data_pagto'] )
			$dados['data_pagto'] = date('Y-m-d',strtotime($dados['data_pagto']));
		
		if ( $dados['data_disp'] )
			$dados['data_disp'] = date('Y-m-d',strtotime($dados['data_disp']));
		
		return $dados;
	}	
	
	public function pago ()
	{
		if ( $this->id_status == self::STATUS_PAGO )
			return;
					
		$modif = array(
					'id_status' => self::STATUS_PAGO,
					'data_pagto' => $this->data_vencimento,
					//'valor_pago' => $this->valor
				 );
		
		if ( $this->atualiza($modif) )
		{
			$this->getFatura()->baixa(Pagamento_Fatura::BAIXA_TIPO_NAOFORCA);
			
			return $resultado;
		} else
		{
			return (new Error('Não foi possível dar baixa no pagamento: '.$this->getId()));
		}
	}
	
	public function cancela ()
	{
		if ( $this->id_status == self::STATUS_CANCELADO )
			return;
					
		$modif = array(
					'id_status' => self::STATUS_CANCELADO
				 );
		
		if ( $this->atualiza($modif) )
		{
			return $resultado;
		} else
		{
			return (new Error('Não foi possível cancelar o pagamento: '.$this->getId()));
		}
	}
	
	public function naopago ()
	{
		if ( $this->id_status == self::STATUS_NAOPAGO )
			return;
					
		$modif = array(
					'id_status' => self::STATUS_NAOPAGO,
					//'valor_pago' => NULL
				 );
		
		if ( $this->atualiza($modif) )
		{
			return $resultado;
		} else
		{
			return (new Error('Não foi possível editar pagamento: '.$this->getId()));
		}
	}
	
	public function pendente ()
	{
		return (bool) ($this->id_status == self::STATUS_NAOPAGO);
	}
	
	public function getIdFatura ()
	{
		return  $this->getFatura()->getId();
		//return $dados['id_cliente'];
	}	
	
	public function render ()
	{
		$dadosboleto = $this->getDados();
		$dadosFatura = $this->getFatura()->getDados();
		$dados = array();
		$teste = false;

		if ( $teste == true )
		{
			$dados['action']      = 'https://www.moip.com.br/PagamentoMoIP.do';
			$dados['id_carteira'] = 'fabio@novaeradesign.com.br';
		} else
		{
			$dados['action']      = 'https://www.moip.com.br/PagamentoMoIP.do';
			$dados['id_carteira'] = 'financeiro@entelco.com.br';
		}
		$dados['url_retorno'] 			= 'http://www.entelco.com.br/default/painel';
		$dados['id_transacao']   		= $this->getId();
		$dados['descricao']   			= 'Pedido feito no site Entelco.com.br';
		$dados['razao']   				= 'Pagamento do pedido: '. $dadosFatura['id_pedido'];
		$dados['nome']					= $dados['razao'];
		$dados['valor']					= number_format($dadosboleto['valor'],2,'','');
		$dados['id_transacao']   		= $this->getId();
		$dados['pagador_nome']   		= $dadosFatura['nome'];
		$dados['pagador_cep']    		= $dadosFatura['cep'];
		$dados['pagador_logradouro']    = $dadosFatura['logradouro'];
		$dados['pagador_numero']	 	= $dadosFatura['numero'];
		$dados['pagador_complemento']	= $dadosFatura['compl'];
		$dados['pagador_bairro'] 		= $dadosFatura['bairro'];
		$dados['pagador_cidade'] 		= $dadosFatura['cidade'];
		$dados['pagador_estado'] 	 	= $dadosFatura['uf'];
		$dados['pagador_pais'] 	 		= 'Brasil';
		$dados['pagador_telefone'] 	 	= $dadosFatura['ddd'] . $dadosFatura['numero'];
		$dados['pagador_email']  		= $dadosFatura['email'];
		
		require_once CORE_HELPER_DEFAULT.'/Pagamento/Cobranca/MoIP.php';
	}
}