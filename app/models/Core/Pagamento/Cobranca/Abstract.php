<?php
require_once CORE.'/Pagamento/Fatura.php'; 

abstract class Pagamento_Cobranca_Abstract
{
	/**
	 * O Id da Cobrança
	 * 
	 * @var int
	 */
	protected $_id;
	
	/**
	 * A fatura a que pertence a cobrança
	 * 
	 * @var Pagamento_Fatura $_fatura
	 */
	protected $_fatura;
	
	/**
	 * Dados da cobrança instanciada
	 * 
	 * @var array
	 */
	protected $_dados; 
	
	
	public function __construct ($fatura=NULL,$id=NULL,array $dados=NULL)
	{
		$this->setId ($id);
		
		if ( NULL !== $dados )
			$this->setDados ($dados);
			
		if ( !$fatura instanceof Pagamento_Fatura )
		{
			if ( $fatura > 0 )
			{
				$fatura = new Pagamento_Fatura($fatura);
			} elseif ( NULL === $fatura && $id > 0 )
			{
				$id_fatura = $this->getIdFatura();
				
				if ( $id_fatura )
					$fatura = new Pagamento_Fatura($id_fatura);
				else
					throw new Exception ('Fatura não existe');
			}
		}

		$this->setFatura ($fatura);
	}
	
	/**
	 * Pega a fatura
	 * 
	 * @return Pagamento_Fatura
	 */
	public function getFatura ()
	{
		return $this->_fatura;
	}
	
	/**
	 * Seta a fatura
	 * 
	 * @return Pagamento_Fatura
	 */
	public function setFatura (Pagamento_Fatura $fatura)
	{
		$this->_fatura = $fatura;
	}
	
	public function setDados (array $dados)
	{
		if ( !is_array($this->_dados) )
			$this->_dados = array();

		$this->_dados = array_merge($this->_dados,$dados);
	}
	
	public function getDados()
	{
		if ( sizeof($this->_dados) == 0 )
		{
			/*$dados = $this->getTabela()->fetchRow($this->getTabela()
													->getAdapter()
													->quoteInto('id_cobranca = ?',$this->getId())
													);*/
													
			$select = $this->getTabela()->getAdapter()->select();
			$tabela = $this->getTabela()->info(Zend_Db_Table_Abstract::NAME);
			$select->from(array('T' => $tabela))
				   ->joinLeft(array('S' => Zend_Registry::get('config')->tb->status), 
				   					'T.id_status = S.id_status',
				   					array('status'))
				   ->where('T.id_cobranca = ?',$this->getId());

			$dados = $select->query(Zend_Db::FETCH_OBJ)->fetchObject();

			if ( $dados )
				$this->setDados((array)$dados);
			else
				throw new Exception ('pagamento inexistente');
		}
		
		return (array) $this->_dados;
	}
	
	abstract public function getIdFatura ();

	public function insere ($dados=NULL)
	{
		if ( $this->getId() )
			return false;
			
		$id = $this->_salva($dados,Model_Data::NOVO);
		
		if ( $id )
			$this->setId($id);
		
		return $id;
	}
	
	public function atualiza ($dados=NULL)
	{		
		return $this->_salva($dados,Model_Data::ATUALIZA);
	}
	
	protected function _salva (array $dados=NULL,$opcao)
	{
		if ( is_array($dados) )
		{
			while ( list($k,$v) = each($dados) )
			{
				if ( in_array($k,$this->_required) )
					$this->_modificados[$k] = $v;
			}
		}
		
		if ( !sizeof($this->_modificados) )
			return;
			
		$modelData = new Model_Data($this->getTabela());
		$modelData->_required($this->_required);
		$modelData->_notNull($this->_notNull);
		
		$this->_modificados = $this->preparaDados($this->_modificados);
		
		$retorno = $modelData->edit($this->getId(),$this->_modificados,NULL,$opcao);
		
		if ( $retorno )
		{
			$this->setDados($this->_modificados);
			$this->_modificados = NULL;
		}
		
		return $retorno;
	}
	
	abstract public function preparaDados($dados);
	
	abstract public function cancela ();
	
	abstract public function pago ();
	
	abstract public function naopago ();
	
	abstract public function pendente();
	
	public function setId ($id)
	{
		$this->_id = $id;
	}
	
	public function getId ()
	{
		return $this->_id;
	}
	
	/**
	 * 
	 * @return string
	 */
	abstract public function render ();
	
	
	public function total ()
	{
		return (float) $this->valor;
	}
	
	public function __get($coluna)
	{
		$this->getDados();
		return $this->_dados[$coluna];
	}
	
    public function __set($coluna, $valor)
    {
        $coluna = (string) $coluna;
        if ( !in_array($coluna, $this->_required) )
            throw new Exception("Specified column \"$coluna\" is not in the table");
        
        $this->_modificados[$coluna] = $valor;
    }
    
    public function getStatus() 
    {
    	if ( !$this->status )
    		$this->_dados = NULL;
    		
    	return $this->status;
    }
}
