<?php
require_once 'app/models/Model_Data.php';
require_once 'app/models/My_Table.php';
require_once CORE.'/Pagamento/Cobranca/PagSeguro.php';
require_once CORE.'/Pagamento/Cobranca/Boleto/Itau.php';
require_once CORE.'/Pagamento/Cobranca/Boleto/Empenho.php';
require_once CORE.'/Pagamento/Cobranca/MoIP.php';

class Pagamento_Fatura
{
	const COBRANCA_DEFAULT = 1;
	
	const BAIXA_TIPO_FORCA = 'forca';
	
	const BAIXA_TIPO_NAOFORCA = 'naoforca';
	
	const PAGO = 16;
	
	const NAOPAGO = 14;	
	
	protected $_id;
	
	/**
	 * Zend_Registry - Config
	 * @var Zend_Registry $_config
	 */
	protected $_config;
	
	/**
	 * Dados da Fatura
	 * @var array
	 */
	protected $_dados;
	
	protected $_pedido;
	
	protected $_preBaixa;
	
	protected $_posBaixa;
	
	protected $_cobranca = NULL;
	
	protected static $_tabela;
	
	protected static $_mensagens;
	
	protected static $_Model_Data_required = array('nome','id_pedido','id_cadastro','nascimento','cpf','rg',
												   'observacao','comentario','skype','id_cliente_tipo','id_tipo_cobranca');
	
	protected static $_Model_Data_NotNull = array('nome','id_cadastro','nascimento','cpf','rg','id_tipo_cobranca');
	
	
	public function __construct ($id,array $config=NULL)
	{
		$this->setTabela($this->setupTabela());
		
		$this->_config = Zend_Registry::get('config'); 
		$select = $this->getTabela()->getAdapter()->select();
		
		$select->from(array('T1' => Zend_Registry::get('config')->tb->cliente))
			     ->joinLeft(array('E' => $this->_config->tb->email),
				  			'T1.id_cliente = E.id_cliente',
				  			array('email'))
			     ->joinLeft(array('EN' => $this->_config->tb->endereco),
				  			'T1.id_cliente = EN.id_cliente',
				  			array('logradouro','numero AS num','compl','bairro','cep','cidade AS city','uf AS estado'))
			     ->joinLeft(array('TE' => $this->_config->tb->telefone),
				  			'T1.id_cliente = TE.id_cliente',
				  			array('ddd','numero'))
			   ->where('T1.id_cliente = ?',$id)
			   ->limit(1);

		$select = $select->query(Zend_Db::FETCH_OBJ);
		$row = (array) $select->fetchObject();
			   
		if ( $row['id_cliente'] > 0 )
		{
			while ( list($k,$v) = each($row) )
				$this->_dados[$k] = $v;
				
			$this->_id = $this->_dados['id_cliente'];
		} else 
		{
			throw new Exception ('Fatura não existe');
		}
	}
	
	public static function nova ($post)
	{
		if ( self::setupTabela() instanceof Error )
			return self::$_mensagens;
		
    	$modelData = new Model_Data(self::$_tabela);
	   	$modelData->_required(self::$_Model_Data_required);
	   	$modelData->_notNull(self::$_Model_Data_NotNull);
	   		
	   	//$post['id_status'] 	  = 28;
	   	
	   	$edt = $modelData->edit(NULL,$post,NULL,Model_Data::NOVO);
	   	
	   	if ( $edt )
	   		return (new self($edt));
	   	else
	   		return (new Error('Erro ao gerar dados de cobrança. Tente novamente em alguns instantes'));
	}
	
	public function edita (array $post)
	{
    	$modelData = new Model_Data($this->getTabela());
	   	$modelData->_required(self::$_Model_Data_required);
	   	//$modelData->_notNull(self::$_Model_Data_NotNull);
	   	
	   	$edt = $modelData->edit($this->getId(),$post,NULL,Model_Data::ATUALIZA);
	   	
	   	if ( $edt )
	   		return $edt;
	   	else
	   		return (new Error('Erro ao editar fatura'));
	}
	
	public function del ()
	{
		//@todo Não implemetado ainda.
	}
	
	/**
	 * 
	 * @param mixed $dados
	 * @param int $tipo
	 * @return Error_Abstract
	 */
	public function novaCobranca ($dados,$tipo=NULL)
	{
		if ( $dados instanceof Pagamento_Cobranca_Abstract )
		{
			if ( $dados->getId() )
			{
			/*	if ( $dados instanceof Pagamento_Cobranca_NotaDebito )
					$tipo = Pagamento_Cobranca_NotaDebito::TIPO;
				else
					$tipo = self::COBRANCA_DEFAULT;*/
					
				$this->_dados[$tipo][$dados->getId()] = $dados;
				
				return (new Success('Cobranca inserida!'));
			} else
			{
				return (new Error('Não é possível inserir uma cobrança vazia.'));
			}
		} elseif ( is_array($dados) )
		{
			if ( NULL === $tipo )
				$tipo = self::COBRANCA_DEFAULT;
				
			switch ($tipo)
			{
				case Pagamento_Cobranca_Boleto_Itau::TIPO :
					$class = 'Pagamento_Cobranca_Boleto_Itau';
				break;
				
				case Pagamento_Cobranca_Boleto_Empenho::TIPO :
					$class = 'Pagamento_Cobranca_Boleto_Empenho';
				break;
				
				case Pagamento_Cobranca_PagSeguro::TIPO :
					$class = 'Pagamento_Cobranca_PagSeguro';
				break;
				
				case Pagamento_Cobranca_MoIP::TIPO :
					$class = 'Pagamento_Cobranca_MoIP';
				break;
				
				default:
					throw new Exception('Erro de Tipo inválido');
				break;
			}
			
			if ( !$class )
				return (new Error('Tipo inválido.'));
				
			$cobranca = new $class($this);
			$retorno = $cobranca->insere($dados);
			
			if ( $retorno > 0 )
			{
				$flag = new My_Table(Zend_Registry::get('config')->tb->cobranca_flag_cob);
				$flag->insert(array(
									'id_cliente' 	   => $this->getId(),
									'id_cobranca' 	   => $cobranca->getId(),
									//'id_tipo_cobranca' => $tipo,
									'criado'           => date('Y-m-d H:i:s')
									)
				 				);
				$this->_dados[self::COBRANCA_DEFAULT][$cobranca->getId()] = $cobranca;
				return (new Success('Cobrança Inserida.'));
			} else
			{
				return (new Error('Erro ao inserir nova cobranca.'));
			}
		} else
		{
			return (new Error('Tipo de dados inválido.'));
		}
	}
	
	public function getCobrancas ($id=NULL,$tipo=NULL)
	{
		if ( $id > 0 && NULL === $ipo )
			$tipo = self::COBRANCA_DEFAULT;

		if ( NULL === $this->_cobranca )
		{
			$cobranca = array();
			$select = $this->getTabela()->getAdapter()->select();
			
			/*$select->from(array('C' => $this->_config->tb->cobranca))
							   ->joinLeft(array('CF' => $this->_config->tb->cobranca_flag_cob),
										 'CF.id_cobranca = C.id_cobranca'
										 )
						       ->where('CF.id_cliente = ?',$this->getId());
						       */

			$select->from(array('C' => $this->_config->tb->cobranca))
						       ->where("C.id_cobranca IN (SELECT id_cobranca "
						       		   ."FROM {$this->_config->tb->cobranca_flag_cob} WHERE id_cliente = {$this->getId()})")
						       ->orWhere("C.id_cobranca IN (SELECT T1.id_cobranca "
						       			 ."FROM {$this->_config->tb->cobranca_flag} as T1 "
						       			 ."LEFT JOIN {$this->_config->tb->inscricao} T2 "
						       			 ."ON T1.id_inscricao = T2.id_inscricao "
						       			 ."WHERE T2.id_cliente = {$this->getId()})");
						       
			if ( $id > 0 && $tipo == self::COBRANCA_DEFAULT )
				$select->where('C.id_cobranca = ?',$id);
				
			$result = $select->query(Zend_Db::FETCH_ASSOC)->fetchAll();
			
			if ( $result )
			{
				foreach ($result as $row)
				{
					switch ($row['id_formapagto'])
					{
						case Pagamento_Cobranca_Boleto_Itau::TIPO :
							if ( !class_exists('Pagamento_Cobranca_Boleto_Itau') )
								require_once CORE.'/Pagamento/Cobranca/Boleto/Itau.php';
							
							$cobranca[$row['id_cobranca']] = 
								new Pagamento_Cobranca_Boleto_Itau($this,$row['id_cobranca'],$row);
						break;
						
						case Pagamento_Cobranca_Boleto_Empenho::TIPO :
							if ( !class_exists('Pagamento_Cobranca_Boleto_Empenho') )
								require_once CORE.'/Pagamento/Cobranca/Boleto/Empenho.php';
							
							$cobranca[$row['id_cobranca']] = 
								new Pagamento_Cobranca_Boleto_Empenho($this,$row['id_cobranca'],$row);
						break;
						
						case Pagamento_Cobranca_PagSeguro::TIPO :
							if ( !class_exists('Pagamento_Cobranca_PagSeguro') )
								require_once CORE.'/Pagamento/Cobranca/PagSeguro.php';
							
							$cobranca[$row['id_cobranca']] = 
								new Pagamento_Cobranca_PagSeguro($this,$row['id_cobranca'],$row);
						break;
						
						case Pagamento_Cobranca_MoIP::TIPO :
							if ( !class_exists('Pagamento_Cobranca_MoIP') )
								require_once CORE.'/Pagamento/Cobranca/MoIP.php';
							
							$cobranca[$row['id_cobranca']] = 
								new Pagamento_Cobranca_MoIP($this,$row['id_cobranca'],$row);
						break;
					}
				}
			}
			
			if ( sizeof($cobranca) && $id > 0 )
				return $cobranca[$id];
			else
				$this->_cobranca = $cobranca;
		}
			
		if ( $tipo )
		{
			if ( $id > 0 )
				return $this->_cobranca[$id];
			else
				return $this->_cobranca;
		} else
		{
			return $this->_cobranca;
		}
	}
	
	public function getId ()
	{
		return (int) $this->_id;
	}
	
	public function getDados ()
	{
		return (array) $this->_dados;
	}
	
	public function preBaixa($tipo)
	{
		$cobrancas = $this->getCobrancas();
		
		foreach ( $cobrancas as $c1 )
		{
			foreach( $c1 as $row )
			{
				if ( $row instanceof Pagamento_Cobranca_Abstract )
				{
					if ( $row->pendente() )
					{
						if ( $tipo == self::BAIXA_TIPO_NAOFORCA )
							return false;
						else
							$row->pago();
					}
				}
			}
		}
		
		return true;
	}
	
	public function posBaixa ()
	{
		$bind = array('id_status' => self::PAGO);
		
		$dados = $this->getDados();
		$where = $this->getTabela()->getAdapter()->quoteInto('id_orcamento = ?',$dados['id_orcamento']);
		
		$result = $this->getTabela()->getAdapter()->update(
					$this->_config->tb->orcamento,
					$bind,
					$where
				  );
	}
	
	public function baixa ($tipo)
	{
		if ( !$this->preBaixa($tipo) )
			return (new Error ('Há faturas pendentes'));
			
		$e = $this->edita(array(
							'fechado'   => date(),
							'id_status' => self::PAGO
						));
		
		if ( !$e instanceof Error )
			$this->posBaixa();
			
		return $e;
	}
	
	public static function getComboDadosUsados ($id)
	{
		if ( self::setupTabela() instanceof Error )
			return;

		$dadosUsados = Array();
			
		$select = new Zend_Db_Select(self::$_tabela->getAdapter());
		
	    $rs = $select->from(array('R' => Zend_Registry::get('config')->tb->representante),array())
    				 ->joinLeft(array('C' => Zend_Registry::get('config')->tb->cadastros),
    				 			'R.id_procurador = C.id_cadastro')
    				 ->where("R.id_artista IN (SELECT id_cadastro FROM ".
    				 		  Zend_Registry::get('config')->tb->orcamento." WHERE id_orcamento = ?)",(int)$id);

    	$r = $rs->query(Zend_Db::FETCH_OBJ);
    	$row = $r->fetchAll();
    	
    	if ( $row )
    	{
    		Zend_Loader::loadClass('Zend_Auth');
    		$me = Zend_Auth::getInstance();
    		
    		if ( $me->hasIdentity() )
    		{
    			$me = $me->getStorage()->read();
    			$email = $me->email;
    		}
    		
    		foreach ($row as $r) 
    		{
    			if ( $r->cpf_cnpj && ($r->pseudonimo || $row->pseudonimo || $row->nome) )
    			{
	    			$info[] = array (
	    				"empresa" => ($r->pseudonimo != NULL?$row->pseudonimo:$row->nome),
	    				"cpf_cnpj" => $r->cpf_cnpj,
	    				"endereco" => $r->endereco,
	    				"comp" => $r->comp,
	    				"cep" => $r->cep,
	    				"bairro" => $r->bairro,
	    				"cidade" => $r->cidade,
	    				"uf" => $r->uf,
	    				"pais" => $r->pais,
	    				"email" => $email,
	    				"tel_com" => $r->tel_com,
	    				"tel_cel" => $r->cel,
	    				"fax" => $r->fax
	    			);
    			}
    		}
    	}
    	
    	$select = new Zend_Db_Select(self::$_tabela->getAdapter());
    	$rs = $select->from(array('FL' => Zend_Registry::get('config')->tb->orcamento_fatura))
    				 ->joinLeft(array('F' => Zend_Registry::get('config')->tb->fatura),'FL.id_fatura = F.id_fatura')
    				 ->where('FL.id_orcamento = ?',(int)$id);

    	$r = $rs->query(Zend_Db::FETCH_OBJ);
    	$row = $r->fetchAll();
    	
    	if ( $row )
    	{
    		foreach ($row as $r) 
    		{
    			if ( $r->cpf_cnpj && $r->empresa )
    			{
	    			$info[] = array (
	    				"empresa" => $r->empresa,
	    				"cpf_cnpj" => $r->cpf_cnpj,
	    				"endereco" => $r->endereco,
	    				"comp" => $r->comp,
	    				"cep" => $r->cep,
	    				"bairro" => $r->bairro,
	    				"cidade" => $r->cidade,
	    				"uf" => $r->uf,
	    				"pais" => $r->pais,
	    				"email" => $r->email,
	    				"tel_com" => $r->tel_com,
	    				"tel_cel" => $r->cel,
	    				"fax" => $r->fax
	    			);
    			}
    		}
    	}
    	

    	$orcamento = new My_Table(Zend_Registry::get('config')->tb->orcamento);
    	$where = $orcamento->select()->where("id_cadastro IN (SELECT id_cadastro FROM ".
    											Zend_Registry::get('config')->tb->orcamento.
    											" WHERE id_orcamento = ?)", (int)$id);
		$rowO = $orcamento->fetchAll($where);
    	
    	if ( $rowO )
    	{
    	    foreach ($rowO as $r) 
    		{
    			if ( $r->cpf_cnpj && $r->empresa )
    			{
	    			$info[] = array (
	    				"empresa" => $r->empresa,
	    				"cpf_cnpj" => $r->cpf_cnpj,
	    				"endereco" => $r->endereco,
	    				"comp" => $r->comp,
	    				"cep" => $r->cep,
	    				"bairro" => $r->bairro,
	    				"cidade" => $r->cidade,
	    				"uf" => $r->uf,
	    				"pais" => $r->pais,
	    				"email" => $email,
	    				"tel_com" => $r->tel_com,
	    				"tel_cel" => $r->tel_cel,
	    				"fax" => $r->fax
	    			);
    			}
    		}
    	}
    	
    	return $info;    	
	}
	
	public function getPedido () 
	{
		if ( $this->pedido )
			return $this->pedido;
			
		$dados = $this->getDados();
			
		$select = new Zend_Db_Select(Zend_Registry::get('db'));
		
		$select->from(array('I' => Zend_Registry::get('config')->tb->inscricao),array())
			   ->joinLeft(array('C' => Zend_Registry::get('config')->tb->cliente), 
			   			  'I.id_cliente = C.id_cliente', 
			   			  array('id_cliente'))
			   ->joinLeft(array('FCOB' => Zend_Registry::get('config')->tb->cobranca_flag_cob), 
			   			  'I.id_cliente = FCOB.id_cliente',
			   			  array())
			   ->joinLeft(array('TC' => Zend_Registry::get('config')->tb->tipo_cobranca ), 
			   			  'C.id_tipo_cobranca = TC.id_tipo_cobranca', 
			   			   array('tipo_cobranca'))
			   ->joinLeft(array('COB' => Zend_Registry::get('config')->tb->cobranca), 
			   			  'FCOB.id_cobranca = COB.id_cobranca',
			   			  array('id_cobranca'))
			   ->joinLeft(array('P' => Zend_Registry::get('config')->tb->pedido), 
			   			  'C.id_pedido = P.id_pedido', array('id_status','id_pedido','total'))
			   ->joinLeft(array('PI' => Zend_Registry::get('config')->tb->pedido_item), 
			   			  'P.id_pedido = PI.id_pedido',
			   			  array('produto'))
			   ->joinLeft(array('PR' => Zend_Registry::get('config')->tb->produto), 
			   			  'PI.id_produto = PR.id_produto',
			   			  array())
			   ->joinLeft(array('PRT' => Zend_Registry::get('config')->tb->produto_turma), 
			   			  'PR.id_produto = PRT.id_produto',
			   			  array('id_turma'))
			   ->joinLeft(array('T' => Zend_Registry::get('config')->tb->turma), 
			   			  'PRT.id_turma = T.id_turma',
			   			  array('cidade','uf', 'data_duracao'))
			   ->joinLeft(array('S' => Zend_Registry::get('config')->tb->status), 
			   			  'P.id_status = S.id_status',
			   			  array('status'))
			   ->joinLeft(array('CUR' => Zend_Registry::get('config')->tb->curso), 
			   			  'T.id_curso = CUR.id_curso',
			   			  array('id_curso','curso','valor'))
				->where('C.id_cadastro = ?',$dados['id_cadastro'])
				->where('C.id_cliente =?',$this->getId())
				->order('I.id_inscricao DESC')
				->having('id_turma > 0')
				->group('I.id_cliente');

/*"SELECT `C`.`id_cliente`, `TC`.`tipo_cobranca`, `COB`.`id_cobranca`, `P`.`id_status`, `P`.`id_pedido`, `P`.`total`, `PI`.`produto`, `PRT`.`id_turma`, `T`.`cidade`, `T`.`uf`, `T`.`data_duracao`, `S`.`status`, `CUR`.`id_curso`, `CUR`.`curso`, `CUR`.`valor` 
FROM `en_inscricao` AS `I`
 LEFT JOIN `en_cliente` AS `C` ON I.id_cliente = C.id_cliente
 LEFT JOIN `en_cobranca_flag_cob` AS `FCOB` ON I.id_cliente = FCOB.id_cliente
 LEFT JOIN `en_tipo_cobranca` AS `TC` ON FCOB.id_tipo_cobranca = TC.id_tipo_cobranca
 LEFT JOIN `en_cobranca` AS `COB` ON FCOB.id_cobranca = COB.id_cobranca
 LEFT JOIN `en_pedido` AS `P` ON C.id_pedido = P.id_pedido
 LEFT JOIN `en_pedido_item` AS `PI` ON P.id_pedido = PI.id_pedido
 LEFT JOIN `en_produto` AS `PR` ON PI.id_produto = PR.id_produto
 LEFT JOIN `en_produto_turma` AS `PRT` ON PR.id_produto = PRT.id_produto
 LEFT JOIN `en_turma` AS `T` ON PRT.id_turma = T.id_turma
 LEFT JOIN `en_status` AS `S` ON P.id_status = S.id_status
 LEFT JOIN `en_curso` AS `CUR` ON T.id_curso = CUR.id_curso 
WHERE (C.id_cadastro = '8') AND (C.id_cliente =651) 
GROUP BY `I`.`id_cliente` 
HAVING (id_turma > 0) 
ORDER BY `I`.`id_inscricao` DESC"
				
				echo $select;*/
				
		$res = $select->query(Zend_Db::FETCH_OBJ)->fetchObject();
		
		if ( $res )
			$this->pedido = $res;
			
		return $this->pedido;
	}
	
	/**
	 *
	 * @return My_Table
	 */
	public function getTabela ()
	{
		if ( !$this->_tabela instanceof My_Table )
			$this->setTabela (self::setupTabela());
		
		return $this->_tabela;
	}
	
	public function setTabela (My_Table $tabela)
	{
		$this->_tabela = $tabela;
	}
	
	/**
	 * 
	 * @param $tabela
	 * @return object Error_Abstract 
	 */
	public static function setupTabela ($tabela=NULL)
	{
		if ( null === $tabela )
			$tabela = Zend_Registry::get('config')->tb->cliente;
		
		try {
			self::$_tabela = new My_Table($tabela);
			require_once 'app/models/Error/Success.php';
			self::$_mensagens = new Success('Tabela instanciada com sucesso');
			
			return self::$_tabela;
		} catch (Exception $e)
		{
			self::$_mensagens = new Error('Erro ao instanciar tabela da fatura');
			return self::$_mensagens;
		}
	}
}