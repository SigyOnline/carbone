<?php
/**
 * App_File_Upload
 *
 * @category   File Upload
 * @copyright  Copyright (c) 2006-2009 Nova Era Design. (http://www.novaeradesign.com.br)
 * @version    2.3
 * 
 */
require_once 'app/models/Filter_Acentos.php';

class App_File_Upload
{
    
    /**
	* root's path
	* @var string
	*/
    protected $root = NULL;
    
    /**
     * path to save
     * @var string 
     */
    protected $path = NULL;
    
    /**
     * $FILE's constant
     * @var array
     */
    protected $file = NULL;
    
    /**
     * image size (if it is)
     * @var string|integer
     */
    protected $size = NULL;
    
    /**
     * upload type = image|file|auto
     * @var string
     */
    protected $type = 'auto';
    
    /**
     * Mime
     * @var string
     */
    protected $mime = NULL;
    
    /**
     * File name
     * @var string
     */
    protected $name = NULL;
    
    /**
     * image from flash
     * @var string
     */
    protected $imageFromFlash = false;    
    
    /**
     * array of allowed extensions
     * @var array
     */
    protected $extensions = array('jpg','gif','png','bmp','tif','dwg','zip','plt','pdf','txt','csv');
    
    /**
     * file extension
     * @var mixed
     */
    protected $ext = NULL;
    
    /**
     * generate the thumbnail for the image
     *
     * @var bool
     */
    protected $generateThumb = false;
    
    /**
     * the final image coords
     *
     * @var array
     */
    protected $imageCoords = array();
    
    /**
     * the final thumb coords
     *
     * @var array
     */
    protected $thumbCoords = array();
    
    /**
     * read or not the txt file 
     * @var bool
     */
    protected $readTXT = false;
    
    /**
     * delete file after process
     * @var bool
     */
    protected $delete = false;
    
    /**
     * where the image was saved
     * @var mixed
     */
    protected $where = NULL;
    
    /**
     * If the destination file already exists, it will be overwritten.
     * @var bool
     */
    protected $overwrite = false;
    
    /**
     * file contents
     * @var string
     */
    protected $fileContents = NULL;
    
    /**
     * Error code
     * @var mixed
     */
    protected $code = NULL;
    
    /**
     * Error Messages
     * @var mixed
     */
    protected $message = NULL;
    
    /**
     * Success
     */
    const SUCCESS = 1;
    
    /**
     * General Failure
     */
    const FAILURE = 0;
    
    /**
     * File error
     */
    const FILE_ERROR = -10;
    
    /**
     * Extension error
     */
    const EXT_ERROR = -20;
    
    /**
     * File exists
     */
    const FILE_EXIST = -30;
    
    /**
     * Directory permission deny or path not exists
     */
    const PATH_DENY = -40;
    
    /**
     * Image size error
     */
    const IMAGE_SIZE = -50;

    /**
     * error reading the file
     */
    const FILE_READING = -60;
    
    /**
     * auto thumbnail name generate
     *
     */
    const THUMB_AUTO_NAME = 'autoName';
    
    /**
     * Class Constructor
     * 
     *
     * @param array $_FILES $file
     * @param string $path path where the will be save
     * @param array $options array of extra options
     * @return void
     */
    public function __construct($file,$path,array $options=array())
    {
        if ( NULL === $path )
            return self::PATH_DENY;
        else
            $this->path = $path;

        if ( !is_array($file) )
            return self::FILE_ERROR;
        else
            $this->file = $file;           
            
       if ( sizeof($options) )
       {
           while ( list($key,$val) = each($options) ) 
               $this->{$key} = $val;
       }

       if ( NULL === $this->root )
       {
           $this->root = $_SERVER['DOCUMENT_ROOT'] . '/';
       }
       
       if ( !$this->_valid() )
           return self::EXT_ERROR;
       
       $this->mime = $this->file['type'];
          
       if ( $this->type == 'auto' )
       {
           if ( preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/i", $this->file["type"]) )

           {
              $this->type = 'image';
           } else 
           {
              $this->type = 'file';
           }
       }

       if ( $this->type == 'image' )
       {
          if ( extension_loaded('gd') )
          { 
	         if ( $this->imageFromFlash || preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/i", $this->file["type"]) ) 
	         {
	             return $this->_image();	             
	         } else 
	         {
	             $this->code    = self::EXT_ERROR;
	             $this->message = 'Image format not supported.';
	         }
          } else 
          {
              $this->code    = self::FAILURE;
              $this->message = 'GD extension is not loaded';
          }
       }
       
       if ( $this->type == 'file' )
       {
           return $this->_file();
       }
       
       return self::FAILURE; 
    }
    
    /**
     * image type upload
     * @return file path
     */
	private function _image()
	{
	    if ( $this->_file() )
	        $filename = $this->root . $this->where;
	    else
            return false;

		if ( $this->ext == 'jpg' || $this->ext == 'jpeg' ) 
		{
			$img_orig = imagecreatefromjpeg($filename);

		} elseif ( $this->ext == 'gif' ) 
		{
			$img_orig = imagecreatefromgif($filename);

		} elseif ( $this->ext == 'png' ) 
		{
			$img_orig = imagecreatefrompng($filename);

		} elseif ( $this->ext == 'bmp' ) 
		{
			$img_orig = imagecreatefromgd($filename);
		}

		// image sizes
		$h_orig = imagesy($img_orig); //height
		$w_orig = imagesx($img_orig); //width		
		
		if ( $this->size )
		{
			if ( $w_orig > $h_orig ) 
			{
				$s_orig = $w_orig;
			} else 
			{
				$s_orig = $h_orig;
			}
			
			if ( NULL === $this->size )
			    $this->size = $s_orig;
			
			// resize
			if ( $s_orig > $this->size ) 
			{
				$p = ((100 * $this->size)/$s_orig)/100;
			} else {
				$p = 1;		
			}
			
			$w_final = $w_orig * $p; // final width
			$h_final = $h_orig * $p; // final height
			
		} elseif ( count($this->imageCoords) )
		{
			/**
			 * @todo Criar o metodo de resample de imagens
			 */
			

		}
		
		$image = @imagecreatetruecolor($w_final, $h_final);
					
		@chmod($filename, 0777);
										
		@imagecopyresampled($image, $img_orig, 0, 0, 0, 0, $w_final+1, $h_final+1, $w_orig, $h_orig);

		try 
		{
		    @imagejpeg($image, $filename);
		} catch (Exception $e)
		{
		    $this->code    = self::IMAGE_SIZE;
		    $this->message = 'Error on resample image'; 
		}

		imagedestroy($img_orig);
		
		$this->code    = self::SUCCESS;
		$this->message = 'Image saved';
		return true;
	}
	
	public static function resample ($filename,array $config,$newImageName=self::THUMB_AUTO_NAME) 
	{		
		if ( !is_file($filename) )
			return false;
			
		$newimgpath = basename($filename);
		$newimgpath = explode('.',$newimgpath);
		array_pop($newimgpath);
		$newimgpath = implode('',$newimgpath);
		$newimgpath = dirname($filename) .'/'. $newimgpath . '_thumb.jpg';
		
		try {
			$img_r = imagecreatefromjpeg($filename);

			$h_orig = imagesy($img_r); //height
			$w_orig = imagesx($img_r); //width		

			$dst_r = ImageCreateTrueColor( $config['tw'], $config['th'] );

			imagecopyresampled($dst_r,$img_r,0,0,$config['x'],$config['y'],
		    $config['tw'],$config['th'],$config['w'],$config['h']);
		    imagejpeg($dst_r, $newimgpath);
		    
		    return $newimgpath;
		    
		} catch (Exception $e)
		{
			return false;
		}
	}
	
	/**
	 * file type upload
	 * @return file path
	 */
	private function _file()
	{
		if ( class_exists('Filter_Acentos') )
			$filter = new Filter_Acentos(false);
		else 
			return false;
		
		$file     = $this->file['name'];
		$name     = basename($this->file['name'],'.'.$this->ext);
		$n        = 1;
			
		$filename = $filter->filter($name) .'.'. $this->ext;
		
		if ( !is_dir($this->root . $this->path . "/") )
			@mkdir ($this->root . $this->path ."/", 0755);
		
		@chmod($this->root . $this->path ."/",0777);
	
		if ( file_exists($this->root.$this->path."/".$filename) && $this->overwrite == false ) 
		{
			$filename = $filter->filter($name) . '_'. $n . '.' . $this->ext;
			
			while ( file_exists($this->root . $this->path . "/" . $filename) ) 
			{
				 $n++;
				 $filename = $filter->filter($name) . '_' . $n . '.' . $this->ext;
			}
		}		
        try 
        {
		    move_uploaded_file($this->file['tmp_name'],$this->root . $this->path . '/' . $filename);
        } catch (Exception $e)
        {
            $this->code    = self::PATH_DENY;
            $this->message = 'Error moving the uploaded file';
            return false;
        }
		
        @chmod($this->root . $this->path ."/",0755);
		@chmod($this->root . $this->path . '/' . $filename, 0755);
		
		$this->where   = $this->path . '/' . $filename;
		$this->name    = $filename;
		$this->code    = self::SUCCESS;
		$this->message = 'File saved';

    	if ( $this->readTXT )
		   return $this->_read();
		else 
		   return $this->where;
	}
	
	/**
	 * type file validation
	 * @return bool
	 */
	public function _valid ()
	{
	   if (! @preg_match("/\.(". implode('|',$this->extensions) ."){1}$/i", $this->file['name'], $ext) )
       {
           $this->code    = self::EXT_ERROR;
           $this->message = 'Extension not allowed';
           return false;
       } else
       {
           $this->ext = trim(strtolower($ext[1]),'.');
           return true;
       }
	}
	
	/**
	 * read file and return contents
	 * @return mixed
	 */
	public function _read ()
	{
	    $filename = $this->root . $this->where;
	    
	    if ( is_readable($filename) && NULL === $this->fileContents )
	    {
	        $this->fileContents = file_get_contents($filename);
	        
	        if ( $this->delete && !$this->_delete() )
	        {
	            $this->code    = self::FILE_READING;
	            $this->message = 'An error ocurred wnen trying to delete the file';
	        }
	        
	        return $this->fileContents;
	        
	    } elseif ( NULL === $this->fileContents ) 
	    {
	        $this->code    = self::FILE_READING;
	        $this->message = 'Error reading the uploaded file';
	        
	        return false;
	    } else 
	    {
	        return $this->fileContents;
	    }
	}
	
	/**
	 * delete the uploaded file
	 * @var bool
	 */
	public function _delete ()
	{
	    return (bool) @unlink($this->root . $this->where);
	}
	
	/**
	 * return where the file was saved
	 *
	 * @return string
	 */
	public function _where ()
	{
		return $this->where;
	}
	
	/**
	 * return Mime
	 *
	 * @return string
	 */
	public function _mime ()
	{
		return $this->mime;
	}
	
	/**
	 * return Mime
	 *
	 * @return string
	 */
	public function _name ()
	{
		return $this->name;
	}
	
	/**
	 * return the code message error
	 * @return mixed
	 */
	public function _getCode ()
	{
	    return $this->code;
	}
	
	/**
	 * return the error message
	 * @return string 
	 */
	public function _messages ()
	{
	    return $this->message;
	}
	
	/**
	 * check if there is a error
	 * @var bool
	 */
	public function _isError ()
	{
	    return (bool) ($this->_getCode() < 1 || NULL === $this->_getCode());
	}
	
	/**
	 * destruct method
	 */
	public function __destruct()
	{
	    if ( true === $this->delete )
	        $this->_delete();
	}
}