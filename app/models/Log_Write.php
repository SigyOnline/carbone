<?php

include_once 'library/Zend/Db/Table/Row/Abstract.php';
require_once 'Zend/Registry.php';

class log extends Zend_Db_Table_Abstract
{
	protected $_primary = 'id_log';

    protected function _setupTableName()
    {
        $this->_name = Zend_Registry::get('config')->tb->log;
        parent::_setupTableName();
    }	
}

class Log_Writer extends Zend_Db_Table_Abstract
{
	const _LOGIN = 'login';
	
	const _ADMIN = 'admin';
	
	const _DESCRICAO = 'descricao';
	
	const _QUANDO = 'quando';
	
	const _INSERT = 'insert';
	
	const _DELETE = 'delete';
	
	const _UPDATE = 'update';
	
	const _TABELA = 'tabela';
	
	protected $values = array();
	
	protected $message = array();
	
	public function init ()
	{
	    $auth = new Zend_Auth_Storage_Session(Zend_Registry::get('config')->www->sessionname);
	    $user = $auth->read();
	    
	    $this->user = $user;
	    
	    $this->start();
	}
	
	public function start()
	{}
	
    /**
     * Inserts a new row.
     *
     * @param  array  $data  Column-value pairs.
     * @return mixed         The primary key of the row inserted.
     */	
    protected function _insert(array $data)
    {
    	$i = parent::insert($data);
    	
    	if ( $i )
    	{
	   		$message = vsprintf($this->message[self::_INSERT],$this->values[self::_INSERT]);
	    	
	   		$log = new log ();
	   		
	   		$log->insert(array(
	   			self::_LOGIN => $this->user->login,
	   			self::_ADMIN => (int) ($this->user->nivel == 3),
	   			self::_QUANDO => New Zend_Db_Expr('Now()'),
	   			self::_DESCRICAO => $message
	   		));
    	}
    	
    	return $i;  
    }
    
    /**
     * Updates existing rows.
     *
     * @param  array        $data  Column-value pairs.
     * @param  array|string $where An SQL WHERE clause, or an array of SQL WHERE clauses.
     * @return int          The number of rows updated.
     */
    protected function _update(array $data, $where)
    {
    	/**
    	 * @todo fazer um fetchAll igual ao do delete colocando um while para adicionar no log os registros atualizados 
    	 */
    	$i = parent::update($data, $where);
    	
        if ( $i && count($this->values[self::_UPDATE]) )
    	{
    		$msgs = $this->values[self::_UPDATE];
    		
    		while ( list($k,$v) = each($msgs) )
    		{  			
		   		$message = vsprintf($this->message[self::_UPDATE],$v);
		    	
		   		$log = new log ();
		   		
		   		$log->insert(array(
		   			self::_LOGIN => $this->user->login,
		   			self::_ADMIN => (int) ($this->user->nivel == 3),
		   			self::_QUANDO => New Zend_Db_Expr('Now()'),
		   			self::_DESCRICAO => $message
		   		));
    		}
    		$this->values[self::_UPDATE] = array();
    	}
    	
        return $i;
    }
    
     /**
     * Deletes existing rows.
     *
     * @param  array|string $where SQL WHERE clause(s).
     * @return int          The number of rows deleted.
     */
    protected function _delete($where)
    {
    	/**
    	 * @todo fazer o fetchAll e o while para adicionar no log os registros excluídos
    	 */
    	$i = parent::delete($where);
    	
        if ( $i && count($this->values[self::_DELETE]) )
    	{
    		$msgs = $this->values[self::_DELETE];
    		
    		while ( list($k,$v) = each($msgs) )
    		{  			
		   		$message = vsprintf($this->message[self::_DELETE],$v);
		    	
		   		$log = new log ();
		   		
		   		$log->insert(array(
		   			self::_LOGIN => $this->user->login,
		   			self::_ADMIN => (int) ($this->user->nivel == 3),
		   			self::_QUANDO => New Zend_Db_Expr('Now()'),
		   			self::_DESCRICAO => $message
		   		));
    		}
    		$this->values[self::_DELETE] = array();
    	}
    	
    	//$val = 'return $a["a"] .": nao e B";';
    	
    	return $i;
    }
}