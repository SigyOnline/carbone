<?php
/**
 *
 * @author fnmx
 * @version alpha 1
 *
 * MenuTree helper
 *
 */
class MenuTree 
{

	/**
	 * @var MenuTree 
	 */
	protected static $_instancia;
	
	/**
	 * links do menu
	 *
	 * @var array
	 */
	protected $_links = array();
	
	/**
	 * separador dos links
	 *
	 * @var string
	 */
	protected $_separador = " > ";
	
	/**
	 * Título da página
	 *
	 * @var string
	 */
	protected $_titulo;
	
	
	/**
	 *  construtor
	 */
	private function __construct() 
	{}
	
	/**
	 * método estático para recuperar a instância
	 *
	 * @return MenuTree This MenuTree object.
	 */
	public static function instancia ()
	{
        if (null === self::$_instancia) 
        {
            self::$_instancia = new self();
        }

        return self::$_instancia;
	}
	
	/**
	 * adiciona links
	 *
	 * @param string $name
	 * @param string $link
	 * @return MenuTree This MenuTree object.
	 */
	public function addLink ($name, $link=NULL)
	{
		if ( NULL !== $link )
			$this->_links[$name] = $link;
		return $this;
	}
	
	/**
	 * deleta links
	 *
	 * @param string $name
	 * @return MenuTree This MenuTree object.
	 */	
	public function delLink ($name)
	{
		if ( NULL !== $this->_links[$name] )
			unset ($this->_links[$name]);
		return $this;
	}
	
	/**
	 * separador
	 *
	 * @param string $separador
	 */
	public function separador ($separador)
	{
		$this->_separador = $separador;
	}
	
	public function tituloPagina ()
	{
		$link = (array) $this->_links;
		
		if ( NULL !== $link['Home'] )
			unset ($link['Home']);
		
		return  $this->_titulo . implode(" {$this->_separador} ", array_keys($link)); 
	}
	
	public function _setTitulo ($titulo)
	{
		$this->_titulo = $titulo;
	}
	
	/**
	 * método mágico
	 *
	 * @return string
	 */
	public function __toString()
	{
		$links = $this->_links;
		$total = sizeof($links);
		$tree  = array(); 
		
		while ( list($k,$v) = each($links) )
		{
			$c++;
			
			if ( $total > ($c)  )
				$tree[] = '<span><a href="'. $v .'">'. $k .'</a></span>' . "\r\n";
			else
				$tree[] = $k;
		}
		
		return implode("<span> {$this->_separador} </span>",$tree);
	}
}