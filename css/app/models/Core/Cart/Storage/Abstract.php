<?php
/**
 * Classe responsavel pelo armazenamento dos itens do carrinho.
 * 
 * @author Fabio fabio@novaeradesign.com.br (Everybody Lies...)
 * @category Carrinho
 * @version Beta 1
 */

abstract class Cart_Storage_Abstract extends ArrayObject 
{
	const DEFAULT_SPACE_NAME = 'Default';
	
	/**
	 * Itens do carrinho
	 *
	 * @var array
	 */
	protected $items = array();
	
	/**
	 * Instância do objeto
	 *
	 * @var Cart_Storage_Abstract $this Cart_Storage_Abstract object
	 */
	protected static $instance = NULL;
	
	/**
	 * carrinho padrão
	 *
	 * @var string
	 */
	protected $defaultSpace;
	
	/**
	 * chave atual dos carrinhos
	 *
	 * @var array
	 */
	protected $key = array();
	
	/**
	 * posição do array
	 *
	 * @var array
	 */
	protected $keyPosition = array();
	
	/**
	 * id do carrinho no BD
	 * @var int
	 */
	protected $id;
	
	/**
	 * id do pagamento escolhido
	 * @var int
	 */
	protected $paymentId;
	
	/**
	 * Construtor
	 * 
	 * Deve ser passado o(s) nome(s) da(s) instância(s)
	 *
	 * @param Array|string $instanceName
	 */
	public function __construct($instanceName=self::DEFAULT_SPACE_NAME,$defaultItemSpace=NULL) 
	{
		if ( NULL === $instanceName )
			$instanceName = self::DEFAULT_SPACE_NAME;
			
		if ( !is_array($instanceName) )
			$instanceName = array($instanceName);
			
		if ( sizeof($instanceName) )
		{
			foreach ($instanceName as $in) 
			{
				$this->addItemsSpace($in);
			}
			
		} else 
			throw new Exception('O nome do carrinho não é válido');
		
		if ( NULL !== $defaultItemSpace )
			$this->setDefaultItemSpace($defaultItemSpace);
		
		$this->init();

		$this->instance = $this;
	}
	
	public function init () 
	{}
	
	/**
	 * Enter description here...
	 *
	 * @return Cart_Storage_Abstract $this Cart_Storage_Abstract object
	 */
	//abstract static public function getInstance ($space=NULL);
	
	/**
	 * Adiciona um carrinho
	 *
	 * @param string|array|Cart_Storage_Abstract $is
	 * @return Cart_Storage_Abstract $this Cart_Storage_Abstract object
	 */
	public function addItemsSpace ($is)
	{
		switch (true) 
		{
			case $is instanceof Cart_Storage_Abstract :
				$isC = $is->getInstanceName();
			
				if ( isset($this->items[$isC]) )
					throw new Exception('Não é possível ainda mesclar carrinhos');
				
				$this->items[$isC] = $is;
				$this->key[$isC] = count($is)-1;
				$this->keyPosition[$isC] = 0;
			break;
			
			case is_string($is) :
				if ( !isset($this->items[$is]) )
				{
					$this->items[$is] = array();
					$this->key[$is] = 0;
					$this->keyPosition[$is] = 0;					
				} else 
					throw new Exception('Carrinho já existente');
			break;
					
			case is_array($is) :
			default :
				throw new Exception("Formato '".gettype($is) ."' inválido");
			break;
		}
		return $this;
	}
	
	/**
	 * Define o carrinho padrão
	 *
	 * @param string $defaultItemSpace
	 * @return Cart_Storage_Abstract $this Cart_Storage_Abstract object
	 */
	public function setDefaultItemSpace ($defaultItemSpace=NULL) 
	{
		$defaultItemSpace = $defaultItemSpace;
		
		if ( array_key_exists($defaultItemSpace,$this->items) )
		{
			$this->defaultSpace = $defaultItemSpace;
		} else
		{
			if ( !array_key_exists(self::DEFAULT_SPACE_NAME,$this->items) && !$this->count() )
			{
				$this->addItemsSpace(self::DEFAULT_SPACE_NAME)
					 ->setDefaultItemSpace(self::DEFAULT_SPACE_NAME);		
			} elseif ( $this->count() == 1 )
			{
				$this->setDefaultItemSpace(key($this->items));	
			} else
				throw new Exception('Carrinho padrão não encontrado');
		}
	
		return $this;
	}
	
	/**
	 * pega o nome do carrinho padrão
	 *
	 * @return string
	 */
	public function getDefaultItemSpace () 
	{
		if ( NULL === $this->defaultSpace )
			$this->setDefaultItemSpace();
					
		return $this->defaultSpace;
		
	}
	
	/**
	 * Adiciona novo item no carrinho
	 * 
	 * @param Cart_Interface_Item $item
	 * @param string|numeric $itemSpace
	 * @return Cart_Storage_Abstract $this Cart_Storage_Abstract object
	 */
	public function addItem (Cart_Interface_Item $item,$itemSpace=NULL)
	{
		if ( NULL === $itemSpace )
			$itemSpace = $this->getDefaultItemSpace();
		
		if ( !empty($itemSpace) && ( is_string($itemSpace) || is_numeric($itemSpace) ) )
		{
			$this->key[$itemSpace]++;
			$this->keyPosition[$itemSpace] = $this->key[$itemSpace];
			$this->items[$itemSpace][(int)$this->key[$itemSpace]] = $item;
		} else
		{
			throw new Exception('Carrinho não encontrado');
		}
		
		return $this;
	}
	
	/**
	 * Pega um item ou o atual
	 *
	 * @param int $id
	 * @param string $itemSpace
	 * @return Cart_Item $this Cart_Item object
	 */
	public function getItem ($id=NULL,$itemSpace=NULL) 
	{
		if ( NULL === $itemSpace )
			$itemSpace = $this->getDefaultItemSpace(); 
		
		if ( NULL === $id )
			$id = $this->keyPosition[$itemSpace];
			
		if ( !$this->items[$itemSpace][$id] )
		{
			return false;
		} else
		{
			$this->keyPosition[$itemSpace] = $id;
			return $this->items[$itemSpace][$id];
		}
	}

	/**
	 * apaga um item do carrinho
	 *
	 * @param mixed $id
	 * @param string $itemSpace
	 */
	public function delItem ($id=NULL,$itemSpace=NULL)
	{
		if ( NULL === $itemSpace )
			$itemSpace = $this->getDefaultItemSpace();
					
		$item = $this->getItem($id,$itemSpace);
		unset($item);
		unset($this->items[$itemSpace][$this->keyPosition[$itemSpace]]);
		$this->keyPosition[$itemSpace] = 0;
	}
	
	/**
	 * retorna um item do carrinho para edição
	 * Atalho para getItem() 
	 *
	 * @param mixed $id
	 * @param string $itemSpace
	 * @return Cart_Item $this Cart_Item object
	 */
	public function editItem ($id=NULL,$itemSpace=NULL)
	{
		if ( NULL === $itemSpace )
			$itemSpace = $this->getDefaultItemSpace();
					
		return $this->getItem($id,$itemSpace);
	}
	
	public function getItems($itemSpace=NULL) 
	{
		if ( NULL === $itemSpace )
			$itemSpace = $this->getDefaultItemSpace();
			
		return $this->items[$itemSpace];
	}
	
	/**
	 * limpa o carrinho ou parte dele
	 *
	 * @param string|NULL $itemSpace
	 */
	public function clean($itemSpace=NULL)
	{
		if ( NULL === $itemSpace )
		{
			$this->items = array();
			$this->key  = array();
			$this->keyPosition = array();
		} else
		{
			unset(
				  $this->items[$itemSpace],
				  $this->key[$itemSpace],
				  $this->keyPosition[$itemSpace]
				  );
		}
		
	}
	
	/**
	 * pega o subtotal
	 *
	 * @param string|NULL $itemSpace
	 * @return float
	 */
	public function subtotal ($itemSpace=NULL)	
	{
		if ( NULL === $itemSpace )
			$spaces = array_keys($this->items);
		else 
			$spaces = array($itemSpace);
			
		$subtotal = 0.00;
			
		$items = $this->items;
			
		foreach ($spaces as $s) 
		{
			if ( $items[$s] instanceof Cart_Storage_Abstract )
			{
				 $subtotal =+ $items[$s]->subtotal();
			} else 
			{
				reset($items[$s]);

				while ( list($k,$v) = each($items[$s]) ) 
				{
					if ( $v instanceof Cart_Interface_Item )
						$subtotal += $v->total();
				}
			}
		}
		
		return $subtotal;
	}
	
	/**
	 * conta os itens do carrinho
	 *
	 * @param string|NULL $itemSpace
	 * @return integer
	 */
	public function count($itemSpace=NULL)
	{
		if ( NULL === $itemSpace )
			return count($this->items);
		else
			return count($this->items[$itemSpace]);
	}
	
	abstract public function getPaymentGateways ();
	
	/**
	 * retorna o total do carrinho, atualmente um link para subtotal
	 *
	 * @param string|NULL $itemSpace
	 * @return float
	 */
	public function total ($itemSpace=NULL) 
	{
		return $this->subtotal($itemSpace);
	}
	
	abstract public function processCheckout ($paymentId);
	
	/**
	 * função __get desabilitada
	 *
	 * @param NULL $v
	 */
	/*public function __get($v)
	{
		throw new Exception('__get desabilitado, use getItem()');
	}*/
	
	/**
	 * função __set desabilitada
	 *
	 * @param unknown_type $k
	 * @param unknown_type $v
	 */
	/*
	public function __set($k,$v) 
	{
		throw new Exception('__set desabilitado, use addItem() ou editItem()');
	}*/
	
	/**
	 * Metodo desconstrutor
	 *
	 */
	public function __destruct() 
	{
	}
}
