<?php

/**
 * Interface dos itens do carrinho, Cart_Storage.
 * 
 * @author Fabio fabio@novaeradesign.com.br
 * @category Cart
 * @version Beta 1
 */

interface Cart_Interface_Item 
{
	public function addQty ($qty=NULL); 

	public function getQty ();

	public function setPrice ($price);

	public function getPrice ();
	
	public function getItem ();

	public function setItem ($item);
	
	public function setProduct ($product);
	
	public function getProduct ();

	public function total ();

	public function preCheckout();
	
	public function posCheckout();
}