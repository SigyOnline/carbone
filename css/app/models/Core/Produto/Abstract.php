<?php
abstract class Produto_Abstract
{
	/**
	 * Dados do produto (preço, estoque, etc)
	 * @var array
	 */
	protected $dados = array ();
	
	protected $id;
	
	/**
	 * Classe que contem o item a ser vendido
	 * @var mixed|array|object
	 */
	protected $item;
	
	protected $tabela;
	
	protected $tabelaRel;
	
	protected $preco;
	
	public function setTabela ($tabela) 
	{
		if ( is_object($tabela) )
			$this->tabela = $tabela;
		else
			$this->tabela = new My_Table($tabela);
	}
	
	/**
	 * 
	 * retorna o My_Table da tabela
	 * @return My_Table
	 */
	public function getTabela ()
	{
		return $this->tabela;
	}
	
	public function setTabelaRel ($tabelaRel)
	{
		if ( is_object($tabelaRel) )
			$this->tabelaRel = $tabelaRel;
		else
			$this->tabelaRel = new My_Table($tabelaRel);
	}
	
	/**
	 * 
	 * retorna o My_Tabela da tabela secundária
	 * @return My_Table
	 */
	public function getTabelaRel ()
	{
		return $this->tabelaRel;
	}
	
	public function setId($id)
	{
		$this->id = $id;
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	abstract public function setDados($esp);
	
	abstract public function getEstoque ();
	
	abstract public function getPreco ();
	
	abstract public function imprimiPreco ();
	
	abstract public function baixaEstoque ($qtde);
	
	public function getDados ()
	{
		return $this->dados;
	}
}