<?php
require_once CORE.'/Pagamento/Cobranca/Abstract.php';

class Pagamento_Cobranca_PagSeguro extends Pagamento_Cobranca_Abstract
{
	const TIPO = 5;
	
	const STATUS_PAGO = 7;
	
	const STATUS_NAOPAGO = 4;
	
	const STATUS_CANCELADO = 6;	
	
	protected $_modificados = array();
	
	protected $_required = array('transacao','pago','valor','valor_bol','linha_1',
								 'linha_2','linha_3','data_emissao','data_venc',
								 'data_pagto','data_disp','id_formapagto','id_status');
	
	protected $_notNull = array('id_status','id_formapagto','transacao',
								'data_venc','valor');
	
	protected $_tabela;	
	
	public $tipo_titulo = 'PagSeguro';
	
	public function getTabela ()
	{
		if ( NULL === $this->_tabela )
		{
			$tabela = Zend_Registry::get('config')->tb->cobranca;
			$this->_tabela = new My_Table($tabela);
		}
		return $this->_tabela;
	}	
	
	public function insere ($dados=NULL)
	{
		if ( !$dados['id_status'] )
			$dados['id_status'] = self::STATUS_NAOPAGO;

		if ( $id = parent::insere($dados) )
		{ 
			$this->transacao = $this->unico_id();
			if ( !parent::atualiza() )
				throw new Exception('Erro ao definir o nosso número');
		}
		return $id;
	}

	protected function unico_id ()
	{
		return $this->_dados['transacao'] = '';
	}
	
	public function preparaDados ($dados)
	{
		$valores = array ('valor');
		
		foreach ( $valores as $i )
		{
			if ( $dados[$i] )
			{
				$dados[$i] = preg_replace('/[^0-9,.]/', '', (string) $dados[$i]);
				$dados[$i] = (float) str_replace(',','.',$dados[$i]);
			}
		}
		$dados['id_formapagto'] = self::TIPO;
		
		if ( !$dados['data_emissao'] )
			$dados['data_emissao'] = date('Y-m-d');
			
		if ( !$dados['data_venc'] && !$this->_dados['data_venc'] )
		{
			switch (date('D')) 
			{
				case 'Fri':
					$data_v 	= date('Y-m-d',strtotime('+4 day'));
				break;
				
				case 'Sat':
					$data_v 	= date('Y-m-d',strtotime('+3 day'));
				break;
				
				default:
					$data_v 	= date('Y-m-d',strtotime('+2 day'));
				break;
			}
			
			$dados['data_venc'] = $data_v;
		} elseif ( $dados['data_venc'] )
		{
			$dados['data_venc'] = date('Y-m-d',strtotime($dados['data_venc']));
		}  
		
		if ( $dados['data_pagto'] )
			$dados['data_pagto'] = date('Y-m-d',strtotime($dados['data_pagto']));
		
		if ( $dados['data_disp'] )
			$dados['data_disp'] = date('Y-m-d',strtotime($dados['data_disp']));
		
		return $dados;
	}
	
	public function pago ()
	{
		if ( $this->id_status == self::STATUS_PAGO )
			return;
					
		$modif = array(
					'id_status' => self::STATUS_PAGO,
					'data_pagto' => $this->data_vencimento,
					//'valor_pago' => $this->valor
				 );
		
		if ( $this->atualiza($modif) )
		{
			$this->getFatura()->baixa(Pagamento_Fatura::BAIXA_TIPO_NAOFORCA);
			
			return $resultado;
		} else
		{
			return (new Error('Não foi possível dar baixa no pagamento: '.$this->getId()));
		}
	}
	
	public function cancela ()
	{
		if ( $this->id_status == self::STATUS_CANCELADO )
			return;
					
		$modif = array(
					'id_status' => self::STATUS_CANCELADO
				 );
		
		if ( $this->atualiza($modif) )
		{
			return $resultado;
		} else
		{
			return (new Error('Não foi possível cancelar o pagamento: '.$this->getId()));
		}
	}
	
	public function naopago ()
	{
		if ( $this->id_status == self::STATUS_NAOPAGO )
			return;
					
		$modif = array(
					'id_status' => self::STATUS_NAOPAGO,
					//'valor_pago' => NULL
				 );
		
		if ( $this->atualiza($modif) )
		{
			return $resultado;
		} else
		{
			return (new Error('Não foi possível editar pagamento: '.$this->getId()));
		}
	}
	
	public function pendente ()
	{
		return (bool) ($this->id_status == self::STATUS_NAOPAGO);
	}
	
	public function getIdFatura ()
	{
		return  $this->getFatura()->getId();
		//return $dados['id_cliente'];
	}	
	
	public function render ()
	{
		$dadosboleto = $this->getDados();
		$dadosFatura = $this->getFatura()->getDados();
		$dados = array();

		if ( $dadosFatura['id_pedido'] )
		{
			$select = $this->getTabela()->getAdapter()->select();
			$select->from(Zend_Registry::get('config')->tb->pedido_item)
					->where('id_pedido = ?',$dadosFatura['id_pedido']);
			
			$res = $select->query(Zend_Db::FETCH_OBJ)->fetchAll(); 
			
			if ( $res )
			{
				foreach ($res as $row) 
				{
					$dados['item'][$row->pedido_item] = array(
										'item_id'    => $row->id_pedido_item,
										'item_descr' => $row->produto,
										'item_quant' => $row->qtde,
										'item_valor' => number_format($row->valor,2,'',''),
										'item_frete' => (int) $row->frete,
										'item_peso'  => (int) $row->peso
									);
				}
			}
		}
		
		$dados['email_cobranca'] = 'pagamentos@entelco.com.br';//'credito@entelco.com.br';
		$dados['cliente_nome']   = $dadosFatura['nome'];
		$dados['cliente_cep']    = $dadosFatura['cep'];
		$dados['cliente_end']    = $dadosFatura['logradouro'];
		$dados['cliente_num']	 = $dadosFatura['numero'];
		$dados['cliente_compl']	 = $dadosFatura['compl'];
		$dados['cliente_bairro'] = $dadosFatura['bairro'];
		$dados['cliente_cidade'] = $dadosFatura['cidade'];
		$dados['cliente_uf'] 	 = $dadosFatura['uf'];
		$dados['cliente_pais'] 	 = 'BRA';
		$dados['cliente_ddd'] 	 = $dadosFatura['ddd'];
		$dados['cliente_tel'] 	 = $dadosFatura['numero'];
		$dados['cliente_email']  = $dadosFatura['email'];
		
		//foreach ($dados as $k => $v) 
			//$dados[$k] = utf8_encode($v);
		
		require_once CORE_HELPER_DEFAULT.'/Pagamento/Cobranca/PagSeguro.php';
	}
}