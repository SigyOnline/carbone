<?php
require_once CORE.'/Pagamento/Cobranca/Abstract.php';

class Pagamento_Cobranca_Boleto_Itau extends Pagamento_Cobranca_Abstract
{
	const TIPO = 1;
	
	const STATUS_PAGO = 7;
	
	const STATUS_NAOPAGO = 4;
	
	const STATUS_CANCELADO = 6;	
	
	protected $_modificados = array();
	
	protected $_required = array('transacao','pago','valor','valor_bol','linha_1',
								 'linha_2','linha_3','data_emissao','data_venc',
								 'data_pagto','data_disp','id_formapagto','id_status');
	
	protected $_notNull = array('id_status','id_formapagto','transacao',
								'data_venc','valor');
	
	protected $_tabela;	
	
	public $tipo_titulo = 'Boleto Itaú';
	
	public function getTabela ()
	{
		if ( NULL === $this->_tabela )
		{
			$tabela = Zend_Registry::get('config')->tb->cobranca;
			$this->_tabela = new My_Table($tabela);
		}
		return $this->_tabela;
	}	
	
	public function insere ($dados=NULL)
	{
		if ( !$dados['id_status'] )
			$dados['id_status'] = self::STATUS_NAOPAGO;

		if ( $id = parent::insere($dados) )
		{ 
			$this->transacao = $this->unico_id();
			if ( !parent::atualiza() )
				throw new Exception('Erro ao definir o nosso número');
		}
		return $id;
	}

	protected function unico_id ()
	{
		$this->getDados();
		
		if ( !$this->_dados['transacao'] )
		{
			$id = array('transacao' => '25'.sprintf("%06d",$this->getId()));
			$this->_dados = $this->_dados + $id;
		}
		
		return $this->_dados['transacao'];
	}
	
	public function preparaDados ($dados)
	{
		$valores = array ('valor');
		
		foreach ( $valores as $i )
		{
			if ( $dados[$i] )
			{
				$dados[$i] = preg_replace('/[^0-9,.]/', '', (string) $dados[$i]);
				$dados[$i] = (float) str_replace(',','.',$dados[$i]);
			}
		}
		$dados['id_formapagto'] = self::TIPO;
		
		if ( !$dados['data_emissao'] )
			$dados['data_emissao'] = date('Y-m-d');
			
		if ( !$dados['data_venc'] && !$this->_dados['data_venc'] )
		{
			switch (date('D')) 
			{
				case 'Fri':
					$data_v 	= date('Y-m-d',strtotime('+4 day'));
				break;
				
				case 'Sat':
					$data_v 	= date('Y-m-d',strtotime('+3 day'));
				break;
				
				default:
					$data_v 	= date('Y-m-d',strtotime('+2 day'));
				break;
			}
			
			$dados['data_venc'] = $data_v;
		} elseif ( $dados['data_venc'] )
		{
			$dados['data_venc'] = date('Y-m-d',strtotime($dados['data_venc']));
		} 
		
		if ( $dados['data_pagto'] )
			$dados['data_pagto'] = date('Y-m-d',strtotime($dados['data_pagto']));
		
		if ( $dados['data_disp'] )
			$dados['data_disp'] = date('Y-m-d',strtotime($dados['data_disp']));
		
		return $dados;
	}
	
	public function pago ()
	{
		if ( $this->id_status == self::STATUS_PAGO )
			return;
					
		$modif = array(
					'id_status' => self::STATUS_PAGO,
					'data_pagto' => $this->data_vencimento,
					//'valor_pago' => $this->valor
				 );
		
		if ( $this->atualiza($modif) )
		{
			$this->getFatura()->baixa(Pagamento_Fatura::BAIXA_TIPO_NAOFORCA);
			
			return $resultado;
		} else
		{
			return (new Error('Não foi possível dar baixa no boleto: '.$this->getId()));
		}
	}
	
	public function cancela ()
	{
		if ( $this->id_status == self::STATUS_CANCELADO )
			return;
					
		$modif = array(
					'id_status' => self::STATUS_CANCELADO
				 );
		
		if ( $this->atualiza($modif) )
		{
			return $resultado;
		} else
		{
			return (new Error('Não foi possível cancelar o boleto: '.$this->getId()));
		}
	}
	
	public function naopago ()
	{
		if ( $this->id_status == self::STATUS_NAOPAGO )
			return;
					
		$modif = array(
					'id_status' => self::STATUS_NAOPAGO,
					//'valor_pago' => NULL
				 );
		
		if ( $this->atualiza($modif) )
		{
			return $resultado;
		} else
		{
			return (new Error('Não foi possível editar boleto: '.$this->getId()));
		}
	}
	
	public function pendente ()
	{
		return (bool) ($this->id_status == self::STATUS_NAOPAGO);
	}
	
	public function getIdFatura ()
	{
		return  $this->getFatura()->getId();
		//return $dados['id_cliente'];
	}
	
	public function render ()
	{
		$dadosboleto = $this->getDados();
		$dadosFatura = $this->getFatura()->getDados();
		
		//$dias_de_prazo_para_pagamento = 5;
		$taxa_boleto = 0;
		$data_venc = date('d/m/Y',strtotime($dadosboleto['data_venc']));
		//$data_venc = date("d/m/Y", time() + ($dias_de_prazo_para_pagamento * 86400));  // Prazo de X dias OU informe data: "13/04/2006"; 
		$valor_cobrado = $dadosboleto['valor']; // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
		$valor_cobrado = str_replace(",", ".",$valor_cobrado);
		$valor_boleto  = number_format($valor_cobrado+$taxa_boleto, 2, ',', '');
		
		$dadosboleto["nosso_numero"] = $this->unico_id();  // Nosso numero - REGRA: Máximo de 8 caracteres!
		$dadosboleto["numero_documento"] = $this->unico_id();	// Num do pedido ou nosso numero
		$dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
		$dadosboleto["data_documento"] = date('d/m/Y',strtotime($dadosboleto['data_emissao'])); // Data de emissão do Boleto
		//$dadosboleto["data_processamento"] = date("d/m/Y"); // Data de processamento do boleto (opcional)
		$dadosboleto["valor_boleto"] = $valor_boleto; 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula
		
		// DADOS DO SEU CLIENTE
		$dadosboleto["sacado"] = $dadosFatura['nome'];
		$dadosboleto["endereco1"] = $dadosFatura['logradouro'] . ($dadosFatura['numero']? ", ". $dadosFatura['numero']:NULL)
								  .($dadosFatura['compl']? " - ". $dadosFatura['compl']:NULL)
								  .($dadosFatura['bairro']? " - ". $dadosFatura['bairro']:NULL)
								  .($dadosFatura['cidade']? " - ". $dadosFatura['cidade']:NULL)
								  .($dadosFatura['uf']? " - ". $dadosFatura['uf']:NULL);
		
		// INFORMACOES PARA O CLIENTE

		$dadosboleto["demonstrativo1"] = "Sr. Caixa, cobrar multa de 2% após o vencimento";
		$dadosboleto["demonstrativo2"] = "Receber até 10 dias após o vencimento";
		$dadosboleto["demonstrativo3"] = "Entelco - http://www.entelco.com.br";
		$dadosboleto["instrucoes1"] = "Em caso de dúvidas entre em contato conosco: contato@entelco.com.br";
		//$dadosboleto["instrucoes2"] = "";
		
		// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
		$dadosboleto["quantidade"] = "";
		$dadosboleto["valor_unitario"] = "";
		$dadosboleto["aceite"] = "";		
		$dadosboleto["especie"] = "R$";
		$dadosboleto["especie_doc"] = "";
		
		// ---------------------- DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO --------------- //
		
		// DADOS DA SUA CONTA - ITAÚ
		$dadosboleto["agencia"] = "4816"; // Num da agência, sem digito
		$dadosboleto["conta"] = "05761";	// Num da conta, sem digito
		$dadosboleto["conta_dv"] = "8"; 	// Digito do Num da conta
		
		// DADOS PERSONALIZADOS - ITAÚ
		$dadosboleto["carteira"] = "109";  // Código da Carteira: pode ser 175, 174, 104, 109, 178, ou 157
		
		// SEUS DADOS
		$dadosboleto["identificacao"] = "ENTELCO";
		$dadosboleto["cpf_cnpj"] = "11.108.062/0001-38";
		$dadosboleto["cidade_uf"] = "São Paulo/SP";
		$dadosboleto["cedente"] = "ENTELCO Tecnologia em Redes de Dados Ltda.";
		
		include_once 'app/models/funcoes_itau.php'; 
		//include_once 'include/layout_itau.php';
		require_once CORE_HELPER_DEFAULT.'/Pagamento/Cobranca/Boleto/Itau.php';
	}
}