<?php
require_once 'app/models/Model_Data.php';
require_once 'app/models/My_Table.php';

/**
 * Descrição..
 *
 * @category   Core
 * @package    Cadastro
 * @copyright  Copyright (c) 2010-2011 Nova Era Design (http://www.novaeradesign.com.br)
 */

abstract class Cadastro_Login_Abstract
{
	const LOGIN_INATIVO = -3;
	
	const LOGIN_INVALIDO = 0;
	
	const LOGADO = 1;
	
	/**
	 * Conexão com o BD
	 * @var Zend_Db_Adapter_Abstract
	 */	
	protected $db;
	
	public function __construct(Zend_Db_Adapter_Abstract $db)
	{
		$this->db = $db;
	}
	
	/**
	 * 
	 * Criar um novo usuário
	 * @param array $dados
	 * @throws Exception
	 * @return Cadastro_Item_Usuario
	 */
	abstract static public function novo ($dados);
	
	static public function carregaClasseUsuario() 
	{
		if ( !class_exists(self::$classeItem) )
			return require_once MYCORE.'/'. str_replace('_', '/', self::$classeItem) .'.php';
	}
	
	abstract public function apaga ($id);
	
	abstract public function login ($login=NULL,$senha=NULL, $opcoes=NULL);
	
	public function getAuth()
	{
		$class = new ReflectionClass($this);
		$const = $class->getConstant('NAME_SPACE_LOGIN');
		$auth = new Zend_Auth_Storage_Session($const);
		
		if ( !$auth->isEmpty() )
			return $auth->read();
		else
			return;
	}
	
	public function sair ()
	{
		$class = new ReflectionClass($this);
		$const = $class->getConstant('NAME_SPACE_LOGIN');
    	$auth = new Zend_Auth_Storage_Session($const);
    	$auth->clear();
    	unset(Zend_Registry::get('config')->session);
	}
	
	abstract public function recuperaSenha ($login);
}