<?php
require_once 'app/models/Model_Data.php';
require_once 'app/models/My_Table.php';
require_once 'app/models/Error/Error.php';
require_once 'app/models/Error/Success.php';
require_once CORE.'/Cadastro/Item/Abstract.php';

Class Cadastro_Item_Inscricao extends Cadastro_Item_Abstract
{
	/**
	 * status para cadastros ativos
	 */
	const STATUS_ATIVO = 9;
	
	/**
	 * Status de cadastros inativos
	 */
	const STATUS_INATIVO = 10;
	
	const CLIENTE_TIPO_DEFAULT = 1;
	
	const CLIENTE_TIPO_ALUNO = 1;
	
	/**
	 * Instância do Item
	 * @var Cadastro_Item_Usuario
	 */
	protected $cadastroItem;
	
	/**
	 * 
	 * Instância Model Data
	 * @var Model_Data
	 */
	protected $tabelaCliente;
	
	/**
	 * 
	 * Instância Model Data
	 * @var Model_Data
	 */
	protected $tabelaInscricao;
	
	/**
	 * 
	 * Campos requiridos pela tabela
	 * @var array
	 */
	protected $requiredCliente = array('nome','id_cadastro','nascimento','cpf','rg','observacao',
									   'comentario','skype','id_cliente_tipo','id_tipo_cobranca');
	
	/**
	 * 
	 * Campos que não podem ser nulos
	 * @var array
	 */
	protected $notNullCliente = array('nome','id_cadastro','nascimento','cpf','rg');
	
	/**
	 * 
	 * Campos requiridos pela tabela
	 * @var array
	 */
	protected $requiredInscricao = array('comosoube','codpromo','convenio','vendedor','empresa',
									 	 'ip','id_cliente','id_turma','id_status');
	
	/**
	 * 
	 * Campos que não podem ser nulos
	 * @var array
	 */
	protected $notNullInscricao = array('ip','id_cliente','id_turma','id_status');
	
	/**
	 * 
	 * Construtor
	 * @param int $id
	 */
	public function __construct($id=NULL, Cadastro_Item_Abstract $item=NULL)
	{
		if ( Zend_Registry::isRegistered('config') )
		{
			if ( Zend_Registry::get('config')->tb->cliente && Zend_Registry::get('config')->tb->inscricao )
			{
				$this->tabelaCliente = new Model_Data(Zend_Registry::get('config')->tb->cliente);
				$this->tabelaInscricao = new Model_Data(Zend_Registry::get('config')->tb->inscricao);
			} else
			{
				throw new Exception('Tabela inválida');
			}
		} else 
		{
			throw new Exception('Config inválido');
		}
		
		if ( $item )
			$this->cadastroItem = $item;
		
		if ( $id > 0 )
			$this->setId($id);
	}
	
	public function setId ($id)
	{
		$this->id = $id;
	}
	
	public function getId ()
	{
		if ( !$this->id )
		{
			throw new Exception('Id não definido'. (int)$this->id);
		}

		return $this->id;
	}
	
	public function getDados ()
	{
		if ( !$this->id )
			return false;
			
		if ( $this->dados && $this->dados['id_cliente'] )
			return $this->dados;
			
		$where = $this->tabelaCliente->_table()->getAdapter()->quoteInto('id_cliente = ?',$this->id);
		$res = $this->tabelaCliente->_table()->fetchRow($where);

		if ( $res )
		{
			$fones = $this->getFones();
			$this->setDados($res->toArray());
			$this->setDados($this->getEndereco());
			$this->setDados($fones);
			$this->setDados($this->_getDadosInscricao());
		} else
		{
			throw new Exception('Usuário não existe');
		}

		return $this->dados;
	}
	
	protected function _getDadosInscricao ()
	{
		if ( $this->dados['id_turma'] )
			return $this->dados;
			
		$where = $this->tabelaInscricao->_table()->getAdapter()->quoteInto('id_cliente = ?',$this->id);
		$res = $this->tabelaInscricao->_table()->fetchRow($where);

		if ( $res )
			$this->setDados($res->toArray());
			
		return $this->dados;
	}
	
	public function setDados($dados)
	{
		$this->dados = array_merge($this->dados,$dados);
	}
	
	public function novo ($dados)
	{
		$res = $this->novoCliente($dados);
		
		if ( $res > 0 )
		{
			$this->setId($res);
			$res2 = $this->novaInscricao($dados);
			
			if ( !$res2 )
			{
				$this->tabela->del($res);
			}
			$this->setDados($dados);
			$this->enviaEmailInscricao();
			return $res;
			
		} else
		{
			return $res; 
			//throw new Exception('Erro ao incluir usuário: ' . $this->tabela->getErro());
		}
	}
	
	protected function novoCliente ($dados)
	{
		$this->tabelaCliente->_notNull($this->notNullCliente);
		$this->tabelaCliente->_required($this->requiredCliente);
		
		$usuario = $this->cadastroItem->getDados();
		
		if ( !$dados['id_cliente_tipo'] )
			$dados['id_cliente_tipo'] = self::CLIENTE_TIPO_DEFAULT;
			
		if ( !$dados['id_tipo_cobranca'] )
			$dados['id_tipo_cobranca'] = 6;
			
		if ( $dados['cpf'] )
			$dados['cpf'] = preg_replace('/[^0-9]/', '', $dados['cpf']);
			
		if ( !$dados['id_cadastro'] > 0 )
			$dados['id_cadastro'] = $this->cadastroItem->getId();
			
		if ( !$dados['nome'] )
			$dados['nome'] = $usuario['nome'];
			
		if ( !$dados['rg'] )
			$dados['rg'] = $usuario['rg'];
			
		$res = $this->tabelaCliente->edit(NULL,$dados,NULL,Model_Data::NOVO);
		
		if ( $res > 0 )
		{
			$this->setId($res);
			
			if ( is_array($dados['fones']) )
			{
				foreach ($dados['fones'] as $k => $v) 
					$this->addFone($v['ddd'], $v['numero'],$k);
			}
			
			if ( strlen($dados['email3']) > 0 )
				$this->addEmail($dados['email3']);
				
			if ( strlen($dados['msn']) > 0 )
				$this->addEmail($dados['msn'], Cadastro_Abstract::TIPO_EMAIL_MSN);
				
			$this->addEndereco($dados);
		}
		return $res;
	}
	
	public function novaInscricao ($dados)
	{
		$this->tabelaInscricao->_notNull($this->notNullInscricao);
		$this->tabelaInscricao->_required($this->requiredInscricao);
		
		if ( !$dados['id_status'] )
			$dados['id_status'] = self::STATUS_ATIVO;
			
		if ( !$dados['id_cliente'] )
			$dados['id_cliente'] = $this->getId();
			
		if ( !$dados['ip'] )
			$dados['ip'] = $_SERVER['REMOTE_ADDR'];
		
		$res = $this->tabelaInscricao->edit(NULL,$dados,NULL,Model_Data::NOVO);

		return $res;
	}
	
	public function edita ($dados)
	{
		/*$this->tabela->_required($this->required);
		//$this->tabela->_notNull($this->notNull);

		if ( $dados['senha'] )
			$dados['senha'] = md5($dados['senha']);
		
		$res = $this->tabela->edit($this->getId(),$dados,NULL,Model_Data::ATUALIZA);
		
		if ( $res > 0 && $dados['telefone'] )
		{
			if ( $dados['id_telefone'] > 0 )
				$this->editaFone($dados['id_telefone'], $dados);
			else 
				$this->addFone($dados['ddd'],$dados['telefone']);
		}
		
		return $res;*/
	}
	
	public function addFone ($ddd,$fone,$tipo=NULL) 
	{
		$filter = new Zend_Filter_Digits();
		$ddd  = $filter->filter($ddd);
		$fone = $filter->filter($fone);
		
		if ( strlen($ddd) > 0 && strlen($fone) > 0 )
		{
			$tabelaFone = new My_Table(Zend_Registry::get('config')->tb->telefone);
			$res2 = $tabelaFone->insert(array(
											'ddd' => $ddd,
											'numero' => $fone,
											'id_telefone_tipo' => (int) $tipo,
											'id_cliente' => $this->getId()
										));

			if ( $res2 )
			{
				$tabFlFone = new My_Table(Zend_Registry::get('config')->tb->fl_cliente_telefone);
				$res3 = $tabFlFone->insert(array(
											'id_telefone' => $res2,
											'id_cliente' => $this->getId()
											));
			}
			
			if ( !$res3 )
			{
				$tabelaFone->delete('id_telefone = '. $res2);
				return false;
			}
		}
		
		return $res2;
	}
	
	public function addEndereco ($dados,$tipo=NULL) 
	{
		$this->editaEndereco($dados,$tipo);
	}
	
	public function editaEndereco ($dados,$tipo=NULL) 
	{
		$required = array('id_endereco_tipo','logradouro','numero','compl','bairro','cep','cidade','uf','id_cliente');
		$notNull  = array('logradouro','numero','bairro','cep','cidade','uf','id_cliente');

		$end = $this->getEndereco();
		
		if ( !$end['id_endereco'] )
		{
			$opcao = Model_Data::NOVO;
		} else
		{
			$opcao = Model_Data::ATUALIZA;
			$notNull = array();
		}
		
		$dados['id_tipo_telefone'] = (int) $tipo;
		$dados['cep'] = preg_replace('/[^0-9]/', '', $dados['cep']);
		$dados['id_cliente'] = $this->getId();
			
		$tabela = new Model_Data(Zend_Registry::get('config')->tb->endereco, NULL, NULL, $required, $notNull);
		$res2 = $tabela->edit($end['id_endereco'],$dados,NULL,$opcao);
		
		return $res2;
	}	
	
	protected function getEndereco ()
	{
		$table = $this->tabelaCliente->_table()->getAdapter()->select();
		$table->from(array('E' => Zend_Registry::get('config')->tb->endereco))
			  ->where('E.id_cliente = ?', $this->getId());
			  
		$result = (array)$table->query()->fetchObject();
		//$this->setDados($result);
		
		return $result;
	}	
	
	public function editaFone ($id,$dados)
	{
		$ddd 	  = preg_replace('/[^0-9]/', '', $dados['ddd']);
		$telefone = preg_replace('/[^0-9]/', '', $dados['telefone']);
		
		$bind = array(
					'ddd' 	   => $ddd,
					'numero' => $telefone
					);
		
		$tabela = new Model_Data(Zend_Registry::get('config')->tb->telefone,NULL,NULL,array_keys($bind));
		return $tabela->edit($id,$bind,NULL,Model_Data::ATUALIZA);
	}
	
	public function delFone($id)
	{
		$tabela = new My_Table(Zend_Registry::get('config')->tb->telefone);
		$where = $tabela->getAdapter()->quoteInto('id_telefone = ?', $id);
		return $tabela->delete($where);
	}
	
	public function getFones ()
	{
		$fones['fones'] = array();
		
		$tabela = $this->tabelaCliente->_table()->getAdapter()->select();
		$tabela->from(array('FTC' => Zend_Registry::get('config')->tb->fl_cliente_telefone))
			   ->joinLeft(array('F' => Zend_Registry::get('config')->tb->telefone), 'FTC.id_telefone = F.id_telefone')
			   ->where('FTC.id_cliente = ?', $this->getId());
		
		$res = $tabela->query(Zend_DB::FETCH_OBJ);
		$result = $res->fetchAll();
		
		if ( $result )
		{
			foreach ( $result as $row )
			{
				$fones['fones'][$row->id_telefone_tipo] = (array) $row;
			}
		}
			   
		return $fones;
	}
	
	public function addEmail ($email,$tipo=NULL) 
	{
		$filter = new Zend_Filter_StripTags();
		$email = $filter->filter($email);
		$email = strtolower($email);
		
	   $x = '\d\w!\#\$%&\'*+\-/=?\^_`{|}~';
	
	   $validate = count($e = explode('@', $email, 3)) == 2
		   && strlen($e[0]) < 65
		   && strlen($e[1]) < 256
		   && preg_match("#^[$x]+(\.?([$x]+\.)*[$x]+)?$#", $e[0])
		   && preg_match('#^(([a-z0-9]+-*)?[a-z0-9]+\.)+[a-z]{2,6}.?$#', $e[1]);		
		
		if ( $validate )
		{
			if ( NULL === $tipo )
				$tipo = Cadastro_Abstract::TIPO_EMAIL_DEFAULT;
			
			$tabela = new My_Table(Zend_Registry::get('config')->tb->email);
			$res2 = $tabela->insert(array(
										'email' => $fone,
										'principal' => 2,
										'tipo' => $tipo,
										'id_cliente' => $this->getId()
										));

			if ( $res2 )
			{
				$tabFl = new My_Table(Zend_Registry::get('config')->tb->fl_cliente_email);
				$res3 = $tabFl->insert(array(
											'id_email' => $res2,
											'id_cliente' => $this->getId()
											));
			}
			
			if ( !$res3 )
			{
				$tabela->delete('id_email = '. $res2);
				return false;
			}
		}
		
		return $res2;
	}	
	
	protected function enviaEmailSenha ($senha)
	{
		$dados = $this->getDados();
		$assunto = 'Sua nova senha';
		$msg = '
Sua nova senha está ok!

Entre agora em: '. Zend_Registry::get('config')->www->host .'/default/login/

Login: '.$dados['email'] .'
Senha: '. $senha .' 

Obrigado.

Equipe Entelco
www.entelco.com.br';
		
		return $this->_enviaEmail($dados['email'], $msg, $assunto);
	}
	
	protected function enviaEmailInscricao ()
	{
		return ;
		$cod = $this->codigoAtivacao();
		$dados = $this->getDados();
		$assunto = 'Ativação de cadastro';
		$msg = '
Obrigado por se cadastrar em nosso site.
Seu cadastro está quase pronto. Para ativá-lo, basta clicar no link abaixo ou copiar e colar a url no browser.

'. Zend_Registry::get('config')->www->host .'/default/login/ativa?&u='. md5($dados['id_cadastro_ativacao'].$this->getId()) .'&a='. $cod .'

Código de ativação: '.$cod .'

Obrigado.

Equipe Entelco
www.entelco.com.br';
		
		return $this->_enviaEmail($dados['email'], $msg, $assunto);
	}
	
	protected function _enviaEmail ($email,$msg,$assunto=NULL,array $opcoes=NULL)
	{
		Zend_Loader::loadClass('Zend_Mail');
		Zend_Loader::loadClass('Zend_Mail_Transport_Smtp');
		
		if ( Zend_Registry::get('config')->email->cadastro->host )
		{
			$host = Zend_Registry::get('config')->email->cadastro->host;
			$from = Zend_Registry::get('config')->email->cadastro->user;
			$name = Zend_Registry::get('config')->email->cadastro->fromName;
			
			$config = array('auth' 		=> 'login',
            		        'username' 	=> Zend_Registry::get('config')->email->cadastro->user,
                    		'password' 	=> Zend_Registry::get('config')->email->cadastro->password);
		} else 
		{
			$host = Zend_Registry::get('config')->email->default->host;
			$from = Zend_Registry::get('config')->email->default->user;
			$name = Zend_Registry::get('config')->email->default->fromName;
			
			$config = array('auth' 		=> 'login',
            		        'username' 	=> Zend_Registry::get('config')->email->default->user,
                    		'password' 	=> Zend_Registry::get('config')->email->default->password);
		}
		
		$tr   = new Zend_Mail_Transport_Smtp($host,$config);
	    $mail = new Zend_Mail('utf-8');
	    $mail->setFrom($from, $name);
		$mail->addTo($email);
		
		if ( $assunto )
			$mail->setSubject($assunto);
			
		if ( $opcoes['isHTML'] === true )
			$mail->setBodyHtml($msg);
		else
			$mail->setBodyText($msg);
			
		return $mail->send($tr);
	}
}