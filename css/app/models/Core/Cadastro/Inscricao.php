<?php
require_once 'app/models/Error/Error.php';
require_once 'app/models/Error/Success.php';
require_once 'app/models/Core/Cadastro/Abstract.php';

class Cadastro_Inscricao extends Cadastro_Abstract
{
	/**
	 * Tabela principal do cadastro 
	 * @var string
	 */
	protected $tabela;
	
	/**
	 * Classe do Item
	 * @param Cadastro_Item_Abstract $classeItem
	 */
	static public $classeItem = 'Cadastro_Item_Inscricao';
	
	public function __construct(Zend_Db_Adapter_Abstract $db)
	{
		if ( Zend_Registry::isRegistered('config') )
		{
			if ( Zend_Registry::get('config')->tb->inscricao )
				$this->tabela = Zend_Registry::get('config')->tb->inscricao;
			else
				throw new Exception('Tabela inválida');
		} else 
		{
			throw new Exception('Config inválido');
		}
		
		parent::__construct($db);
	}
	
	/**
	 * 
	 * Criar um novo usuário
	 * @param array $dados
	 * @throws Exception
	 * @return Cadastro_Item_Usuario
	 */
	static public function novo ($dados,Cadastro_Item_Abstract $item)
	{
		self::carregaClasseUsuario();

		$class = new self::$classeItem(NULL,$item);
		
		if ( $class instanceof Cadastro_Item_Abstract )
		{
			$res = $class->novo($dados);
			if ( $res > 0 )
				return $class;
			else
				return $res;
		} else 
		{
			throw new Exception('A classe do item não é válida');
		}
	}
	
	static public function carregaClasseUsuario() 
	{
		if ( !class_exists(self::$classeItem) )
			return require_once CORE.'/'. str_replace('_', '/', self::$classeItem) .'.php';
	}
	
	public function apaga ($id)
	{
		
	}
}