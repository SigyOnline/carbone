<?php
abstract class Cadastro_Permissao_Abstract
{
	protected $regras = array();
	
	const REGRA_DEFAULT = 'default';
	
	public function addRegra($regra, $grupo=NULL) 
	{
		if ( $grupo === NULL )
			$grupo = self::REGRA_DEFAULT;
			
		$this->regras[$grupo][$regra] = true;
	}
}