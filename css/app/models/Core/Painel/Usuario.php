<?php
require_once CORE.'/Painel/Abstract.php';

class Painel_Usuario extends Painel_Abstract
{
	const ACESSOS_ULTIMO = 'ultimo';
	
	const ACESSOS_ULTIMOS_5 = 'ultimo5';
	
	const ACESSOS_TODOS = 'todos';
	
	protected $caminhoDashboardItens = array();
	
	protected $prefixoDashboardItens = 'MyCore_Painel_Item_';
	
	protected $dashboardItens = array();
	
	/**
	 * Usuário logado
	 * @var Cadastro_Usuario
	 */
	protected $usuario;
	
	/**
	 * Adaptador de banco de dados
	 * @var Zend_Db_Adapter_Abstract
	 */
	protected $db;
	
	public function __construct (Cadastro_Abstract $usuario, Zend_Db_Adapter_Abstract $db)
	{
		$this->usuario = $usuario;
		$this->db = $db;
		$this->caminhoDashboardItens = 'app/models/MyCore/Painel/Item';
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $tipo
	 * @return Zend_Db_Table_Rowset_Abstract
	 */
	public function acessos ($tipo=NULL)
	{
		require_once 'app/models/My_Table.php';
		$tabela = new My_Table(Zend_Registry::get('config')->tb->acessos_login);
		$where = $tabela->select();
		$login = $this->usuario->getAuth();
		
		$where->where('login = ?',$login->email)
			   ->where('tipo_login = ?',Cadastro_Usuario::TIPO_LOGIN);

		switch ($tipo) 
		{
			case self::ACESSOS_TODOS:
				$res = $tabela->fetchAll($where,'ultimo_acesso DESC');
			break;
			
			case self::ACESSOS_ULTIMO:
				$res = $tabela->fetchAll($where,'ultimo_acesso DESC',1,1);
			break;
			
			case self::ACESSOS_ULTIMOS_5:
				$res = $tabela->fetchAll($where,'ultimo_acesso DESC',5);
			break;
		}
		
		return $res;
	}
	
	public function getBemVindo ()
	{
		$dados = $this->usuario->getAuth();
		return $dados->nome;
	}
	
	public function getInfo ()
	{
		$acesso = $this->acessos(self::ACESSOS_TODOS);
		if ( $acesso )
			$acesso = $acesso->toArray();
			 
		$dados = $this->usuario->getAuth();
		//$linkAcesso = '/default/painel/acessos';
		$linkAcesso = '#';
		$dataAcesso = ($acesso[1]?new Zend_Date($acesso[1]['ultimo_acesso']):'--');
		$ipAcesso = ($acesso[1]?$acesso[1]['ip']:'--');
		$acessoNumero = count($acesso);
		require_once CORE_HELPER_DEFAULT.'/Painel/Usuario/Info.php';
		return;
	}
	
	public function dashboard()
	{
		if ( sizeof($this->dashboardItens) )
			return $this->dashboardItens;

		Cadastro_Usuario::carregaClasseUsuario();
		$classeUsuario = Cadastro_Usuario::$classeItem;
		$usuario = $this->usuario->getAuth();
		$usuario = new $classeUsuario($usuario->id_usuario);
			
		$componente= new Zend_Db_Select(Zend_Registry::get('db'));
	    $componente->from (array('C'=>Zend_Registry::get('config')->tb->componente))
	     		->joinLeft(array('CS' =>Zend_Registry::get('config')->tb->fl_componente_usuario),
				    		'CS.id_componente = C.id_componente',
				   	  		array('id_componente','id_fl_componente_usuario','id_usuario','config'))
				->where('CS.id_usuario = ?', (int)$usuario->getId());
		$componente = $componente->query(Zend_Db::FETCH_OBJ);
        $query = (array)$componente->fetchAll();

        //foreach ($query as $var )
		//print_r($var->componente);

		
		try {
			//$dir = scandir($this->caminhoDashboardItens);
			
			foreach ( $query as $row )
			{
				$item = $row->componente .'.php';
				if ( !is_file($this->caminhoDashboardItens . DIRECTORY_SEPARATOR . $item) )
					continue;
					
				require_once $this->caminhoDashboardItens . DIRECTORY_SEPARATOR . $item;
				
				$className = basename($item,'.php');
				$className = $this->prefixoDashboardItens . $className;
				
				if ( class_exists($className) )
				{
					$class = new $className($usuario,$row->id_fl_componente_usuario);
					
					if ( $class instanceof Painel_Item_Interface )
						$this->dashboardItens[] = $class;
					else
						continue;
				} else 
				{
					throw new Exception("Class '{$className}' doesn't exist.");
				}
			}

			return $this->dashboardItens;
			
		} catch (Exception $e)
		{
			throw new Exception($e);
			return false;
		}
		
	}
}