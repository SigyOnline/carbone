<?php
abstract class Error_Abstract
{
	protected $code;
	
	protected $message;
	
	protected $pattern = '<div class="error">%s</div>';
	
	protected $decorator = '%s';
	
	public function __construct ($message=NULL,$code=NULL)
	{
		if ( NULL !== $message )
			$this->addMessage($message);
	}
	
	public function addMessage ($message)
	{
		$this->message[] = $message;
		return $this;
	}
	
	public function render ()
	{
		foreach ( $this->message  as $m )
			$message .= sprintf($this->decorator,$m);//. PHP_EOL;
			
		return sprintf($this->pattern,$message);
	}
	
	public function __toString()
	{
		return ''. $this->render();
	}
}