<?php
require_once 'app/models/Error/Abstract.php';

class Error2 extends Error_Abstract
{
	protected $pattern = '<div class="alert alert-danger">%s</div>';
}