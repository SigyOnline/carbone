<?php
require_once 'app/models/Error/Abstract.php';

class Success extends Error_Abstract
{
	protected $pattern = '<div class="alert alert-success">%s</div>';
}