<?php
function printError ($error)
{
	return '<div class="error">'. htmlentities(utf8_decode($error)) .'</div>';
}


function printDadosFicha ($dado,$tipo=false)
{
	if ( empty($dado) )
		$dado = '--';
		
	switch (strtolower($tipo)) 
	{
		case 'id':
			return sprintf("%06d",$dado);
		break;
		
		case 'cpf':			
			return printDadosFicha(mascaraCpf($dado));
		break;
		
		case 'nascimento':
		case 'falecimento':
			return printDadosFicha(printData($dado));
		break;
		
		case 'ddd':
			return printDadosFicha("($dado)");
		break;
		
		case 'tel':
			return printDadosFicha(printTel($dado));
		break;
		
		case 'site':
			return printDadosFicha(rtrim(ltrim($dado,'http://'),'/'));
		break;
		
		default:
			return "<b>$dado</b>";
		break;
	}
}

function mascaraCpf($cpf){
    $cpf1 = str_split($cpf, 3);
    $fcpf = array_pop($cpf1);
    return implode('.', $cpf1).'-'.$fcpf;
}

function printTel($tel)
{
	$tel1 = substr($tel, 0,strlen($tel) - 4);
	$tel2 = substr($tel, strlen($tel) - 4, strlen($tel));
	
	return $tel1 .'-'. $tel2;
}

function printData ($data, $mascara='d/m/Y')
{
	return date($mascara, strtotime($data));
}

function checked ($data, $val)
{
	return ($data == $val?'<img src="/images/fichas/check.png" width="16" height="16">':'<img src="/images/fichas/unchecked.gif" width="13" height="13">');
}

function ListCheckbox($seletor = "", $checked = "",$custom = NULL,$nome = "custom",$numtds = 1)
{
	switch ($seletor) 
	{
		/*
		case "curso" :
			$values['01'] = "Técnico Pleno em Eletrotécnica";
			$values['02'] = "Técnico Pleno em Eletronica";
			$values['03'] = "Técnico Pleno em Telecomunicações";
			$values['04'] = "Técnico em Segurança no Trabalho";
		break;
		*/
		
		case "custom" :
			$values = $custom;
			$seletor = $nome;
		break;
	}
	
	$check = "";
	$cont = 0;
	
	while ( list($key,$val) = each($values) )
	{
		 
		if ( is_array($checked) )
		{
    		if ( in_array($key, $checked) )
    			$a = "checked=checked";
    		else
    			$a = NULL;
		} elseif ( $key == $checked )
			$a = "checked=checked";
		
		$check .= "<input ".$a." type=checkbox name=".$seletor."[] value=".$key."><small>".$val."</small><br>";
		//$check .= "<input ".$a." type=checkbox name=".$seletor."[] value=".$key."><small>".$val."</small><br> \n";
		
		if ( $cont == $numtds )
		{
			//$check .= "</tr><tr>";
			$cont = 0;
		} else
			$cont++;
		
	}
	return $check;
}

function ListBox($seletor = "", $selected = "",$custom=NULL,$tag=NULL)
{
	switch ($seletor) 
	{
		case "agerating" :
			$values['16|19'] = "16 a 19";
			$values['19|23'] = "19 a 23";
			$values['23|27'] = "22 a 27";
			$values['27|33'] = "27 a 33";
			$values['33|40'] = "33 a 40";
			$values['41']    = "acima de 41";
		break;
		
		case "pay" :
			$values['380|700']   = "R$ 380,00 a R$ 700,00";
			$values['700|1200']  = "R$ 700,00 a R$ 1200,00";
			$values['1200|1700'] = "R$ 1200,00 a R$ 1700,00";
			$values['2200|3000'] = "R$ 2200,00 a R$ 3000,00";
			$values['3000|4000'] = "R$ 3000,00 a R$ 4000,00";
			$values['4000']      = "acima de R$ 4000,00";
		break;
		
		case "day" :
			for ($i=1;$i<32;$i++)			
				$values["{$i}"] = $i;			
		break;
		
		case "Gender" :
			$values['M'] = "Masculino";
			$values['F'] = "Feminino";
		break;

		case "estado_civil" :
			$values['Solteiro'] = "Solteiro";
			$values['Casado'] = "Casado";
			$values['Viúvo'] = "Viúvo";
			$values['Divorciado'] = "Divorciado";
		break;

		case "hour" :
			for ($i=0;$i<24;$i++)
			{
				if (strlen($i) == 1)
					$i = "0". $i;

				$values["{$i}"] = $i;
			}	
		break;
		
		case "minute" :
			for ($i=0;$i<60;$i++)
			{
				if (strlen($i) == 1)
					$i = "0". $i;

				$values["{$i}"] = $i;
			} 
		break;

		case "month2" :
			for ($i=1;$i<13;$i++)			
				$values["{$i}"] = date('M',strtotime('1999-'.$i.'-01'));			
		break;
		
		case "month" :
			$values['01'] = "Janeiro";
			$values['02'] = "Fevereiro";
			$values['03'] = "Março";
			$values['04'] = "Abril";
			$values['05'] = "Maio";
			$values['06'] = "Junho";
			$values['07'] = "Julho";
			$values['08'] = "Agosto";
			$values['09'] = "Setembro";
			$values['10'] = "Outubro";
			$values['11'] = "Novembro";
			$values['12'] = "Dezembro";
		break;

		case "states" :	
			$values['AC'] =	'Acre';
			$values['AL'] =	'Alagoas';
			$values['AM'] =	'Amazonas';
			$values['AP'] =	'Amapá';
			$values['BA'] =	'Bahia';
			$values['CE'] =	'Ceará';
			$values['DF'] =	'Distrito Federal';
			$values['ES'] =	'Espírito Santo';
			$values['GO'] =	'Goiás';
			$values['MA'] =	'Maranhão';
			$values['MG'] = 'Minas Gerais';
			$values['MS'] = 'Mato Grosso do Sul';
			$values['MT'] = 'Mato Grosso';
			$values['PA'] = 'Pará';
			$values['PB'] = 'Paraíba';
			$values['PE'] = 'Pernambuco';
			$values['PI'] = 'Piauí';
			$values['PR'] = 'Paraná';
			$values['RJ'] = 'Rio de Janeiro';
			$values['RN'] = 'Rio Grande do Norte';
			$values['RO'] = 'Rondônia';
			$values['RR'] = 'Roraima';
			$values['RS'] = 'Rio Grande do Sul';
			$values['SC'] = 'Santa Catarina';
			$values['SE'] = 'Sergipe';
			$values['SP'] = 'São Paulo';
			$values['TO'] = 'Tocantins';
			//$values['NA'] = 'Outros';
		break;
		
		case "yearF" :
			for ($i=date("Y");$i<(date("Y")+15);$i++)			
				$values["{$i}"] = $i;
		break;
		
		case "yearI" :
			for ($i=(date("Y")+1);$i>(date("Y")-2);$i--)				
				$values["{$i}"] = $i;
		break;
		
		case "yearP" :
			for ($i=date("Y");$i>(date("Y")-100);$i--)			
				$values["{$i}"] = $i;			
		break;
		
		case "custom" :
			$values = $custom;
		break;
	}
	
	$form = "";
	
	while ( list($key,$val) = each($values) )
	{
		$a  = ($key == $selected)
					?" selected "
					:"";
					
		$form .= "<option $tag value='{$key}'$a title='{$val}'>{$val}</option>";
		//$form .= "<option value='{$key}'$a>{$val}</option> \n";
	}
	return $form;
}
function GeraSenha ($letters = 8, $nums = 2, $word = '')
{
    $chars = array(array('b' , 'd' , 'f' , 'g' , 'h' , 'k' , 'l' , 'm' , 'n' , 'p' , 'r' , 's' , 't' , 'v' , 'w' , 'z') ,
                   array('a' , 'e' , 'i' , 'o' , 'u')
                   );
    unset($pw, $nm);
    foreach (range(0, $letters - 1) as $i)
        $pw .= $chars[$i % 2][array_rand($chars[$i % 2])];
    foreach (range(1, $nums) as $i)
        $nm .= rand(0, 9);
    $keys = array(1 => ucfirst($word) , ucfirst($pw) , $nm);
    $rand_keys = array_rand($keys, 3);
    return $keys[$rand_keys[0]] . $keys[$rand_keys[1]] . $keys[$rand_keys[2]];
}	

function Thumb($imagem="",$gerar=0,$tamanho=0,$extras="")
{
	//if ( !strpos($imagem,'http://') )
		//$imagem = 'http://' . $_SERVER['HTTP_HOST'] . '/'. (ltrim($imagem,'/'));

	$imagem  = ltrim($imagem,'/');
	$imagem  = ( empty($imagem) || !file_exists($imagem) )? SEM_IMAGEM : $imagem;
	$img     = @getimagesize($imagem);
	if ( !strpos($imagem,'http://') )
		$imagem = 'http://' . $_SERVER['HTTP_HOST'] . '/'. (ltrim($imagem,'/'));
	//$imagem  = 'http://' . $_SERVER['HTTP_HOST'] . '/'. (ltrim($imagem,'/'));
	$tamanho = ( $tamanho == 0 )? TAMANHO_FOTO_DESCRICAO : $tamanho;
		
	//pegando os tamanhos originais (width e height)
	if ($img[0] > $img[1]) {
		$tamanho_orig = $img[0];
	} else {
		$tamanho_orig = $img[1];
	}
	$porcento = ( $tamanho_orig > $tamanho )?((100 * $tamanho)/$tamanho_orig)/100:1;
		
	if ( $gerar == 0 )
		return $porcento;

	if ( $gerar == 1 ) 
	{
		$w = $img[0]*$porcento;
		$h = $img[1]*$porcento;
		
		$gerado = "<img src=\"$imagem\" width=\"$w\" height=\"$h\" border=\"0\"$extras>";
		return $gerado;
	}
	
	if ( $gerar == 2 ) 
	{
		$w = $tamanho;
		$h = $tamanho;
		
		$gerado = "<img src=\"$imagem\" width=\"$w\" height=\"$h\" border=\"0\"$extras>";
		return $gerado;
	}	
}
