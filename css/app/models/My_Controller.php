<?php
require_once MYCORE. '/SEO.php';

class My_Controller extends Zend_Controller_Action
{
    public function init ()
    {
    	$this->_helper->getHelper('viewRenderer')->setViewSuffix('php');
    	// if ( $this->getRequest()->getControllerName() == 'index' )
    		//$this->_redirect('painel');
    	
    	$this->config  = Zend_Registry::get('config');
		$this->db      = Zend_Registry::get('db');
		$this->session = Zend_Registry::get('session');
		$this->view->pagetitle 		= Zend_Registry::get('config')->www->pagetitle;
		$this->view->baseModule 	= $this->getRequest()->getModuleName();
		$this->view->baseController = $this->getRequest()->getControllerName();
		$this->view->baseAction 	= $this->getRequest()->getActionName();
		$this->view->baseUrl 		= Zend_Registry::get('config')->www->baseurl;
		$this->view->baseImg 		= Zend_Registry::get('config')->www->baseimg;
		$this->view->id = $this->id = $this->getRequest()->getParam('id');

		if ( $this->getRequest()->getModuleName() == 'admin')
		{
			if ($this->getRequest()->getControllerName() != 'import')
			{
				//require_once CORE.'/Cadastro/Usuario.php';
				require_once MYCORE . '/Cadastro/Admin.php';
				$usuario = new Cadastro_Admin($this->db);
				$auth = $usuario->getAuth();
				$this->auth = $auth;
				$this->me = $this->view->me = $auth;
				Zend_Registry::set('me',$this->view->me);
				if ( is_null($auth) && ( $this->getRequest()->getControllerName() !== 'login' 
						&& $this->getRequest()->getControllerName() !== 'error' && $this->getRequest()->getControllerName() !== 'integrarc') 
				   )
				{
					return $this->_redirect($this->getRequest()->getModuleName()  . '/login/?r='.urlencode($this->getRequest()->get('REQUEST_URI')));
				}
			}
		}
		
		$this->view->seo = new SEO();
		Zend_Registry::set('seo', $this->view->seo);
		require_once 'app/models/MyCore/Cadastro/Consultafiltro.php';
		$this->cfiltro = new Cadastro_Consultafiltro($this->db);
// 		$this->view->fases_header = $cfiltro->getFase();
		
		/*if($auth->id_grupo != 1)
		{
			switch ($this->view->baseController) 
			{
				case 'faq':
				case 'grupo':
				case 'produto':
				case 'status':
				case 'linha':
				case 'categoria':
				case 'usuario':
				case 'produto/pesquisa':
				case 'faq/duvida':
				case 'assunto':
				     $this->_redirect('painel');
				break;
			}
		}*/
		$this->imovel = new Cadastro_Imovel($this->db);
		/* Banner */
		//$this->view->destaque = $this->imovel->getSuperDestaque();
		
		$this->ini();
    }
    
    public function ini ()
    {}
}