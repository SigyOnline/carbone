<?php
require_once CORE.'/Cadastro/Item/Inscricao.php';

class Produto_Factory 
{
	const PRODUTO_STATUS_ATIVO = 16;
	
	const PRODUTO_STATUS_INATIVO = 17;
	
	public static function factory($item,$tipo=NULL)
	{
		switch (true)
		{
			case $item instanceof Cadastro_Item_Inscricao :
				$select = new Zend_Db_Select(Zend_Registry::get('db'));
				$tabela = Zend_Registry::get('config')->tb->cliente;
				$tabRel = Zend_Registry::get('config')->tb->inscricao;
				
				$id = $item->getId();
				$dados = $item->getDados();
				
				$select = new Zend_Db_Select(Zend_Registry::get('db'));
				$select->from(array('T' => Zend_Registry::get('config')->tb->produto))
					   ->joinLeft(array('RT' => Zend_Registry::get('config')->tb->produto_turma),'T.id_produto = RT.id_produto',array('id_turma'))
					   ->where('RT.id_turma = ?',$dados['id_turma'])
					   ->where('T.id_status = ?',self::PRODUTO_STATUS_ATIVO);
					   
				$stm = $select->query(Zend_Db::FETCH_OBJ);
				$res = $stm->fetchAll();
				
				if ( $res )
				{
					require_once 'app/models/MyCore/Produto/Turma.php';
					$obj = array();
					
					foreach ($res as $row) 
					{
						if ( $row->estoque > 0 )
						{
							$o = new Produto_Turma($item);
							$o->setDados((array)$row);
							$o->setId($row->id_produto);
							$obj[] = $o;
						}
					}
					
					if ( sizeof($obj) )
					{
						return $obj;
					} else 
					{
						return false;
					}
				}
		
			break;
		}
		
		throw new Exception('Produto não encontrado');
	}
}