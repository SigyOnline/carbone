<?php
require_once CORE.'/Produto/Abstract.php';
require_once CORE.'/Cadastro/Item/Inscricao.php';
require_once 'app/models/My_Table.php';

class Produto_Turma extends Produto_Abstract
{
	protected $tabela;
	
	protected $nid = 'id_produto';	
	
	protected $nidRel = 'id_turma';
	
	protected $idTipoPagamento;
	
	public function __construct ($item,$id=NULL) 
	{
		if ( $item instanceof Cadastro_Item_Inscricao )
		{
			$this->setTabela(Zend_Registry::get('config')->tb->produto);
			$this->item = $item->getDados();
		}
		
		if ( is_array($item) )
		{
			$this->item = $item;
		}
		
		$this->setDados($this->item);
		
		if ( $id > 0 )
		{
			$this->setId($id);
		}
	}
	
	public function setDados($esp)
	{
		if ( !is_array($esp) )
			return;
			
		$this->dados = $this->dados + $esp;
		$this->preco = $this->dados['preco'];
	}
	
	public function getEstoque ()
	{
		return (int) $this->dados['estoque'];
	}
	
	public function getDesconto()
	{
		$desconto = 0;
		
		if ( $this->idTipoPagamento )
		{
			$select = $this->getTabela()->getAdapter()->select();
			$tblNome = $this->getTabela()->info(My_Table::NAME); 
			$select->from(array('TP' => Zend_Registry::get('config')->tb->promocao))
				   ->where('TP.id_formapagto = ?',$this->idTipoPagamento)
				   ->where("TP.{$this->nid} = ?",$this->getId());
				   
			$res = $select->query(Zend_Db::FETCH_OBJ)->fetchObject();
			
			if ( $res )
			{
				if ( $res->valor )
					$desconto = -$res->valor;
					
				if ( $res->porcentagem )
					$desconto += -($this->dados['valor']) * (($res->porcentagem/100));
			}
		}
		
		return $desconto;
	}
	
	public function getPreco ()
	{
		$this->preco = $this->dados['valor'];
		return (float) $this->preco + $this->getDesconto();
	}
	
	public function setFormaPagto ($esp) 
	{
		if ( $esp instanceof Pagamento_Cobranca_Abstract )
			$id = $esp->TIPO;
		else 
			$id = $esp;
			
		$this->idTipoPagamento = $id;
	}
	
	public function baixaEstoque ($qtde)
	{
		$qtde = (int) $qtde;
		$tabela = $this->getTabela()->info(My_Table::NAME);
		$relTurma = Zend_Registry::get('config')->tb->produto_turma;
		$turma = Zend_Registry::get('config')->tb->turma;
				
		$res = $this->getTabela()->getAdapter()->query(
						"UPDATE {$tabela} SET estoque = estoque-{$qtde} WHERE {$this->nid} = {$this->getId()}"
					);
					/*
		$res = $this->getTabela()->getAdapter()->query(
						"UPDATE {$turma} SET cadeiras = cadeiras-{$qtde} WHERE id_turma IN ". 
						"(SELECT id_turma FROM {$relTurma} WHERE {$this->nid} = {$this->getId()})"
					);*/
	}
	
	public function imprimiPreco ()
	{
		return number_format($this->getPreco(),2,',','.');
	}
}