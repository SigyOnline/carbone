<?php
require_once CORE.'/Cadastro/Abstract.php';
require_once 'app/models/MyCore/Cadastro/Interface.php';

class Cadastro_Categoria extends Cadastro_Abstract implements Cadastro_Interface
{
	protected $paginacao = true;
	
	protected $_config = array();
	
	protected $porPagina = 10;
	
	protected $linha;
	
	protected $required = array('id_categoria', 'categoria'
	);
	
	protected $notNull = array('categoria');
	
	protected $id;
	
	public function ini ()
	{
	
		$this->_config = array('tbImoveis' => Zend_Registry::get('config')->tb->imoveis,
							   'tbCarac' => Zend_Registry::get('config')->tb->caracteristica_imovel,
							   'tbCat' => Zend_Registry::get('config')->tb->categorias,
							   'tbFoto' => Zend_Registry::get('config')->tb->fotos, 
							   'tbVideo' => Zend_Registry::get('config')->tb->videos 
		);		
	}
	
	public function lista ($filtro = NULL, $order = NULL ) 
	{
    	$listagem = new Zend_Db_Select($this->db);
    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);
    	
    	$listagem->from($this->_config['tbCat']);

    	if ( $this->paginacao === true )
    	{
    		require_once 'Zend/Paginator.php';
    		
			$this->paginator = Zend_Paginator::factory($listagem);
			$this->paginator->setItemCountPerPage($this->porPagina);
			Zend_Paginator::setDefaultScrollingStyle('Sliding');
			Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginacao.php');
			$this->paginator->setCurrentPageNumber((int)$_GET['p']);
			$result = $this->paginator;
    	} else
    	{
    		$result = $listagem->query()->fetchObject();
    		//$listagem->query(Zend_Db::FETCH_OBJ)->rowCount();
    	}
    	return $result;
	}
	
	public function listagem ($filtro = NULL, $order = NULL )
	{
		$listagem = new Zend_Db_Select($this->db);
		$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);
		 
		$listagem->from($this->_config['tbCat'])
				 ->order('categoria ASC');
	
		$result = $listagem->query()->fetchAll();
		return $result;
	}
	
	public function paginar ($esp=true) 
	{
		$this->paginacao = (bool) $esp;
	}
	
	public function setId ($id)
	{
		$this->id = $id;
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function getDados($id)
	{
		$pag = $this->paginacao;
		$this->paginar(false);
		$res = current($this->lista(array('id_categoria = ?' => $id)));
		$this->paginar($pag);
		
		return $res;
	}
	

	public function novo ($dados) 
	{
		$res = $this->_salva($dados, Model_Data::NOVO);
		
		if ( $res > 0 )
		{
			$this->setId($res);
		}
		
		return $res;
	}
	
	public function edita ($id,$dados) 
	{
	
		$this->setId($id);
		$res = $this->_salva($dados, Model_Data::ATUALIZA,$id);
		
		return $res;
	}
	
	protected function _salva ($dados,$opt,$id=NULL)
	{
    	$tabela = new Model_Data(
    						$this->_config['tbCat'],
    						NULL,
    						NULL,
    						$this->required,
    						$this->notNull);
    							
    	$edt = $tabela->edit($id,$dados,NULL,$opt);
		
    	return $edt;
    }
	
	public function apaga ($id) 
	{
		$produto = new My_Table($this->_config['tbCat']);
		$produto->delete($produto->getAdapter()->quoteInto('id_categoria =?',$id));
		
		$produto = new My_Table($this->_config['tbCat']);
		//$faq = new Cadastro_Faq($this->db);
		return $produto->delete($produto->getAdapter()->quoteInto('id_categoria =?',$id));
	}
}
