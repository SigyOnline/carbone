<?php
require_once 'app/models/Model_Data.php';
require_once 'app/models/My_Table.php';
require_once 'app/models/Error/Error.php';
require_once 'app/models/Error/Success.php';
require_once CORE.'/Cadastro/Item/Abstract.php';
require_once CORE.'/Cadastro/Abstract.php';

Class Cadastro_Item_Usuario extends Cadastro_Item_Abstract
{
	/**
	 * status para cadastros ativos
	 */
	const STATUS_ATIVO = 11;
	
	/**
	 * Status de cadastros inativos
	 */
	const STATUS_INATIVO = 12;
	
	/**
	 * Status para cadastros novos, porém não ativados
	 */
	const STATUS_NOVO = 13;
	
	/**
	 * Status do código de ativação quando disponível ainda
	 */
	const ATIVACAO_STATUS_ATIVAR = 15;
	
	/**
	 * Status do código de ativação quando já usado
	 */
	const ATIVACAO_STATUS_ATIVADO = 14;
	
	/**
	 * 
	 * Instância Model Data
	 * @var Model_Data
	 */
	protected $tabela;
	
	/**
	 * 
	 * Campos requiridos pela tabela
	 * @var array
	 */
	protected $required = array('id_cadastro','id_status','nome','contato','email','senha','cpf_cnpj',
								'rg','skype','nascimento','token','criado','modificado');
	
	/**
	 * 
	 * Campos que não podem ser nulos
	 * @var array
	 */
	protected $notNull = array('id_cliente','email','senha');
	
	/**
	 * 
	 * Construtor
	 * @param int $id
	 */
	public function __construct($id=NULL)
	{
		if ( Zend_Registry::isRegistered('config') )
		{
			if ( Zend_Registry::get('config')->tb->cliente )
				$this->tabela = new Model_Data(Zend_Registry::get('config')->tb->cliente);
			else
				throw new Exception('Tabela inválida');
		} else 
		{
			throw new Exception('Config inválido');
		}
		
		if ( is_string($id) && strpos($id, '@') > 0 )
		{
			$id = $this->getIdByEmail($id);
		}
			
		if ( $id > 0 )
			$this->setId($id);
	}
	
	public function setId ($id)
	{
		$this->id = $id;
	}
	
	public function getId ()
	{
		if ( !$this->id )
			throw new Exception('Id não definido');

		return $this->id;
	}
	
	public function getIdByEmail($email)
	{
		$email = preg_replace('/[^0-9A-Za-z.@_]/', '', $email);
		$tabela = $this->tabela->_table();
		$res = $tabela->fetchRow(
							$tabela->select()->where('email = ?',$email)
						);
						
		if ( $res )
			return $res->id_cadastro;
	}
	
	public function getDados ()
	{
		if ( !$this->id )
			return false;
			
		if ( $this->dados && $this->dados['id_cadastro'] )
			return $this->dados;
			
		$where = $this->tabela->_table()->getAdapter()->quoteInto('id_cadastro = ?',$this->id);
		$res = $this->tabela->_table()->fetchRow($where);

		if ( $res )
		{
			$fones = $this->getFones();
			$emails = $this->getEmails();
			$this->setDados($this->getEndereco());
			$this->setDados($res->toArray());
			$this->setDados(array('cpf' => $res->cpf_cnpj));
			$this->setDados($fones);
			$this->setDados($emails);
		} else
		{
			throw new Exception('Usuário não existe');
		}

		return $this->dados;
	}
	
	public function setDados($dados)
	{
		$this->dados = array_merge($this->dados,$dados);
	}
	
	public function novo ($dados)
	{
		$this->tabela->_notNull($this->notNull);
		$this->tabela->_required($this->required);
		
		$res = $this->tabela->_table()->fetchRow(
							$this->tabela->_table()->select()->where('email = ?',$dados['email'])
				);
		if ( $res )
			return self::ERRO_INSERIR_DUPLICADO;
		
		$dados['id_status'] = self::STATUS_NOVO;
		$dados['senha'] = self::cripto($dados['senha']);
		
		$res = $this->tabela->edit(NULL,$dados,NULL,Model_Data::NOVO);
		
		if ( $res > 0 )
		{
			$this->setId($res);
			$res2 = $this->addFone($dados['ddd'], $dados['fone']);
			
			if ( !$res2 && ( $dados['ddd'] > 0 && $dados['fone'] > 0 ) )
			{
				$this->tabela->del($res);
				return Model_Data::ERRO_INSERIR_REGISTRO;
			}
			$this->setDados($dados);
			$this->codigoAtivacao();
			$this->enviaEmailCadastro();
			return $res;
			
		} else
		{
			return $res; 
			//throw new Exception('Erro ao incluir usuário: ' . $this->tabela->getErro());
		}
	}
	
	protected function codigoAtivacao ()
	{
		$dados = $this->getDados();
		
		if ( !$dados['codigo'] )
		{
			if ( $dados['id_status'] == self::STATUS_NOVO )
			{
				$ativacao = new My_Table(Zend_Registry::get('config')->tb->cadastro_ativacao);
				$where = $ativacao->select();
				
				$resultado = $ativacao->fetchRow($where
													->where('id_cadastro = ?', $this->getId())
													->where('id_status = ?',self::ATIVACAO_STATUS_ATIVAR)
												);
				
				if ( !$resultado )
				{
					$ativacao = '';
			
					for ($i=0;$i<40;$i++) 
						$ativacao .= chr(rand(0,127));
							
					$ativacao = preg_replace('/[^0-9A-Za-z]/', '', $ativacao);
					
					$dadosAtivacao = array(
										'id_status'   => self::ATIVACAO_STATUS_ATIVAR,
										'id_cadastro' => $this->getId(),
										'codigo'	  => $ativacao
									 );
					$ativacao = new Model_Data(Zend_Registry::get('config')->tb->cadastro_ativacao,NULL, NULL,array_keys($dadosAtivacao));
					$res = $ativacao->edit(NULL,$dadosAtivacao,NULL,Model_Data::NOVO);
					
					if ( $res > 0 )
					{
						$this->setDados(array(
										'codigo' => $dadosAtivacao['codigo'],
										'id_cadastro_ativacao' => $res
									  ));
					} else 
					{
						return false;
					}
				} else 
				{
					$dadosResultado = $resultado->toArray();
					$this->setDados(array(
									'codigo'=> $dadosResultado['codigo'],
									'id_cadastro_ativacao' => $dadosResultado['id_cadastro_ativacao']
									));
				}
				
				$dados = $this->getDados();				
			}
		}

		return $dados['codigo'];

	}
	
	public function ativaConta ($u,$cod)
	{
		$dados = $this->getDados();
		
		$tabela = new My_Table(Zend_Registry::get('config')->tb->cadastro_ativacao);
		$where = $tabela->select()
						->where("(md5(CONCAT(id_cadastro_ativacao,id_cadastro))) = ?",$u)
						->where('codigo = ?',$cod)
						->where('id_status = ?',self::ATIVACAO_STATUS_ATIVAR);
						//echo $where->assemble();
		$res = $tabela->fetchRow($where);
		
		/**
		 * @todo catalogar e exibir todos os tipos de erro
		 */
		if ( $res )
		{
			$this->setId($res->id_cadastro);
			$this->tabela->_required($this->required);
			if ( $this->tabela->edit($this->getId(),array('id_status' => self::STATUS_ATIVO), NULL, Model_Data::ATUALIZA) > 0 )
			{
				$model = new Model_Data($tabela);
				$model->_required(array('id_status'));
				$model->edit($res->id_cadastro_ativacao, array('id_status' => self::ATIVACAO_STATUS_ATIVADO), 
							 NULL, Model_Data::ATUALIZA);
							 
				return true;
			} else 
			{
				return false;
			}
		} else 
		{
			return false;
		}
	}
	
	public function reativarConta () 
	{
		$dados = $this->getDados();
		
		if ( $dados['id_status'] == self::STATUS_ATIVO )
			return false;
		
		if ( $dados['id_status'] == self::STATUS_INATIVO )
			$this->edita(array('id_status' => self::STATUS_NOVO));
			
		$ativacao = new My_Table(Zend_Registry::get('config')->tb->cadastro_ativacao);
		$where = $ativacao->select();
		
		$resultado = $ativacao->fetchRow($where
											->where('id_cadastro = ?', $this->getId())
											->where('id_status = ?',self::ATIVACAO_STATUS_ATIVAR)
										);
		if ( $resultado )
			$resultado->delete();
		
		return $this->enviaEmailCadastro();
	}
	
	public function edita ($dados)
	{
		$this->tabela->_required($this->required);
		//$this->tabela->_notNull($this->notNull);

		if ( strlen($dados['senha']) > 0 )
			$dados['senha'] = self::cripto($dados['senha']);
		else 
			unset($dados['senha']);
		
		$res = $this->tabela->edit($this->getId(),$dados,NULL,Model_Data::ATUALIZA);
		
		if ( sizeof($dados['fones']) )
		{
			foreach ($dados['fones'] as $key => $item) 
			{
				if ( $item['id_telefone'] > 0 )
					$this->editaFone($item['id_telefone'], $item);
				else 
					$this->addFone($item['ddd'],$item['numero'],$key);
			}
		}
				
		if ( sizeof($dados['emails']) )
		{
			while( list($k,$v) = each($dados['emails']) )
			{
				if ( strlen($v['email']) > 0 )
				{
					if ( $v['id_email'] > 0 )
						$this->editaEmail($v['id_email'],$v['email'],$k);
					else
						$this->addEmail($v['email'],$k);
				}
			}	
			//if ( strlen($dados['msn']) > 0 )
			//	$this->addEmail($dados['msn'], Cadastro_Abstract::TIPO_EMAIL_MSN);
		}		
		
		if ( $dados['logradouro'] )
		{
			$this->editaEndereco($dados);
		}
		
		return $res;
	}
	
	public function addFone ($ddd,$fone,$tipo=Cadastro_Abstract::TIPO_TEL_RES) 
	{
		$ddd  = preg_replace('/[^0-9]/', '', $ddd);
		$fone = preg_replace('/[^0-9]/', '', $fone);
		//$tipo     = preg_replace('/[^0-9]/', '', $tipo);
		if ( $ddd && $fone )
		{
			$tabelaFone = new My_Table(Zend_Registry::get('config')->tb->telefone);
			$res2 = $tabelaFone->insert(array(
											'ddd' => $ddd,
											'numero' => $fone,
											'id_telefone_tipo' => $tipo
										));

			if ( $res2 )
			{
				$tabFlFone = new My_Table(Zend_Registry::get('config')->tb->fl_cadastro_telefone);
				$res3 = $tabFlFone->insert(array(
											'id_telefone' => $res2,
											'id_cadastro' => $this->getId()
											));
			}
			
			if ( !$res3 )
			{
				$tabelaFone->delete('id_telefone = '. $res2);
				return false;
			}
		}
		
		return $res2;
	}
	
	public function addEmail ($email,$tipo=NULL) 
	{
		$filter = new Zend_Filter_StripTags();
		$email = $filter->filter($email);
		$email = strtolower($email);
		
	   $x = '\d\w!\#\$%&\'*+\-/=?\^_`{|}~';
	
	   $validate = count($e = explode('@', $email, 3)) == 2
		   && strlen($e[0]) < 65
		   && strlen($e[1]) < 256
		   && preg_match("#^[$x]+(\.?([$x]+\.)*[$x]+)?$#", $e[0])
		   && preg_match('#^(([a-z0-9]+-*)?[a-z0-9]+\.)+[a-z]{2,6}.?$#', $e[1]);		
		
		if ( $validate )
		{
			if ( NULL === $tipo )
				$tipo = Cadastro_Abstract::TIPO_EMAIL_DEFAULT;
			
			$tabela = new My_Table(Zend_Registry::get('config')->tb->email);
			$res2 = $tabela->insert(array(
										'email' => $email,
										'principal' => 2,
										'tipo' => $tipo
										//'id_cliente' => $this->getId()
										));

			if ( $res2 )
			{
				$tabFl = new My_Table(Zend_Registry::get('config')->tb->cadastro_email);
				$res3 = $tabFl->insert(array(
											'id_email' => $res2,
											'id_cadastro' => $this->getId()
											));
			}
			
			if ( !$res3 )
			{
				$tabela->delete('id_email = '. $res2);
				return false;
			}
		}
		
		return $res2;
	}
	
	public function editaEmail ($id,$email=NULL,$tipo=NULL)
	{
		$filter = new Zend_Filter_StripTags();
		$email = $filter->filter($email);
		$email = strtolower($email);
		
	   $x = '\d\w!\#\$%&\'*+\-/=?\^_`{|}~';
	
	   $validate = count($e = explode('@', $email, 3)) == 2
		   && strlen($e[0]) < 65
		   && strlen($e[1]) < 256
		   && preg_match("#^[$x]+(\.?([$x]+\.)*[$x]+)?$#", $e[0])
		   && preg_match('#^(([a-z0-9]+-*)?[a-z0-9]+\.)+[a-z]{2,6}.?$#', $e[1]);		
		
		if ( $validate )
		{
			$dados = array();
			
			$dados['email'] = $email;
			
			if ( NULL !== $tipo )
				$dados['tipo'] = $tipo;
			
			$tabela = new My_Table(Zend_Registry::get('config')->tb->email);
			$res2 = $tabela->update($dados,$tabela->getAdapter()->quoteInto('id_email = ?', $id));
		}
		
		return $res;
	}
	
	public function editaFone ($id,$dados)
	{
		$ddd 	  = preg_replace('/[^0-9]/', '', $dados['ddd']);
		$telefone = preg_replace('/[^0-9]/', '', $dados['numero']);
		$tipo     = preg_replace('/[^0-9]/', '', $dados['id_telefone_tipo']);
		
		$bind = array(
					'ddd' 	   => $ddd,
					'numero'   => $telefone,
					'id_telefone_tipo' => $tipo
					);
		
		$tabela = new Model_Data(Zend_Registry::get('config')->tb->telefone,NULL,NULL,array_keys($bind));
		return $tabela->edit($id,$bind,NULL,Model_Data::ATUALIZA);
	}
	
	public function getFones ()
	{
		$fones['fones'] = array();
		
		$tabela = $this->tabela->_table()->getAdapter()->select();
		$tabela->from(array('FTC' => Zend_Registry::get('config')->tb->cadastro_telefone))
			   ->joinLeft(array('F' => Zend_Registry::get('config')->tb->telefone), 'FTC.id_telefone = F.id_telefone')
			   ->where('FTC.id_cadastro = ?', $this->getId());
		
		$res = $tabela->query(Zend_DB::FETCH_OBJ);
		$result = $res->fetchAll();
		
		if ( $result )
		{
			foreach ( $result as $row )
			{
				$fones['fones'][$row->id_telefone_tipo] = (array) $row;
			}
		}
			   
		return $fones;
	}
	
	public function getEmails ()
	{
		$dados['emails'] = array();
		
		$tabela = $this->tabela->_table()->getAdapter()->select();
		$tabela->from(array('E' => Zend_Registry::get('config')->tb->email))
			   ->joinLeft(array('EC' => Zend_Registry::get('config')->tb->cadastro_email),'E.id_email = EC.id_email',array())
			   ->where('EC.id_cadastro = ?', $this->getId());
		
		$res = $tabela->query(Zend_DB::FETCH_OBJ);
		$result = $res->fetchAll();
		
		if ( $result )
		{
			foreach ( $result as $row )
			{
				$dados['emails'][$row->tipo] = (array) $row;
			}
		}
			   
		return $dados;
	}	
	
	public function editaEndereco ($dados,$tipo=NULL) 
	{
		$required = array('id_endereco_tipo','logradouro','numero','compl','bairro','cep','cidade','uf','id_cliente');
		$notNull  = array('logradouro','numero','bairro','cep','cidade','uf','id_cliente');

		$end = $this->getEndereco();
		
		if ( !$end['id_endereco'] )
		{
			$opcao = Model_Data::NOVO;
		} else
		{
			$opcao = Model_Data::ATUALIZA;
			$notNull = array();
		}
		
		$dados['id_tipo_telefone'] = (int) $tipo;
		$dados['cep'] = preg_replace('/[^0-9]/', '', $dados['cep']);
			
		$tabela = new Model_Data(Zend_Registry::get('config')->tb->endereco, NULL, NULL, $required, $notNull);
		$res2 = $tabela->edit($end['id_endereco'],$dados,NULL,$opcao);
		
		if ( $res2 > 0 && $opcao == Model_Data::NOVO )
		{
			$tb = new My_Table(Zend_Registry::get('config')->tb->fl_cadastro_endereco);
			$tb->insert(array(
						 'id_endereco' => $res2,
						 'id_cadastro' => $this->getId()
					   ));
		}
		
		return $res2;
	}
	
	protected function getEndereco ()
	{
		$table = $this->tabela->_table()->getAdapter()->select();
		$table->from(array('E' => Zend_Registry::get('config')->tb->endereco))
			  ->joinLeft(array('CE' => Zend_Registry::get('config')->tb->fl_cadastro_endereco), 
			  			 'E.id_endereco = CE.id_endereco', array())
			  ->where('CE.id_cadastro = ?',$this->getId());
			  
		$result = (array)$table->query()->fetchObject();
		//$this->setDados($result);
		
		return $result;
	}
	
	public function novaSenha ($senha=NULL)
	{
		if ( NULL === $senha )
		{
			$senha = '';
			
			while ( strlen($senha)  < 10 )
				$senha .= preg_replace('/[^0-9A-Za-z]/', '', chr(rand(0,127)));
		} else
		{
			if ( !strlen($senha) < 10 )
				return false;
		}

		if ( $this->edita(array('senha' => $senha))  > 0 )
		{
			return $this->enviaEmailSenha($senha);
		} else 
		{
			return false;
		}
	}
	
	public static function cripto ($senha)
	{
		$filter = new Zend_Filter_Encrypt();
		$filter->setVector(Zend_Registry::get('config')->www->encryptvector);
			
		return base64_encode($filter->filter($senha));
	}
	
	public static function descripto ($senha)
	{
		$filter = new Zend_Filter_Decrypt();
		$filter->setVector(Zend_Registry::get('config')->www->encryptvector);
			
		return (string) $filter->filter(base64_decode($senha));
	}
	
	protected function enviaEmailSenha ($senha)
	{
		$dados = $this->getDados();
		$assunto = 'Sua nova senha';
		
		$msg = '
Sua nova senha está ok!

Entre agora em: {==url==}default/login/

Login: '.$dados['email'] .'
Senha: '. $senha .' 

Obrigado.

Equipe Entelco
www.entelco.com.br';
		
		return $this->_enviaEmail($dados['email'], $msg, $assunto);
	}
	
	protected function enviaEmailCadastro ()
	{
		$cod = $this->codigoAtivacao();
		$dados = $this->getDados();
		$assunto = 'Ativação de cadastro';
		$msg = '
Obrigado por se cadastrar em nosso site.
Seu cadastro está quase pronto. Para ativá-lo, basta clicar no link abaixo ou copiar e colar a url no browser.

{==url==}default/login/ativa?&u='. md5($dados['id_cadastro_ativacao'].$this->getId()) .'&a='. $cod .'

Código de ativação: '.$cod .'

Obrigado.

Equipe Entelco
www.entelco.com.br';
		
		return $this->_enviaEmail($dados['email'], $msg, $assunto);
	}
	
	protected function _enviaEmail ($email,$msg,$assunto=NULL,array $opcoes=NULL)
	{
		Zend_Loader::loadClass('Zend_Mail');
		Zend_Loader::loadClass('Zend_Mail_Transport_Smtp');
		
		if ( Zend_Registry::get('config')->email->cadastro->host )
		{
			$host = Zend_Registry::get('config')->email->cadastro->host;
			$from = Zend_Registry::get('config')->email->cadastro->user;
			$name = Zend_Registry::get('config')->email->cadastro->fromName;
			
			$config = array('auth' 		=> 'login',
            		        'username' 	=> Zend_Registry::get('config')->email->cadastro->user,
                    		'password' 	=> Zend_Registry::get('config')->email->cadastro->password);
		} else 
		{
			$host = Zend_Registry::get('config')->email->default->host;
			$from = Zend_Registry::get('config')->email->default->user;
			$name = Zend_Registry::get('config')->email->default->fromName;
			
			$config = array('auth' 		=> 'login',
            		        'username' 	=> Zend_Registry::get('config')->email->default->user,
                    		'password' 	=> Zend_Registry::get('config')->email->default->password);
		}
		
		$request = Zend_Controller_Front::getInstance()->getRequest(); 
		$url = $request->getScheme().'://'.$request->getHttpHost();
		$url = rtrim($url,'/').'/';
		$msg = str_replace('{==url==}', $url, $msg);
		
		$tr   = new Zend_Mail_Transport_Smtp($host,$config);
	    $mail = new Zend_Mail('utf-8');
	    $mail->setFrom($from, $name);
		$mail->addTo($email);
		
		if ( $assunto )
			$mail->setSubject($assunto);
			
		if ( $opcoes['isHTML'] === true )
			$mail->setBodyHtml($msg);
		else
			$mail->setBodyText($msg);
			
		return $mail->send($tr);
	}
}