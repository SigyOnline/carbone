<?php
require_once 'app/models/Model_Data.php';
require_once 'app/models/My_Table.php';
require_once 'app/models/Error/Error.php';
require_once 'app/models/Error/Success.php';
require_once CORE.'/Cadastro/Item/Abstract.php';
require_once CORE.'/Cadastro/Login/Abstract.php';

Class Cadastro_Item_PousadaLogin extends Cadastro_Item_Abstract
{
	/**
	 * status para cadastros ativos
	 */
	const STATUS_ATIVO = 11;
	
	/**
	 * Status de cadastros inativos
	 */
	const STATUS_INATIVO = 12;
	
	/**
	 * Status para cadastros novos, porém não ativados
	 */
	const STATUS_NOVO = 13;
	
	/**
	 * Status do código de ativação quando disponível ainda
	 */
	const ATIVACAO_STATUS_ATIVAR = 15;
	
	/**
	 * Status do código de ativação quando já usado
	 */
	const ATIVACAO_STATUS_ATIVADO = 14;
	
	/**
	 * 
	 * Instância Model Data
	 * @var Model_Data
	 */
	protected $tabela;
	
	/**
	 * 
	 * Campos requiridos pela tabela
	 * @var array
	 */
	protected $required = array('id_login_pousada','nome','login','senha','email','criado','modificado');
	
	/**
	 * 
	 * Campos que não podem ser nulos
	 * @var array
	 */
	protected $notNull = array('id_login_pousada','nome','login','senha');
	
	/**
	 * 
	 * Construtor
	 * @param int $id
	 */
	public function __construct($id=NULL)
	{
		if ( Zend_Registry::isRegistered('config') )
		{
			if ( Zend_Registry::get('config')->tb->login_pousada )
				$this->tabela = new Model_Data(Zend_Registry::get('config')->tb->login_pousada);
			else
				throw new Exception('Tabela inválida');
		} else 
		{
			throw new Exception('Config inválido');
		}
		
		if ( is_string($id) && strpos($id, '@') > 0 )
		{
			$id = $this->getIdByEmail($id);
		}
			
		if ( $id > 0 )
			$this->setId($id);
	}
	
	public function setId ($id)
	{
		$this->id = $id;
	}
	
	public function getId ()
	{
		if ( !$this->id )
			throw new Exception('Id não definido');

		return $this->id;
	}
	
	public function getIdByEmail($email)
	{
		$email = preg_replace('/[^0-9A-Za-z.@_]/', '', $email);
		$tabela = $this->tabela->_table();
		$res = $tabela->fetchRow(
							$tabela->select()->where('email = ?',$email)
						);
						
		if ( $res )
			return $res->id_login_adm;
	}
	
	public function getDados ()
	{
		if ( !$this->id )
			return false;
			
		if ( $this->dados && $this->dados['id_login_pousada'] )
			return $this->dados;
			
		$where = $this->tabela->_table()->getAdapter()->quoteInto('id_login_pousada = ?',$this->id);
		$res = $this->tabela->_table()->fetchRow($where);

		if ( !$res )
		{
			throw new Exception('Usuário não existe');
		}

		return $this->dados;
	}
	
	public function setDados($dados)
	{
		$this->dados = array_merge($this->dados,$dados);
	}
	
	public function novo ($dados)
	{
		$this->tabela->_notNull($this->notNull);
		$this->tabela->_required($this->required);
		
		$res = $this->tabela->_table()->fetchRow(
							$this->tabela->_table()->select()->where('email = ?',$dados['email'])
				);
		if ( $res )
			return self::ERRO_INSERIR_DUPLICADO;
		
		$dados['id_status'] = self::STATUS_NOVO;
		$dados['senha'] = self::cripto($dados['senha']);
		
		$res = $this->tabela->edit(NULL,$dados,NULL,Model_Data::NOVO);
		
		if ( $res > 0 )
		{
			$this->setId($res);
			return $res;
		} else
		{
			return $res; 
		}
	}
	
	public function edita ($dados)
	{
		$this->tabela->_required($this->required);
		//$this->tabela->_notNull($this->notNull);

		if ( strlen($dados['senha']) > 0 )
			$dados['senha'] = self::cripto($dados['senha']);
		else 
			unset($dados['senha']);
		
		$res = $this->tabela->edit($this->getId(),$dados,NULL,Model_Data::ATUALIZA);
		
		return $res;
	}
	
	
	public function novaSenha ($senha=NULL)
	{
		if ( NULL === $senha )
		{
			$senha = '';
			
			while ( strlen($senha)  < 10 )
				$senha .= preg_replace('/[^0-9A-Za-z]/', '', chr(rand(0,127)));
		} else
		{
			if ( !strlen($senha) < 10 )
				return false;
		}

		if ( $this->edita(array('senha' => $senha))  > 0 )
		{
			return $this->enviaEmailSenha($senha);
		} else 
		{
			return false;
		}
	}
	
	public static function cripto ($senha)
	{
		return md5(Zend_Registry::get('config')->www->encryptvector.$senha);
	}
	
	public static function descripto ($senha)
	{
		return $senha;
	}
	
	protected function enviaEmailSenha ($senha)
	{
		$dados = $this->getDados();
		$assunto = 'Sua nova senha';
		
		$msg = '
Sua nova senha está ok!

Entre agora em: {==url==}default/login/

Login: '.$dados['email'] .'
Senha: '. $senha .' 

Obrigado.

Equipe Entelco
www.entelco.com.br';
		
		return $this->_enviaEmail($dados['email'], $msg, $assunto);
	}
	
	protected function _enviaEmail ($email,$msg,$assunto=NULL,array $opcoes=NULL)
	{
		Zend_Loader::loadClass('Zend_Mail');
		Zend_Loader::loadClass('Zend_Mail_Transport_Smtp');
		
		if ( Zend_Registry::get('config')->email->cadastro->host )
		{
			$host = Zend_Registry::get('config')->email->cadastro->host;
			$from = Zend_Registry::get('config')->email->cadastro->user;
			$name = Zend_Registry::get('config')->email->cadastro->fromName;
			
			$config = array('auth' 		=> 'login',
            		        'username' 	=> Zend_Registry::get('config')->email->cadastro->user,
                    		'password' 	=> Zend_Registry::get('config')->email->cadastro->password);
		} else 
		{
			$host = Zend_Registry::get('config')->email->default->host;
			$from = Zend_Registry::get('config')->email->default->user;
			$name = Zend_Registry::get('config')->email->default->fromName;
			
			$config = array('auth' 		=> 'login',
            		        'username' 	=> Zend_Registry::get('config')->email->default->user,
                    		'password' 	=> Zend_Registry::get('config')->email->default->password);
		}
		
		$request = Zend_Controller_Front::getInstance()->getRequest(); 
		$url = $request->getScheme().'://'.$request->getHttpHost();
		$url = rtrim($url,'/').'/';
		$msg = str_replace('{==url==}', $url, $msg);
		
		$tr   = new Zend_Mail_Transport_Smtp($host,$config);
	    $mail = new Zend_Mail('utf-8');
	    $mail->setFrom($from, $name);
		$mail->addTo($email);
		
		if ( $assunto )
			$mail->setSubject($assunto);
			
		if ( $opcoes['isHTML'] === true )
			$mail->setBodyHtml($msg);
		else
			$mail->setBodyText($msg);
			
		return $mail->send($tr);
	}
}