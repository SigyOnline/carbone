<?php
require_once CORE.'/Cadastro/Abstract.php';
require_once 'app/models/MyCore/Cadastro/Interface.php';

class Cadastro_Caracteristica extends Cadastro_Abstract implements Cadastro_Interface
{
	protected $paginacao = true;
	
	protected $_config = array();
	
	protected $porPagina = 10;
	
	protected $linha;
	
	protected $required = array('id_imovel', 'id_tipo_caracteristica_imovel', 'label', 'valor');
	
	protected $notNull = array();
	
	protected $id;
	
	public function ini ()
	{
	
		$this->_config = array('tbImoveis' => Zend_Registry::get('config')->tb->imoveis,
							   'tbCarac' => Zend_Registry::get('config')->tb->caracteristica_imovel,
							   'tbCat' => Zend_Registry::get('config')->tb->categorias,
							   'tbFoto' => Zend_Registry::get('config')->tb->fotos, 
							   'tbVideo' => Zend_Registry::get('config')->tb->videos 
		);		
	}
	public function lista ( $filtro = NULL, $order = NULL )
	{
		$listagem = new Zend_Db_Select($this->db);
		$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);
		 
		$listagem->from($this->_config['tbCarac']);
	
		//     	if ( $filtro )
			//     	{
			//     		if ( is_array($filtro) )
				//     		{
				//     			while ( list($k,$v) = each($filtro) )
					//     				$listagem->where($k,$v);
					//     		}
			//     	}
		$listagem->where('id_imovel = ?', $filtro);
		if ( $this->paginacao === true )
		{
			require_once 'Zend/Paginator.php';
	
			$this->paginator = Zend_Paginator::factory($listagem);
			$this->paginator->setItemCountPerPage($this->porPagina);
			Zend_Paginator::setDefaultScrollingStyle('Sliding');
			Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginacao.php');
			$this->paginator->setCurrentPageNumber((int)$_GET['p']);
			$result = $this->paginator;
		} else
		{
			$result = $listagem->query(Zend_Db::FETCH_ASSOC)->fetchAll();
			//$listagem->query(Zend_Db::FETCH_OBJ)->rowCount();
		}
		return $result;
	}
	
	public function listagem ( $id_imovel, $id_tipo_caracteristica)
	{
		$listagem = new Zend_Db_Select($this->db);
		$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);
			
		$listagem->from($this->_config['tbCarac']);

		$listagem->where('id_imovel = ?', $id_imovel);
		$listagem->where('id_tipo_caracteristica_imovel = ?', $id_tipo_caracteristica);

		$result = $listagem->query(Zend_Db::FETCH_OBJ)->fetchAll();
		
		return $result;
	}
	public function listagemCaracteristica ( $procurar )
	{
		
		$listagem = new Zend_Db_Select($this->db);
		$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);
			
		$listagem->from($this->_config['tbCarac'], array('label'));
		
		$listagem->where("label LIKE '%{$procurar}%'");
		
		$listagem->group("label");
		
		$result = $listagem->query(Zend_Db::FETCH_ASSOC)->fetchAll();
		
		return $result;
		
	}
	
	public function paginar ($esp=true)
	{
		$this->paginacao = (bool) $esp;
	}
	
	public function setId ($id)
	{
		$this->id = $id;
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function inserirCaracteriscas($post, $id_imovel, $tipo)
	{
		
		foreach ($post as $k => $v)
		{
			$dados = array('id_imovel' => $id_imovel, 'label'=>$k, 'valor'=>$v, 'id_tipo_caracteristica_imovel'=> $tipo);
			$this->novo($dados);	
			
		}
		
	}
	
	public function getDados($id)
	{
		$pag = $this->paginacao;
		$this->paginar(false);
		$res = current($this->lista(array('id_caracteristica_imovel = ?' => $id)));
		$this->paginar($pag);
	
		return $res;
	}
	
	
	public function novo ($dados)
	{
		$res = $this->_salva($dados, Model_Data::NOVO);
	
		if ( $res > 0 )
		{
			$this->setId($res);
		}
	
		return $res;
	}
	
	public function edita ($id,$dados)
	{
	
		$this->setId($id);
		$res = $this->_salva($dados, Model_Data::ATUALIZA,$id);
	
		return $res;
	}
	
	protected function _salva ($dados,$opt,$id=NULL)
	{
		$tabela = new Model_Data(
				$this->_config['tbCarac'],
				NULL,
				NULL,
				$this->required,
				$this->notNull);
			
		$edt = $tabela->edit($id,$dados,NULL,$opt);
	
		return $edt;
	}
	
	public function apaga ($id)
	{
		$produto = new My_Table($this->_config['tbCarac']);
		//$faq = new Cadastro_Faq($this->db);
		return $produto->delete($produto->getAdapter()->quoteInto('id_caracteristica_imovel =?',$id));
	}
	
}	