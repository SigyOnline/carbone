<?php
require_once CORE.'/Cadastro/Abstract.php';
require_once 'app/models/MyCore/Cadastro/Interface.php';

class Cadastro_Newsletter extends Cadastro_Abstract implements Cadastro_Interface
{
	protected $paginacao = true;
	
	protected $_config = array();
	
	protected $porPagina = 10;
	
	protected $linha;
	
	protected $required = array();
	
	protected $notNull = array();
	
	protected $id;
	
	public function ini ()
	{
	
		$this->_config = array('tbNewsletter' => Zend_Registry::get('config')->tb->newsletter,
							   'tbCampanha' => Zend_Registry::get('config')->tb->campanha,
							   'tbCampanhaFlag' => Zend_Registry::get('config')->tb->campanha_flag,
							   'tbImoveis' => Zend_Registry::get('config')->tb->imoveis,
							   'tbCat' => Zend_Registry::get('config')->tb->categorias,
		);		
	}
	
	public function GetNewsletter($email)
	{
		$listagem = new Zend_Db_Select($this->db);
		$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);
	
		$listagem->from($this->_config['tbNewsletter']);
		$listagem->where('email = "'.$email.'"');
	
		$result = $listagem->query()->fetchObject();
		 
		return $result;
	}
	
	public function lista ($filtro = NULL, $order = NULL ) 
	{
    	$listagem = new Zend_Db_Select($this->db);
    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);
    	
    	$listagem->from($this->_config['tbNewsletter']);

    	if ( $this->paginacao === true )
    	{
    		require_once 'Zend/Paginator.php';
    		
			$this->paginator = Zend_Paginator::factory($listagem);
			$this->paginator->setItemCountPerPage($this->porPagina);
			Zend_Paginator::setDefaultScrollingStyle('Sliding');
			Zend_View_Helper_PaginationControl::setDefaultViewPartial('pagina.php');
			$this->paginator->setCurrentPageNumber((int)$_GET['p']);
			$result = $this->paginator;
    	} else
    	{
    		$result = $listagem->query()->fetchAll();
    		//$listagem->query(Zend_Db::FETCH_OBJ)->rowCount();
    	}
    	return $result;
	}
	
	public function paginar ($esp=true) 
	{
		$this->paginacao = (bool) $esp;
	}
	
	public function setId ($id)
	{
		$this->id = $id;
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function getDados($id)
	{
		$pag = $this->paginacao;
		$this->paginar(false);
		$res = current($this->lista(array('id_noticia = ?' => $id)));
		$this->paginar($pag);
		
		return $res;
	}
	

	public function novo ($dados) 
	{
		$res = $this->_salva($dados, Model_Data::NOVO);
		
		if ( $res > 0 )
		{
			$this->setId($res);
		}
		
		return $res;
	}
	
	public function novonews ($dados) 
	{
		$res = $this->_salvanews($dados, Model_Data::NOVO);
		
		if ( $res > 0 )
		{
			$this->setId($res);
		}
		
		return $res;
	}
	
	public function novoflag ($dados) 
	{
		$res = $this->_salvaflag($dados, Model_Data::NOVO);
		
		if ( $res > 0 )
		{
			$this->setId($res);
		}
		
		return $res;
	}
	
	public function edita ($id,$dados) 
	{
	
		$this->setId($id);
		$res = $this->_salva($dados, Model_Data::ATUALIZA,$id);
		
		return $res;
	}
	
	protected function _salva ($dados,$opt,$id=NULL)
	{
		
    	$tabela = new Model_Data(
    						$this->_config['tbNewsletter'],
    						NULL,
    						NULL,
    						$this->required,
    						$this->notNull);
    	
    	$edt = $tabela->edit($id,$dados,NULL,$opt);
		
    	return $edt;
    }
    
	protected function _salvanews ($dados,$opt,$id=NULL)
	{
		
    	$tabela = new Model_Data(
    						$this->_config['tbCampanha'],
    						NULL,
    						NULL,
    						$this->required,
    						$this->notNull);
    	$tabela->load_options('imagem',array('path' => 'imagens/news',
    			'type' => 'image',
    			// 									 'size' => 800,
    			'root' => '' ));
    	
    	$edt = $tabela->edit($id,$dados,NULL,$opt);
		
    	return $edt;
    }
    
	protected function _salvaflag ($dados,$opt,$id=NULL)
	{
		
    	$tabela = new Model_Data(
    						$this->_config['tbCampanhaFlag'],
    						NULL,
    						NULL,
    						$this->required,
    						$this->notNull);
    	
    	$edt = $tabela->edit($id,$dados,NULL,$opt);
		
    	return $edt;
    }

    public function RetornaImoveis($id)
    {
    	$listagem = new Zend_Db_Select($this->db);
    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);
    	
    	$listagem->from(array('I' => $this->_config['tbImoveis']));
    	$listagem->joinLeft( array('C' => $this->_config['tbCampanhaFlag'] ), 
    						'I.codigo = C.id_imovel', 
    						array('id_imovel'));
    	$listagem->joinLeft( array('CA' => $this->_config['tbCat'] ),
    			'I.categoria = CA.id_categoria',
    			array('categoria as cat'));
    	$listagem->where('id_campanha = ?', $id);
    	
    	$result = $listagem->query()->fetchAll();
    	
    	return $result;
    }
    
    public function RetornaCampanha($id)
    {
    	$listagem = new Zend_Db_Select($this->db);
    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);
    	
    	$listagem->from(array('I' => $this->_config['tbCampanha']));
    	$listagem->where('id_campanha = ?', $id);
    	
    	$result = $listagem->query()->fetchObject();
    	
    	return $result;
    }
    
    public function ListaCampanha()
    {
    	$listagem = new Zend_Db_Select($this->db);
    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);
    	
    	$listagem->from(array('I' => $this->_config['tbCampanha']));
    	
    	$result = $listagem->query()->fetchAll();
    	return $result;
    }
    
    public function MontaCampanha($dados)
    {
    	foreach ($dados as $d)
    	{
    		
    		
    	$imoveis .= '<table class="flexibleContainer" cellpadding="0" cellspacing="0" align="{imovelpos}" border="0" width="260" style="margin-bottom: 25px;">
					<tbody>
					<tr>
					<td class="imageContent" align="left" valign="top">
					<a href="'.Zend_Registry::get('config')->www->host.'imoveis/detalhe/id/'.$d->codigo.'">
					<img src="'.$d->foto_destaque.'" class="flexibleImage" title="'.$d->cat.' - '.$d->cidade.'" alt="'.$d->cat.' - '.$d->cidade.'" width="260" height="160">
					</a>
					</td>
					</tr>
					<tr>
					<td height="10" valign="top">
					</td>
					</tr>
					<tr>
					<td style="font-family: Arial; font-size:14px; padding-bottom:10px;" valign="top">
					<div style="font-size: 15px; color: rgb(74, 74, 74); font-weight: bold; text-align: left; padding-bottom: 10px; position: relative;" class="txt-headColor">
					<p style="padding: 0px; margin: 0px;">
					'.$d->cat.'
					</p>
					</div>
					<div style="color: rgb(164, 164, 164); position: relative;" class="txt-titleColor">
					<p style="padding: 0px; margin: 0px;">
					Ref.: '.$d->codigo.' <br />
					'.$d->bairro.' - '.$d->cidade.'<br />
					'.($d->dormitorios == "" ? "":"Dormitórios ".$d->dormitorios).'<br>		
					'.($d->vagas == "" ? "":"Vagas ".$d->vagas).'<br>		
					'.($d->area_privativa == "" ? "":"Metragem ".$d->area_privativa.'m²').'
					</p>
					</div>
					</td>
					</tr>
					<tr>
					<td class="textContentLast" align="left" valign="top">
					<table class="ButtonBg" style="background: none repeat scroll 0% 0% rgb(197, 47, 51); font-family: Arial; border-radius: 3px; font-size: 12px;" cellpadding="0" cellspacing="0" border="0" width="130">
					<tbody>
					<tr>
					<td class="txt-btnColor" style="text-align: center; padding: 0px 10px; line-height: 35px; color: rgb(255, 255, 255); position: relative;" align="center" height="35px" valign="middle">
					<a style="color: rgb(255, 255, 255); text-decoration: none;" href="'.Zend_Registry::get('config')->www->host.'/imoveis/detalhe/id/'.$d->codigo.'">Consultar</a>
					</td>
					</tr>
					</tbody>
					</table>
					</td>
					</tr>
					</tbody>
					</table>';	
    	}
    	return $imoveis;
    }
    
	public function apaga ($id) 
	{
		$produto = new My_Table($this->_config['tbNewsletter']);
		//$faq = new Cadastro_Faq($this->db);
		return $produto->delete($produto->getAdapter()->quoteInto('id_newsletter =?',$id));
	}
}
