<?php
require_once CORE.'/Cadastro/Abstract.php';
require_once 'app/models/MyCore/Cadastro/Interface.php';

class Cadastro_Consultafiltro extends Cadastro_Abstract implements Cadastro_Interface
{
	protected $paginacao = true;
	
	protected $_config = array();
	
	protected $porPagina = 20;
	
	protected $linha;
	
	protected $required = array('');
	
	protected $notNull = array('');
	
	protected $id;
	
	public function ini ()
	{
		$this->_config = array('tbImoveis' => Zend_Registry::get('config')->tb->imoveis,
							   'tbCategoria' => Zend_Registry::get('config')->tb->categorias 
		);				
	}

	public function ListaCidade()
	{
			$listagem = new Zend_Db_Select($this->db);
	    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);
	    	
	    	$listagem->from($this->_config['tbImoveis'],array('cidade'));
	    	$listagem->group('cidade');
	    	//$listagem->order(new Zend_Db_Expr("cidade!='Rio Claro'"));
	    	$listagem->order('cidade ASC');
	    	$result = $listagem->query()->fetchAll();
    	
    	return $result;
	    	
	}
	
	public function GetCategoria($id)
	{
		$listagem = new Zend_Db_Select($this->db);
		$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);
	
		$listagem->from($this->_config['tbCategoria'])
		->where('id_categoria IN (SELECT DISTINCT categoria
								FROM sg_imoveis)')
									->order('categoria');
		$listagem->where('id_categoria =? ', $id);
	
		$result = $listagem->query()->fetchObject();
		 
		return $result->categoria;
	
	}
	
	public function getFase()
	{
	
		return array(
				'Lançamento',
				'Breve Lançamento',
				'Em Construção',
				'Pronto para Morar'
		);
	}
	
	public function ListaBairro($cidade = null)
	{
			$listagem = new Zend_Db_Select($this->db);
	    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);
	    	
	    	$listagem->from($this->_config['tbImoveis'],array('bairro'));
	    	
	    	if( !is_null($cidade))
	    		$listagem->where('cidade = ?', $cidade);
	    	
// 	    	$listagem->where("bairro NOT IN(NULL,'')");
			$listagem->group('bairro');
	    	$result = $listagem->query()->fetchAll();
    	
    	return $result;
	    	
	}
	
	public function ListaFaseObra()
	{
			$listagem = new Zend_Db_Select($this->db);
	    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);
	    	
	    	$listagem->from($this->_config['tbImoveis'],array('fase_obra'))
	    			 ->where("fase_obra NOT IN(NULL,'')");
	    	$listagem->group('fase_obra');
	    	$result = $listagem->query()->fetchAll();
    	
    	return $result;
	    	
	}
	
	public function ListaFinalidade()
	{
			$listagem = new Zend_Db_Select($this->db);
	    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);
	    	
	    	$listagem->from($this->_config['tbImoveis'], array('finalidade'))
	    			 ->where("finalidade NOT IN (NULL,'')");
	    	$listagem->group('finalidade');
	    	$result = $listagem->query()->fetchAll();
    	
    	return $result;
	}
	
	public function ListaStatus()
	{
			$listagem = new Zend_Db_Select($this->db);
	    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);
	    	
	    	$listagem->from($this->_config['tbImoveis'],array('status',"(status = 'VENDA') as O "))
	    			 ->group('status')
	    			 ->order(array('O desc','status ASC'));
    	 
	    	$result = $listagem->query()->fetchAll();
    	return $result;
	}
	
	public function ListaCategoria()
	{
			$listagem = new Zend_Db_Select($this->db);
	    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);
	    	
	    	$listagem->from($this->_config['tbCategoria'])
	    			 ->where('id_categoria IN (SELECT DISTINCT categoria
								FROM sg_imoveis)')
	    			 ->order('categoria');
	    	$result = $listagem->query()->fetchAll();
    	
    	return $result;
	    	
	}
	
	public function lista ($filtro=NULL, $order=NULL ) 
	{
    	$listagem = new Zend_Db_Select($this->db);
    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);
    	
    	$listagem->from(array('C' => $this->_config['tbCliente']));
    	$listagem->joinLeft(array('P'=>$this->_config['tbProdutor']), 'C.id_produtor = P.id_produtor', array('id_produtor', 'nome'));
    	$listagem->joinLeft(array('G'=>$this->_config['tbGrupo']), 'C.id_grupo = G.id_grupo', array('id_grupo'));
    	if ( $order === NULL )
    	{
    		$listagem->order('C.segurado ASC');
    	} else
    	{
    	    if(is_array($order))
    			$listagem->order($order);
    		else 
    			$listagem->order('C.'.$order.' ASC');
    	}		 
    	if ( $filtro )
    	{
    		/*if ( is_array($filtro) )
    		{
    			while ( list($k,$v) = each($filtro) ) 
    				$listagem->where($k,$v);
    		}*/
    		if($filtro['id_produtor']>0){
    			$listagem->where('C.id_produtor=?',$filtro['id_produtor']);
    		}
    		if($filtro['id_grupo']>0){
    			$listagem->where('C.id_grupo=?',$filtro['id_grupo']);
    		}
    		if($filtro['segurado']!= ''){
    			$listagem->where('C.segurado LIKE ?',$filtro['segurado'].'%');
    		}
    		if($filtro['fisica_juridica']>0){
    			$listagem->where('C.fisica_juridica=?',$filtro['fisica_juridica']);
    		}
    		if($filtro['cpf_cnpj']!=''){
    			$listagem->where('C.cpf_cnpj=?',$filtro['cpf_cnpj']);
    		}
    		if($filtro['uf']!=''){
    			$listagem->where('C.uf=?',$filtro['uf']);
    		}
    		
    	}
    	//exit($listagem);		 
    	if ( $this->paginacao === true )
    	{
    		require_once 'Zend/Paginator.php';
    		
			$this->paginator = Zend_Paginator::factory($listagem);
			$this->paginator->setItemCountPerPage($this->porPagina);
			Zend_Paginator::setDefaultScrollingStyle('Sliding');
			Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginacao.php');
			$this->paginator->setCurrentPageNumber((int)$_GET['p']);
			$result = $this->paginator;
    	} else
    	{
    		$result = $listagem->query()->fetchAll();
    	}
    	
    	return $result;
	}
	
	public function paginar ($esp=true) 
	{
		$this->paginacao = (bool) $esp;
	}
	
	public function setId ($id)
	{
		$this->id = $id;
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function getDados($id)
	{
		$pag = $this->paginacao;
		$this->paginar(false);
		$res = current($this->lista(array('id_produto = ?' => $id)));
		$this->paginar($pag);
		
		return $res;
	}
	

	public function novo ($dados) 
	{
		$res = $this->_salva($dados, Model_Data::NOVO);
		
		if ( $res > 0 )
		{
			$this->setId($res);
		}
		
		return $res;
	}
	
	public function edita ($id,$dados) 
	{
	
		$this->setId($id);
		$res = $this->_salva($dados, Model_Data::ATUALIZA,$id);
		
		return $res;
	}
	
	protected function _salva ($dados,$opt,$id=NULL)
	{
    	$tabela = new Model_Data(
    						$this->_config['tbCliente'],
    						NULL,
    						NULL,
    						$this->required,
    						$this->notNull);
    							
    	$edt = $tabela->edit($id,$dados,NULL,$opt);
    	
    	return $edt;
    }
	

	public function apaga ($id) 
	{
		$produto = new My_Table($this->_config['tbCliente']);
		//$faq = new Cadastro_Faq($this->db);
		return $produto->delete($produto->getAdapter()->quoteInto('id_cliente =?',$id));
	}
	
}