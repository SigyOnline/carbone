<?php
require_once CORE.'/Cadastro/Abstract.php';
require_once 'app/models/MyCore/Cadastro/Interface.php';

class Cadastro_Foto extends Cadastro_Abstract implements Cadastro_Interface
{
	protected $paginacao = false;
	
	protected $_config = array();
	
	protected $porPagina = 10;
	
	protected $linha;
	
	protected $required = array();
	
	protected $notNull = array('foto');
	
	protected $id;
	
	public function ini ()
	{
	
		$this->_config = array('tbImoveis' => Zend_Registry::get('config')->tb->imoveis,
							   'tbCarac' => Zend_Registry::get('config')->tb->caracteristica_imovel,
							   'tbCat' => Zend_Registry::get('config')->tb->categorias,
							   'tbFoto' => Zend_Registry::get('config')->tb->fotos, 
							   'tbVideo' => Zend_Registry::get('config')->tb->videos 
		);		
	}
	
	public function lista ( $filtro = NULL, $order = NULL ) 
	{
    	$listagem = new Zend_Db_Select($this->db);
    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);
    	
    	$listagem->from($this->_config['tbFoto']);

//     	if ( $filtro )
//     	{
//     		if ( is_array($filtro) )
//     		{
//     			while ( list($k,$v) = each($filtro) )
//     				$listagem->where($k,$v);
//     		}
//     	}
    	$listagem->where('id_imovel = ?', $filtro);
    	if ( $this->paginacao === true )
    	{
    		require_once 'Zend/Paginator.php';
    		
			$this->paginator = Zend_Paginator::factory($listagem);
			$this->paginator->setItemCountPerPage($this->porPagina);
			Zend_Paginator::setDefaultScrollingStyle('Sliding');
			Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginacao.php');
			$this->paginator->setCurrentPageNumber((int)$_GET['p']);
			$result = $this->paginator;
    	} else
    	{
    		$result = $listagem->query(Zend_Db::FETCH_ASSOC)->fetchAll();
    		//$listagem->query(Zend_Db::FETCH_OBJ)->rowCount();
    	}
    	return $result;
	}
	
	public function paginar ($esp=true) 
	{
		$this->paginacao = (bool) $esp;
	}
	
	public function setId ($id)
	{
		$this->id = $id;
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function getDados($id)
	{
		$pag = $this->paginacao;
		$this->paginar(false);
		$res = current($this->lista(array('id_foto = ?' => $id)));
		$this->paginar($pag);
		
		return $res;
	}
	

	public function novo ($dados) 
	{
		$res = $this->_salva($dados, Model_Data::NOVO);
		
		if ( $res > 0 )
		{
			$this->setId($res);
		}
		
		return $res;
	}
	
	public function edita ($id,$dados) 
	{
	
		$this->setId($id);
		$res = $this->_salva($dados, Model_Data::ATUALIZA,$id);
		
		return $res;
	}
	
	protected function _salva ($dados,$opt,$id=NULL)
	{
    	$tabela = new Model_Data(
    						$this->_config['tbFoto'],
    						NULL,
    						NULL,
    						$this->required,
    						$this->notNull);
    							
    	$edt = $tabela->edit($id,$dados,NULL,$opt);
		
    	return $edt;
    }
	
	public function apaga ($id) 
	{
		$produto = new My_Table($this->_config['tbFoto']);
		return $produto->delete($produto->getAdapter()->quoteInto('id_foto =?',$id));
	}
	
	public function inserirFotos($files, $post, $id)
	{
		$path = 'imagens/imoveis/';
// 		$keys = array_keys($files['name']);
// 		$filter = new Zend_Filter_Alnum();
		
		foreach ($files['name'] as $i => $v )
		{
			$nome_arquivo = $files['name'][$i];			
			if ( !$nome_arquivo ) continue;

				$ext = substr($nome_arquivo, -4);
				$nome_arquivo = str_replace($ext, '', $nome_arquivo);
				$nome_arquivo = preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities($nome_arquivo));
				$nome_arquivo = str_replace(' ', '_', $nome_arquivo);
// 				$nome_arquivo = $filter->filter($nome_arquivo);
				
				if ( is_file($path . $nome_arquivo . $ext) )
				{
					$y=0;
					do {
						$y++;
						$nome_arquivo = $nome_arquivo . "_{$y}"; 
						
					} while ( is_file($path . $nome_arquivo . $ext) );
					
				}
				
				if ( move_uploaded_file($files['tmp_name'][$i], $path . $nome_arquivo . $ext) )
				{
					$dados = array(
						'destaque' => $post['Imagem'][ $i ]['destaque'],
						'ordem' => (empty($post['Imagem'][ $i ]['ordem']))?0:$post['Imagem'][ $i ]['ordem'],
						'descricao' => $post['Imagem'][ $i ]['descricao'],
						'tipo' => $post['Imagem'][ $i ]['tipo'],
						'foto' => $path . $nome_arquivo . $ext,
						'id_imovel' => $id		
					);
					$this->novo($dados);
				}
				$i++; 
		}
			
	}
	public function fotoCategoria()
	{
		return array(
				'Imóvel',
				'Empreendimento',
				'Plantas',
				'Perspectiva Artística'
		);
	}
}
