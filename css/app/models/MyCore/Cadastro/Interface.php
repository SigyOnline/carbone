<?php
interface Cadastro_Interface
{
	public function lista ($filtro=NULL);
	
	public function paginar ($esp=true);
	
	public function setId ($id);
	
	public function getId();
	
	public function getDados($id);
	
	public function novo ($dados);
	
	public function edita ($id,$dados);
	
	public function apaga ($id);
}