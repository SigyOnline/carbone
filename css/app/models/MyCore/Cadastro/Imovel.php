<?php

require_once CORE.'/Cadastro/Abstract.php';

require_once 'app/models/MyCore/Cadastro/Interface.php';

require_once 'app/models/MyCore/FiltroValues.php';

class Cadastro_Imovel extends Cadastro_Abstract implements Cadastro_Interface

{

	protected $paginacao = true;

	

	protected $_config = array();

	

	protected $porPagina = 10;

	

	protected $linha;

	

	protected $required = array();

	

	protected $notNull = array();

	

	protected $id;

	

	public function ini ()

	{

	

		$this->_config = array('tbImoveis' => Zend_Registry::get('config')->tb->imoveis,

							   'tbCarac' => Zend_Registry::get('config')->tb->caracteristica_imovel,

							   'tbCat' => Zend_Registry::get('config')->tb->categorias,

							   'tbFoto' => Zend_Registry::get('config')->tb->fotos, 

							   'tbVideo' => Zend_Registry::get('config')->tb->videos, 

							   'tbPesquisado' => Zend_Registry::get('config')->tb->pesquisados, 

							   'tbNoticia' => Zend_Registry::get('config')->tb->noticia, 

							   'tbIdade' => Zend_Registry::get('config')->tb->idade, 

							   'tbSimulador' => Zend_Registry::get('config')->tb->simulador, 

							   'tbCarac' => Zend_Registry::get('config')->tb->caracteristica_imovel, 

							   'tbSeo' => Zend_Registry::get('config')->tb->seo, 

							   'tbBanner' => Zend_Registry::get('config')->tb->banner, 

							   'tbDepoimento' => Zend_Registry::get('config')->tb->depoimento

		);		

	}

	

	public function ListaCarac()

	{

		$listagem = new Zend_Db_Select($this->db);

		$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);

	

		$listagem->from($this->_config['tbCarac']);

		$listagem->group('label');

		$listagem->order('label');

		$listagem->where('valor > 0 AND valor != "Nao"');

		$result = $listagem->query()->fetchAll();

		 

		return $result;

	

	}

	

	public function ListaBanner()

	{

		$listagem = new Zend_Db_Select($this->db);

		$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);

	

		$listagem->from($this->_config['tbBanner']);

		$listagem->where('ativo = ? ', 1);

		$result = $listagem->query()->fetchAll();

		 

		return $result;

	

	}

	public function ListaBannerHome()

	{

		$subquery = new Zend_Db_Select($this->db);
		$subquery->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);

		$subquery->from($this->_config['tbBanner'],array('id_imovel'));
		$subquery->where('ativo = ? ', 1);

		$listagem = new Zend_Db_Select($this->db);
		$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);

		$listagem->from(array('I' => $this->_config['tbImoveis']))
				 ->join(array('C' => $this->_config['tbCat']), 'I.categoria = C.id_categoria');
		$listagem->where('codigo IN('.$subquery.')');

		$result = $listagem->query()->fetchAll();

		return $result;

	}

	

	public function ListaDepoimento()

	{

		$listagem = new Zend_Db_Select($this->db);

		$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);

	

		$listagem->from($this->_config['tbDepoimento']);

		$listagem->where('status = ? ', 1);

		$listagem->order('RAND()');

		$result = $listagem->query()->fetchAll();

		 

		return $result;

	

	}

	

	public function GetImoveisDetalhe($id)

	{

			$listagem = new Zend_Db_Select($this->db);

	    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);

	    	

	    	$listagem->from(array('I' => $this->_config['tbImoveis']))

	    			 ->join(array('C' => $this->_config['tbCat']), 'I.categoria = C.id_categoria');

	    	$listagem->where('codigo = ?', $id);

		  	

	    	$result = $listagem->query()->fetchObject();

    	

    	return $result;

	    	

	}

	

	public function GetImoveisVideo($id)

	{

		$listagem = new Zend_Db_Select($this->db);

		$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);

	

		$listagem->from(array('I' => $this->_config['tbVideo']));

		$listagem->where('id_imovel = ?', $id);

		 

		$result = $listagem->query()->fetchObject();

		 

		return $result;

	

	}

	

	public function GetMapa($id)

	{

			$listagem = new Zend_Db_Select($this->db);

	    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);

	    	

	    	$listagem->from($this->_config['tbImoveis']);

	    	$listagem->where('codigo = ?', $id);

		  	

	    	$result = $listagem->query()->fetchObject();

    	

    	return $result;

	    	

	}

	

	public function ListaBairro($cidade = NULL)

	{

			$listagem = new Zend_Db_Select($this->db);

	    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);

	    	

	    	$listagem->from($this->_config['tbImoveis'],array('bairro'));

	    	$listagem->where('cidade LIKE "%'. $cidade.'%"');

	    	//$listagem->where('bairro !=  ""');

	    	$listagem->group('bairro');

	    	$result = $listagem->query()->fetchAll();

    	

    	return $result;

	}

	

	public function ListaNoticiaHome()

	{

			$listagem = new Zend_Db_Select($this->db);

	    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);

	    	

	    	$listagem->from($this->_config['tbNoticia']);

	    	$listagem->where('destaque = ?',(int)1);

	    	$listagem->limit(4);

	    	$result = $listagem->query()->fetchAll();

    	

    	return $result;

	}

	

	public function ListaZona($zona)

	{

			$listagem = new Zend_Db_Select($this->db);

	    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);

	    	

	    	$listagem->from($this->_config['tbImoveis'],array('bairro'));

	    	$listagem->where('zona LIKE "%'.$zona.'%"');

	    	$listagem->group('bairro');

	    	$result = $listagem->query()->fetchAll();

    	

    	return $result;

	}

	

	public function ListaAba($fase)

	{

		$listagem = new Zend_Db_Select($this->db);

	    $listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);

	    	

	    $listagem->from(array( 'I' => $this->_config['tbImoveis']))

	    		 ->joinLeft(array( 'C' => $this->_config['tbCat']), 'I.categoria = C.id_categoria')

	    		 //->where('status = ?',$fase)

	    		 ->where("em_destaque IN (1,'sim')")

	    		 ->order('RAND()')

	    		 ->limit(5);

	    $result = $listagem->query()->fetchAll();

    	

    	return $result;

	}

	

	public function ListaPesquisado($limit)

	{

		$listagem = new Zend_Db_Select($this->db);

	    $listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);

	    	

	    $listagem->from(array( 'P' => $this->_config['tbPesquisado']))

	    		 ->joinLeft(array( 'I' => $this->_config['tbImoveis']), 'P.id_imovel = I.codigo')

	    		 ->joinLeft(array( 'CA' => $this->_config['tbCat']), 'I.categoria = CA.id_categoria')

	    		// ->order('qtde ASC')

	    		 ->group('P.id_imovel')

	    		 ->limit($limit);

	    	

	    $result = $listagem->query()->fetchAll();

    	

    	return $result;

	}

	

	public function RetornaVisualizado($id)

	{

		$listagem = new Zend_Db_Select($this->db);

	    $listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);

	    	

	    $listagem->from(array( 'P' => $this->_config['tbPesquisado']));

	    $listagem->where('id_imovel = ?', $id);

	    	

	    $result = $listagem->query()->fetchObject();

    	

    	return $result;

	}

	

	public function GetCaracteristica($id)

	{

			$listagem = new Zend_Db_Select($this->db);

	    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);

	    	

	    	$listagem->from(array('I' => $this->_config['tbImoveis']),array('codigo'));

	    	$listagem->joinLeft(array('C'=>$this->_config['tbCarac']),'C.id_imovel = I.codigo', array('label','valor'));

	    	$listagem->where('C.id_imovel = ?', $id)

	    			 ->where('C.id_tipo_caracteristica_imovel = ?', 1)

	    			 ->where("valor not in ('Nao','NAO', '0','',' ')");

		  	

	    	$result = $listagem->query()->fetchAll();

    	

    	return $result;

	    	

	}

	

	public function GetCaracteristicaCondominio($id)

	{

			$listagem = new Zend_Db_Select($this->db);

	    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);

	    	

	    	$listagem->from(array('I' => $this->_config['tbImoveis']),array('codigo'));

	    	$listagem->joinLeft(array('C'=>$this->_config['tbCarac']),'C.id_imovel = I.codigo', array('label','valor'));

	    	$listagem->where('C.id_imovel = ?', $id);

	    	$listagem->where('C.id_tipo_caracteristica_imovel = ?', 2)

	    			 ->where("valor not in ('Nao','NAO', '0',' ')");

		  	

	    	$result = $listagem->query()->fetchAll();

    	

    	return $result;

	    	

	}

	

	public function Getfoto($id)

	{

			$listagem = new Zend_Db_Select($this->db);

	    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);

	    	

	    	$listagem->from(array('I' => $this->_config['tbImoveis']),array('codigo'));

	    	$listagem->joinLeft(array('F'=>$this->_config['tbFoto']),'F.id_imovel = I.codigo', array('foto','foto_pequena','descricao','data','destaque','tipo','id_foto'));

	    	$listagem->where('F.id_imovel = ?', $id);

		  	

	    	$result = $listagem->query()->fetchAll();

    	

    	return $result;

	}

	

	public function GetCaracteristicaBusca()

	{

			$listagem = new Zend_Db_Select($this->db);

	    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);

	    	

	    	$listagem->from($this->_config['tbCarac']);

	    	$listagem->where("valor not in ('Nao', '0','')");

	    	$listagem->group('label');

		  	
		// exit($listagem);

	    	$result = $listagem->query()->fetchAll();

    	

    	return $result;

	    	

	}

	

	public function GetSeo($id)

	{

			$listagem = new Zend_Db_Select($this->db);

	    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);

	    	

	    	$listagem->from($this->_config['tbSeo']);

	    	$listagem->where("id_imovel = ?",$id);

		  	

	    	$result = $listagem->query()->fetchAll();

	    	

    	return $result;

	}

	

	public function lista ($filtro = NULL, $order = NULL ) 

	{

		/*

		 * Filtro Mobile

		 */

		$filtro_novo = array();

		foreach( $filtro as $f => $v )

		{

			if ( !( is_array($filtro[$f]) && empty($filtro[$f][0]) ) && !empty($v))

			{

				$filtro_novo[$f] = $v;

			}

		}

		

		$filtro = $filtro_novo;

		

    	$listagem = new Zend_Db_Select($this->db);

    	$filtroValues = new FiltroValues($listagem);

    	

    	$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);

    	

    	$listagem->from(array('I' => $this->_config['tbImoveis']));

    	$listagem->joinLeft( array('C' => $this->_config['tbCat'] ), 

    						'I.categoria = C.id_categoria', 

    						array('categoria as cat'));

    	

    	if ( $filtro ){



    		if( !empty($filtro['imopagina']) ){

    			$this->porPagina = $filtro['imopagina'];

    		}

    		

    		//Order BY Venda

    		if (!isset($filtro['ordenar']) || empty($filtro['ordenar'])) {

    			$listagem->order('I.codigo DESC');

    		}

    		elseif( $filtro['ordenar'] == 'menor' && $filtro['status']  == 'VENDA' )

    		{

    			$listagem->order('valor_venda ASC');

    			

    		}elseif ( $filtro['ordenar'] == 'maior' && $filtro['status']  == 'VENDA' )

    		{

    			$listagem->order('valor_venda DESC');

    			

    		}

    		

    		elseif ( $filtro['ordenar'] == 'menor' && empty($filtro['status']) )

    		{

    			$listagem->order('valor_venda ASC');

    			

    		}elseif ($filtro['ordenar'] == 'maior' && empty($filtro['status']))

    		{

    			$listagem->order('valor_venda DESC');

    		}

    		

    		//Order by Aluguel

    		

    		if( $filtro['ordenar'] == 'menor' && $filtro['status']  == 'ALUGUEL' )

    		{

    			$listagem->order('valor_locacao ASC');

    			

    		}elseif ( $filtro['ordenar'] == 'maior' && $filtro['status']  == 'ALUGUEL' )

    		{

    			$listagem->order('valor_locacao DESC');

    			

    		}

    		

    		//Order by bairro

    		

    		if( $filtro['ordenar'] == 'bairro' )

    		{

    			$listagem->order('I.bairro ASC');

    		}

    		

    		//Order by categoria

    		

    		if( $filtro['ordenar'] == 'categoria' )

    		{

    			$listagem->order('C.categoria ASC');

    		}

    		

    		//Order by categoria

    		

    		if( $filtro['ordenar'] == 'metragem' )

    		{

    			$listagem->order('I.area_privativa ASC');

    		}

    		

    		if( $filtro['status']  == 'VENDA' ){

    			$listagem->where('status = "'. $filtro['status'].'" OR status LIKE  "%VENDA E ALUGUEL%"');

    		}

    		

    		if ($filtro['status'] == 'ALUGUEL')

    		{

				$listagem->where('status = "Aluguel" OR status LIKE  "%VENDA E ALUGUEL%"');    			

    		}

    		

    		if ($filtro['status'] == 'lancamento')

    		{

    			$listagem->where('lancamento = "sim"');

    		}

    		

    		if(!empty($filtro['cidade'])){

    			$listagem->where('cidade = ?',$filtro['cidade']);

    		}

    		

    		if(!empty($filtro['organico'])){

    			

    			$listagem->where("palavra_chave LIKE ?", "%{$filtro['organico']}%");    			

    		}

    		

    		if(!empty($filtro['fase_obra'])){

    			

	    		if ( is_array($filtro['fase_obra']) )

	    			{

// 		    				$cont = 0;

		    				

// 		    				foreach ($filtro['fase_obra'] as $value)

							{

// 								if ( $cont == 0 )

// 								{

		    						$listagem->where('fase_obra IN (?) ',$filtro['fase_obra']);

// 								}else{

// 									$listagem->orWhere('fase_obra = "'.$value.'"');

// 								}

	

// 								$cont++;

							}	

	    			}else{

	    					$listagem->where('fase_obra = ?', $filtro['fase_obra']);

	    			}

    			

    		}

    		

    		if(!empty($filtro['label'])){

    			

	    		if ( is_array($filtro['label']) )

	    			{

// 		    				$cont12 = 0;

		    				

// 		    				foreach ($filtro['label'] as $value)

							{

// 								if ( $cont12 == 0 )

// 								{

		    						$listagem->where('label IN (?) ',$filtro['label']);

// 								}else{

// 									$listagem->orWhere('label = "'.$value.'"');

// 								}

	

// 								$cont12++;

							}	

	    			}else{

	    					$listagem->where('CA.label = ?', $filtro['label']);

	    			}

    			

    		}

    		

    		if(!empty($filtro['bairro']))

    		{

    			if ( is_array($filtro['bairro']) )

    			{

	    				$cont = 0;

	    				

// 	    				foreach ($filtro['bairro'] as $value)

						{

// 							if ( $cont == 0 )

// 							{

	    						$listagem->where('bairro IN (?)',$filtro['bairro']);

// 							}else{

// 								$listagem->orWhere('bairro = "'.$value.'"');

// 							}



							$cont++;

						}	

    			}else{

    					$listagem->where('bairro = ?', $filtro['bairro']);

    			}

    			

    			}

//     		exit($listagem);

    		if(!empty($filtro['categoria'])){

    			

    			if ( is_array($filtro['categoria']) )

    			{

    					$cat = implode(",", $filtro['categoria']);

    					$listagem->where('I.categoria IN ('.$cat.')');

    			}else{

    					$listagem->where('I.categoria = ?',$filtro['categoria']);

    			}

    		}

    		

    		if(!empty($filtro['dormitorios'])){

    			

    			if ( is_array($filtro['dormitorios']) )

    			{

    					$dorm = implode(",", $filtro['dormitorios']);

    					$listagem->where('dormitorios IN ('.$dorm.')');

    			}else{

    					$listagem->where('dormitorios = ?', (int)$filtro['dormitorios']);

    			} 

    		}

    		

    		if(!empty($filtro['suites'])){

    			

    			if ( is_array($filtro['suites']) )

    			{

    					$sui = implode(",", $filtro['suites']);

    					$listagem->where('suites IN ('.$sui.')');

    			}else{

    					$listagem->where('suites = ?', (int)$filtro['suites']);

    			} 

    		}

    		

    		if(!empty($filtro['vagas'])){

    			

    			if ( is_array($filtro['vagas']) )

    			{

    					$sui = implode(",", $filtro['vagas']);

    					$listagem->where('vagas IN ('.$sui.')');

    			}else{

    					$listagem->where('vagas = ?', (int)$filtro['vagas']);

    			} 

    		}

    		

    		

    		if(!empty($filtro['area_privativa'])){

    		

    			$filtroValues->filtroBetween($filtro['area_privativa'], 'area_privativa');

    		}

    		

    		if( $filtro['status'] != 'VENDA'  && !empty($filtro['valor_locacao'])){

    		

    			$filtroValues->filtroBetween($filtro['valor_locacao'], 'valor_locacao');

    		}

    		

    		if($filtro['status'] != 'ALUGUEL'  && !empty($filtro['valor_venda'])){

    		

    			$filtroValues->filtroBetween($filtro['valor_venda'], 'valor_venda');

    		}

    		

    		   		

    		if(!empty($filtro['finalidade'])){

    			

    		

    			if ( is_array($filtro['finalidade']) )

    			{

    				$var = "'".implode("','", $filtro['finalidade'])."'";

    				$listagem->where('finalidade IN ('.$var.')');

    			}else{

    				$listagem->where('finalidade = ?', $filtro['finalidade']);

    			}

    		}

    		

    		if(!empty($filtro['regiao']))

    		{

    			if ( is_array($filtro['regiao']) )

    			{

    				$var = "'".implode("','", $filtro['regiao'])."'";

    				$listagem->where('regiao IN ('.$var.')');

    			} else 

    			{

    				$listagem->where('regiao = ?', $filtro['regiao']);

    			}

    		}

    		

    	}else{



    		$listagem->order('I.codigo DESC');

    	}

    	

    	

    	if($_GET['debug'])

    	{

    		print_r($filtro);

    		print('<hr>');

    		exit($listagem);



    	}

    	if ( $this->paginacao === true )

    	{

    		require_once 'Zend/Paginator.php';

    		

			$this->paginator = Zend_Paginator::factory($listagem);

			$this->paginator->setItemCountPerPage($this->porPagina);

			$this->paginator->setPageRange(5);

			Zend_Paginator::setDefaultScrollingStyle('Sliding');

			Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginacao.php');

			$this->paginator->setCurrentPageNumber((int)$_GET['p']);

			$result = $this->paginator;

    	} else

    	{

    		$result = $listagem->query()->fetchAll();

    		//$listagem->query(Zend_Db::FETCH_OBJ)->rowCount();

    	}

    	

    	return $result;

	}

	

	public function paginar ($esp=true) 

	{

		$this->paginacao = (bool) $esp;

	}

	

	public function setId ($id)

	{

		$this->id = $id;

	}

	

	public function getId()

	{

		return $this->id;

	}

	

	public function getDados($id)

	{

		$pag = $this->paginacao;

		$this->paginar(false);

		$res = current($this->lista(array('codigo = ?' => $id)));

		$this->paginar($pag);

		

		return $res;

	}

	



	public function novo ($dados) 

	{

		

		$res = $this->_salva($dados, Model_Data::NOVO);

		

		if ( $res > 0 )

		{

			$this->setId($res);

		}

		

		return $res;

	}

	

	public function novopes ($dados) 

	{

		

		$res = $this->_salvapes($dados, Model_Data::NOVO);

		

		if ( $res > 0 )

		{

			$this->setId($res);

		}

		

		return $res;

	}

	

	public function edita ($id,$dados) 

	{

		$this->setId($id);

		$res = $this->_salva($dados, Model_Data::ATUALIZA,$id);

		

		return $res;

	}

	

	public function editapes ($id,$dados) 

	{

		$this->setId($id);

		$res = $this->_salvapes($dados, Model_Data::ATUALIZA,$id);

		

		return $res;

	}

	

	protected function _salva ($dados,$opt,$id=NULL)

	{

    	$tabela = new Model_Data(

    						$this->_config['tbImoveis'],

    						NULL,

    						NULL,

    						$this->required,

    						$this->notNull);

    							

    	$tabela->load_options('foto_destaque',array('path' => 'imagens/imoveis',

    			'type' => 'image',

    			// 									 'size' => 800,

    			'root' => '' ));

    	$tabela->load_options('logo_empreendimento',array('path' => 'imagens/imoveis',

    			'type' => 'image',

    			// 									 'size' => 800,

    			'root' => '' ));

    	

    	$edt = $tabela->edit($id,$dados,NULL,$opt);

    	

//     	if ( $edt > 0 && !empty( $_FILES['fotos'] ) )

//     		$this->inserirFotos( $_FILES['fotos'] );

    		

    	return $edt;

    }

	

	protected function _salvapes ($dados,$opt,$id=NULL)

	{

    	$tabela = new Model_Data(

    						$this->_config['tbPesquisado'],

    						NULL,

    						NULL,

    						array('id_imovel'),

    						$this->notNull);

    							

    	

    	$edt = $tabela->edit($id,$dados,NULL,$opt);

    	

//     	if ( $edt > 0 && !empty( $_FILES['fotos'] ) )

//     		$this->inserirFotos( $_FILES['fotos'] );

    		

    	return $edt;

    }

	

    public function addCaracteristica ($label, $valor)

    {

    	if ( empty($label) || !$this->id )

    		return false;

    	

    	return $this->_addCaracInfra($label,$valor,1);

    }

    

    public function addInfra ($label, $valor)

    {

    	if ( empty($label) || !$this->id )

    		return false;

    	

    	return $this->_addCaracInfra($label,$valor,2);

    }

    

    protected function _addCaracInfra($label,$valor,$tipo)

    {

    	$dados = array(

    			'id_imovel' 					=> $this->id,

    			'id_tipo_caracteristica_imovel' => $tipo,

    			'label' 						=> $label,

    			'valor'							=> $valor

    	);

    	

    	return $this->db->insert($this->_config['tbCarac'], $dados);

    }

    

    public function addFoto ($dados)

    {

    	if ( !is_array($dados) )

    		return;

    	

    	if ( !$dados['id_imovel'] )

    		$dados['id_imovel'] = $this->id;

    	

    	if ( $dados['descricao'] )

    		$dados['descricao'] = base64_encode($dados['descricao']);

    	

    	return $this->db->insert($this->_config['tbFoto'], $dados);

    }

    

    public function addVideo ($dados)

    {

    	if ( !is_array($dados) )

    		return;

    	

    	if ( !$dados['id_imovel'] )

    		$dados['id_imovel'] = $this->id;

    	

    	if ( $dados['descricao'] )

    		$dados['descricao'] = base64_encode($dados['descricao']);

    	

    	return $this->db->insert($this->_config['tbVideo'], $dados);

    }

    

	public function inserirFotos($files)

	{

		if ( sizeof($files) > 0 && $this->getId() > 0 )

		{

			$foto = new Cadastro_Foto_Imovel($this->db,$this->getId());

			

			while ( list($k,$v) = each($files) )

			{

				$res += $foto->inserir($v);

			}

			return $res;

		} else 

		{

			return false;

		}

	}	

	public function apaga ($id) 

	{

		$produto = new My_Table($this->_config['tbHistorico']);

		$produto->delete($produto->getAdapter()->quoteInto('id_venda =?',$id));

		

		$produto = new My_Table($this->_config['tbVenda']);

		//$faq = new Cadastro_Faq($this->db);

		return $produto->delete($produto->getAdapter()->quoteInto('id_venda =?',$id));

	}

	public function listagem()

	{

		$listagem = new Zend_Db_Select($this->db);

		$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_OBJ);

		 

		$listagem->from(array('I' => $this->_config['tbImoveis']))

			->join(array('C' => $this->_config['tbCat']), 'I.categoria = C.id_categoria', array('categoria AS tipo'));

    	if ( $this->paginacao === true )

    	{

    		require_once 'Zend/Paginator.php';

    		

			$this->paginator = Zend_Paginator::factory($listagem);

			$this->paginator->setItemCountPerPage($this->porPagina);

			Zend_Paginator::setDefaultScrollingStyle('Sliding');

			Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginacao.php');

			$this->paginator->setCurrentPageNumber((int)$_GET['p']);

			$result = $this->paginator;

    	} else

    	{

    		$result = $listagem->query()->fetchAll();

    		//$listagem->query(Zend_Db::FETCH_OBJ)->rowCount();

    	}

    	

    	return $result;

	}

	public function getFinalidades()

	{

		return array(

				'COMERCIAL',

				'MISTO',

				'RESIDENCIAL',

				'RURAL'

		);

	}

	

	public function getFase()

	{

		return array(

				'Lançamento',

				'Breve Lançamento',

				'Em Construção',

				'Pronto para Morar'

		);

	}

	

	public function getZonas()

	{

		return array(

				'Zona Sul',

				'Zona Norte',

				'Zona Leste',

				'Zona Oeste',

				'Centro',

		);

	}

	

	public function getBairroPorZona()

	{

		//SELECT bairro,regiao FROM sigyonli_sg8.sg_imoveis group by bairro, regiao

		// Having regiao is not null order by regiao ASC

		$listagem = new Zend_Db_Select($this->db);

	

		$listagem->from(array('I' => $this->_config['tbImoveis']), array('bairro','regiao'))

				 ->group(array('bairro','regiao'))

				 ->order('regiao ASC')

				 ->having('regiao is not NULL');

	

		$result = $listagem->query(Zend_db::FETCH_ASSOC)->fetchAll();

		$r = array();

	

		foreach ( $result as $row )

			$r[$row['regiao']][] = $row['bairro'];

	

		return $r;

	}

	

	public function fotosImovel($id,$tipo=NULL)

	{

		$listagem = new Zend_Db_Select($this->db);

		

		$listagem->from(array('I' => $this->_config['tbFoto']));

		$listagem->where('id_imovel = ?', $id);

		

		if ( !empty($tipo) ) 

			$listagem->where('tipo = ?', $tipo);

		 

		$result = $listagem->query(Zend_db::FETCH_ASSOC)->fetchAll();

		return $result;

	}

	

	public function ImoveisSemelhantes($id_categoria, $status)

	{

		$listagem = new Zend_Db_Select($this->db);

		$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_ASSOC);

	

		$listagem->from(array('I'=>$this->_config['tbImoveis']))

		->join(array('C' => $this->_config['tbCat']), 'I.categoria = C.id_categoria', array('categoria AS cat'));

		$listagem->where('I.categoria = ?', $id_categoria);

		$listagem->where('I.status = ?', $status);

		$listagem->order('RAND()');

		$listagem->limit(3);

		$result = $listagem->query()->fetchAll();

			

		return $result;

	}

	

	public function ImoveisSemelhantesAvancado($imovel)

	{

		$listagem = new Zend_Db_Select($this->db);

		$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_ASSOC);

	

		$listagem->from(array('I'=>$this->_config['tbImoveis']))

		->join(array('C' => $this->_config['tbCat']), 'I.categoria = C.id_categoria', array('categoria AS cat'));

		$listagem->where('I.categoria = ?', $imovel->id_categoria);		

		$listagem->where('I.status = ?', $imovel->status);

		$listagem->where('I.bairro = ?', $imovel->bairro);

		$listagem->where('I.codigo <> ?', $imovel->codigo);

		

		if( $imovel->status == 'VENDA' || $imovel->status == 'VENDA E LOCACAO') 

		{

			$p = $imovel->valor_venda * 0.2;

			$v1 = $imovel->valor_venda - $p;

			$v2 = $imovel->valor_venda + $p;

			

			$listagem->where("valor_venda BETWEEN $v1 AND $v2 ");

			

		} elseif ($imovel->status == 'ALUGUEL') 

		{

			$p = $imovel->valor_locacao * 0.2;

			$v1 = $imovel->valor_locacao - $p;

			$v2 = $imovel->valor_locacao + $p;

		}

		

		$listagem->order('RAND()');

		$listagem->limit(3);

		$result = $listagem->query()->fetchAll();

			

		return $result;

	}

	public function ImoveisUltimosVisualizados($id, $lista_de_imoveis)

	{

		$key = array_search($id, $lista_de_imoveis);

		

	

		if($key !== false)

			unset($lista_de_imoveis[$key]);

	

		$listagem = new Zend_Db_Select($this->db);

		$listagem->getAdapter()->setFetchMode(Zend_Db::FETCH_ASSOC);

	

		$listagem->from(array('I'=>$this->_config['tbImoveis']))

		->join(array('C' => $this->_config['tbCat']), 'I.categoria = C.id_categoria', array('categoria AS cat'));

		$listagem->where("I.codigo IN ('" . implode("', '", array_reverse($lista_de_imoveis)) . "')");

		//$listagem->order('RAND()');

		$listagem->limit(3);

		//exit($listagem);

		$result = $listagem->query()->fetchAll();

			

		return $result;

	}

	

	public function getSuperDestaque()

	{

		$listagem = new Zend_Db_Select($this->db);

		

		$listagem->from(array('I' => $this->_config['tbImoveis']))

			->joinLeft(array('C'=> $this->_config['tbCat']), 'I.categoria = C.id_categoria');

			

		$listagem->where('I.em_destaque = ?', 'Sim');

		$listagem->order('RAND()');

		$listagem->limit(4);

		

		$result = $listagem->query(Zend_db::FETCH_ASSOC)->fetchAll();

		

		return $result;

	}

	

	public function getImoveisCategoria($categoria)

	{

		$listagem = new Zend_Db_Select($this->db);

		

		$listagem->from(array('I' => $this->_config['tbImoveis']))

			->joinLeft(array('C'=> $this->_config['tbCat']), 'I.categoria = C.id_categoria');

			

		$listagem->where('I.em_destaque = ?', 'Sim');

		//$listagem->order('RAND()');

		$listagem->limit(6);

		

		$result = $listagem->query(Zend_db::FETCH_ASSOC)->fetchAll();

		

		return $result;

	}

	

	public function getImoveisFinalidade($finalidade)

	{

		$listagem = new Zend_Db_Select($this->db);

		

		$listagem->from(array('I' => $this->_config['tbImoveis']))

				 ->joinLeft(array('C'=> $this->_config['tbCat']), 'I.categoria = C.id_categoria')

			

				 ->where('I.em_destaque = ?', 'Sim')

				 ->where('I.finalidade = ?',$finalidade)

		//		 ->order('RAND()')

				 ->limit(6);

		

		$result = $listagem->query(Zend_db::FETCH_ASSOC)->fetchAll();

		

		return $result;

	}

	

	public function getImoveisStatus($status)

	{

		$listagem = new Zend_Db_Select($this->db);

		

		$listagem->from(array('I' => $this->_config['tbImoveis']))

			->joinLeft(array('C'=> $this->_config['tbCat']), 'I.categoria = C.id_categoria');

			

		if ($status == 'LANCAMENTO') 

		{

			$listagem->where('I.lancamento = "Sim"');

		}else{

			

			$listagem->where('I.status = "'. $status.'" OR I.status = "VENDA E LOCACAO"');

		} 

		

		$listagem->where('I.super_destaque_web = ?', 'Sim');

		$listagem->order('RAND()');

		$listagem->limit(6);

		//exit($listagem);

		$result = $listagem->query(Zend_db::FETCH_ASSOC)->fetchAll();

		

		return $result;

	}

	

	public function getImoveisFavoritos($favoritos)

	{

		$listagem = new Zend_Db_Select($this->db);

		

		$listagem->from(array('I' => $this->_config['tbImoveis']))

			->joinLeft(array('C'=> $this->_config['tbCat']), 'I.categoria = C.id_categoria');

			

		$listagem->where('I.codigo IN(?)', $favoritos);

		

		$result = $listagem->query(Zend_db::FETCH_ASSOC)->fetchAll();

		

		return $result;

	}

	

	public function tabelaIdade($idade)

	{

		$listagem = new Zend_Db_Select($this->db);

		

		$listagem->from($this->_config['tbIdade']);

		$listagem->where("sg_de <= '".$idade."' AND sg_ate >= '".$idade."'");

			

		$result = $listagem->query(Zend_db::FETCH_ASSOC)->fetchObject();

		

		return $result->ds_valor;

	}

	

	function simuladorVenda()

	{

		//Tipo = 2 quer dizer que os valores serÃ£o para simulaÃ§Ã£o normal.

		//Tipo = 1 quer dizer que os valores serÃ£o para simulaÃ§Ã£o a partir do valor da parcela.

		//Busca todas as informaÃ§Ãµes da tabela simulador que tipo seja igual = 2

		//Padrao verificando valor do imovel.

		

		$listagem = new Zend_Db_Select($this->db);

		

		$listagem->from($this->_config['tbSimulador']);

		$listagem->where("sg_tipo = ?",2);

			

		$conSimulador = $listagem->query(Zend_db::FETCH_ASSOC)->fetchObject();

		

		$juros_a_a = $conSimulador->sg_juros_a_a; // %

		$tx_adm = $conSimulador->sg_tx_adm; // R$

		$tr_a_a = $conSimulador->sg_tr_a_a; // %

		$per_itbi = $conSimulador->sg_percentual_itbi; // % Percentual ITBI

		$tx_avaliacao = $conSimulador->sg_tx_avaliacao; //%

		$avaliacao_finan = $conSimulador->sg_aval_finan; //AvaliaÃ§Ã£o no financiamento

		$itbi_finan = $conSimulador->sg_itbi_finan; //ITBI no financiamento

		$aliquoda_dfi = $conSimulador->sg_aliquota_dfi;

		

		//Valores dinamicos, muda de acordo com cliente no site escolher.

		//Variavel recebe get que o cliente colocou para efetuar a simulaÃ§Ã£o, caso ele nÃ£o tenha colocado nada. ela recebe valor DEFAULT do banco de dados.

		$idade_cli = isset($_POST['idadeCliente']) && $_POST['idadeCliente'] != ""?$_POST['idadeCliente']:$conSimulador->resultado['sg_idade_cliente'];

		$meses = isset($_POST['mesesCliente']) && $_POST['mesesCliente'] != ""?$_POST['mesesCliente']:$conSimulador->resultado['sg_prazo'];

		if(isset($_POST['mesesCliente']) && $_POST['mesesCliente'] != "" && $_POST['mesesCliente'] > 360){

			$meses = 360;

			echo("<script>alert('Financimanto maximo é 360 meses - ".$_POST['mesesCliente']." ');</script>");

		}

		//Valor do imovel Removendo os ponto e trocando onde tem virula por ponto. serve para calculo.

		//Muito importante que valor venha corretamente.

		$valor_imovel = str_replace(".","",$_POST['valorImovel']);

		$valor_imovel = str_replace(",",".",$valor_imovel);

		//Chama FunÃ§Ã£o tabelaIdade, passando a idade do cliente. retornando o valor de calculo baseado na idade.

		$idadeCalculo = $this->tabelaIdade($idade_cli);

		//Destroi ConexÃ£o;

		//Criado para pagina que faz simulaÃ§Ã£o sem dados. somente com dados de referencia.

		if(!isset($_POST['refer_banco']) && $_POST['refer_banco'] == "" && $idade_cli != 0 && $meses != 0 && $valor_imovel != 0)

		{

			///////////////////////////////////////////////////////////////////////////////

			///////////////////////////////////////////////////////////////////////////////

			//////////////////////////////// Calculos /////////////////////////////////////

			///////////////////////////////////////////////////////////////////////////////

			///////////////////////////////////////////////////////////////////////////////

			//Se existe valor de entrada

			if(isset($_POST['valorEntrada']) && $_POST['valorEntrada'] != "")

			{

				//Entrada recebe o valor, tratando os ponto e virula

				$entrada = str_replace(".","",$_POST['valorEntrada']);

				$entrada = str_replace(",",".",$entrada);

				$vintePorcento = $valor_imovel * 0.2;

				if($entrada < $vintePorcento){

					$entrada = $valor_imovel * 0.2;

					$mostraValorError = "R$ ".number_format($entrada, 2, ',', '.');

					echo("<script>alert('Valor de Entrada minimo é de 20% - $mostraValorError ');</script>");

				}

			} else {

				//Se nÃ£o de DEFAULT considerar 20% do valor total de entrada.

				$entrada = $valor_imovel * 0.2;

			}

			//Valor total menos o valor de entrada.

			$valor_Financiado = $valor_imovel - $entrada;

			//Seguro DFI

			$seguro_dfi = $valor_imovel*$aliquoda_dfi;

			//Valor ITBI

			$val_itbi = ($per_itbi / 100) * $valor_imovel;

			//Valor Financiamento

			$opcao01 = $itbi_finan == "s"?$val_itbi:0;

			$opcao02 = $avaliacao_finan == "s"?$tx_avaliacao:0;

			$valor_finan = $valor_Financiado + $opcao01 + $opcao02;

			//Juros/Amount

			$juros_amount = ($valor_finan / $meses) + ($valor_finan * (pow (( 1 + $juros_a_a / 100 ) , ( 1 / 12 )) - 1 )) ;

			// Seguro MIP

			$seguro_mip = $valor_finan * ($idadeCalculo / 100);

			// Valor Primeira Parcela

			$primeira_parc = $juros_amount + $seguro_dfi + $seguro_mip + $tx_adm;

			//I2 - BASE DE CALCULO

			$i2 = ((1 / $meses) + (pow (( 1 + $juros_a_a / 100 ) , ( 1 / 12 )) - 1 )) + ($idadeCalculo / 100);

			$i2 = ($primeira_parca - $seguro_dfi) / $i2;

			//Renda Liquida de Apoio

			$rla = ($primeira_parc / 0.27);

	

			/////////////////////// Calculos Parcelas //////////////////////////////////

			$saldoAtual = 0;

			//// rpt

			for($pl = 2;$pl <= $meses+1; $pl++){

				//Linha da Parcela. Verificando se for mais de 361 bloquiar no 361 se nÃ£o efetuar calculos.

				$plx = $pl > 361?361:$pl;

				//Saldo Anterior, primeira vez recebe o valor financiado.

				$saldoAnterior = $saldoAtual > 0 ?$saldoAtual:$valor_Financiado;

				//Salario anterior dividido por meses - linha da parcela + 2

				$amortizacao = $saldoAnterior / ($meses - $plx + 2);

				//Calculando Saldo Atual.

				$saldoAtual = ($saldoAnterior - $amortizacao) * (pow (( $tr_a_a / 100 + 1 ) , ( 1 / 12 )));

				//Calculando Juros.

				$juros = $saldoAnterior * (pow (( $juros_a_a / 100 + 1 ) , ( 1 / 12 )) - 1 );

				//Somando Juros mais amortizaÃ§aÃµ

				$amort_juros = $amortizacao + $juros;

				//Seguro Mip da Parcela

				$seguro_mipParc = $saldoAnterior * ($idadeCalculo / 100);

				//Valor Total = soma do amortizaÃ§Ã£o + seguro dfi + seguro mip da parcela + tx adm

				$vlrParcelaFinal[$pl] = $amort_juros + $seguro_dfi + $seguro_mipParc + $tx_adm;

			}

			//Indice qual valor devo pegar, se for mais de 360 entÃ£o fico no 360 se nÃ£o pego mes digitado +1;

			$indiceValor = $meses >= 360?361:$meses+1;

			$ultima_parc = $vlrParcelaFinal[$indiceValor];

	

			////////////////////////////////////////////////////////////////////////////////

			////////////////////////////////////////////////////////////////////////////////

			////////////////////////////////////////////////////////////////////////////////

			////////////////////////////////////////////////////////////////////////////////

		} else {

			if($idade_cli == 0 || $meses == 0 || $valor_imovel == 0 )

			{

				if(!isset($_POST['refer_banco']) && $_POST['refer_banco'] == ""){

					//echo("<script>alert('Erro, Verifique se preencheu todos os campos');< /script>");

				}

			}

			$valor_imovel = 0;

			$entrada = 0;

			$meses = 0;

			$idade_cli = 0;

		}

		

		$simularVenda = array();

		

		$simularVenda['valor_imovel'] = number_format($valor_imovel, 2, ',', '.');

		$simularVenda['entrada'] = number_format($entrada, 2, ',', '.');

		$simularVenda['meses'] = $meses;

		$simularVenda['idade_cli'] = $valor_imovel;

		$simularVenda['primeira_parc'] = number_format($primeira_parc, 2, ',', '.');

		$simularVenda['ultima_parc'] = number_format($ultima_parc, 2, ',', '.');

		

		return $simularVenda;

	}

	

	function simuladorAluguel()

	{

		//Busca todas as informaÃ§Ãµes da tabela simulador que tipo seja igual = 2

		//Tipo = 2 quer dizer que os valores serÃ£o para simulaÃ§Ã£o normal.

		//Tipo = 1 quer dizer que os valores serÃ£o para simulaÃ§Ã£o a partir do valor da parcela.

		

		$listagem = new Zend_Db_Select($this->db);

		

		$listagem->from($this->_config['tbSimulador']);

		$listagem->where("sg_tipo = ?",1);

			

		$conSimulador = $listagem->query(Zend_db::FETCH_ASSOC)->fetchObject();

		

		$juros_a_a = $conSimulador->sg_juros_a_a; // %

		$tx_adm = $conSimulador->sg_tx_adm; // R$

		$tr_a_a = $conSimulador->sg_tr_a_a; // %

		$per_itbi = $conSimulador->sg_percentual_itbi; // % Percentual ITBI

		$tx_avaliacao = $conSimulador->sg_tx_avaliacao; //%

		$avaliacao_finan = $conSimulador->sg_aval_finan; //AvaliaÃ§Ã£o no financiamento

		$itbi_finan = $conSimulador->sg_itbi_finan; //ITBI no financiamento

		$aliquoda_dfi = $conSimulador->sg_aliquota_dfi;

		

		$idade_cli = isset($_POST['idadeCliente']) && $_POST['idadeCliente'] != ""?$_POST['idadeCliente']:$conSimulador->resultado['sg_idade_cliente'];

		$meses = isset($_POST['mesesCliente']) && $_POST['mesesCliente'] != ""?$_POST['mesesCliente']:$conSimulador->resultado['sg_prazo'];

		

		$entrada = str_replace(".","",$_POST['valorAluguel']);

		$entrada = str_replace(",",".",$entrada);

		$vrl_1_parcela = str_replace(".","",$_POST['valorParcela']);

		$vrl_1_parcela = str_replace(",",".",$vrl_1_parcela);

		

		$idadeCalculo = $this->tabelaIdade($idade_cli);

		

		if($idade_cli == 0 || $meses == 0 || $entrada == 0 || $vrl_1_parcela == 0)

		{

			//echo("<script>alert('Erro, Verifique se preencheu todos os campos');< /script>");

		} else {

			///////////////////////////////////////////////////////////////////////////////

			///////////////////////////////////////////////////////////////////////////////

			//////////////////////////////// Calculos /////////////////////////////////////

			///////////////////////////////////////////////////////////////////////////////

			///////////////////////////////////////////////////////////////////////////////

			//Valor Financiamento

			

			$valor_finan = ( 1 / $meses ) + (pow (( 1 + $juros_a_a / 100 ) , ( 1 / 12 )) - 1 ) + ($idadeCalculo / 100);

			$valor_finan = $vrl_1_parcela / $valor_finan;

			//Seguro DFI

			$seguro_dfi = ($entrada+$valor_finan)*$aliquoda_dfi;

			//I2 - BASE DE CALCULO

			$i2 = ((1 / $meses) + (pow (( 1 + $juros_a_a / 100 ) , ( 1 / 12 )) - 1 )) + ($idadeCalculo / 100);

			$i2 = ($vrl_1_parcela - $seguro_dfi) / $i2;

			// Seguro MIP

			$seguro_mip = $i2 * ($idadeCalculo / 100);

			//Juros/Amount

			$juros_amount = ($i2 / $meses) + ($i2 * (pow (( 1 + $juros_a_a / 100 ) , ( 1 / 12 )) - 1 )) ;

			//Valor ITBI

			$val_itbi =(($entrada + $valor_finan)/100) * $per_itbi;

			//Renda Liquida de Apoio

			$rla = ($vrl_1_parcela / 0.27);

			////////////////////////////////////////////////////////////////////////////////

			////////////////////////////////////////////////////////////////////////////////

			////////////////////////////////////////////////////////////////////////////////

			////////////////////////////////////////////////////////////////////////////////

		}

		//Monta Simulador. Exibindo Valores Finais.

		$simularAluguel = '

	<div id="box-simule" class="fl radius3 marg-top30">

                <h1 class="fnt16 fnt-branco bold marg-top5">FaÃ§a agora mesmo a simulaÃ§Ã£o deste imÃ³vel:</h1>

                <div class="w160 marg-top10 marg-right12 fl">

                    <label for="id="vlr-aluguel-s1"" class="fl w100 fnt11 fnt-branco">Valor Aluguel (R$):</label>

                    <input type="text" id="vlr-aluguel-s1" class="input01 w155 fl marg-top5" value="'.number_format($entrada, 2, ',', '.').'" />

                </div>

                <div class="w160 marg-top10 marg-right12 fl">

                    <label for="vlr-parcela-s1" class="fl w100 fnt11 fnt-branco">Valor da parcela (R$):</label>

                    <input type="text" class="input01 w155 fl marg-top5" id="vlr-parcela-s1" value="'.number_format($vrl_1_parcela, 2, ',', '.').'"/>

                </div>

                <div class="w160 marg-top10 marg-right12 fl">

                    <label for="prazo-s1" class="fl w100 fnt11 fnt-branco">Prazo (meses)</label>

                    <input type="text" class="input01 w155 fl marg-top5" id="prazo-s1" value="'.$meses.'"/>

                </div>

                <div class="w160 marg-top10 fl">

                    <label for="idade-s1" class="fl w100 fnt11 fnt-branco">Sua Idade:</label>

                    <input type="text" class="input01 w155 fl marg-top5" id="idade-s1" value="'.$idade_cli.'"/>

                </div>

                <input type="submit" class="fl fnt16 btn-submit fnt-branco opacity bold" value="OK" onClick="calculaSimulador(\'ALUGUEL\')" />

                <div class="fl w100 marg-top10">

                    <h2 class="fnt16 fnt-branco bold">Com esse valor de aluguel vocÃª pode comprar um imÃ³vel de R$: <span class="fnt16 fnt-amarelo bold">R$ '.number_format($valor_finan, 2, ',', '.').'</span></h2>

                </div>

            </div>';

	

		if($valor_finan != "" && !isset($_POST['semimob'])){

			//Chama FunÃ§Ã£o para Trazer os imoveis compativel com a simulaÃ§Ã£o passando o valor final do financiamento.

			$simularAluguel .= deixeDePagarAluguel($valor_finan,$_POST['metragem'],$_POST['tpCat']);

		}

	

		//Retorna Simulador Completo de LocaÃ§Ã£o.

		return $simularAluguel;

	}

}

