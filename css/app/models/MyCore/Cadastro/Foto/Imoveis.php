<?php
require_once MYCORE .'/Cadastro/Foto/Abstract.php';

Class Cadastro_Foto_Imoveis extends Cadastro_Foto_Abstract
{
	protected $path = 'imagens/imoveis';
	
	protected $id;
	
	/**
	 * 
	 * Tb relacionamento
	 * @var My_Table
	 */
	protected $tabelaRel;
	
	/**
	 * 
	 * Construtor
	 * @param Zend_Db_Adapter_Abstract $db
	 * @param int $id id do produto
	 */
	public function __construct (Zend_Db_Adapter_Abstract $db,$id) 
	{
		$this->id = $id;
		$this->_config['tbRel'] = Zend_Registry::get('config')->tb->fotos;
		$this->tabelaRel = new My_Table($this->_config['tbRel']);
		
		parent::__construct($db);
	}
	
	public function inserir ($files)
	{
		$res = parent::inserir($files);
		return $res;
		
		if ( $res > 0 )
		{
			$r = $this->tabelaRel->insert(array('id_imovel' => $this->id,
								  		        'id_foto'    => $res
										  ));
										
			if ( !$r )
				throw new Exception('Não inserida');
		}
		return $res;
	}
	
	public function apaga ($id)
	{
		$where = $this->tabelaRel->getAdapter()->quoteInto(
										'id_foto = ?',$id
									);
		$res = parent::apaga($id);

		if ( $res )
			$this->tabelaRel->delete($where);
		
		return $res;
	}
	
	public function listaFoto(array $where=NULL) 
	{
		$select = new Zend_Db_Select($this->db);
		$select->from(array('F' => $this->_config['tbFoto']))
			   ->joinLeft(array('RF' => $this->_config['tbRel']), 'F.id_foto = RF.id_foto', array('foto_status'));
			   
		if ( sizeof($where) )
		{
			foreach ($where as $k => $w)
				$select->where($k, $w);
		} else
		{
			if ( !$this->id )
				throw new Exception('Sem ID');
			
			$select->where('RF.id_acomodacoes = ?',$this->id);
		}
		
		return $select->query()->fetchAll();
	}
	public function listaFotoHome(array $where=NULL) 
	{
		$select = new Zend_Db_Select($this->db);
		$select->from(array('F' => $this->_config['tbFoto']))
			   ->joinLeft(array('RF' => $this->_config['tbRel']), 'F.id_foto = RF.id_foto', array('foto_status'));
			   
		if ( sizeof($where) )
		{
			foreach ($where as $k => $w)
				$select->where($k, $w);
		} else
		{
			if ( !$this->id )
				throw new Exception('Sem ID');
			
			$select->where('RF.foto_status = ?',1);
			$select->where('RF.id_acomodacoes = ?',$this->id);
		}
		
		return $select->query()->fetchAll();
	}
}