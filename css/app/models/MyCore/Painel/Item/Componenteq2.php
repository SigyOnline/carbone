<?php
require_once 'app/models/Core/Painel/Item/Interface.php';

Class MyCore_Painel_Item_Componenteq2 implements Painel_Item_Interface
{
	public function __construct (Cadastro_Item_Abstract $usuario, $flag) 
	{
		$this->flag = $flag;
		$this->usuario = $usuario;
	}
	
	public function render()
	{
		$id = $this->usuario->getId();
		
        $q2 = new Zend_Db_Select(Zend_Registry::get('db'));
	    $q2->from (array('C'=>Zend_Registry::get('config')->tb->componente))
	     		->joinLeft(array('CU' => Zend_Registry::get('config')->tb->fl_componente_usuario),
				    		'C.id_componente = CU.id_componente',
				   	  		array('id_componente','id_fl_componente_usuario','config','id_usuario'))
	     	    ->where('C.componente = ?','Componenteq2')
	     	    ->where('CU.id_fl_componente_usuario = ?',(int)$this->flag);
	    $q2 = $q2->query(Zend_Db::FETCH_OBJ);
		$com_q2 = (array)$q2->fetchObject();  
		$cat = unserialize($com_q2['config']);
		
        $cat_q2= new Zend_Db_Select(Zend_Registry::get('db'));
	    $cat_q2->from (array('CA'=>Zend_Registry::get('config')->tb->categoria))
				->where('CA.id_categoria = ?',(int) $cat['id_categoria']);	
		$cat_q2 = $cat_q2->query(Zend_Db::FETCH_OBJ);
        $cat_q2 = (array)$cat_q2->fetchObject();		
        
        $q2_status = new Zend_Db_Select(Zend_Registry::get('db'));
	    $q2_status->from (array('CH'=>Zend_Registry::get('config')->tb->chamado),
	    						array('id_chamado','count(CH.id_status) count'))
	    		  ->joinLeft(array('C' => Zend_Registry::get('config')->tb->categoria),
				    		'C.id_categoria = CH.id_categoria',
				   	  		array('id_categoria','categoria'))
	    		  ->joinLeft(array('S' => Zend_Registry::get('config')->tb->status),
				    		'S.id_status = CH.id_status',
				   	  		array('id_status','status'))
				  ->where('CH.id_categoria = ?', (int)$cat['id_categoria']) 
				  ->order('CH.id_chamado ASC')
				  ->group('CH.id_status');
		$q2_status = $q2_status->query(Zend_Db::FETCH_OBJ);
        $q2_status = $q2_status->fetchAll(); 		
		
		require (CORE_HELPER_DEFAULT.'/Painel/Item/Componenteq2.php');
	}
	
	public function edit()
	{
 	   $verifica = new Zend_Db_Select(Zend_Registry::get('db'));
	   $verifica->from(Zend_Registry::get('config')->tb->fl_componente_usuario)
	               ->where('id_fl_componente_usuario = ?',(int)$this->flag);
	   $verifica = $verifica->query(Zend_Db::FETCH_OBJ);
       $user = (array)$verifica->fetchObject();
	   $dados = unserialize($user['config']);

	   $categoria = new Zend_Db_Select(Zend_Registry::get('db'));
	   $categoria->from(Zend_Registry::get('config')->tb->categoria);
	   $categoria = $categoria->query(Zend_Db::FETCH_OBJ);
	   $categoria = $categoria->fetchAll();
	   
	   require (CORE_HELPER_DEFAULT.'/Painel/Item/Edit/Componenteq2.php');
	}
}