<?php
require_once CORE . '/Painel/Item/Interface.php';

Class MyCore_Painel_Item_Componenteq1 implements Painel_Item_Interface
{
	/**
	 *@var Cadastro_Item_Abstract
	 */
	protected $usuario;
	
	public function __construct (Cadastro_Item_Abstract $usuario,$flag) 
	{
		$this->flag = $flag;
		$this->usuario = $usuario;
	}
	
	public function render()
	{
		$id = $this->usuario->getId();
		
		$componente= new Zend_Db_Select(Zend_Registry::get('db'));
	    $componente->from (array('C'=>Zend_Registry::get('config')->tb->componente))
	     		->joinLeft(array('CS' =>Zend_Registry::get('config')->tb->fl_componente_usuario),
				    		'CS.id_componente = C.id_componente',
				   	  		array('id_componente','id_fl_componente_usuario','id_usuario','config'))
				->where('CS.id_fl_componente_usuario = ?', (int)$this->flag);
		$componente = $componente->query(Zend_Db::FETCH_OBJ);
        $query = (array)$componente->fetchObject(); 
        $config = unserialize($query['config']);
        
        $categoria= new Zend_Db_Select(Zend_Registry::get('db'));
	    $categoria->from (array('CA'=>Zend_Registry::get('config')->tb->categoria))
				->where('CA.id_categoria = ?',(int) $config['id_categoria']);	
		$categoria = $categoria->query(Zend_Db::FETCH_OBJ);
        $categoria = (array)$categoria->fetchObject();
         
        $status= new Zend_Db_Select(Zend_Registry::get('db'));
	    $status->from (array('S'=>Zend_Registry::get('config')->tb->status))
				->where('S.id_status = ?', (int)$config['id_status']);	
		$status = $status->query(Zend_Db::FETCH_OBJ);
        $status = (array)$status->fetchObject();
         
        $chamado= new Zend_Db_Select(Zend_Registry::get('db'));
	    $chamado->from (array('CH'=>Zend_Registry::get('config')->tb->chamado))
	    		  ->joinLeft(array('CAT' =>Zend_Registry::get('config')->tb->categoria),
				    		'CAT.id_categoria = CH.id_categoria',
				   	  		array('id_categoria','categoria'))
	    		  ->joinLeft(array('ST' => Zend_Registry::get('config')->tb->status),
				    		'ST.id_status = CH.id_status',
				   	  		array('id_status','status'))
				  ->where('CH.id_categoria = ?', (int)$config['id_categoria']) 
				  ->where('CH.id_status = ?', (int)$config['id_status']) 
	              ->limit($config['quantidade']); 
		$res = $chamado->query(Zend_Db::FETCH_OBJ)->fetchAll();
		
		require (CORE_HELPER_DEFAULT.'/Painel/Item/Componenteq1.php');
	}
	
	public function edit()
	{
		   $verifica = new Zend_Db_Select(Zend_Registry::get('db'));
	       $verifica->from(Zend_Registry::get('config')->tb->fl_componente_usuario)
	               ->where('id_fl_componente_usuario = ?',(int)$this->flag);
		   $verifica = $verifica->query(Zend_Db::FETCH_OBJ);
		   $user = (array)$verifica->fetchObject();
		   print_r($user);
		   $dados = unserialize($user['config']);
		
           $categoria = new Zend_Db_Select(Zend_Registry::get('db'));
	       $categoria->from(Zend_Registry::get('config')->tb->categoria);
		   $categoria = $categoria->query(Zend_Db::FETCH_OBJ);
		   $categoria = $categoria->fetchAll();
		   
		   $status = new Zend_Db_Select(Zend_Registry::get('db'));
		   $status->from(Zend_Registry::get('config')->tb->status);
		   $status = $status->query(Zend_Db::FETCH_OBJ);
		   $status = $status->fetchAll();
		   
		
		require (CORE_HELPER_DEFAULT.'/Painel/Item/Edit/Componenteq1.php');
	}
}