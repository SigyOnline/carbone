<?php
require_once 'app/models/MyCore/Cadastro/Noticia.php';

class Admin_NoticiaController extends My_Controller
{
	public function ini()
	{
	}
	public function indexAction()
	{
		$obj = new Cadastro_Noticia($this->db);

		if ( $_GET['excluir'] > 0 )
		{
			$r = $obj->apaga($_GET['excluir']);
			
			if ( $r > 0 )
				$this->view->e = new Success('Noticia apagada com sucesso.');
			else 
				$this->view->e = new Error('Falha ao apagar. Nenhuma alteração foi feita.');
		}
		
		$this->view->result = $obj->lista();
		
	}
	
	public function novoAction()
	{
		$this->view->result = array();
		if ( $_POST )
		{
			$post = $_POST;
			$post['data'] = date('Y-m-d', strtotime(str_replace('/', '-', $post['data'])));

			$noticia = new Cadastro_Noticia($this->db);
			$res = $noticia->novo($post);
			
			if ( $res > 0 )
			{
				$this->_redirect($this->view->baseModule . '/'. $this->view->baseController . '/editar/id/' . $res . '?ok');
			} else 
			{
				$this->view->e = new Error('Erro ao cadastrar noticia.');
				$this->view->result = $post;
			}
			
		}
		echo $this->view->render('noticia/form.php');
		exit;
	}
	
	public function editarAction()
	{
		
		if ($this->id < 1 )
			$this->_redirect($this->view->baseModule . '/'. $this->view->baseController);
		
		if ( isset($_GET['ok']) )
			$this->view->e = new Success('Nova Noticia adicionada');
		
		$imoveis = new Cadastro_Noticia($this->db);
		
		if ( $_POST )
		{
			$post = $_POST;
			$post['data'] = date('Y-m-d', strtotime(str_replace('/', '-', $post['data'])));
			
			$res = $imoveis->edita($this->id, $post);
			
			if ( $res > 0 )
			{
				$this->view->e = new Success('Noticia atualizada com sucesso.');
			}else 
			{
				$this->view->e = new Error('Falha ao atualizar. Nenhuma alteração foi feita.');
			}
		}
		
		$this->view->result = $imoveis->GetNoticia($this->id);
		
		echo $this->view->render('noticia/form.php');
		exit;
	}
}	