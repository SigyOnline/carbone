<?php
require_once 'app/models/MyCore/Cadastro/Banner.php';

class Admin_BannerController extends My_Controller
{
	public function ini()
	{
		//$this->_redirect('admin');
	}
	public function indexAction()
	{
		$obj = new Cadastro_Banner($this->db);

		if ( $_GET['excluir'] > 0 )
		{
			$r = $obj->apaga($_GET['excluir']);
			
			if ( $r > 0 )
				$this->view->e = new Success('Banner apagado com sucesso.');
			else 
				$this->view->e = new Error('Falha ao apagar. Nenhuma alteração foi feita.');
		}
		
		$this->view->result = $obj->lista();
		
	}
	
	public function novoAction()
	{
		$this->view->result = array();
		if ( $_POST )
		{
			$imoveis = new Cadastro_Banner($this->db);
			$res = $imoveis->novo($_POST);
			
			if ( $res > 0 )
			{
				$this->_redirect($this->view->baseModule . '/'. $this->view->baseController . '/editar/id/' . $res . '?ok');
			} else 
			{
				$this->view->e = new Error('Erro ao cadastrar imóvel');
				$this->view->result = $_POST;
			}
			
		}
		echo $this->view->render('banner/form.php');
		exit;
	}
	
	public function editarAction()
	{
		
		if ($this->id < 1 )
			$this->_redirect($this->view->baseModule . '/'. $this->view->baseController);
		
		if ( isset($_GET['ok']) )
			$this->view->e = new Success('Novo banner adicionado');
		
		$imoveis = new Cadastro_Banner($this->db);
		
		if ( $_POST )
		{
			$res = $imoveis->edita($this->id, $_POST);
			
			if ( $res > 0 )
			{
				$this->view->e = new Success('Banner atualizado com sucesso.');
			}else 
			{
				$this->view->e = new Error('Falha ao atualizar. Nenhuma alteração foi feita.');
			}
		}
		
		$this->view->result = $imoveis->getDados($this->id);
		
		echo $this->view->render('banner/form.php');
		exit;
	}
}	