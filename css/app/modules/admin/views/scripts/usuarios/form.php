<?php echo $this->render('header.php'); ?>
<div id="page-wrapper">
	<div class="row">
	    <div class="col-lg-12">
	        <h1 class="page-header">Usuários</h1>
	    </div>
	    <!-- /.col-lg-12 -->
	</div>
	<div class="row">
		<div class="col-lg-12">
		    <div class="panel panel-default">
		        <div class="panel-heading">
		            Cadastrar Usuário
		        </div>
		        <!-- /.panel-heading -->
				<div class="panel-body">
				<?php if ( $this->e )
	echo $this->e;?>
				<div class="row">
				    <div class="col-lg-6">
				        <form role="form" action="" method="post" name="form">
				            <div class="form-group">
				                <label>Nome</label>
				                <input class="form-control" type="text" name="nome" value="<?php echo $this->dados['nome'] ?>" placeholder="Digite o nome do usuário..." required="required">
				            </div>
				            <div class="form-group">
				                <label>Email</label>
				                <input class="form-control" type="email" name="email"  value="<?php echo $this->dados['email'] ?>" placeholder="email@email.com.br..." required="required">
				            </div>
				            <div class="form-group">
				                <label>Login</label>
				                <input class="form-control" type="text" name="login"  value="<?php echo $this->dados['login'] ?>" placeholder="usuarioetc..." required="required">
				            </div>
				            <div class="form-group">
				                <label>Senha</label>
				                <input class="form-control" type="password" name="senha"  value="<?php echo $this->dados['senha'] ?>" placeholder="Senha" required="required">
				            </div>				            
				            <button type="submit" class="btn btn-primary">Salvar</button>
				        </form>
				    </div>
				</div>
				<!-- /.row (nested) -->
			</div>
	        <!-- /.panel-body -->
	    </div>
	    <!-- /.panel -->
	</div> <!-- /.col-12 -->
</div> <!-- /.col-12 -->

			
			
<?php echo $this->render('footer.php'); ?>