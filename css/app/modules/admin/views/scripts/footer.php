	</div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?php echo $this->baseUrl ?>sbadmin/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo $this->baseUrl ?>sbadmin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Datepicker -->
    <script src="<?php echo $this->baseUrl ?>sbadmin/bower_components/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo $this->baseUrl ?>sbadmin/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="<?php echo $this->baseUrl ?>sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo $this->baseUrl ?>sbadmin/bower_components/datatables-plugins/integration/bootstrap/dataTables.bootstrap.min.js"></script>
    
    <!-- Morris Charts JavaScript -->
    <!-- <script src="<?php echo $this->baseUrl ?>/sbadmin/bower_components/raphael/raphael-min.js"></script>
    <script src="<?php echo $this->baseUrl ?>/sbadmin/bower_components/morrisjs/morris.min.js"></script>
    <script src="<?php echo $this->baseUrl ?>/sbadmin/js/morris-data.js"></script>-->

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo $this->baseUrl ?>sbadmin/dist/js/sb-admin-2.js"></script>
    <script>
    $(document).ready(function() {
    	$.datepicker.regional['pt-BR'] = {
    			closeText: 'Fechar',
    			prevText: '&lt;Anterior',
    			nextText: 'Próximo&gt;',
    			currentText: 'Hoje',
    			monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho',
    			'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
    			monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
    			'Jul','Ago','Set','Out','Nov','Dez'],
    			dayNames: ['Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sabado'],
    			dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
    			dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
    			weekHeader: 'Sm',
    			dateFormat: 'dd/mm/yy',
    			firstDay: 0,
    			isRTL: false,
    			showMonthAfterYear: false,
    			yearSuffix: ''};

    	

    	$.datepicker.setDefaults($.datepicker.regional['pt-BR']);
    	$('.datepicker').datepicker({
    	});
    	$(".alert").delay(5000).fadeOut(1000);
        $('#dataTables, .dataTables').DataTable({
                responsive: true,
                "language": {
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "Pesquisar",
                    "oPaginate": {
                        "sNext": "Próximo",
                        "sPrevious": "Anterior",
                        "sFirst": "Primeiro",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    }
                }
                    
        });
    });
    </script>
</body>

</html>