<?php echo $this->render('header.php'); ?>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Imóveis</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            Imóveis Cadastrados
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Tipo de Imóvel</th>
                                            <th>Finalidade</th>
                                            <th>Empreendimento</th>
                                            <th>Cidade</th>
                                            <th>Segmento</th>
                                            <th>Valor</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($this->result as $result):?>
                                        <tr>
                                            <td><?php echo $result->codigo; ?></td>
                                            <td><?php echo $result->tipo; ?></td>
                                            <td><?php echo $result->finalidade; ?></td>
                                            <td><?php echo $result->empreendimento; ?></td>
                                            <td><?php echo $result->cidade; ?></td>
                                            <td><?php echo $result->status; ?></td>
                                            <td><?php echo number_format( $result->valor_venda ? $result->valor_venda : $result->valor_locacao,2,',','.'); ?></td>
                                            <td nowrap="nowrap">
                                            	<a href="<?php echo $this->baseUrl ?>admin/imovel/editar/id/<?php echo $result->codigo; ?>"><span class="fa fa-edit"></span></a>
                                            	<a href="<?php echo $this->baseUrl ?>admin/imovel?excluir=<?php echo $result->codigo; ?>" onclick="return confirm('Deseja realmente excluir este imóvel?');"><span class="fa fa-trash-o"></span></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
</div>

<?php echo $this->render('footer.php'); ?>
