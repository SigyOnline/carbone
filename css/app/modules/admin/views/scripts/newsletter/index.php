<?php echo $this->render('header.php'); ?>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Newsletter</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            Cadastrados
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <?php if ( $this->e ) echo $this->e;?>
                            <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="datsaTables">
                                    <thead>
                                        <tr>
                                            <th><a href="<?php echo $this->baseUrl ?>admin/newsletter?exportar=sim">Exportar Lista</a></th>
                                        </tr>
                                    </thead>
                                   </table>
						<table class="table table-striped table-bordered table-hover" id="dataTables">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nome</th>
                                            <th>Email</th>
                                            <th>Data</th>
                                            <th></th>
                                           <!--  <th></th>  -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    	if ( $this->result ): 
                                    		foreach ($this->result as $row) :?>
                                        <tr class="odd gradeX">
                                            <td><?php echo $row->id_newsletter;?></td>
                                            <td><?php echo $row->nome;?></td>
                                            <td><?php echo $row->email;?></td>
                                            <td class="center">
                                            	<?php echo date('d-m-Y', strtotime($row->criado)); ?></td>
                                            <td class="center">
                                            	<!--  <a href="<?php  echo $this->baseUrl; ?><?php  echo $this->baseModule; ?>/<?php echo $this->baseController; ?>/editar/id/<?php echo $row->id_noticia; ?>"><span class="fa fa-edit"></span></a>-->
                                            	 <a href="<?php echo $this->baseUrl ?>admin/newsletter?excluir=<?php echo $row->id_newsletter; ?>" onclick="return confirm('Deseja realmente excluir este registro?');"><span class="fa fa-trash-o"></span></a> 
                                            </td>
                                        </tr>
                                    <?php 	endforeach;
                                    	endif; 
                                    ?>
                                    </tbody>
                                </table>
                                <?php //echo $this->result; ?>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
</div>

<?php echo $this->render('footer.php'); ?>