<?php echo $this->render('header.php'); 
if ( $_POST )
	$b = $_POST;
else
	$b = (array) $this->result;

$campanha =  Zend_Registry::get('config')->www->host.'/newsletter/campanha'.$this->id.'.php'; 
?>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Campanha <?php echo $this->id; ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
<div class="row">
	<div class="col-lg-12">
	    <div class="panel panel-default">
	        <div class="panel-heading">
	           Html Campanha
	        </div>
	        <!-- /.panel-heading -->
<div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" action="" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label>Html</label>
                                            <textarea class="form-control" rows="15" name="noticia" id="editor1"><?php echo file_get_contents($campanha);?></textarea>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6">
                                <?php if ( $this->result->imagem )
                                		echo "Preview: <br><img src='" . $this->baseUrl . $this->result->imagem ."' style='max-width:400px;max-height:400px' />";?>
                                
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
	        <!-- /.panel-body -->
	    </div>
	    <!-- /.panel -->
	</div> <!-- /.col-6 -->
</div>     
</div>           
<?php echo $this->render('footer.php'); ?>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo $this->baseUrl ?>/sbadmin/bower_components/ckeditor/ckeditor.js"></script>
<script src="<?php echo $this->baseUrl ?>/sbadmin/js/jquery.maskedinput.min.js"></script>
