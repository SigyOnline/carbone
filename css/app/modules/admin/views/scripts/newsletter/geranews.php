<?php echo $this->render('header.php'); ?>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Gerar Campanha</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
             <div class="row">
             <div class="col-lg-12">
                <div class="panel panel-default">
                        <div class="panel-heading">
                           Campos Campanha
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        	<form role="form" action="" method="post" enctype="multipart/form-data">
                        	 <div class="row">
								<div class="form-group col-lg-3">
								    <label>Titulo Da News</label>
								    <input class="form-control" name="titulo">
								</div>
							</div>
                        	 <div class="row">
								<div class="form-group col-lg-3">
								    <label>Link</label>
								    <input class="form-control" name="link" placeholder="http://www.exemplo.com.br">
								</div>
							</div>
							<div class="row">
								<div class="form-group col-lg-3">
								    <label>Referência dos Imóveis</label>
								    <input class="form-control" name="ref" placeholder="Ex:100,200,300">
								</div>
                        	 </div>
							<div class="row">
								<div class="form-group col-lg-2">
								    <label>Imagem</label>
								    <input name="imagem" type="file">
								</div>
                        	 </div>
                        	 <div class="row">
								<div class="form-group col-lg-3">
								    <label>Link Facebook</label>
								    <input class="form-control" name="facebook">
								</div>
                        	 </div>
                        	 <div class="row">
								<div class="form-group col-lg-3">
								    <label>Link Twitter</label>
								    <input class="form-control" name="twitter">
								</div>
                        	 </div>
                        	 <div class="row">
								<div class="form-group col-lg-3">
								    <label>Link Youtube</label>
								    <input class="form-control" name="youtube">
								</div>
                        	 </div>
                        	 <div class="row">
								<div class="form-group col-lg-3">
								    <label>Descrição</label>
								    <textarea class="form-control" name="descricao"></textarea>
								</div>
                        	 </div>
                        	 <div class="row">
								<div class="form-group col-lg-3">
								    <label>Data Cadastro</label>
								    <input class="form-control datepicker" name="data" placeholder="30/10/2016">
								</div>
                        	 </div>
                        	 <div class="row">
                        	 	<div class="form-group col-lg-4">
                        	 		<button class="btn btn-warning">Cadastrar</button>
                        	 		<!-- <button class="btn btn-warning">Total de Acessos</button>  -->
                        	 	</div>
                        	 </div>
                        	 </form>
                        </div>
                </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                <div class="panel panel-default">
                        <div class="panel-heading">
                          Campanhas Cadastradas
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <?php if ( $this->e ) echo $this->e;?>
                            <div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables">
                                    <thead>
                                        <tr>
                                            <th>Id Campanha</th>
                                            <th>Titulo Da Campanha</th>
                                            <th>Link Da Campanha</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    	if ( $this->result ): 
                                    		foreach ($this->result as $row) :
                                    	?>
                                        <tr class="odd gradeX">
                                            <td><?php echo $row->id_campanha; ?></td>
                                            <td><?php echo $row->titulo; ?></td>
                                            <td><a target="_blank" href="<?php echo Zend_Registry::get('config')->www->host.'/newsletter/campanha'.$row->id_campanha.'.php'; ?>"><?php echo Zend_Registry::get('config')->www->host.'/newsletter/campanha'.$row->id_campanha.'.php'; ?></a></td>
                                             <td><a href="<?php echo $this->baseUrl ?>admin/newsletter/gerahtml/id/<?php echo $row->id_campanha; ?>">Gerar Html</a></td>
                                        </tr>
                                    <?php 	endforeach;
                                    	endif; 
                                    ?>
                                    </tbody>
                                </table>
                                <?php //echo $this->result; ?>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
</div>
<?php echo $this->render('footer.php'); ?>
