				<table width="930" border="0" align="center" cellpadding="5" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif;font-size:12px;">
				  <tr>
				    <td width="300" align="left" valign="top" bgcolor="#84A069"><img src="http://www.entelco.com.br/images/logo_mail.jpg" width="230" height="82" /></td>
				    <td width="300" align="left" valign="top" bgcolor="#84A069">&nbsp;</td>
				    <td width="300" align="left" valign="top" bgcolor="#84A069">&nbsp;</td>
				  </tr>
					  <tr>
					    <td colspan="3" align="center" valign="top" bgcolor="#CFEDB3">
					    	Sua inscriçao no Curso: <strong>%%==curso==%%</strong> foi efetuada.
					    </td>
					  </tr>
					  <tr>
					    <td colspan="3" align="center" valign="top" bgcolor="#CFEDB3">
					    	Turma: <strong>%%==turma==%%</strong>
					    </td>
					  </tr>
				  <tr>
				    <td align="right" valign="top" bgcolor="#CFEDB3">Para efetuar o pagamento acesse o seu painel:</td>
				    <td align="left" valign="top" bgcolor="#CFEDB3">
				    	<a style="color:#000000; font-weight:bold" 
				    		href="%%==linkPainel==%%" target="_blank">%%==linkPainel==%%</a>
				    </td>
				    <td align="left" valign="top" bgcolor="#CFEDB3">Valor: <strong>R$ %%==valor==%% </strong></td>
				  </tr>
				  <tr>
				    <td align="left" valign="top" bgcolor="#eaffcd">Nome: <strong>%%==nome==%%</strong></td>
				    <td align="left" valign="top" bgcolor="#eaffcd">CPF: <strong>%%==cpf==%%</strong> RG: <strong>%%==rg==%%</strong></td>
				    <td align="left" valign="top" bgcolor="#eaffcd">Data Nasc.: <strong>%%==nascimento==%%</strong></td>
				  </tr>
				  <tr>
				    <td align="left" valign="top" bgcolor="#eaffcd">Enderço: <strong>%%==endereco==%%</strong></td>
				    <td align="left" valign="top" bgcolor="#eaffcd">Bairro: <strong>%%==bairro==%%</strong></td>
				    <td align="left" valign="top" bgcolor="#eaffcd">Cep: <strong>%%==cep==%%</strong></td>
				  </tr>
				  <tr>
				    <td colspan="2" align="left" valign="top" bgcolor="#eaffcd">Cidade: <strong>%%==cidade==%%</strong></td>
				    <td align="left" valign="top" bgcolor="#eaffcd">Uf: <strong>%%==uf==%%</strong></td>
				  </tr>
				  <tr>
				    <td align="left" valign="top" bgcolor="#eaffcd">TelRes: <strong>%%==telResidencial==%%</strong></td>
				    <td align="left" valign="top" bgcolor="#eaffcd">TelCom: <strong>%%==telCom==%%</strong></td>
				    <td align="left" valign="top" bgcolor="#eaffcd">Cel: <strong>%%==celular==%%</strong></td>
				  </tr>
				  <tr>
				    <td align="left" valign="top" bgcolor="#eaffcd">Email1: %%==email1==%%</td>
				    <td align="left" valign="top" bgcolor="#eaffcd">Email2: %%==email2==%%</td>
				    <td align="left" valign="top" bgcolor="#eaffcd">MSN: %%==msn==%%</td>
				  </tr>
				   <tr>
				    <td colspan="3" align="right" valign="top" bgcolor="#eaffcd"><strong>IMPORTANTE: A inscri&ccedil;&atilde;o ser&aacute; efetivada somente ap&oacute;s a confirma&ccedil;&atilde;o do pagamento identificado pela ENTELCO.</strong></td>
				  </tr>
				  <tr>
				    <td colspan="3" align="right" valign="top" bgcolor="#84A069" style="color:#CFEDB3;font-size:14px;"><strong>Entelco - Todos os Direitos Reservados</strong></td>
				  </tr>
				</table>