﻿<?php echo $this->render("header.php"); ?>
<main class="pr border_top">

		<section class="center">

			<!-- titulo -->

            <h1 class="pa noMobile ttl_internas outro_position41">Imóveis Favoritos</h1>

            <!-- no results -->

			<section id="no_result" <?php echo (count($this->result) == 0 )?'':'style="display: none;"'?>>

                <!--<div class="pa imagem_casal"><img src="src/imagem-casal.png" alt="Imagem referente a não retorno de sua pesquisa" /></div>-->

                <div class="bg_faixa">

                    <div class="center">

                        <h1 class="bold">NÃO HÁ IMÓVEIS FAVORITOS.</h1>

                        <p class="">Adicione clicando na estrela do imóvel para adicionar aos favoritos.</p>

                        <div class="comporta_links"><a href="" class="bold opacity">IR PARA HOME</a><a href="contato" class="bold opacity">FALE CONOSCO </a></div>

                    </div>

                </div>

            </section>

            <aside class="excluir"><a href="javascript:;"  onclick="removerTodosFav();">[X] Excluir Todos</a></aside>

			<ul id="padrao_lista">

				<!-- item -->
<?php foreach ( $this->result as $result) : 
	            	if ( $result['status'] == 'VENDA' )
	            	{
	            		$valor = number_format($result['valor_venda'], 2, ',', '.');
	            	}elseif( $result['status'] == 'ALUGUEL' )
	            	{
	            		$valor = number_format($result['valor_locacao'], 2, ',', '.');
	            	}elseif ( strtoupper($result['status']) == 'VENDA E ALUGUEL')
	            	{
                        $valor = number_format($result['valor_venda'], 2, ',', '.') . " / " . number_format($result['valor_locacao'], 2, ',', '.');
	            	}else
	            	{
	            		$valor = number_format($result['valor_locacao'], 2, ',', '.');
	            	}
            	?>
                <li class="imovel_destaque" id="fav-<?php echo $result['codigo']; ?>">

                    <img src="<?php echo ($result['foto_destaque'])? $result['foto_destaque'] : 'src/foto-indisponivel.jpg'; ?>" alt="Foto do imóvel em destaque" style="width:360px; height:220px;"/>

                    <div class="referencia_destaque pa">REF.: <?php echo $result['codigo']; ?></div>

                    <div class="pos_info">

                        <h1><?php echo $result['bairro']; ?> / <?php echo $result['uf']; ?></h1>

                        <h2><?php echo $result['cat']; ?></h2>

                    </div>

                    <div class="infos">

                        <h1 class="local"><?php echo $result['bairro']; ?> / <?php echo $result['uf']; ?></h1>

                        <h2 class="tipo"><?php echo $result['cat']; ?></h2>

                        <span class="acabamento"></span>

                        <h2 class="itens"><?php if( !empty($result['dormitorios']) ):  echo $result['dormitorios']; ?> Dorm(s)  |<?php endif; ?> <?php if( !empty($result['vagas']) ): echo $result['vagas']; ?> Vaga(s) |<?php endif; ?> <?php if( !empty($result['suites']) ): echo $result['suites']; ?> Suíte(s)<?php endif; ?></h2>

                        <h2 class="valor"><?php echo $valor; ?></h2>

                        <a href="<?php echo $this->seo->getLink($result); ?>" class="">CONSULTE</a>

                        <div class="box_check posLeft">

                            <input type="checkbox" name="favorito" id="fav" />

                            <label for="fav" class="icon_favorito <?php echo Cadastro_FavCom::getInstance()->getFavClass($result['codigo']); ?>" onclick="favoritosRemove(this, <?php echo $result['codigo']; ?>);" title="favorito"></label>

                        </div>

                        <div class="box_check posRight">

                            <label for="compare" class="icon_compare <?php echo Cadastro_FavCom::getInstance()->getComClass($result['codigo']); ?>" onclick="comparar(this, <?php echo $result['codigo']; ?>);" title="compare"></label>

                        </div>

                    </div>    

                </li>
 <?php endforeach; ?>
                <!-- item -->

			</ul>	

			<!-- <nav id="paginacao">

				<div class="pag_number">

					<div class="number_nav active">1</div>

					<div class="number_nav" onclick="atualizaimoveis(2)">2</div>

					<div class="number_nav" onclick="atualizaimoveis(3)">3</div>

					<div class="number_nav" onclick="atualizaimoveis(4)">4</div>

					<div class="number_nav" onclick="atualizaimoveis(5)">5</div>

                </div>

			</nav>  -->

		</section>

	</main>

<?php echo $this->render("footer.php"); ?>
<script>
function favoritosRemove(ob, id_imovel) {
	$("#fav-" + id_imovel).remove();
	
	$.post('imoveis/favcom', {'item':'fav', 'acao':'remove', 'id':id_imovel }, function (data) {
		if(parseInt(data) == 0) {
			$("#resultado_off").fadeIn();
		}
		atualizaFav(data);
	});
}   			
function removerTodosFav()
{
	$.post('imoveis/favcom', {'item':'fav','acao':'removertodos'}, function(data){
		location.href = location.href;  
	});
}
</script>
