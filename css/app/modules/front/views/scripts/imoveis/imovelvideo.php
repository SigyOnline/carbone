<?php echo $this->render("header.php"); ?>
<div id="box_ttlMobile">
		<div class="center">
			<h1>VÍDEO DO IMÓVEL</h1>
		</div>
	</div>
    <!--conteudo-->
	<article class="content pad010">
	<?php 
		if( !empty($this->dados->video) ):
		
			if( strpos($this->dados->video, 'embed') )
			{
				$link = $this->dados->video;
			}else 
			{
				//$link = 'https://www.youtube.com/embed/' .substr($this->dados->video, strpos($this->dados->video, 'v=')+2, strlen($this->dados->video));
				$link = 'https://www.youtube.com/embed/' . $this->dados->video;
			}
     ?>
        <iframe width="100%" height="360" src="<?php echo $link; ?>" frameborder="0" allowfullscreen></iframe>
    <?php endif; ?>    
        <a href="javascript:window.history.go(-1)" class="btn_back">VOLTAR PARA PÁGINA ANTERIOR</a>
    </article>
<?php echo $this->render("footer.php"); ?>