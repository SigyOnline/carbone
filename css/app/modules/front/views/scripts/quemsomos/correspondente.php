<?php echo $this->render("header.php"); ?>
		<main class="pr border_top">
		<section class="center">
			<!-- titulo -->
            <h1 class="pa noMobile ttl_internas">Correspondente Bancário</h1>
			<section class="box_img_padrao">
<!--				<h2 class="sub_ttl"></h2>-->
                
                <div class="corresp_img"></div>
                
				<p class="paragrafo_internas">Este é um serviço direcionado aos que buscam praticidade no atendimento e agilidade nas contratações, como: </p>
                
				<li> Abertura de Conta;</li>
                <li> Contratação de crédito consignado;</li>
                <li> Portabilidade de crédito consignado INSS;</li>
                <li> Seguros de Vida;</li>
                <li> Financiamento habitacional;</li>
                <li> Análise do perfil financeiro e indicação da melhor opção de financiamento através de cálculos de tarifas e taxas bancárias;</li> 
                <li> Análise da documentação do comprador, imóvel e vendedor;</li>
                <li> Acompanhamento do processo, desde a aprovação do crédito, vistoria do imóvel, escritura, registro do imóvel no respectivo cartório de registro e a liberação dos valores junto ao banco;</li>
                                
                <p class="paragrafo_internas">Entre em <a href='contato' class="contato_lightbox"><strong>contato conosco</strong></a> e obtenha mais informações.</p>
			</section>
				
		</section>
	
<?php echo $this->render("footer.php"); ?>
	</main><!-- /main -->

