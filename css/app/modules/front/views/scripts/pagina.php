<?php if ($this->pageCount): ?> 
<?php 
if ( sizeof($_GET) > 0 ):
	$g = $_GET; 
	$q = array();
	
	unset($g['p']);
	
	while (list($k,$v) = each($g)) 
	{
		if(is_array($v))
		{
			foreach ( $v as $value )
				$q[] = $k .'[]='.$value; 	
		}else
		{
				$q[] = $k .'='.$v;
		} 
	}
	
	$q = '&' . implode('&',$q);
endif;
$front = Zend_Controller_Front::getInstance();
?>
    			
					<?php foreach ($this->pagesInRange as $page): ?> 
					  <?php if ($page != $this->current): ?>
					   <div class="number_nav"><a href="<?php echo $this->baseUrl . $front->getRequest()->getControllerName() 
					   									. '/'. $front->getRequest()->getActionName() .'/?p='.$page . $q; ?>"><?php echo $page; ?></a></div> 
					  <?php else: ?>
					  <div class="number_nav active">  <a href="<?php echo $this->baseUrl . $front->getRequest()->getControllerName() 
					   									. '/'. $front->getRequest()->getActionName() .'?p='.$page . $q; ?>" class="active"><?php echo $page; ?></a></div> 
					  <?php endif; ?>
					<?php endforeach; ?>				
						
<?php endif; ?>
