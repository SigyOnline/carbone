<?php echo $this->render("header.php"); ?>
        <main class="pr border_top">
        <section class="center">
            <!-- titulo -->
            <h1 class="pa noMobile ttl_internas">JM FOX / Noticias</h1>
             
     	<section class="center">
    		<!-- titulo -->
    		<div class="boxTitulo">
                <h2>Notícia</h2>
            </div>
			<div id="detalhe_noticia" class="row">
				<figure>
					<img src="<?php echo ($this->noticia->imagem)? $this->noticia->imagem : 'src/foto-indisponivel.jpg'; ?>" alt="Imagem da notícia" />
				</figure>
				<span class="date"><?php echo date('d-m-Y',strtotime($this->noticia->data)); ?></span>
				<h3><?php echo $this->noticia->titulo; ?></h3>
				<p><?php echo $this->noticia->noticia; ?></p>
			</div>
            <!-- btn back -->
            <a href="servicos/portaldenoticias" class="btn_back">VOLTAR PARA PORTAL NOTÍCIAS</a> 
        </section>

        </section>
    </main>
<?php echo $this->render("footer.php"); ?>
    </main><!-- /main -->
</body>
</html>