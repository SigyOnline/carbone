<?php echo $this->render("header.php"); ?>
<main class="pr border_top">
	<section class="center">
		<!-- titulo -->
		<h5 class="pa noMobile ttl_internas">Nosso Serviços</h5>
		<h2>A Carbone Imóveis além de intermediar a venda ou locação de imóveis, oferece:</h2><br>
		<div class="fotosevicos">
			<img src="src/servicos.jpg" alt="">
		</div>
		<section class="box_img_padrao servtext">
			<p class="paragrafo_internas">ASSESSORIA</p>
			<p class="paragrafo_internas">- Jurídica nos contratos de locação, venda e inventário.</p>
			<p class="paragrafo_internas">- Financiamento bancário.</p>
			<p class="paragrafo_internas">- Contratação de Seguro Fiança para locação.</p>
			<p class="paragrafo_internas">- Avaliação de imóveis para venda ou locação.</p>
			<p class="paragrafo_internas">- Equipe altamente treinada para o melhor atendimento do cliente.</p>
		</section>

	</section>
</main>
<?php echo $this->render("footer.php"); ?>
</main><!-- /main -->
</body>
</html>

