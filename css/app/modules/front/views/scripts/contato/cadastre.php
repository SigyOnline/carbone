<?php echo $this->render("header.php"); ?>
<main class="pr border_top">
		<section class="center">
            <!-- titulo -->
            <h1 class="pa noMobile ttl_internas">Cadastre seu Imóvel</h1>
			<form action="contato/enviado" id="form_cadastre" method="post" enctype="multipart/form-data">
			 <input type="hidden" value="Compramos-Seu-Terreno" name="Assunto" />
				<h2 class="ttl_form fl w100">Seus Dados</h2>
                <h3>Preencha o formulário abaixo. Um de nossos consultores entrará em contato e agendará uma visita para avaliação.</h3>
				<!--Linha Formulario-->
                <div class="linha_form">
                    <div class="box_input02">
                        <label for="cad01" class="label01">Nome <span>(*)</span></label>
                        <input type="text" id="cad01" class="input01" name="Nome" maxlength="30" required="required"/>
                    </div>
                    
                    <div class="box_input02">
                        <label for="cad02" class="label01">Seu E-mail</label>
                        <input type="email" id="cad02" class="input01" name="Email" maxlength="30"/>
                    </div>
                </div>
                
<!--
                    <div class="box_input">
                        <label for="cadEndereço" class="label01">Endereço <span>(*)</span></label>
                        <input type="text" id="cadEndereço" class="input01" name="Endereco" required="required" maxlength="30"/>
                    </div>
                    <div class="box_input">
                        <label for="cad03" class="label01">Cidade <span>(*)</span></label>
                        <input type="text" id="cad03" class="input01" name="Cidade" required="required" maxlength="50"/>
                    </div>
                    <div class="box_input">
                        <label for="cadBairro" class="label01">Bairro <span>(*)</span></label>
                        <input type="text" id="cadBairro" class="input01" name="Bairro" required="required" maxlength="50"/>
                    </div>
-->
                
	                <div class="linha_form">
                    
	                    <div class="box_input_DDDc">
	                        <label for="cad03" class="label01">DDD <span>(*)</span></label>
	                        <input type="tel" pattern="\[0-9]{2,3}$" id="cad03" class="input01" name="Telefone" maxlength="3" required="required"/>
                        </div>
	                    <div class="box_input_Telc">
	                        <label for="cad03" class="label01">Telefone 00000-0000 <span>(*)</span></label>
	                        <input type="tel" pattern="\[0-9]{4,5}-[0-9]{4}$" id="cad03" class="input01" name="Telefone" maxlength="9" required="required"/>
	                    </div>
	                    <div class="box_input_DDDc">
	                        <label for="cad03" class="label01">DDD</label>
	                        <input type="tel" pattern="\[0-9]{2,3}$" id="cad03" class="input01" name="Telefone" maxlength="3"/>
                        </div>
	                    <div class="box_input_Telcr">
	                        <label for="cad04" class="label01">Telefone 00000-0000</label>
	                        <input type="tel" pattern="\[0-9]{4,5}-[0-9]{4}$" id="cad04" class="input01" name="Celular" maxlength="10"/>
	                    </div>
               
                    </div>
                    

                
                <h2 class="ttl_form fl w100">Dados do Imóvel / Terreno</h2>	
                <!--Linha Formulario-->
                <div class="linha_form">
                    <div class="box_input02">
                        <label for="cadPretensao" class="label02">Pretensão <span>(*)</span></label>
                        <select name="Pretensao" id="cadPretensao" class="select01">
                            <option value="Selecione">Selecione</option>
                            <option value="Venda">Locação</option>
                            <option value="Alugar">Venda</option>
                            <option value="Alugar">Locação e Venda</option>
                        </select>
                    </div>
<!--
                    <div class="box_input">
                        <label for="cadfinalidade" class="label01">Finalidade</label>
                        <select name="Finalidade" id="cadfinalidade" class="select01">
                            <option value="">Selecione finalidade</option>
                            <option value="Residencial">Residencial</option>
                            <option value="Comercial">Comercial</option>
                            <option value="Venda">Venda</option>
                            <option value="Locação">Locação</option>
                            <option value="Lançamentos">Lançamentos</option>
                        </select>
                    </div>
-->
                    <div class="box_input02">
                        <label for="cadTipoImovel" class="label02">Tipo do Imóvel <span>(*)</span></label>
                        <select name="TipoImovel" id="cadTipoImovel" class="select01">
                            <option value="">Selecione</option>

                            <?php foreach ($this->categoria as $key) {?>
                                <option value="<?php echo $key->categoria?>"><?php echo $key->categoria?></option>
                            <?php }?>

                        </select>
                    </div>
<!--
                    <div class="box_input">
                        <label for="cadAreaConstruida" class="label01">Área Construída</label>
                        <input type="text" id="cadAreaConstruida" class="input01" name="AreaConstruida" maxlength="10"/>
                    </div> 
                    <div class="box_input">
                        <label for="cadDorm" class="label01">Dormitórios</label>
                        <select name="Dorm" id="cadDorm" class="select01">
                            <option value="">Selecione</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>    
                        </select>
                    </div>
                    <div class="box_input">
                        <label for="cadsuite" class="label01">Suítes</label>
                        <select name="Suite" id="cadsuite" class="select01">
                            <option value="">Selecione</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>    
                        </select>
                    </div>
                    <div class="box_input">
                        <label for="cadvaga" class="label01">Garagem / Vagas</label>
                        <select name="Vaga" id="cadvaga" class="select01">
                            <option value="">Selecione</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>    
                        </select>
                    </div>
                    <div class="box_input">
                        <label for="cadBanheiro" class="label01">Banheiros</label>
                        <select name="Banheiro" id="cadBanheiro" class="select01">
                            <option value="">Selecione</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>    
                        </select>
                    </div>
                    <div class="box_input">
                        <label for="cad13" class="label01">Endereço</label>
                        <input type="text" id="cad13" class="input01" name="Endereco_imovel" maxlength="10"/>
                    </div>
                    <div class="box_input">
                        <label for="cad14" class="label01">Cidade</label>
                        <input type="text" id="cad14" class="input01" name="Cidade_imovel" maxlength="30"/>
                    </div>
                    <div class="box_input">
                        <label for="cad15" class="label01">Bairro</label>
                        <input type="text" id="cad15" class="input01" name="Bairro_imovel" maxlength="30"/>
                    </div>
                    <div class="box_input">
                        <label for="cad16" class="label01">Valor pretendido</label>
                        <input type="text" id="cad16" class="input01" name="Valor_Pretendido" maxlength="10"/>
                    </div>
                    <div class="box_input">
                        <label for="cadFin" class="label01">Imóvel Financiado?</label>
                        <select name="imovelFinanciado" id="cadFin" class="select01">
                            <option value="Selecione">Selecione</option>
                            <option value="Sim">Sim</option>
                            <option value="Não">Não</option>  
                        </select>
                    </div>
                    <div class="box_input">
                        <label for="cadCond" class="label01">Paga Condomínio?</label>
                        <select name="condominioPaga" id="cadCond" class="select01">
                            <option value="">Selecione</option>
                            <option value="Sim">Sim</option>
                            <option value="Não">Não</option>  
                        </select>
                    </div>
                    <div class="box_input">
                        <label for="cad17" class="label01">Valor do Condomínio</label>
                        <input type="text" id="cad17" class="input01" name="Metragem" maxlength="10"/>
                    </div>
                </div>
-->
                <!-- linha formulario-->
<!--
                <div class="fl w100 marg_top15">
                    <label for="codDescr" class="label01">Descrição</label>
                    <textarea id="codDescr" class="txtarea01 w100" cols="2" rows="2" name="Descricao" maxlength="50"></textarea>
                </div>
-->
                <!-- linha formulario-->
<!--
                <div class="fl w100 marg_top15">
                    <label for="codObs" class="label01">Observações</label>
                    <textarea id="codObs" class="txtarea01 w100" cols="2" rows="2" name="observacoes" maxlength="50"></textarea>
                </div>
-->
                <!-- linha formulario-->
                <div class="fl w100 marg_top15">
                    <h2 class="subttl_form">Adicione até 5 fotos do imóvel:</h2>
                    <div class="box_input02">
                        <label for="cadFile" class="label01">Foto01</label>
                        <input type="file" id="cadFile" class="" name="fotos[]" maxlength="10"/>
                    </div>
                    <div class="box_input02">
                        <label for="cadFile2" class="label01">Foto02</label>
                        <input type="file" id="cadFile2" class="" name="fotos[]" maxlength="10"/>
                    </div>
                    <div class="box_input02">
                        <label for="cadFile3" class="label01">Foto03</label>
                        <input type="file" id="cadFile3" class="" name="fotos[]" maxlength="10"/>
                    </div>
                    <div class="box_input02">
                        <label for="cadFile4" class="label01">Foto04</label>
                        <input type="file" id="cadFile4" class="" name="fotos[]" maxlength="10"/>
                    </div>
                    <div class="box_input02">
                        <label for="cadFile5" class="label01">Foto05</label>
                        <input type="file" id="cadFile5" class="" name="fotos[]" maxlength="10"/>
                    </div>
                </div>
                <div class="box_captcha_internas marg_top30">
<!--                     <img class="" src="capsigy/captcha.php" alt="código captcha" id="captcha"> -->
                    <div class="alinha2">
                        <div class="g-recaptcha" data-sitekey="6LeSM1sUAAAAAPj0pDQiXjin0d2D83TrvXSFT32W"></div>
<!--                         <input type="button" class="captcha_loader_detal captcha_loader_internas" name="Limpar" value="Atualizar Código" onClick="document.getElementById('captcha').src='capsigy/captcha.php?ID=SigyOnline' + Math.random();"/>               
                        <input type="text" name="code" class="input_capt_internas" value="Digite o código" onClick="apagaCampo(this)" onBlur="preencheCampo(this)"/>
                        <p id="retornoCap" class="marg_cod fonte_red"></p> -->
                    </div>
                    <div class="box_sendText2"> 
                        <p class="sendText">Campos com <span>(*)</span> devem ser preenchidos</p>
                        <input type="submit" class="btn_enviar_form btn_padrao" value="ENVIAR"/>
                    </div>
                </div>
			</form>	
		</section>
	</main>
    
<?php echo $this->render("footer.php"); ?>

<script type="text/javascript">
jQuery(function(){
			jQuery( '#form_cadastre' ).ajaxForm( {
				 beforeSend: function() { 
						$('#form_cadastre input[type=submit]').attr({'value': 'ENVIANDO...', 'disabled':true});
			        },
		        success: function( data ) {
			        if(data)
			        {
			            if(parseInt(data.error) > 0 )
				        {
			            	$('#form_cadastre input[type=submit]').attr({'value': 'ENVIAR', 'disabled':false});
			            	$("#retornoCapLight").html('*Código inválido');
							alert(data.msg);
			            }
			            else
			            {
			            	$('#form_cadastre input[type=submit]').attr({'value': 'ENVIADO', 'disabled':true});
			            	window.location = 'contato/emailenviado';
			            	//alert(data.msg);					            
			            }
			        }
		        }
		    } );

		});
			
</script>
