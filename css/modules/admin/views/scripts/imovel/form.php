<?php echo $this->render('header.php'); ?>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Imóveis</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
<div class="row">
	<div class="col-lg-12">
	    <div class="panel panel-default">
	        <div class="panel-heading">
	            <?php echo empty($this->result) ? 'Cadastrar' : 'Editar'; ?> Imóvel
	        </div>
	        <form id="form-imovel" action="" name="form-imovel" method="post" enctype="multipart/form-data">
	        <!-- /.panel-heading -->
	        <div class="panel-body">
	        <?php if ( $this->e ) echo $this->e;?>
	            <!-- Nav tabs -->
	            <ul class="nav nav-tabs">
	                 <li class="active"><a href="#info-gerais" data-toggle="tab">Informações Gerais</a>
	                </li>
	                <li><a href="#localizacao" data-toggle="tab">Localização</a>
	                </li>
	                <li><a href="#dependencias" data-toggle="tab">Dependências / Complementos</a>
	                </li>
	                <li><a href="#segmento" data-toggle="tab">Valores</a>
	                </li>
	                <li><a href="#caracteristicas" data-toggle="tab">Características Unidade</a>
	                <li><a href="#caracteristicas_cond" data-toggle="tab">Características Condomínio</a>
	                </li>
	                <li><a href="#imagens" data-toggle="tab">Imagens</a>
	                </li> 
					 <li><a onclick="initSeo();" href="#seo" data-toggle="tab">Seo</a> 
	                </li>
	            </ul>
	
	            <!-- Tab panes -->
	            <div class="tab-content">
	                <div class="tab-pane active" id="info-gerais">
	                	<?php if ( $this->id > 0 ):?>
	                	<div class="row">
			                <div class="form-group col-lg-2">					
								<label>codigo</label> <input class="form-control" name="codigo"
									id="codigo" value="<?php echo $this->result['codigo']; ?>" disabled="disabled">
							</div>
	                	</div>
	                	<?php endif; ?>
	                	<div class="row">
			                <div class="form-group col-lg-2">					
								<label>Referência</label> <input class="form-control" name="referencia"
									id="referencia" value="<?php echo $this->result['referencia']; ?>">
							</div>
	                	</div>
	                 	<div class="form-group">
                                            <label>Portifólio</label>
                                            <label class="radio-inline">
                                                <input type="radio" name="portifolio" id="optionsRadiosInline1" 
                                                	value="1" <?php echo ($this->result['portifolio'] == 1? 'checked':NULL);?>>Sim
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="portifolio" id="optionsRadiosInline2" value="0"
                                                	<?php echo ($this->result['portifolio'] != 1? 'checked':NULL);?>>Não
                                            </label>
                                        </div>
	                	<div class="row">
			                <div class="form-group col-lg-4">					
								<label>Empreendimento</label> <input class="form-control" name="empreendimento"
									id="empreendimento" value="<?php echo $this->result['empreendimento']; ?>">
							</div>
	                	</div>
	                	<div class="row">
			                <div class="form-group col-lg-3">					
								<label>Finalidade</label> 
								<select class="form-control" id="finalidade" name="finalidade">
									<option value="">Selecione</option>
									<?php foreach ( $this->finalidades as $finalidade ) :?>
									<option value="<?php echo $finalidade?>" <?php echo ( $this->result['finalidade'] == $finalidade ) ?'selected="selected"':''; ?>><?php echo $finalidade; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
	                	</div>
	                	<div class="row">
			                <div class="form-group col-lg-3">					
								<label>Tipo</label> 
								<select class="form-control" id="categoria" name="categoria">
									<option value="">Selecione</option>
									<?php foreach ( $this->categorias as $categoria ) :?>
									<option value="<?php echo $categoria->id_categoria; ?>" <?php echo ( $this->result['id_categoria'] == $categoria->id_categoria ) ? 'selected="selected"':''; ?>><?php echo $categoria->categoria; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
	                	</div>
	                	<div class="row">
			                <div class="form-group col-lg-3">					
								<label>Fase da Obra</label> 
								<select class="form-control" id="fase" name="fase_obra">
									<option value="">Selecione</option>
									<?php foreach ( $this->fases as $fase ) :?>
									<option value="<?php echo $fase; ?>" <?php echo ( $this->result['fase_obra'] == $fase ) ? 'selected="selected"':''; ?>><?php echo $fase; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
			                <div class="form-group col-lg-3">					
								<label>Percentual da Obra</label>
								<div class="input-group"> 
								<input class="form-control" name="percentual_obra"
									id="obra_andamento" value="<?php echo $this->result['percentual_obra']; ?>" onkeypress="return SomenteNumero(event);">
									<span class="input-group-addon">%</span></div>
							</div>
	                	</div>
	                	<div class="row">
			                <div class="form-group col-lg-6">					
								<label>Logo do Empreendimento</label> <input type="file" class="form-control" name="logo_empreendimento"
									id="logo_empreendimento" value="<?php echo $this->result['logo_empreendimento']; ?>">
							</div>
	                	</div>
	                	<div class="row">
							<?php if( $this->result['logo_empreendimento'] ):?>
			                <div class="form-group col-lg-6">
			                	<a href="/<?php echo $this->result['logo_empreendimento']; ?>" target="_blank">
					                <img src="/<?php echo $this->result['logo_empreendimento'] ?>" width="101" height="74">
				                </a>
			                </div>
			                <?php endif; ?>					
	                	</div>
	                	<div class="row">
			                <div class="form-group col-lg-3">					
								<label>Destaque</label> 
								<select class="form-control" id="em_destaque" name="em_destaque">
									<option value="">Selecione</option>
									<option value="1" <?php echo ( $this->result['em_destaque'] == 1 ) ? 'selected="selected"':''; ?>>Sim</option>
									<option value="0" <?php echo ( $this->result['em_destaque'] == 0 ) ? 'selected="selected"':''; ?>>Não</option>
								</select>
							</div>
	                	</div>
	                	<div class="row">
			                <div class="form-group col-lg-3">					
								<label>Super Destaque</label> 
								<select class="form-control" id="super_destaque_web" name="super_destaque_web">
									<option value="">Selecione</option>
									<option value="1" <?php echo ( $this->result['super_destaque_web'] == 1 ) ? 'selected="selected"':''; ?>>Sim</option>
									<option value="0" <?php echo ( $this->result['super_destaque_web'] == 0 ) ? 'selected="selected"':''; ?>>Não</option>
								</select>
							</div>
	                	</div>
	                	<div class="row">
			                <div class="form-group col-lg-4">					
								<label>Vídeo</label>
								<input class="form-control" name="link_video" placeholder="Ex: https://www.youtube.com/watch?v=UYgsysge"
									id="link_video" value="<?php echo $this->result['link_video']; ?>">
							</div>
	                	</div>
	                	<div class="row">
			                <div class="form-group col-lg-8">					
								<label>Descrição do Imóvel</label> 
								<textarea class="form-control" name="descricao"
									id="editor1" ><?php echo base64_decode($this->result['descricao']); ?></textarea>
							</div>
	                	</div>
	                </div>
	                <div class="tab-pane fade" id="localizacao">
	                    <div class="row">
			                <div class="form-group col-lg-2">					
								<label>Cep</label> <input class="form-control" name="cep"
									id="cep" value="<?php echo $this->result['cep']; ?>" onkeypress="return SomenteNumero(event);" onblur="cepComplete($('#cep'));"> <a href="javascript:;" onclick="cepComplete($('#cep'));">Consultar</a>
							</div>
	                	</div>
	                    <div class="row">
			                <div class="form-group col-lg-6">					
								<label>Endereço</label> <input class="form-control" name="endereco"
									id="endereco" value="<?php echo $this->result['endereco']; ?>">
							</div>
	                	</div>
	                    <div class="row">
			                <div class="form-group col-lg-3">					
								<label>Cidade</label> <input class="form-control" name="cidade"
									id="cidade" value="<?php echo $this->result['cidade']; ?>">
							</div>
			                <div class="form-group col-lg-3">					
								<label>Bairro</label> <input class="form-control" name="bairro"
									id="bairro" value="<?php echo $this->result['bairro']; ?>">
							</div>
	                	</div>
	                    <div class="row">
			                <div class="form-group col-lg-3">					
								<label>Bairro Comercial</label> <input class="form-control" name="bairro_comercial"
									id="bairro_comercial" value="<?php echo $this->result['bairro_comercial']; ?>">
							</div>
			                <div class="form-group col-lg-3">					
								<label>Região</label>
								<select class="form-control" id="regiao" name="regiao">
									<option value="">Selecione</option>
									<?php foreach ( $this->regiao as $regiao ) :?>
									<option value="<?php echo $regiao; ?>" <?php echo ( $this->result['regiao'] == $regiao ) ? 'selected="selected"':''; ?>><?php echo $regiao; ?></option>
									<?php endforeach; ?>
								</select> 
							</div>
	                	</div>
	                    <div class="row">
			                <div class="form-group col-lg-3">					
								<label>Latitude</label> <input class="form-control" name="latitude"
									id="latitude" value="<?php echo $this->result['latitude']; ?>">
							</div>
			                <div class="form-group col-lg-3">					
								<label>Longitude</label> <input class="form-control" name="longitude"
									id="longitude" value="<?php echo $this->result['longitude']; ?>">
							</div>
	                	</div>
	                </div>
	                <div class="tab-pane fade" id="dependencias">
						<div class="row">
			                <div class="form-group col-lg-3">					
								<label>Valor Condomínio</label> <input class="form-control money" name="valor_condominio"
									id="valor_condominio" value="<?php echo number_format( $this->result['valor_condominio'], 2,',', '.'); ?>">
							</div>
	                	</div>
						<div class="row">
			                <div class="form-group col-lg-2">					
								<label>Domitórios de</label> <input class="form-control" name="dormitorios"
									id="dormitorios" value="<?php echo $this->result['dormitorios']; ?>" onkeypress="return SomenteNumero(event);">
							</div>
			                <div class="form-group col-lg-3">					
								<label>Domitórios até</label> <input class="form-control" name="dormitorios_ate"
									id="dormitorios_ate" value="<?php echo $this->result['dormitorios_ate']; ?>" onkeypress="return SomenteNumero(event);">
							</div>
	                	</div>
						<div class="row">
			                <div class="form-group col-lg-2">					
								<label>Suítes de</label> <input class="form-control" name="suites"
									id="suites" value="<?php echo $this->result['suites']; ?>" onkeypress="return SomenteNumero(event);">
							</div>
			                <div class="form-group col-lg-3">					
								<label>Suítes até</label> <input class="form-control" name="suites_ate"
									id="suites_ate" value="<?php echo $this->result['suites_ate']; ?>" onkeypress="return SomenteNumero(event);">
							</div>
	                	</div>
	                	<div class="row">
			                <div class="form-group col-lg-2">					
								<label>Vagas de</label> <input class="form-control" name="vagas"
									id="vagas" value="<?php echo $this->result['vagas']; ?>" onkeypress="return SomenteNumero(event);">
							</div>
			                <div class="form-group col-lg-3">					
								<label>Vagas até</label> <input class="form-control" name="vagas_ate"
									id="vagas_ate" value="<?php echo $this->result['vagas_ate']; ?>" onkeypress="return SomenteNumero(event);">
							</div>
	                	</div>
						<div class="row">
			                <div class="form-group col-lg-2">					
								<label>Área Terreno</label> <input class="form-control" name="area_terreno"
									id="area_terreno" value="<?php echo $this->result['area_terreno']; ?>" onkeypress="return SomenteNumero(event);">
							</div>
			                <div class="form-group col-lg-2">					
								<label>Área Construída</label> <input class="form-control" name="area_construida"
									id="area_construida" value="<?php echo $this->result['area_construida']; ?>" onkeypress="return SomenteNumero(event);">
							</div>
	                	</div>
						<div class="row">
			                <div class="form-group col-lg-2">					
								<label>Área Privativa</label> <input class="form-control" name="area_privativa"
									id="area_privativa" value="<?php echo $this->result['area_privativa']; ?>" onkeypress="return SomenteNumero(event);">
							</div>
			                <div class="form-group col-lg-2">					
								<label>Área Privativa Até</label> <input class="form-control" name="area_privativa_ate"
									id="area_privativa_ate" value="<?php echo $this->result['area_privativa_ate']; ?>" onkeypress="return SomenteNumero(event);">
							</div>
	                	</div>
						<div class="row">
			                <div class="form-group col-lg-2">					
								<label>Área Total</label> <input class="form-control" name="area_total"
									id="area_total" value="<?php echo $this->result['area_total']; ?>" onkeypress="return SomenteNumero(event);">
							</div>
			                <div class="form-group col-lg-2">					
								<label>Área Útil</label> <input class="form-control" name="area_util"
									id="area_util" value="<?php echo $this->result['area_util']; ?>" onkeypress="return SomenteNumero(event);">
							</div>
	                	</div>
                	</div>
	                <div class="tab-pane fade" id="segmento">
	                	<div class="row">
			                <div class="form-group col-lg-3">					
								<label>Status</label> 
								<select class="form-control" id="status" name="status">
									<option value="">Selecione</option>
									<?php foreach ( $this->status as $status ) :?>
									<option value="<?php echo $status; ?>" <?php echo ( $this->result['status'] == $status ) ? 'selected="selected"':''; ?>><?php echo $status; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
	                	</div>
						<div class="row">
			                <div class="form-group col-lg-3">					
								<label>Valor de Venda</label> <input class="form-control money" name="valor_venda"
									id="valor_venda" value="<?php echo number_format($this->result['valor_venda'], 2,',', '.'); ?>">
							</div>
	                	</div>
						<div class="row">
			                <div class="form-group col-lg-3">					
								<label>Valor de Locação</label> <input class="form-control money" name="valor_locacao"
									id="valor_locacao" value="<?php echo number_format($this->result['valor_locacao'], 2,',', '.'); ?>">
							</div>
	                	</div>
	                </div>
	                <div class="tab-pane fade" id="caracteristicas">
	                	<div class="row">
			                <div class="form-group col-lg-3">					
								<label>Características Unidade</label> <input class="form-control typeahead" name=""
									id="carac_unidade" value="">
							</div>
							<div class="form-group col-lg-3">					
								<label>Valor</label> <input class="form-control"  name=""
									id="carac_unidade_valor" value=" ">
							</div>
			                <div class="form-group col-lg-1">	
			                	<label>&nbsp;</label>				
								<input id="addUniCarc" type="button" class="form-control btn btn-sm btn-success" value="+">
							</div>
	                	</div>
							<hr>
	                	<div class="row">
	                		<div id="result-carac" style="margin-left:10px;">
	                		
	                		</div>
	                	</div>
	                	<?php if ( !empty($this->caracteristicas_unidade) ): ?>
	                	<div class="row">
	                	<div class="col-lg-12">
			                <div class="panel panel-default">
			                        <div class="panel-heading">
			                            Características da Unidade
			                        </div>
			                        <!-- /.panel-heading -->
			                        <div class="panel-body">
			                            <div class="dataTable_wrapper">
			                                <table class="table table-striped table-bordered table-hover dataTables" id="dt-caracUnidade">
			                                    <thead>
			                                        <tr>
			                                            <th>Característica</th>
			                                            <th>Valor</th>
			                                            <th>&nbsp;</th>
			                                        </tr>
			                                    </thead>
			                                    <tbody>
			                                    <?php foreach($this->caracteristicas_unidade as $result):?>
			                                        <tr>
			                                            <td><?php echo $result->label; ?></td>
			                                            <td><?php echo $result->valor; ?></td>
			                                            <td>
			                                            	<a href="javascript:;" class="excluirCatUni" data-id="<?php echo $result->id_caracteristica_imovel; ?>"><span class="fa fa-trash-o"></span></a>
			                                            </td>
			                                        </tr>
			                                    <?php endforeach; ?>
			                                    </tbody>
			                                </table>
			                            </div>
			                            <!-- /.table-responsive -->
			                        </div>
			                        <!-- /.panel-body -->
			                    </div>
			                
			                </div>
			                <!-- /.col-lg-12 -->
	                	</div>
	                	<?php endif; ?>
	                </div>
	                <div class="tab-pane fade" id="caracteristicas_cond">
	                	<div class="row">
			                <div class="form-group col-lg-3">					
								<label>Características Condomínio</label> <input class="form-control typeahead" name=""
									id="carac_condominio" value="">
							</div>
							<div class="form-group col-lg-3">					
								<label>Valor</label> <input class="form-control"  name=""
									id="carac_condominio_valor" value=" ">
							</div>
			                <div class="form-group col-lg-1">	
			                	<label>&nbsp;</label>				
								<input id="addConCarc" type="button" class="form-control btn btn-sm btn-success" value="+">
							</div>
	                	</div>
							<hr>
	                	<div class="row">
	                		<div id="result-carac-cond" style="margin-left:10px;">
	                		
	                		</div>
	                	</div>
	                	<?php if ( !empty($this->caracteristicas_condominio) ): ?>
	                	<div class="row">
	                	<div class="col-lg-12">
			                <div class="panel panel-default">
			                        <div class="panel-heading">
			                            Características do Condomínio
			                        </div>
			                        <!-- /.panel-heading -->
			                        <div class="panel-body">
			                            <div class="dataTable_wrapper">
			                                <table class="table table-striped table-bordered table-hover dataTables" id="dt-caracCondominio">
			                                    <thead>
			                                        <tr>
			                                            <th>Característica</th>
			                                            <th>Valor</th>
			                                            <th>&nbsp;</th>
			                                        </tr>
			                                    </thead>
			                                    <tbody>
			                                    <?php foreach($this->caracteristicas_condominio as $result):?>
			                                        <tr>
			                                            <td><?php echo $result->label; ?></td>
			                                            <td><?php echo $result->valor; ?></td>
			                                            <td>
			                                            	<a href="javascript:;" class="excluirCatCond" data-id="<?php echo $result->id_caracteristica_imovel; ?>"><span class="fa fa-trash-o"></span></a>
			                                            </td>
			                                        </tr>
			                                    <?php endforeach; ?>
			                                    </tbody>
			                                </table>
			                            </div>
			                            <!-- /.table-responsive -->
			                        </div>
			                        <!-- /.panel-body -->
			                    </div>
			                
			                </div>
			                <!-- /.col-lg-12 -->
	                	</div>
	                	<?php endif; ?>
	                </div>
	                <div class="tab-pane fade" id="imagens">
	                <?php if ( (isset( $this->fotos ) && count( $this->fotos ))  || $this->result['foto_destaque'] != ''): ?>
                        <table class="table table-striped table-bordered responsive table-condensed">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Foto Principal?</th>
                                    <th>Descrição</th>
                                    <th>URL da Imagem</th>
                                    <th>Tipo</th>
                                    <th>Ordem</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if( !empty($this->result[ 'foto_destaque' ])): ?>
                            <tr id="fotoprincipal">
                                    <td>#</td>
                                    <td>Sim</td>
                                    <td></td>
                                    <td>
                                        <a href="<?php echo $this->baseUrl .'/' . $this->result[ 'foto_destaque' ] ?>" target="_blank" title="<?php echo $imagem[ 'foto_destaque' ] ?>">
                                            <i class="glyphicon glyphicon-link"></i>
                                            <?php echo basename($this->result[ 'foto_destaque' ]) ?>
                                        </a>
                                    </td>
                                    <td><?php echo $this->result['tipo']; ?></td>
                                    <td></td>
                                    <td>

                                        <a class="btn btn-xs btn-danger btn-delimg" title="Remover esta imagem" href="javascript:apagarPrincipal(<?php echo $this->id; ?>)">
                                            <i class="glyphicon glyphicon-remove"></i>
                                        </a>
                                         
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php

                            foreach( $this->fotos  as $imagem ) {
                        ?>
                                <tr id="foto-<?php echo $imagem['id_foto']; ?>">
                                    <td><?php echo $imagem['id_foto'] ?></td>
                                    <td>Não</td>
                                    <td><?php echo(strlen($imagem[ 'descricao' ]) > 50?substr($imagem[ 'descricao' ], 0, 50)."...":$imagem[ 'descricao' ]);  ?></td>
                                    <td>
                                        <a href="<?php echo $this->baseUrl .''. $imagem[ 'foto' ] ?>" target="_blank" title="<?php echo $imagem[ 'foto' ] ?>">
                                            <i class="glyphicon glyphicon-link"></i>
                                            <?php echo basename($imagem[ 'foto' ]) ?>
                                        </a>
                                    </td>
                                    <td><?php echo $imagem['tipo']; ?></td>
                                    <td><?php echo $imagem[ 'ordem' ] ?></td>
                                    <td>
                                        <a class="btn btn-xs btn-danger btn-delimg" title="Remover esta imagem" href="javascript:deletarImagem(<?php echo $imagem['id_foto']; ?>)">
                                            <i class="glyphicon glyphicon-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                        <?php }  ?>

                            </tbody>
                        </table>
                    
                        <hr>
                    <?php endif; ?>
	                <?php 
	                $this->index = 1; 
	                echo $this->render('/imovel/fotoimovel.php'); 
	                ?>
	                </div>
	                 <div class="tab-pane fade in" id="seo">
						<div class="row">
			                <div class="form-group col-lg-6">					
								<label>Title</label> 
								<input class="form-control" name="seo[title]"
									id="seo_title" value="<?php echo $this->seo->title; ?>">
							</div>
	                	</div>		 
	                	<div class="row">                
							<div class="form-group col-lg-6">					
								<label>Description</label> 
								<textarea class="form-control" name="seo[description]"
									id="seo_description"><?php echo $this->seo->description; ?></textarea>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-lg-6">					
								<label>Keywords</label> 
								<textarea class="form-control" name="seo[keywords]"
									id="seo_keywords"><?php echo $this->seo->keywords; ?></textarea>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-lg-6">					
								<label>Url</label> 
								<input class="form-control" name="seo[url]"
									id="seo_url" value="<?php echo empty($this->result) ? '' : $this->seo->url; ?>">
							</div>
						</div>
	                 </div>
	            </div>
	            <input class="btn btn-sm btn-info" type="submit" value="Salvar">
	        </div>
	        <!-- /.panel-body -->
	    </div>
	    <!-- /.panel -->
       </form>
	</div> <!-- /.col-6 -->
</div>     
</div>           
<?php echo $this->render('footer.php'); ?>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo $this->baseUrl ?>/sbadmin/bower_components/ckeditor/ckeditor.js"></script>
<script src="<?php echo $this->baseUrl ?>/sbadmin/bower_components/typeahead/js/bootstrap-typeahead.js"></script>
<script src="<?php echo $this->baseUrl ?>/sbadmin/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo $this->baseUrl ?>/sbadmin/js/jquery.maskMoney.js"></script>
<script type="text/javascript">
    function removerFotoImovel( index ) {
        $( '#foto-imovel-' + index ).remove();
        return false;
    }
    
    function addFotoImovel( index, el ) {
        
        var parent = $( el ).parents( '.imagens-acoes' ).get(0);
        
        
        $.get( 
            '<?php echo $this->baseUrl ?>/<?php echo $this->baseModule ?>/<?php echo $this->baseController; ?>/fotoimovel',
            { 'index' : index },
            function( html ) {
                if ( html ) {
                    $( '#imagens' ).append( html );
                    $( '.btn-danger', parent ).removeClass( 'hidden' );
                    $( el ).remove();
                }
            }
        )
    }
function SomenteNumero(e){
	 var tecla=(window.event)?event.keyCode:e.which;
	 if((tecla>47 && tecla<58)) return true;
	 else{
	 if (tecla==8 || tecla==0) return true;
	 else  return false;
	 }
	}
function cepComplete(cep) {
	if (cep.length > 0 ) {
		$.post('<?php echo $this->baseUrl . '/' . $this->baseModule . '/' . $this->baseController . '/cep'; ?>',  {'cep' : $(cep).val()}, function(data){
			$("#cidade").val(data.localidade);
			$("#endereco").val(data.logradouro);
			$("#bairro").val(data.bairro);
			$("#bairro_comercial").val(data.bairro);
		}); 
	}
	
}
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace( 'editor1' ,{
		toolbar : 'basic'
			});
function deletarImagem(id){
	$.post('<?php echo $this->baseUrl . '/' . $this->baseModule . '/' . $this->baseController . '/apagafoto/'; ?>', {'id':id}, function (data){
		$('#foto-'+id).remove();
	});	
}

function excluirCarac(id)
{
	$.post('<?php echo $this->baseUrl . '/' . $this->baseModule . '/' . $this->baseController . '/apagacaracteristica/'; ?>', {'id':id}, function (data){
		$('#caracterisca-'+id).remove();
	});	
}
function apagarPrincipal(id){

	if(confirm('Deseja realmente apagar a foto principal?')) {
		$.get('<?php echo $this->baseUrl . '/' . $this->baseModule . '/' . $this->baseController . '/apagarfotoprincipal/id/'; ?>'+id, function(data){
			$("#fotoprincipal").remove();
		}); 
	}
	
}

$(function(){

	$(".money").maskMoney({showSymbol:false, symbol:"R$", decimal:",", thousands:"."});
	$('#cep').mask('99999-999');

	$(".op_destaque").click(function(){
		var op = $(".op_destaque");

		for ( i=0; i < op.length; i++) {
			$(op[i]).parent().parent().find('.arquivo').attr('name', 'endfoto[' + $(op[i]).val() + ']');
		}
		
		if( $(this).prop( "checked" ) ) {
			$(this).parent().parent().find('.arquivo').attr('name', 'foto_destaque');
		}
	});
		
	$('#addUniCarc').click(function(){
		var carac = $('#carac_unidade').val();
		var valor = $('#carac_unidade_valor').val();
		if(carac != '') {
			var html = '<p class="caracteristicas"><b>'+carac+':</b><br>'+valor;
			html += '<input type="hidden" name="caracteristica['+carac+']" value="'+valor+'"> '; 
			html += '<a class="btn btn-xs btn-danger btn-delimg" title="Remover esta caracteristica" href="javascript:;" class="caracterItem"><i class="glyphicon glyphicon-remove"></i></a>';
			html += '<hr></p>';
			$('#result-carac').append(html);
		}
		$('#carac_unidade').val('');
		$('#carac_unidade_valor').val('');
		$('.btn-delimg').click(function(){
			$(this).parent().remove();
		});
			
	});
	$('#addConCarc').click(function(){
		var carac = $('#carac_condominio').val();
		var valor = $('#carac_condominio_valor').val();
		if(carac != '') {
			var html = '<p class="caracteristicas"><b>'+carac+':</b><br>'+valor;
			html += '<input type="hidden" name="caracteristica_cond['+carac+']" value="'+valor+'"> '; 
			html += '<a class="btn btn-xs btn-danger btn-delimg" title="Remover esta caracteristica" href="javascript:;" class="caracterItem"><i class="glyphicon glyphicon-remove"></i></a>';
			html += '<hr></p>';
			$('#result-carac-cond').append(html);
		}
		$('#carac_condominio').val('');
		$('#carac_condominio_valor').val('');
		$('.btn-delimg').click(function(){
			console.log($(this).parent());
			$(this).parent().remove();
		});
			
	});
	$('#carac_unidade').typeahead({
		ajax: {
			url: '/admin/imovel/getcaracteristicas/?',
			timeout:500,
			method: 'get',
			triggerLength: 3,
			displayField: 'label',
			preDispatch: function (query) {
				return {
					nome: query
				};
			}
		},
		display: 'label',
		val: 'label'
	});
	$('#carac_condominio').typeahead({
		ajax: {
			url: '/admin/imovel/getcaracteristicas/?',
			timeout:500,
			method: 'get',
			triggerLength: 3,
			displayField: 'label',
			preDispatch: function (query) {
				return {
					nome: query
				};
			}
		},
		display: 'label',
		val: 'label'
	});
	
	var tableUnidade = $('#dt-caracUnidade').DataTable();
	 
	$('#dt-caracUnidade').on( 'click', 'a.excluirCatUni', function () {

		if( confirm('Deseja realmente apagar esta característica?')) {
			tableUnidade
		        .row( $(this).parents('tr') )
		        .remove()
		        .draw();
			excluirCarac($(this).data('id'));
		}
	} );	
	
	var tableCond = $('#dt-caracCondominio').DataTable();
	 
	$('#dt-caracCondominio').on( 'click', 'a.excluirCatCond', function () {

		if( confirm('Deseja realmente apagar esta característica?')) {
			tableCond
		        .row( $(this).parents('tr') )
		        .remove()
		        .draw();
			excluirCarac($(this).data('id'));
		}
	} );	
		 /*
	$('#carac_unidade').typeahead({
		ajax: {
			url: '/admin/imovel/getcaracteristicas/?',
			timeout:500,
			method: 'get',
			triggerLength: 3,
			displayField: 'name',
			preDispatch: function (query) {
				return {
					nome: query
				};
			}
		},
		display: 'name',
		val: 'id'
	});
		*/
});	
function initSeo(){
	
}
</script> 

