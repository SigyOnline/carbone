<?php echo $this->render('header.php'); ?>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Banners</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            Cadastrados
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <?php if ( $this->e ) echo $this->e;?>
                            <div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Título</th>
                                            <th>Descrição</th>
                                            <th>Link</th>
                                            <th>Local</th>
                                            <th>Status</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    	if ( $this->result ): 
                                    		foreach ($this->result as $row) :?>
                                        <tr class="odd gradeX">
                                            <td><?php echo $row->id_popup;?></td>
                                            <td><?php echo $row->titulo;?></td>
                                            <td><?php echo (strlen($row->descricao) > 50?substr($row->descricao, 0, 50)."...":$row->descricao);?></td>
                                            <td class="center"><a href="<?php echo $row->link;?>" target="_blank">
                                            	<?php echo (strlen($row->link) > 30?substr($row->link, 0, 30)."...":$row->link);?></a></td>
                                            <?php if( $row->interno == 1 ):?>
                                                <td class="center">Interno</td>
                                                <?php else:?>
                                                    <td class="center">Home</td>
                                            <?php endif; ?>        	
                                            <td class="center"><?php echo ($row->ativo == 1?'Ativo': 'Inativo');?></td>
                                            <td class="center">
                                            	<a href="<?php  echo $this->baseUrl; ?>admin/popup/editar/id/<?php echo $row->id_popup; ?>"><span class="fa fa-edit"></span></a>
                                            	<a href="<?php echo $this->baseUrl ?>admin/popup?excluir=<?php echo $row->id_popup; ?>" onclick="return confirm('Deseja realmente excluir este registro?');"><span class="fa fa-trash-o"></span></a>
                                            </td>
                                        </tr>
                                    <?php 	endforeach;
                                    	endif; 
                                    ?>
                                    </tbody>
                                </table>
                                <?php //echo $this->result; ?>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
</div>

<?php echo $this->render('footer.php'); ?>