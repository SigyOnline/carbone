<?php echo $this->render('header.php'); 
if ( $_POST )
	$b = $_POST;
else
	$b = (array) $this->result;
?>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Depoimentos</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
<div class="row">
	<div class="col-lg-12">
	    <div class="panel panel-default">
	        <div class="panel-heading">
	            Cadastrar Depoimento
	        </div>
	        <!-- /.panel-heading -->
<div class="panel-body">
<?php if ( $this->e )
	echo $this->e;?>
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" action="" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label>Nome</label>
                                            <input class="form-control" name="nome" value="<?php echo $b['nome'];?>" placeholder="Digite um título">
                                        </div>
                                        <div class="form-group">
                                            <label>Descrição</label>
                                            <textarea class="form-control" rows="3" name="texto"><?php echo $b['texto'];?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Status</label>
                                            <label class="radio-inline">
                                                <input type="radio" name="status" id="optionsRadiosInline1" value="1" <?php echo ($b['status'] == 1? 'checked':NULL);?>>Ativo
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="status" id="optionsRadiosInline2" value="2" <?php echo ($b['status'] != 1? 'checked':NULL);?> >Inativo
                                            </label>
                                        </div>
                                        <button type="reset" class="btn btn-default">Restaurar</button>
                                        <button type="submit" class="btn btn-primary">Salvar</button>
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6">
                                <?php if ( $this->result->imagem )
                                		echo "Preview: <br><img src='". $this->baseUrl . $this->result->imagem ."' style='max-width:400px;max-height:400px' />";?>
                                
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
	        <!-- /.panel-body -->
	    </div>
	    <!-- /.panel -->
	</div> <!-- /.col-6 -->
</div>     
</div>           
<?php echo $this->render('footer.php'); ?>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo $this->baseUrl ?>/sbadmin/bower_components/ckeditor/ckeditor.js"></script>
<script src="<?php echo $this->baseUrl ?>/sbadmin/js/jquery.maskedinput.min.js"></script>
<script>
function SomenteNumero(e){
	 var tecla=(window.event)?event.keyCode:e.which;
	 if((tecla>47 && tecla<58)) return true;
	 else{
	 if (tecla==8 || tecla==0) return true;
	 else  return false;
	 }
	}
function cepComplete(cep) {
	if (cep.length > 0 ) {
		$.post('<?php echo $this->baseUrl . '/' . $this->baseModule . '/' . $this->baseController . '/cep'; ?>',  {'cep' : $(cep).val()}, function(data){
			$("#cidade").val(data.localidade);
			$("#bairro").val(data.bairro);
		}); 
	}
	
}
	$('#cep').mask('99999-999');
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
</script> 