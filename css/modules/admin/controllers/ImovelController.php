<?php
require_once 'app/models/MyCore/Cadastro/Imovel.php';
require_once 'app/models/MyCore/Cadastro/Categoria.php';
require_once 'app/models/MyCore/Cadastro/Foto.php';
require_once 'app/models/MyCore/Cadastro/Caracteristica.php';
require_once 'app/models/MyCore/Cadastro/Seo.php';


class Admin_ImovelController extends My_Controller
{
	public function ini()
	{}
	
	public function indexAction()
	{
		$imoveis = new Cadastro_Imovel($this->db);
		
			if ( $_GET['excluir'] > 0 )
			{
				$imoveis->apaga($_GET['excluir']);
				$this->_redirect('admin/imovel/');
			}
			
		$imoveis->paginar(false);
		$this->view->result = $imoveis->listagem();
	}

	public function cadastrarAction()
	{
		$this->view->result = array();
		$imoveis = new Cadastro_Imovel($this->db);
		$carac = new Cadastro_Caracteristica($this->db);
		$fotos = new Cadastro_Foto($this->db);
		
		
		if ( $_POST )
		{
			$post = $_POST;
			
			foreach($post as $k => $v)
			{
				if (empty($v) )
					$post[$k] = null;
			}
			
			$post['valor_condominio'] = (int)str_replace(array('.',','), array('', '.'), $post['valor_condominio']);
			$post['valor_venda'] = (int)str_replace(array('.',','), array('', '.'), $post['valor_venda']);
			$post['valor_locacao'] = (int)str_replace(array('.',','), array('', '.'), $post['valor_locacao']);
			if ($post['destaque'] == 1) {
				$post['foto_destaque'] = 'imagens/imoveis/'.$_FILES['endfoto']['name'][1];
			}
			
			$res = $imoveis->novo($post);
			
			if ( $res > 0 )
			{
				if ( count($post['caracteristica']) )
				{
					$carac->inserirCaracteriscas($post['caracteristica'], $res, 1);
				}
					
				if ( count($post['caracteristica_cond']) )
				{
					$carac->inserirCaracteriscas($post['caracteristica_cond'], $res, 2);
				}
				
				$fotos->inserirFotos($_FILES['endfoto'], $_POST, $res);
				
				$this->_redirect('admin/imovel/editar/id/'.$res.'?ok');
			} else 
			{
				$this->view->e = new Error('Erro ao cadastrar imóvel');
				$this->view->result = $_POST;
			}
			
		}
		$categoria = new Cadastro_Categoria($this->db);
		$this->view->categorias = $categoria->lista();
		
		$this->view->finalidades = $imoveis->getFinalidades();
		$this->view->fases = $imoveis->getFase();
		$this->view->regiao = $imoveis->getZonas();
		$this->view->fotos = array();
		$this->view->status = array('Venda', 'Aluguel', 'Venda e Aluguel');
		$this->view->foto_categoria = $fotos->fotoCategoria();
		
		echo $this->view->render('imovel/form.php');
		exit;
	}
	
	public function editarAction()
	{
		if ($this->id < 1 )
			$this->_redirect($this->view->baseUrl . '/' . $this->view->baseModule . '/'. $this->view->baseController);
		
		$this->view->id = $this->id;
		$imoveis = new Cadastro_Imovel($this->db);
		$fotos = new Cadastro_Foto($this->db);
		$carac = new Cadastro_Caracteristica($this->db);
		$seo = new Cadastro_Seo($this->db);
		
		if ( isset($_GET['ok']) )
			$this->view->e = new Success('Imóvel adicionado com sucesso');
		
		if ( $_POST )
		{
			$campos_tratamento = array(
					'dormitorios',
					'dormitorios_ate',
					'suites',
					'suites_ate',
					'valor_condominio',
					'valor_locacao',
					'valor_venda',
					'valor_iptu',
					'locacao_anual',
					'locacao_temporada',
					'venda',
					'aptos_edificio',
					'aptos_andar',
					'vagas',
					'vagas_ate',
					'area_total',
					'area_privativa',
					'area_privativa_ate',
					'area_util',
					'area_terreno',
					'area_construida',
					'salas',
					'percentual_obra',
					
			);
			$post = $_POST;
			
			foreach($post as $k => $v)
			{
				if ($v == '' )
				{
					if( in_array($k, $campos_tratamento) )
					{
						$post[$k] = 0;
					}else 
					{
						$post[$k] = null;
					}	
				}
			}
		
			
			$post['valor_condominio'] = (int)str_replace(array('.',','), array('', '.'), $post['valor_condominio']);
			$post['valor_venda'] = (int)str_replace(array('.',','), array('', '.'), $post['valor_venda']);
			$post['valor_locacao'] = (int)str_replace(array('.',','), array('', '.'), $post['valor_locacao']);
			if ($post['destaque'] == 1) {
				$post['foto_destaque'] = 'imagens/imoveis/'.$_FILES['endfoto']['name'][1];
			}
			
			$res = $imoveis->edita($this->id, $post);
			
			if ( count($post['caracteristica']) )
			{
				$carac->inserirCaracteriscas($post['caracteristica'], $this->id, 1);	
			}
			
			if ( count($post['caracteristica_cond']) )
			{
				$carac->inserirCaracteriscas($post['caracteristica_cond'], $this->id, 2);
			}

			$post['seo']['id_imovel'] = $this->id;
			$seo->edita($this->id, $post['seo']);
			
			if ( $res > 0  || sizeof($_FILES) )
			{
				$fotos->inserirFotos($_FILES['endfoto'], $post, $this->id);
				$this->view->e = new Success('Imóvel atualizado com sucesso.');
			}else 
			{
				$this->view->e = new Error('Falha ao atualizar imóvel.');
			}
		}
		
		$this->view->finalidades = $imoveis->getFinalidades();
		$this->view->fases = $imoveis->getFase();
		$this->view->regiao = $imoveis->getZonas();
		
		$categoria = new Cadastro_Categoria($this->db);
		$this->view->categorias = $categoria->lista(); 
		
		$caracteristicas = new Cadastro_Caracteristica($this->db);
		$this->view->caracteristicas_unidade = $caracteristicas->listagem($this->id, 1);
		$this->view->caracteristicas_condominio = $caracteristicas->listagem($this->id, 2);
		
		$this->view->seo = $seo->getDados($this->id);
		
		$this->view->fotos = array();
		$this->view->fotos = $fotos->lista($this->id);
		$this->view->index = count($this->view->fotos);
		$this->view->foto_categoria = $fotos->fotoCategoria();
		$this->view->result = (array) $imoveis->GetImoveisDetalhe($this->id);
		
		$this->view->status = array('Venda', 'Aluguel', 'Venda e Aluguel');
		
		echo $this->view->render('imovel/form.php');
		
		exit;
	}
	
	public function deletarAction()
	{
		if ($this->id < 1 )
			$this->_redirect($this->view->baseUrl . '/' . $this->view->baseModule . '/'. $this->view->baseController);
		
		$imoveis = new Cadastro_Imovel($this->db);
		
		$imoveis->apaga($this->id);
		$this->_redirect($this->view->baseUrl . '/' . $this->view->baseModule . '/'. $this->view->baseController);
		
	}
	
	public function fotoimovelAction()
	{
		$fotos = new Cadastro_Foto($this->db);
		$this->view->foto_categoria = $fotos->fotoCategoria();
		
		$this->view->index = $_GET['index'];
	}
	
	public function cepAction()
	{
		header('Content-Type: application/json');
		$result = file_get_contents('http://viacep.com.br/ws/' . $_POST['cep'] . '/json/');
		exit($result);		
	}
	
	public function getcaracteristicasAction()
	{
		$result = array();
		if( !empty($_GET['nome']) )
		{
			$carac = new Cadastro_Caracteristica($this->db);
			$result = $carac->listagemCaracteristica($_GET['nome']);
		}
		
		header('Content-Type: application/json');
		exit( json_encode($result) );
	}
	
	public function apagafotoAction()
	{
		if ( $this->id > 0 )
		{
			$fotos = new Cadastro_Foto($this->db);
			$fotos->apaga($this->id);
		}
		exit;
	}
	
	public function apagacaracteristicaAction()
	{
		if($_POST['id'] > 0)
		{
			$carac = new Cadastro_Caracteristica($this->db);
			$carac->apaga($_POST['id']);
		}	
		exit;
	}
	public function apagarfotoprincipalAction()
	{
		if ( $this->id > 0)
		{
			$imovel = new Cadastro_Imovel($this->db);
			$dados = $imovel->GetImoveisDetalhe($this->id);
			$imovel->edita($this->id, array('foto_destaque'=>''));
			unset($dados->foto_destaque);
		}
		exit;
	}
	
	public function seoAction()
	{
		if( $_POST )
		{
			$response = array();
			
			if ( $_POST['empreendimento'] != '' )
			{
				$response['title'] = $_POST['empreendimento'];
			}
			
			if ( $_POST['descricao'] != '' )
			{
				$response['description'] = strip_tags( $_POST['descricao'] );
				
				if ( strlen( $response['description'] ) > 200 )
				{
					$response['description'] = substr($response['description'], 0, strpos($response['description'], ' ', 180) );
				}
				
			}
			
			header('Content-Type: application/json');
			exit( json_encode($response) );
		}
	}
	
}	