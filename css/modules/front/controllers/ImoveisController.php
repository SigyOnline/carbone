<?php



require_once MYCORE. '/Cadastro/Imovel.php';

require_once MYCORE. '/Cadastro/FavCom.php';

require_once MYCORE. '/Cadastro/Caracteristica.php';

require_once  MYCORE. '/Cadastro/Consultafiltro.php';



Class ImoveisController extends My_Controller

{

	public function ini()

	{

		$this->obj = new Cadastro_Imovel($this->db);

		$this->filtro = new Cadastro_Consultafiltro($this->db);

		

		if ( $_GET['modo'] == 'lista' || $_GET['modo'] == 'box' ) 

			$_SESSION['modo'] = $this->view->modo = $_GET['modo'];

		elseif ( !$this->view->modo && $_SESSION['modo'] )

			$this->view->modo = $_SESSION['modo'];

		else 

			$_SESSION['modo'] = $this->view->modo = 'box'; 

	}

	

	public function imovelmapaAction()

	{

		$this->view->dados = $this->obj->GetImoveisDetalhe($this->id);

	}

	

	public function imovelfotoAction()

	{

		$this->view->foto_imovel = $this->obj->fotosImovel($this->id);

		$this->view->foto_empreendimento = $this->obj->fotosImovel($this->id, 'empreendimento');

		$this->view->foto_plantas = $this->obj->fotosImovel($this->id, 'planta');

		$this->view->foto_perspectiva = $this->obj->fotosImovel($this->id, 'perspectiva');

	}

	

	public function imovelvideoAction()

	{

		$this->view->dados = $this->obj->GetImoveisVideo($this->id);

	}

	

	public function indexAction ()

	{

		$this->view->dado = $this->obj->lista($_GET);

		$this->view->carac = $this->obj->GetCaracteristicaBusca();

		$this->view->finalidade = $this->filtro->ListaFinalidade();

		$this->view->cidade = $this->filtro->ListaCidade();

		$this->view->categoria = $this->filtro->ListaCategoria();

		$this->view->bairro = $this->filtro->ListaBairro();

		$this->view->fase_obra = $this->filtro->ListaFaseObra();

		$this->view->status = $this->filtro->ListaStatus();

	}

	

	public function listaAction ()

	{

		$this->view->dado = $this->obj->lista($_GET);

		$this->view->finalidade = $this->filtro->ListaFinalidade();

		$this->view->cidade = $this->filtro->ListaCidade();

		$this->view->categoria = $this->filtro->ListaCategoria();

		$this->view->bairro = $this->filtro->ListaBairro();

		$this->view->fase_obra = $this->filtro->ListaFaseObra();

	}

	

	public function detalheAction ()

	{

		$request = new Zend_Controller_Request_Http();

		$uri = $request->getRequestUri(); 

		

		if ( !$this->id && !empty($_POST['ref']) )

		{

			$this->id = (int)$_POST['ref'];

			if ( $this->id > 0 )

				$this->redirect('imoveis/detalhe/id/'. $this->id);

			

		} elseif ( strpos($uri, '.html') != false )

		{

			$this->id = @str_replace('.html', '', end(explode('/', $uri)));

		}

		

		if ( !$this->id && $_POST['ref'] < 0 && strpos($uri, '.html') == false  )

			$this->redirect('/');

		

		if( !is_array($_SESSION['ultimos_visualizados'])) $_SESSION['ultimos_visualizados'] = array();

		

		if(!in_array($this->id, $_SESSION['ultimos_visualizados']))

		{

			array_push($_SESSION['ultimos_visualizados'], $this->id);

		}

		$this->view->id    = $this->id; 

		$this->view->dados = $this->obj->GetImoveisDetalhe($this->id);

		$this->view->foto = $this->obj->Getfoto($this->id);

		$this->view->carac = $this->obj->GetCaracteristica($this->id);

		$this->view->caraccondo = $this->obj->GetCaracteristicaCondominio($this->id);

		$this->view->foto_imovel = $this->obj->fotosImovel($this->id);

		$this->view->foto_empreendimento = $this->obj->fotosImovel($this->id, 'empreendimento');

		$this->view->foto_plantas = $this->obj->fotosImovel($this->id,'planta');

		$this->view->foto_perspectiva = $this->obj->fotosImovel($this->id, 'perspectiva');

		$this->view->video_imovel = $this->obj->GetImoveisVideo($this->id);

// 		$carac = new Cadastro_Caracteristica($this->db);

		$this->view->caracteristica = $this->obj->GetCaracteristica($this->id);//$carac->listagem($this->id, 1);

		$this->view->caracteristica_cond = $this->obj->GetCaracteristicaCondominio($this->id);//$carac->listagem($this->id, 2);

		$this->view->imoveis_semelhantes =  $this->obj->ImoveisSemelhantesAvancado($this->view->dados);

		$this->view->imoveis_ultimos =  $this->obj->ImoveisUltimosVisualizados($this->id, $_SESSION['ultimos_visualizados']);

		

	}

	

	public function bairroAction ()

	{

		$this->view->dados = $this->obj->ListaBairro($this->id);

	}

	

	public function mapaAction ()

	{

		$this->view->mapa = $this->obj->GetMapa($this->id);	

	}

	

	public function favoritosAction ()

	{

		$this->view->title = 'Imóveis Favoritos';

		$this->view->result = array();

	

		if( Cadastro_FavCom::getInstance()->count(Cadastro_FavCom::FAVORITOS) > 0 )

		{

			$this->view->result = $this->obj->getImoveisFavoritos(Cadastro_FavCom::getInstance()->getItens(Cadastro_FavCom::FAVORITOS));

		}

	}

	

	public function compararAction ()

	{

		$this->view->title = 'Imóveis Comparar';

		$this->view->result = array();

	

		if( Cadastro_FavCom::getInstance()->count(Cadastro_FavCom::COMPARAR) > 0 )

		{

			$this->view->result = $this->obj->getImoveisFavoritos(Cadastro_FavCom::getInstance()->getItens(Cadastro_FavCom::COMPARAR));

		}

	}

	

	public function favcomAction()

	{

		$favcom = Cadastro_FavCom::getInstance();

	

		if( $_POST['item'] == 'fav' && $_POST['acao'] == 'add' )

		{

			echo $favcom->addFav($_POST['id']);

		}

	

		if( $_POST['item'] == 'fav' && $_POST['acao'] == 'remove' )

		{

			echo $favcom->removeFav($_POST['id']);

		}

	

		if( $_POST['item'] == 'fav' && $_POST['acao'] == 'removertodos' )

		{

			echo $favcom->removerTodos(Cadastro_FavCom::FAVORITOS);

		}

	

		exit;

	}

	

	public function comAction()

	{

		$favcom = Cadastro_FavCom::getInstance();

	

		if( $_POST['item'] == 'com' && $_POST['acao'] == 'add' )

		{

			echo $favcom->addCom($_POST['id']);

		}

	

		if( $_POST['item'] == 'com' && $_POST['acao'] == 'remove' )

		{

			echo $favcom->removeCom($_POST['id']);

		}

	

		if( $_POST['item'] == 'com' && $_POST['acao'] == 'removertodos' )

		{

			echo $favcom->removerTodos(Cadastro_FavCom::COMPARAR);

		}

	

		exit;

	}

	

}