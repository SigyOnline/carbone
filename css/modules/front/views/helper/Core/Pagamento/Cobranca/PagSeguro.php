<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<form method="post" action="https://pagseguro.uol.com.br/checkout/checkout.jhtml" id="pagseguro">
<input type="hidden" name="email_cobranca"
value="<?php echo $dados['email_cobranca'];?>">
<input type="hidden" name="tipo" value="CP">
<input type="hidden" name="moeda" value="BRL">
<?php 
$i = 0;
foreach ( $dados['item'] as $item ) :
$i++;
?>
<input type="hidden" name="item_id_<?php echo $i;?>" value="<?php echo $item['item_id'];?>">
<input type="hidden" name="item_descr_<?php echo $i;?>" 
value="<?php echo utf8_decode($item['item_descr']);?>">
<input type="hidden" name="item_quant_<?php echo $i;?>" value="<?php echo $item['item_quant'];?>">
<input type="hidden" name="item_valor_<?php echo $i;?>" value="<?php echo $item['item_valor'];?>">
<input type="hidden" name="item_frete_<?php echo $i;?>" value="<?php echo $item['item_frete'];?>">
<input type="hidden" name="item_peso_<?php echo $i;?>" value="<?php echo $item['item_peso'];?>">
<?php endforeach;?>
<!-- INÍCIO DOS DADOS DO USUÁRIO -->
<input type="hidden" name="cliente_nome" 
value="<?php echo $dados['cliente_nome'];?>">
<input type="hidden" name="cliente_cep" value="<?php echo $dados['cliente_cep'];?>">
<input type="hidden" name="cliente_end" 
value="<?php echo $dados['cliente_end'];?>">
<input type="hidden" name="cliente_num" value="<?php echo $dados['cliente_num'];?>">
<input type="hidden" name="cliente_compl" value="<?php echo $dados['cliente_compl'];?>">
<input type="hidden" name="cliente_bairro" 
value="<?php echo $dados['cliente_bairro'];?>">
<input type="hidden" name="cliente_cidade" 
value="<?php echo $dados['cliente_cidade'];?>">
<input type="hidden" name="cliente_uf" value="<?php echo $dados['cliente_uf'];?>">
<input type="hidden" name="cliente_pais" value="<?php echo $dados['cliente_pais'];?>">
<input type="hidden" name="cliente_ddd" value="<?php echo $dados['cliente_ddd'];?>">
<input type="hidden" name="cliente_tel" value="<?php echo $dados['cliente_tel'];?>">
<input type="hidden" name="cliente_email" 
value="<?php echo $dados['cliente_email'];?>">
<script type="text/javascript">
document.getElementById('pagseguro').submit();
</script>
<!-- FIM DOS DADOS DO USUÁRIO -->
<!--
<input type="image" 
src="https://p.simg.uol.com.br/out/pagseguro/i/botoes/pagamentos/99x61-pagar-assina.gif" 
name="submit" alt="Pague com PagSeguro - é rápido, grátis e seguro!">-->
</form>
</body>
</html>
