<?php echo $this->render("header.php"); ?>
		<main class="pr border_top">
		<section class="center">
			<!-- titulo -->
            <h1 class="pa noMobile ttl_internas">Exclusividade</h1>
			<section class="box_img_padrao">
				<h2 class="sub_ttl">Benefícios para o proprietário, para o comprador e para o inquilino.</h2>
                
                
				<p class="paragrafo_internas">A <strong>Xavier Camargo Imobiliária</strong> oferece aos seus clientes vendas e locações com Exclusividade, com benefícios para os interessados em vender, comprar e alugar.</p>
                
                <p class="paragrafo_internas marg_top40">Melhor para o proprietário:</p>
                                  
                
				<li> Centralização nas negociações.</li>
                <li> Plano de marketing de acordo com as características do imóvel e das demandas do cliente.</li>
                <li> Dedicação total do corretor, que ganha foco na venda ou na locação.</li>
                <li> Rapidez no processo.</li>
                
                 <p class="paragrafo_internas marg_top40">Melhor para o comprador ou locatário</p>
                                  
                
				<li> Facilidade de obtenção de crédito imobiliário através da assessoria em crédito imobiliário.</li>
                <li> Facilidade na locação através de opções de garantias locatícias.</li>
                <li> Redução do tempo para a locação do imóvel.</li>
                
                
                  
                                
                
			</section>
				
		</section>
	
<?php echo $this->render("footer.php"); ?>
	</main><!-- /main -->

