<?php echo $this->render("header.php");
$dado = $this->dado;
?>
<div style="clear:both;"></div>
<main class="pr border_top">
		<section class="center">
			<!-- resultado -->
			<article id="box_empre">

                <!-- titulo -->
                <h1 class="pa noMobile ttl_internas">Empreendimentos</h1>

				<div class="lista_box_empre">
                                       
                    <div class="icon_const pa"></div>
                                        
                    <img src="imagens/empreendimentos/monalisa.png"/>
                    
                    <h1>Mona Lisa</h1>
                    
                    <h5>Descrição do imóvel:</h5>
                    
                    <p>Apartamento com 93,55 m², piso porcelanato e revestimento até o teto. Hall de entrada, sala de estar / jantar, 3 dormitórios sendo 1 suíte mais banheiro social, cozinha e área de serviço interligada a ampla varanda Grill. Condomínio conta com uma área de lazer completa para toda a família com churrasqueira, espaço gourmet, jardim, piscina, playground, portaria 24h, porteiro eletrônico, quadra poliesportiva, sala de jogos, salão de festas e segurança.</p>
                    
                    <div class="tarja_fim"></div>

                </div>

   				<div class="lista_box_empre">
                  
                    <div class="icon_chave pa"></div>
                   
                    <img src="imagens/empreendimentos/portinari.png"/>
                    
                    <h1>Residencial Portinari</h1>
                    
                    <h5>Descrição do imóvel:</h5>
                    
                    <p>Condomínio com apartamentos de: 3 e 4 dormitórios (3 suítes) com 140m², 3 suítes (1 master) com 110m² e coberturas duplex. Com até 4 vagas de garagem. Um clube completo de 10.000m² com 3 piscinas sendo: (1 piscina coberta e aquecida, 1 piscina infantil e 1 piscina com raia), sauna seca e sauna a vapor, espaço gourmet, salão de festas, área fitness, sala infantil, recanto das mamães, living, quadra poliesportiva, quadra de bocha, área caminhada e pista de cooper, sala de cinema e espaço mulher.</p>
                    
                    <div class="tarja_fim"></div>

                </div>
                
   				<div class="lista_box_empre">
                   
                    <div class="icon_chave pa"></div>
                    
                    <img src="imagens/empreendimentos/diNapoli.png"/>
                    
                    <h1>Cond. Residencial Torres Di Napoli</h1>
                    
                    <h5>Descrição do imóvel:</h5>
                    
                    <p>Apartamento de 86 m² com 3 dormitórios, banheiro social, lavanderia e varanda gourmet com 2 vagas de garagem. O condomínio conta com churrasqueira, espaço gourmet, jardim, piscina, sauna, playground, portaria 24h, porteiro eletrônico, quadra poliesportiva, sala de jogos, sala fitness e salão de festas.</p>
                    
                    <div class="tarja_fim"></div>

                </div>
                
   				<div class="lista_box_empre">
                   
                    <div class="icon_chave pa"></div>
                    
                    <img src="imagens/empreendimentos/saoCaetanoII.png"/>
                    
                    <h1>Loteamento Resid. São Caetano II</h1>
                    
                    <h5>Descrição do imóvel:</h5>
                    
                    <p>O Loteamento Jardim São Caetano II possui lotes comerciais e residenciais, com infraestrutura completa, com modernos conceitos de urbanização e amplas vias, tudo pensado para sua total qualidade de vida. Lotes a partir de 160 m².</p>
                    
                    <div class="tarja_fim"></div>

                </div>
                
   				<div class="lista_box_empre">
                   
                    <div class="icon_chave pa"></div>
                    
                    <img src="imagens/empreendimentos/mariaAurora.png"/>
                    
                    <h1>Residencial Maria Aurora</h1>
                    
                    <h5>Descrição do imóvel:</h5>
                    
                    <p>Condomínio com 2 torres de 7 andares cada com 4 apartamentos por andar, com 53 m², sala de estar e jantar, cozinha, 2 dormitórios sendo 1 suíte, banheiro social e varanda com 1 vaga de garagem. O condomínio conta com jardim e porteiro eletrônico.</p>
                    
                    <div class="tarja_fim"></div>

                </div>
                
   				<div class="lista_box_empre">
                   
                    <div class="icon_chave pa"></div>
                    
                    <img src="imagens/empreendimentos/diRoma.png"/>
                    
                    <h1>Condomínio Resid. Torres di Roma</h1>
                    
                    <h5>Descrição do imóvel:</h5>
                    
                    <p>Composto de 96 apartamento de 86m² de 3 dormitórios sendo um deles suíte; banho social; banho suíte; copa cozinha com área de serviço conjugada; sala de estar – jantar. Condomínio conta com: salão de festas, piscina e área fitness.</p>
                    
                    <div class="tarja_fim"></div>

                </div>
                
   				<div class="lista_box_empre">
                   
                    <div class="icon_chave pa"></div>
                    
                    <img src="imagens/empreendimentos/portalVitoria.png"/>
                    
                    <h1>Residencial Portal Vitória</h1>
                    
                    <h5>Descrição do imóvel:</h5>
                    
                    <p>Apartamentos inteligentes de 60m² e 58m² dimensionados para oferecerem conforto, com 1 dormitório (49m²); 2 dormitórios sem varanda (58m²); 2 dormitórios com varanda (60m²). Condomínio conta com churrasqueira, espaço gourmet, jardim, piscina, playground, portaria 24h, quadra de esporte, quiosque e segurança.</p>
                    
                    <div class="tarja_fim"></div>

                </div>
                
   				<div class="lista_box_empre">
                   
                    <div class="icon_chave pa"></div>
                    
                    <img src="imagens/empreendimentos/portalCaribe.png"/>
                    
                    <h1>Residencial Portal do Caribe</h1>
                    
                    <h5>Descrição do imóvel:</h5>
                    
                    <p>Apartamentos de 2, 3 e até 4 dormitórios com excelente localização no bairro. O condomínio conta com churrasqueira, jardim, piscina, playground, portaria 24h, porteiro eletrônico e salão de festas. </p>
                    
                    <div class="tarja_fim"></div>

                </div>
                
   				<div class="lista_box_empre">
                   
                    <div class="icon_chave pa"></div>
                    
                    <img src="imagens/empreendimentos/camposVerdes.png"/>
                    
                    <h1>Condomínio Resid. Campos Verdes</h1>
                    
                    <h5>Descrição do imóvel:</h5>
                    
                    <p>Condomínio de casas com sobrados de 3 e 4 dormitórios de 101 m² e 126 m². O condomínio conta com churrasqueira, espaço gourmet, jardim, piscina, playground, portaria 24h, quadra poliesportiva, quiosque e salão de festas.</p>
                    
                    <div class="tarja_fim"></div>

                </div>

                <div class="lista_box_empre">
                   
                    <div class="icon_chave pa"></div>
                    
                    <img src="imagens/empreendimentos/CondResColorado.png"/>
                    
                    <h1>Cond. Res. Colorado</h1>
                    
                    <h5>Descrição do imóvel:</h5>
                    
                    <p>Condomínio de casas com completa área de lazer, jardim, churrasqueira, espaço gourmet, piscina, playground, portaria 24h, quiosque e salão de festa.</p>
                    
                    <div class="tarja_fim"></div>

                </div>

                <div class="lista_box_empre">
                   
                    <div class="icon_chave pa"></div>
                    
                    <img src="imagens/empreendimentos/CondResCalifornia.png"/>
                    
                    <h1>Cond. Res. Califórnia </h1>
                    
                    <h5>Descrição do imóvel:</h5>
                    
                    <p>Condomínio de casas com completa área de lazer, portaria 24h, playground, piscina, jardim, espaço gourmet e churrasqueira.</p>
                    
                    <div class="tarja_fim"></div>

                </div>

                <div class="lista_box_empre">
                   
                    <div class="icon_chave pa"></div>
                    
                    <img src="imagens/empreendimentos/CondResViscondeRioClaro.png"/>
                    
                    <h1>Cond. Res. Visconde do Rio Claro</h1>
                    
                    <h5>Descrição do imóvel:</h5>
                    
                    <p>Edifício com 12 andares e apartamentos com 86,12m², 3 ou 2 dormitórios (sendo 1 suíte), sala, cozinha, banheiro social, área de serviço e lavanderia. Condomínio conta com uma completa área de lazer, churrasqueira, espaço gourmet, jardim, piscina, sauna, portaria 24h, sala de jogos e salão de festas.</p>
                    
                    <div class="tarja_fim"></div>

                </div>

                <div class="lista_box_empre">
                   
                    <div class="icon_chave pa"></div>
                    
                    <img src="imagens/empreendimentos/EdViena.png"/>
                    
                    <h1>Ed. Viena</h1>
                    
                    <h5>Descrição do imóvel:</h5>
                    
                    <p>Apartamentos de 78,59m² com 3 dormitórios (sendo 1 suíte), cozinha, lavanderia, sala 2 ambientes e banheiro social. Condomínio com jardim, portaria 24h e salão de festas.</p>
                    
                    <div class="tarja_fim"></div>

                </div>

                <div class="lista_box_empre">
                   
                    <div class="icon_chave pa"></div>
                    
                    <img src="imagens/empreendimentos/EdCondePrates.png"/>
                    
                    <h1>Ed. Conde Prates</h1>
                    
                    <h5>Descrição do imóvel:</h5>
                    
                    <p>Apartamentos na região central de 155,45m² com 3 ou 4 dormitórios (3 suítes), apartamentos de 90,40m² com 3 dormitórios (1 suíte) e apartamentos de 78,72m² com 3 dormitórios (1 suíte). Condomínio com piscina, espaço gourmet, portaria 24h salão de festas e garagem no subsolo.</p>
                    
                    <div class="tarja_fim"></div>

                </div>

                <div class="lista_box_empre">
                   
                    <div class="icon_chave pa"></div>
                    
                    <img src="imagens/empreendimentos/JdOrquideas.png"/>
                    
                    <h1>Loteamento Jardim das Orquídeas</h1>
                    
                    <h5>Descrição do imóvel:</h5>
                    
                    <p>Loteamento no centro de Ipeúna possui lotes comerciais e residenciais, com infraestrutura completa, amplas vias. Tudo pensado para sua total qualidade de vida.</p>
                    
                    <div class="tarja_fim"></div>

                </div>

    		</article>

		</section>
	</main>
<?php echo $this->render("footer.php"); ?>

