<?php echo $this->render("header.php"); ?>



		<main class="pr border_top">
		<section class="center">
			<!-- titulo -->
            <h1 class="pa noMobile ttl_internas">Quem Somos</h1>
			<section class="box_img_padrao">
<!--				<h2 class="sub_ttl"></h2>-->
                <div class="quemsomos_img"></div>
                
                <p class="paragrafo_internas">Confiabilidade se conquista com tempo, conhecimento especializado e dedicação exclusiva aos interesses de cada cliente.</p>

				<p class="paragrafo_internas">Há mais de 20 anos, a <strong>Carbone Imóveis</strong> além de oferecer ótimas opções de imóveis, conta com uma equipe de profissionais altamente capacitada para assessorar na compra, venda e locação de imóveis residenciais, comerciais e grandes áreas.</p>

				<p class="paragrafo_internas">Ao longo de todos estes anos a <strong>Carbone Imóveis</strong> adquiriu extenso e profundo conhecimento das melhores práticas em Corretagem e Consultoria. Hoje a <strong>Carbone Imoveis</strong> é considerada uma das mais sólidas e respeitada empresa do setor.</p>

				<p class="paragrafo_internas">Nossa empresa é equipada com tecnologia atualizada e constituída por profissionais 100% dedicados à sua tranquilidade.</p>

				<p class="paragrafo_internas">Integrados através de uma eficiente rede de comunicação, os profissionais da <strong>Carbone Imóveis</strong> são constantemente treinados e atualizados para oferecer aos clientes serviços de qualidade nitidamente superior.</p>

				<ul class="botoesquem">
					<li><a href="localizacao"><i class="fa fa-map-marker"></i> Localize-nos<br><span>Saiba aonde estamos</span></a></li>
					<li><a href="contato"><i class="fa fa-comments"></i> Fale Conosco<br><span>Entre em contato</span></a></li>
					<li><a href="servicos"><i class="fa fa-calendar"></i> Nossos Serviços<br><span>Saiba mais</span></a></li>
				</ul>
                
			</section>
				
		</section>
	
<?php echo $this->render("footer.php"); ?>
	</main><!-- /main -->

