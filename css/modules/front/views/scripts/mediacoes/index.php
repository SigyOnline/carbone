<?php echo $this->render("header.php"); ?>
        <script type="text/javascript" src="js/carouselgaleria/jquery.js"></script>
        <script type="text/javascript" src="js/carouselgaleria/jquery.jcarousel.min.js"></script>

        <script type="text/javascript" src="js/carouselgaleria/jcarousel.ajax.js"></script>
		<main class="pr border_top">
		<section class="center detalhes">
			<!-- titulo -->
            <!--h1 class="pa noMobile ttl_internas">Empresa / Quem Somos</h1-->
         <div class="imgeend">
            <img src="src/imagem-destaque.jpg"  style="width:90%; height:480px;">
            <div class="pos_info">
                    <h2 class="sub_ttl">Nome / Fornecedor</h2>
                    <p><b>Endereço:</b> Rua Estados Unidos, 1725 – Jardim America – São Paulo – SP</p>
					
					<p><b>Telefone:</b> (11) 0000-0000 | (11) 0000-0000</p>
            </div> 
         </div>



			<section class="box_img_padrao detalhes">
				<div class="text">
				<h2 class="sub_ttl">Descrição</h2>
				<p class="paragrafo_internas">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
				<p class="paragrafo_internas">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
				<br>
				<h2 class="sub_ttl">Galeria de Fotos</h2>
                <!--div class="jcarousel-wrapper">
                <div class="jcarousel ">
                    <ul>
                        <li><a class="fancybox" rel="group" href="src/imagem-destaque.jpg" title="">
                            <img src="src/imagem-destaque.jpg" alt="Image 1"></a>
                        </li>

                        <li><a class="fancybox" rel="group" href="src/imagem-destaque.jpg" title="">
                            <img src="src/imagem-destaque.jpg" alt="Image 2"></a>
                        </li>
                        <li><a class="fancybox" rel="group" href="src/imagem-destaque.jpg" title="">
                            <img src="src/imagem-destaque.jpg" alt="Image 3"></a>
                        </li>
                        <li><a class="fancybox" rel="group" href="src/imagem-destaque.jpg" title="">
                            <img src="src/imagem-destaque.jpg" alt="Image 4"></a>
                        </li>
                        <li><a class="fancybox" rel="group" href="src/imagem-destaque.jpg" title="">
                            <img src="src/imagem-destaque.jpg" alt="Image 5"></a>
                        </li>
                        <li><a class="fancybox" rel="group" href="src/imagem-destaque.jpg" title="">
                            <img src="src/imagem-destaque.jpg" alt="Image 6"></a>
                        </li>
                        <li><a class="fancybox" rel="group" href="src/imagem-destaque.jpg" title="">
                            <img src="src/imagem-destaque.jpg" alt="Image 7"></a>
                        </li>
                        <li><a class="fancybox" rel="group" href="src/imagem-destaque.jpg" title="">
                            <img src="src/imagem-destaque.jpg" alt="Image 8"></a>
                        </li>
                    </ul>
                </div>

                <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
                <a href="#" class="jcarousel-control-next">&rsaquo;</a>

                <!--p class="jcarousel-pagination"></p-->
            </div-->
            <div class="wrapper">

            <div class="jcarousel-wrapper">
                <div class="jcarousel">
                    <ul>
                        <li><a class="fancybox" rel="group" href="src/imagem-destaque.jpg" title="">
                            <img src="src/imagem-destaque.jpg" alt="Image 1"></a>
                        </li>

                        <li><a class="fancybox" rel="group" href="src/imagem-destaque.jpg" title="">
                            <img src="src/imagem-destaque.jpg" alt="Image 2"></a>
                        </li>
                        <li><a class="fancybox" rel="group" href="src/imagem-destaque.jpg" title="">
                            <img src="src/imagem-destaque.jpg" alt="Image 3"></a>
                        </li>
                        <li><a class="fancybox" rel="group" href="src/imagem-destaque.jpg" title="">
                            <img src="src/imagem-destaque.jpg" alt="Image 4"></a>
                        </li>
                        <li><a class="fancybox" rel="group" href="src/imagem-destaque.jpg" title="">
                            <img src="src/imagem-destaque.jpg" alt="Image 5"></a>
                        </li>
                        <li><a class="fancybox" rel="group" href="src/imagem-destaque.jpg" title="">
                            <img src="src/imagem-destaque.jpg" alt="Image 6"></a>
                        </li>
                        <li><a class="fancybox" rel="group" href="src/imagem-destaque.jpg" title="">
                            <img src="src/imagem-destaque.jpg" alt="Image 7"></a>
                        </li>
                        <li><a class="fancybox" rel="group" href="src/imagem-destaque.jpg" title="">
                            <img src="src/imagem-destaque.jpg" alt="Image 8"></a>
                        </li>
                    </ul>
                    <!--div class="loading">Loading carousel items...</div-->
                </div>

                <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
                <a href="#" class="jcarousel-control-next">&rsaquo;</a>
            </div>
        </div>
				</div>
			</section>
			<!--section class="box_grupo">
				
			</section-->	
	<section id="box_contatos" class="noMobile marg_top50">
	<section class="center">
		<h1 class="ttl pa">
			Fale Conosco<!--strong></strong-->
		</h1>
		<ul>
			<li> <span class="icon_telefone" style="background-color:#ED3237;"></span>
					<h1><b>Central de Atendimento</b></h1>
					<p>
						(11) 5071-9069 <br />(11) 99641-4508
					</p>
			</li>
			<li><a href="contato/ligamos" class="ligamos_lightbox"> <span class="icon_celular" style="background-color:#ED3237;"></span>
					<h1>
						<b>Ligamos <br />Para você</b>
					</h1>
					<p>Solicite nosso contato.</p>
			</a></li>
			<li><a href="contato/contatoemail"> <span class="icon_email" style="background-color:#ED3237;" ></span>
					<h1>
					<b>Contato <br />por e-mail</b>
					</h1>
					<p>Fale conosco de forma rápida</p>
			</a></li>
			<li><a href=""> <span class="icon_online" style="background-color:#ED3237;" ></span>
					<h1>
						<b>Corretor <br /></b>
						<strong>ONLINE</strong>
					</h1>
					<p>Tire suas dúvidas.</p>
			</a></li>
		</ul>
	</section>
</section>
		</section>

	</main>
<?php echo $this->render("footer.php"); ?>
	</main><!-- /main -->
    

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.1.min.js"><\/script>')</script>

        <script src="js/fancybox/fancybox/jquery.fancybox.js"></script>
        <script src="js/fancybox/fancybox/jquery.fancybox-buttons.js"></script>
        <script src="js/fancybox/fancybox/jquery.fancybox-thumbs.js"></script>
        <script src="js/fancybox/fancybox/jquery.easing-1.3.pack.js"></script>
        <script src="js/fancybox/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function() {
            $(".fancybox").fancybox();
            });
        </script>
</body>
</body>
</html>