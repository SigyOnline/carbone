<?php echo $this->render("header.php");
$dado = $this->dado;
?>
<div style="clear:both;"></div>
<main class="pr border_top">
		<section class="center">
			<!-- resultado -->
			<article id="box_resultado">
                <!-- box nav -->
                <section id="nav_resultados">
	                <nav class="box_select noDesktop">
                        <div class="fake_select_resultado">
                            <select name="imopagina" id="imopagina_">
                                <option value="">IMÓVEIS POR PÁGINA</option>
                                <option value="12" <?php echo ( $_GET['imopagina'] == "12" ? 'checked="checked"' : " " ); ?>>VISUALIZAR 12 POR PÁGINA</option>
                                <option value="24" <?php echo ( $_GET['imopagina'] == "24" ? 'checked="checked"' : " " ); ?>>VISUALIZAR 24 POR PÁGINA</option>
                                <option value="36" <?php echo ( $_GET['imopagina'] == "36" ? 'checked="checked"' : " " ); ?>>VISUALIZAR 36 POR PÁGINA</option>
                            </select>
                        </div>
                        <div class="fake_select_resultado">
                            <select name="ordenar" id="ordenar" class="">
                                <option value="">ORDERNAR POR</option>
                                <option value="menor">Menor Valor</option>
                                <option value="maior">Maior Valor</option>
                                <option value="Bairro">Bairro</option>
                                <option value="categoria">Tipo</option>
                                <!-- <option value="metragem">Área Útil</option> -->
                            </select>
                        </div>
	                </nav>
                    <ul id="nav_ordernar" class="fl noOnlyTablet">
                        <li>ORDENAR POR</li>
                        <li><a id="menor" href="javascript:;" class="ordl opacity  <?php echo ( $_GET['ordenar'] == "menor" ? 'active' : " " ); ?>">Menor Valor</a></li>
                        <li><a id="maior" href="javascript:;" class="ordl opacity <?php echo ( $_GET['ordenar'] == "maior" ? 'active' : " " ); ?>">Maior Valor</a></li>
                        <li><a id="bairro" href="javascript:;" class="ordl opacity <?php echo ( $_GET['ordenar'] == "bairro" ? 'active' : " " ); ?>">Bairro</a></li>
                        <li><a id="categoria" href="javascript:;" class="ordl opacity <?php echo ( $_GET['ordenar'] == "categoria" ? 'active' : " " ); ?>">Tipo</a></li>
<!--                         <li><a id="metragem" href="javascript:;" class="ordl opacity <?php echo ( $_GET['ordenar'] == "metragem" ? 'active' : " " ); ?>">Área Útil</a></li> -->
                    </ul>
	                <nav class="tipo_visualizacao">
	                	<a href="javascript:;" class="btn_visualizacao <?php if ( $this->modo == 'box' ) echo 'active';?>" onclick="ListBox('box')"><span class="ico_box"></span>BOX</a>
                        <a href="javascript:;" class="btn_visualizacao <?php if ( $this->modo == 'lista' ) echo 'active';?>" onclick="ListBox('lista')"><span class="ico_lista"></span>LISTA</a>
	                </nav>
                </section>
                <!-- titulo -->
                <h1 class="pa noMobile ttl_internas"><?php echo $dado->getTotalItemCount(); ?> Resultado(s)</h1>
                <!-- no results -->
             	<?php if ( count($dado) == 0 ): ?>
				<section id="no_result">
                    <!-- <div class="pa imagem_casal"><img src="/src/imagem-casal.png" alt="Imagem referente a não retorno de sua pesquisa" /></div> -->
                    <div class="bg_faixa">
    					<div class="center">
    						<h1 class="bold">SUA PESQUISA NÃO TEVE RESULTADOS.</h1>
    						<p class="">Não encontramos imóveis com as características que você selecionou. <br/>Faça uma nova busca.</p>
    						<div class="comporta_links"><a href="/"class="bold opacity">IR PARA HOME</a><a href="/contato"class="bold opacity">FALE CONOSCO </a></div>
    					</div>
                    </div>
				</section>

				<?php else:
					if ( $this->modo == 'lista' ): ?>
					<ul id="lista_linha" class="modo_view">
					<?php foreach ($dado as $d ): ?>
					<!-- item -->
                    <li>
                        <div class="box_left">
                        	<a href="<?php echo $this->seo->getLink((array)$d); ?>">
                                <img src="<?php echo (empty($d->foto_destaque))?'src/foto-indisponivel.jpg':$d->foto_destaque; ?>" alt="Foto do Imóvel"/>
                                <div class="referencia_destaque pa">REF.: <?php echo $d->codigo; ?></div>
                                <span class="efeito pa"></span>
                            </a>
						</div>
						<div class="box_right">
                            <h1 class="local"><?php echo $d->fase_obra; ?></h1>
                            <h1 class="local"><?php echo $d->bairro; ?> / <?php echo $d->cidade; ?></h1>
                            
                            <span class="acabamento"></span>
                            <h2 class="itens">

                          <?php if( $d->dormitorios > 0 ):?>    <?php echo $d->dormitorios; ?>  <?php if( $d->dormitorios_ate > 0 ):?> até <?php echo $d->dormitorios_ate; ?><?php endif; ?>  Dorm(s) |<?php endif; ?>

                          <?php if( $d->area_privativa > 0 ):?>    <?php echo $d->area_privativa; ?>  <?php if( $d->area_privativa_ate > 0 ):?> até <?php echo $d->area_privativa_ate; ?><?php endif; ?>  Área Útil |<?php endif; ?>

                          <?php if( $d->vagas > 0 ):?>   <?php echo $d->vagas; ?> <?php if( $d->vagas_ate > 0 ):?> até <?php echo $d->vagas_ate; ?><?php endif; ?> Vaga(s) |<?php endif; ?>

                          <?php if( $d->suites > 0 ):?>   <?php echo $d->suites; ?> <?php if( $d->suites_ate > 0):?> até <?php echo $d->suites_ate; ?><?php endif; ?> Suíte(s) <?php endif; ?> </h2>

                   			<?php if ($d->status == 'VENDA' && $d->valor_venda > 0 ): ?>
                            <h2 class="valor">R$ <?php echo number_format($d->valor_venda,2,',','.'); ?></h2>
                            <?php elseif ($d->status == 'ALUGUEL' && $d->valor_locacao > 0 ): ?>
                            <h2 class="valor">R$ <?php echo number_format($d->valor_locacao,2,',','.'); ?></h2>
                            <?php elseif ($d->status == 'VENDA E ALUGUEL' && $d->valor_venda > 0 && $d->valor_locacao > 0): ?>
                            <h2 class="valor">R$ <?php echo number_format($d->valor_venda,2,',','.'); ?></h2> | <h2 class="valor">R$ <?php echo number_format($d->valor_locacao,2,',','.'); ?></h2>
                            <?php endif; ?>
                            <a href="<?php echo $this->seo->getLink((array)$d); ?>" class="btn_padrao pa radius4 fnt_center">CONSULTE</a>
                            <div class="box_check posLeft">
                                <input type="checkbox" name="favorito" id="fav<?php echo $d->codigo?>" />
                                <label for="fav<?php echo $d->codigo?>" class="icon_favorito <?php echo Cadastro_FavCom::getInstance()->getFavClass($d->codigo); ?>" onclick="favoritos(this, <?php echo $d->codigo; ?>);" title="favorito"></label>
                            </div>
                            <div class="box_check posRight">
                                <input type="checkbox" name="favorito" id="compare<?php echo $d->codigo?>" />
                                <label for="compare<?php echo $d->codigo?>" class="icon_compare <?php echo Cadastro_FavCom::getInstance()->getComClass($d->codigo); ?>" onclick="comparar(this, <?php echo $d->codigo; ?>);" title="compare"></label>
                            </div>
                        </div>
                    </li>
                    <?php endforeach;
                    else :
                    ?>
				<ul id="lista_box" class="modo_view">
					<?php foreach ($dado as $d ):
					?>
					<!-- item -->
                    <li class="imovel_destaque">
                        <img style="height:250px;" src="<?php echo (empty($d->foto_destaque))?'src/foto-indisponivel.jpg':$d->foto_destaque; ?>" alt="Foto do imóvel em destaque" />
                        <div class="referencia_destaque pa">REF.: <?php echo $d->codigo; ?></div>
                        <div class="pos_info">
                            <h1><?php echo $d->bairro; ?> / <?php echo $d->cidade; ?></h1>
                            <h2><?php echo $d->cat; ?></h2>
                        </div>
                        <div class="infos">
                            <h1 class="local"><?php echo $d->fase_obra; ?></h1>
                            <h1 class="local"><?php echo $d->bairro; ?> / <?php echo $d->cidade; ?></h1>
                            <h2 class="tipo"><?php echo $d->cat; /*if (empty($d->empreendimento)){ echo $d->cat;}else{echo $d->empreendimento;}*/ ?></h2>
                            <span class="acabamento"></span>
                            <h2 class="itens">

                              <?php if( $d->dormitorios > 0 ):?> <?php echo $d->dormitorios; ?> <?php if( $d->dormitorios_ate > 0 ):?> até <?php echo $d->dormitorios_ate; ?><?php endif; ?>  Dorm(s) |<?php endif; ?>

                              <?php if( $d->area_privativa > 0 ):?>    <?php echo $d->area_privativa; ?>  <?php if( $d->area_privativa_ate > 0 ):?> até <?php echo $d->area_privativa_ate; ?><?php endif; ?>  Área Útil |<?php endif; ?>

                              <?php if( $d->vagas > 0 ):?>  <?php echo $d->vagas; ?> <?php if( $d->vagas_ate > 0 ):?> até <?php echo $d->vagas_ate; ?><?php endif; ?> Vaga(s) |<?php endif; ?>

                              <?php if( $d->suites > 0 ):?>  <?php echo $d->suites; ?> <?php if( $d->suites_ate > 0):?> até <?php echo $d->suites_ate; ?><?php endif; ?> Suíte(s)<?php endif; ?></h2>

                            <?php if ($d->status == 'VENDA'  && $d->valor_venda > 0): ?>
                            <h2 class="valor">R$ <?php echo number_format($d->valor_venda,2,',','.'); ?></h2>
                            <?php elseif ($d->status == 'ALUGUEL'  && $d->valor_locacao > 0): ?>
                            <h2 class="valor">R$ <?php echo number_format($d->valor_locacao,2,',','.'); ?></h2>
                            <?php elseif ($d->status == 'VENDA E ALUGUEL' && $d->valor_venda > 0 && $d->valor_locacao > 0): ?>
                            <h2 class="valor">R$ <?php echo number_format($d->valor_venda,2,',','.'); ?></h2> | <h2 class="valor">R$ <?php echo number_format($d->valor_locacao,2,',','.'); ?></h2>
                            <?php endif; ?>
                            <a href="<?php echo $this->seo->getLink((array)$d); ?>" class="">CONSULTE</a>
                            <div class="box_check posLeft">
                            	<input type="checkbox" name="favorito" id="favlb<?php echo $d->codigo?>" />
                                <label for="favlb<?php echo $d->codigo?>" class="icon_favorito <?php echo Cadastro_FavCom::getInstance()->getFavClass($d->codigo); ?>" onclick="favoritos(this, <?php echo $d->codigo; ?>);" title="favorito"></label>
                            </div>
                            <div class="box_check posRight">
                            	<input type="checkbox" name="favorito" id="comparelb<?php echo $d->codigo?>" />
                                <label for="comparelb<?php echo $d->codigo?>" class="icon_compare <?php echo Cadastro_FavCom::getInstance()->getComClass($d->codigo); ?>" onclick="comparar(this, <?php echo $d->codigo; ?>);" title="compare"></label>
                            </div>
                        </div>
                    </li>
                    <!-- item -->
              		<?php endforeach;
              			endif;
              		?>
				</ul>
				<?php endif;?>

				<nav id="paginacao">
					<div class="pag_number">
					<?php echo $dado; ?>
                    </div>
				</nav>
    		</article>
    		<!-- Filtro -->
    		<aside id="box_filtros">
    		<form action="imoveis" method="get" name="form1" id="form1">
    		<input type="hidden" id="ordenarcampo" name="ordenar" value="<?php echo $_GET['ordenar']?>">
                <div class="fake_select_resultado">
                    <select name="imopagina" id="imopagina" class="">
                     	<option value="">IMÓVEIS POR PÁGINA</option>
                        <option value="12" <?php echo ( $_GET['imopagina'] == "12" ? 'selected="selected"' : " " ); ?>>VISUALIZAR 12 POR PÁGINA</option>
                        <option value="24" <?php echo ( $_GET['imopagina'] == "24" ? 'selected="selected"' : " " ); ?>>VISUALIZAR 24 POR PÁGINA</option>
                        <option value="36" <?php echo ( $_GET['imopagina'] == "36" ? 'selected="selected"' : " " ); ?>>VISUALIZAR 36 POR PÁGINA</option>
                    </select>
                </div>
	            <div id="filtros">
	            <?php if ( $this->status ) :?>
	                <h2 class="subttl_filtro">Status</h2>
	            <?php foreach ( $this->status as $row ) :
	            	if($row->status == 'VENDA E ALUGUEL') continue;
	            	
	            ?>
	                <div class="item_filtros">
	                    <input type="radio" id="cod<?php echo $row->status?>" class="input_filtro" value="<?php echo $row->status?>" name="status"
	                    <?php echo (strtolower($row->status) == strtolower($_GET['status'])? 'checked="checked"' : NULL)?>/>
	                    <label for="cod<?php echo $row->status?>" class="label_filtro label_radio active"><?php echo $row->status?></label>
	                </div>
	                <?php endforeach;?>
	            <?php endif; ?>
	                <?php if ( $this->fase_obra ):?>
	                <h2 class="subttl_filtro">Fase da Obra</h2>
	              	<?php foreach ( $this->fase_obra as $fase ): ?>
								<?php @$checked5 = (in_array($fase->fase_obra , $_GET['fase_obra']))?'checked="checked"':'';
								 if ( !empty($fase->fase_obra) ) :
								 ?>
                    <div class="item_filtros">
                        <input <?php echo $checked5; ?> type="checkbox" id="codLançamentos<?php echo $fase->fase_obra; ?>" class="input_filtro" value="<?php echo $fase->fase_obra; ?>" name="fase_obra[]"/>
                        <label for="codLançamentos<?php echo $fase->fase_obra; ?>" class="label_filtro"><?php echo $fase->fase_obra; ?></label>
                    </div>
		                <?php endif;
		                endforeach;
		                endif;?>

	                <h2 class="subttl_filtro">Tipo de Imóvel</h2>
	             	<?php foreach ( $this->categoria as $cat ): ?>
								<?php @$checkedc = (in_array($cat->id_categoria, $_GET['categoria']))?'checked="checked"':'';
								 if (!empty($cat->id_categoria))
										{
								 ?>
	                <div class="item_filtros">
	                    <input <?php echo $checkedc; ?> name="categoria[]" type="checkbox" id="cod<?php echo $cat->categoria; ?>" class="input_filtro" value="<?php echo $cat->id_categoria; ?>"/>
	                    <label for="cod<?php echo $cat->categoria; ?>" class="label_filtro"><?php echo $cat->categoria; ?></label>
	                </div>
	               <?php } endforeach; ?>

	               <?php if ( $this->cidade ) :?>
                    <h2 class="subttl_filtro">Cidade</h2>
                    <select name="cidade" class="select_filtro cidadef">
                        <option value="" selected="selected">Selecione uma cidade</option>
                    <?php foreach ( $this->cidade as $c ):
                    		if ( !$c->cidade ) continue;
							$ci = str_replace('+',' ',$_GET['cidade']);
								?>
								<?php $s = ( $c->cidade == $ci ? 'selected="selected"' : " " ); ?>
                       <option value="<?php echo $c->cidade; ?>" <?php echo $s; ?>><?php echo $c->cidade; ?></option>
					<?php endforeach; ?>
                    </select>
					<?php endif;?>
                    <?php if ( $this->bairro ):?>
                    <h2 class="subttl_filtro">Bairro</h2>
                    <div class="item_filtros">
                        <!-- <h3 class="defalut_input">Selecione uma cidade</h3> -->
                    </div>
                    <div class="box_bairros">
                        <?php $_SESSION['bairro'] = isset($_GET['bairro']) ? $_GET['bairro'] : ''; ?>
                    <!-- <?php foreach ( $this->bairro as $b ): ?>
								<?php @$checked4 = (in_array($b->bairro, $_GET['bairro']))?'checked="checked"':'';
								 if ( !empty($b->bairro) ):
								 ?>
                        <div class="item_filtros">
                            <input <?php echo $checked4; ?> type="checkbox" id="cod<?php echo $b->bairro; ?>" class="input_filtro" value="<?php echo $b->bairro; ?>" name="bairro[]"/>
                            <label for="cod<?php echo $b->bairro; ?>" class="label_filtro"><?php echo $b->bairro; ?></label>
                        </div>
                  <?php 	endif;
                  		 endforeach;?> -->

                    </div>
                    <?php endif;?>
                    <?php if (/*$this->carac*/ false): ?>
	                <h2 class="subttl_filtro">Característica do Imóvel</h2>

	              <?php foreach ( $this->carac as $carac ): ?>
	              <?php @$checked10 = (in_array($carac->id_caracteristica_imovel, $_GET['caracteristica']))?'checked="checked"':'';
								 if (!empty($carac->id_caracteristica_imovel))
										{
								 ?>
	                <div class="item_filtros">
	                    <input <?php echo $checked10; ?> type="checkbox" id="alarme<?php echo $carac->id_caracteristica_imovel; ?>" class="input_filtro" value="<?php echo $carac->id_caracteristica_imovel; ?>" name="caracteristica[]"/>
	                    <label for="alarme<?php echo $carac->id_caracteristica_imovel; ?>" class="label_filtro"><?php echo $carac->label; ?></label>
	                </div>
	              <?php } endforeach;?>
	              <?php endif; ?>
	                <h2 class="subttl_filtro">Dormitórios</h2>
	                <div class="item_filtros">
	                <?php for ($i = 1; $i <= 5; $i++): ?>
								<?php @$checked1 = (in_array($i, $_GET['dormitorios']))?'checked="checked"':'';
								 if (!empty($i))
										{
								 ?>
	                    <input <?php echo $checked1; ?> type="checkbox" id="cod17" class="input_filtro" value="<?php echo $i; ?>" name="dormitorios[]"/>
	                    <label for="cod17" class="label_filtro"><?php echo $i; ?></label>
	                 <?php } endfor; ?>
	                </div>

	                <h2 class="subttl_filtro">Suítes</h2>
	                <div class="item_filtros">
	                <?php for ($y = 1; $y <= 5; $y++): ?>
								<?php @$checked2 = (in_array($y, $_GET['suites']))?'checked="checked"':'';
								 if (!empty($y))
										{
								 ?>
	                    <input <?php echo $checked2; ?> type="checkbox" id="cod22" class="input_filtro" value="<?php echo $y; ?>" name="suites[]"/>
	                    <label for="cod22" class="label_filtro"><?php echo $y; ?></label>
	                 <?php } endfor; ?>
	                </div>

	                <h2 class="subttl_filtro">Vagas</h2>
	                <div class="item_filtros">
	                <?php for ($j = 1; $j <= 5; $j++): ?>
								<?php @$checked3 = (in_array($j, $_GET['vagas']))?'checked="checked"':'';
								 if (!empty($j))
										{
								 ?>
	                    <input <?php echo $checked3; ?> type="checkbox" id="cod27" class="input_filtro" value="<?php echo $j; ?>" name="vagas[]"/>
	                    <label for="cod27" class="label_filtro"><?php echo $j == 5? $j. ' ou mais':$j; ?></label>
	                <?php } endfor; ?>
	                </div>

	                <!--<h2 class="subttl_filtro">Área Útil</h2>
	                <div class="item_filtros">
	                    <input type="checkbox" id="cod42" class="input_filtro" value="0a50" name="area_privativa[]" <?php echo (@in_array('0a50', $_GET['area_privativa']))?'checked="checked"':'' ?>/>
	                    <label for="cod42" class="label_filtro">Até 50m</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="cod43" class="input_filtro" value="51a100" name="area_privativa[]" <?php echo (@in_array('51a100', $_GET['area_privativa']))?'checked="checked"':'' ?>/>
	                    <label for="cod43" class="label_filtro">51m - 100m</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="cod44" class="input_filtro" value="101a150" name="area_privativa[]" <?php echo (@in_array('101a150', $_GET['area_privativa']))?'checked="checked"':'' ?>/>
	                    <label for="cod44" class="label_filtro">101m - 150m</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="cod45" class="input_filtro" value="151a200" name="area_privativa[]" <?php echo (@in_array('151a200', $_GET['area_privativa']))?'checked="checked"':'' ?>/>
	                    <label for="cod45" class="label_filtro">151m - 200m</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="cod46" class="input_filtro" value="201a250" name="area_privativa[]" <?php echo (@in_array('201a250', $_GET['area_privativa']))?'checked="checked"':'' ?>/>
	                    <label for="cod46" class="label_filtro">201m - 250m</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="m111" class="input_filtro" value="251a300" name="area_privativa[]" <?php echo (@in_array('251a300', $_GET['area_privativa']))?'checked="checked"':'' ?>/>
	                    <label for="m111" class="label_filtro">251m - 300m</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="m145" class="input_filtro" value="301a350" name="area_privativa[]" <?php echo (@in_array('301a350', $_GET['area_privativa']))?'checked="checked"':'' ?>/>
	                    <label for="m145" class="label_filtro">301m - 350m</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="m165" class="input_filtro" value="351a400" name="area_privativa[]" <?php echo (@in_array('351a400', $_GET['area_privativa']))?'checked="checked"':'' ?>/>
	                    <label for="m165" class="label_filtro">351m - 400m</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="m195" class="input_filtro" value="401a500" name="area_privativa[]" <?php echo (@in_array('401a500', $_GET['area_privativa']))?'checked="checked"':'' ?>/>
	                    <label for="m195" class="label_filtro">401m - 500m</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="acima200" class="input_filtro" value="500" name="area_privativa[]" <?php echo (@in_array('500', $_GET['area_privativa']))?'checked="checked"':'' ?>/>
	                    <label for="acima200" class="label_filtro">Acima de 500m</label>
	                </div>-->

                    <h2 class="subttl_filtro">Faixa de Valores (Venda)</h2>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod47" class="input_filtro" value="0a200000" name="valor_venda[]" <?php echo (@in_array('0a200000', $_GET['valor_venda']))?'checked="checked"':'' ?>/>
                        <label for="cod47" class="label_filtro">Até R$ 200.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod48" class="input_filtro" value="200000a300000" name="valor_venda[]" <?php echo (@in_array('200000a400000', $_GET['valor_venda']))?'checked="checked"':'' ?>/>
                        <label for="cod48" class="label_filtro">R$ 200.000,00 a R$ 400.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod49" class="input_filtro" value="400000a600000" name="valor_venda[]" <?php echo (@in_array('400000a600000', $_GET['valor_venda']))?'checked="checked"':'' ?>/>
                        <label for="cod49" class="label_filtro">R$ 400.000,00 a R$ 600.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod50" class="input_filtro" value="600000a800000" name="valor_venda[]" <?php echo (@in_array('600000a800000', $_GET['valor_venda']))?'checked="checked"':'' ?>/>
                        <label for="cod50" class="label_filtro">R$ 600.000,00 a R$ 800.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod51" class="input_filtro" value="800000a1000000" name="valor_venda[]" <?php echo (@in_array('800000a1000000', $_GET['valor_venda']))?'checked="checked"':'' ?>/>
                        <label for="cod51" class="label_filtro">R$ 800.000,00 a R$ 1.000.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod57" class="input_filtro" value="1000000" name="valor_venda[]" <?php echo (@in_array('1000000', $_GET['valor_venda']))?'checked="checked"':'' ?>/>
                        <label for="cod57" class="label_filtro">Acima de R$ 1.000.000,00</label>
                    </div>

                    <h2 class="subttl_filtro">Faixa de Valores (Locação)</h2>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod58" class="input_filtro" value="0a2000" name="valor_locacao[]" <?php echo (@in_array('0a2000', $_GET['valor_locacao']))?'checked="checked"':'' ?>/>
                        <label for="cod58" class="label_filtro">Até R$ 1.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod59" class="input_filtro" value="1000a2000" name="valor_locacao[]" <?php echo (@in_array('1000a2000', $_GET['valor_locacao']))?'checked="checked"':'' ?>/>
                        <label for="cod59" class="label_filtro">R$ 1.000,00 a R$ 2.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod59" class="input_filtro" value="2000a3000" name="valor_locacao[]" <?php echo (@in_array('2000a3000', $_GET['valor_locacao']))?'checked="checked"':'' ?>/>
                        <label for="cod59" class="label_filtro">R$ 2.000,00 a R$ 3.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod59" class="input_filtro" value="3000a4000" name="valor_locacao[]" <?php echo (@in_array('3000a4000', $_GET['valor_locacao']))?'checked="checked"':'' ?>/>
                        <label for="cod59" class="label_filtro">R$ 3.000,00 a R$ 4.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod66" class="input_filtro" value="4000" name="valor_locacao[]" <?php echo (@in_array('4000', $_GET['valor_locacao']))?'checked="checked"':'' ?>/>
                        <label for="cod66" class="label_filtro">Acima de R$ 4.000,00</label>
                    </div>
	            </div>
	            <input type="hidden" id="modo" name="modo" value="<?php echo $this->modo;?>">
                <input type="reset" name="limpar" value="LIMPAR FILTROS" class="btn_zerar btn_padrao" onclick="location.href='./imoveis/'"/>
    		</aside>
    		</form>
		</section>
	</main>
<?php echo $this->render("footer.php"); ?>
<script type="text/javascript">
$(function(){
	    $("#form1").on("change", "input:checkbox,input:radio", function(){
	        $("#form1").submit();
	    });

	    $("#imopagina").change(function(){
	        $("#form1").submit();
	    });

	    $(".cidadef").change(function(){
	        $("#form1").submit();
	    });

	    $(".ordl").click(function(){

		    var id = $(this).attr('id');
	        $("#ordenarcampo").val(id);
	        $("#form1").submit();
	    });


	    ListBox = function (t)
		{
			switch (t)
			{
				case 'box':
					$("#modo").val("box");
				break;

				default:
					$("#modo").val("lista");
				break;
			}

			$("#form1").submit();
		}
});
$('.cidadef').change(function(){
    $('box_bairros input').prop('checked', false);
    var id = $(this).val();
    $.post("<?php echo Zend_Registry::get('config')->www->host;?>/imoveis/bairro/id/"+id,{tipo:"filtro"},function(data){
        $(".box_bairros").html(data);
    }); 
});

if($('.cidadef').val() != ''){
    var id = $('.cidadef').val();
    $.post("<?php echo Zend_Registry::get('config')->www->host;?>/imoveis/bairro/id/"+id,{tipo:"filtro"},function(data){
        $(".box_bairros").html(data);
    }); 
}

    var cidade = '<?php echo $_GET['cidade'];?>';
    if (cidade !="") {
        $.post("<?php echo Zend_Registry::get('config')->www->host;?>/imoveis/bairro/id/"+cidade,{tipo:"select"},function(data){
            $(".retornabairro").html(data);
        });
    }

</script>
