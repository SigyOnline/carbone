<?php echo $this->render("header.php");
$server = $_SERVER['SERVER_NAME'];
$endereco = $_SERVER ['REQUEST_URI'];
$whatsapp = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

if(!empty($_SESSION['back'])){
    echo "<script>alert('Preencha o código Captcha.');</script>";
}
unset($_SESSION['back']);
?>

<style>
.ts-slide {
    width: 120px !important;
}
.lista-estabelecimentos .active span {
    background-color: #B41B1E !important;
}
</style>
<main class="pr border_top">
  <section class="center">
    <!-- titulo -->
    <h1 class="pa noMobile ttl_internas outro_position">Detalhes do Imóvel</h1>
    <!-- no results -->
    <!--            <section id="no_result"> -->
        <!--                 <div class="pa imagem_casal"><img src="src/imagem-casal.png" alt="Imagem referente a não retorno de sua pesquisa" /></div> -->
        <!--                 <div class="bg_faixa"> -->
            <!--                     <div class="center"> -->
                <!--                         <h1 class="bold">SUA PESQUISA NÃO TEVE RESULTADOS.</h1> -->
                <!--                         <p class="">Não encontramos imóveis com as características que você selecionou. <br/>Faça uma nova busca.</p> -->
                <!--                         <div class="comporta_links"><a href="index" class="bold opacity">IR PARA HOME</a><a href="./contato.php" class="bold opacity">FALE CONOSCO </a></div> -->
                <!--                     </div> -->
                <!--                 </div> -->
                <!--             </section> -->
                <!-- destaque -->
                <div class="noDesktop noOnlyTablet noPrint favorito_destaque pa fr marg_right-40 <?php echo Cadastro_FavCom::getInstance()->getComClass($this->dados->codigo); ?>" style="font-size: 20px;" onclick="comparar(this, <?php echo $this->dados->codigo; ?>);"><span class="fa fa-check"></span></div>

                <div class="noDesktop noOnlyTablet noPrint favorito_destaque pa fr <?php echo Cadastro_FavCom::getInstance()->getFavClass($this->dados->codigo); ?>" style="left: 50px;font-size: 20px;" onclick="favoritos(this, <?php echo $this->dados->codigo; ?>);"><span class="fa fa-star"></span></div>

                <section id="box_detalhes">

                    <aside id="info_desktop">

                        <div class="pa btns_fav-comp">

                            <div class="btn_favorece"><a href='imoveis/favoritos'>Favoritos</a></div>
                            
                            <div class="btn_comparar"><a href='imoveis/comparar'>Comparar</a></div>

                        </div>

                        <h1 class="item_nome"><?php //echo $this->dados->empreendimento; ?></h1>

                        <div class="box_check pa posRight favorito">

                            <label for="fav" class="icon_favorito <?php echo Cadastro_FavCom::getInstance()->getFavClass($this->dados->codigo); ?>" onclick="favoritos(this, <?php echo $this->dados->codigo; ?>);" title="favorito"></label>
                        </div>

                        <div class="box_check pa posRight">

                            <label for="compare" class="icon_compare <?php echo Cadastro_FavCom::getInstance()->getComClass($this->dados->codigo); ?>" onclick="comparar(this, <?php echo $this->dados->codigo; ?>);" title="compare"></label>

                        </div>

                        <div class="pa btns_sociais">

                            <div id="bts_likes">
                                <div class="addthis_native_toolbox" style="display: inline-block;"></div>
                                <div class="fb-share-button" data-href="" data-layout="button_count" style="margin-left: 3%;"></div>

                                <div class="whatsapp">
                                    <a target="_blank" href='https://api.whatsapp.com/send?text=<?php echo $this->dados->bairro."+http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];  ?>' data-action="share/whatsapp/share"><span style="margin-left: 5px;" class="fa fa-whatsapp"></span> Enviar</a>
                                </div>

<!--                             <div class="addthis_native_toolbox"></div>
                            <div class="addthis_native_toolbox" style="display: inline-block;"></div>
                            <div class="fb-share-button" data-href="" data-layout="button_count" style="bottom: 8px;"></div> -->
                            </div>

                        </div>

                        <h2 class="item_tipo"><?php echo $this->dados->categoria; ?></h2>
                        <h3 class="item_REF">REF: <?php echo $this->dados->codigo; ?></h3>
                        <div class="bairroDesc">
                         <h3 class="item_local"><?php echo $this->dados->bairro; ?> - <?php echo $this->dados->cidade; ?></h3>
                         <div id="bts_likes">

                    </div>
                    
                    <div class="<?php echo $this->dados->logo_empreendimento ? "imagem_empreendimento" : "dn";?>">
                        <img src="<?php echo $this->dados->logo_empreendimento; ?>" alt="Logo do empreendimento" /></div>
                        <ul class="<?php echo $this->dados->logo_empreendimento ? "lista_info" : "lista_info_full";?>">
                            <li class=""></li>
                            <?php if( !empty($this->dados->dormitorios) ):?>
                                <li class=""><span class="fa fa-bed marg_right1"></span><?php echo $this->dados->dormitorios; ?> Dormitório(s)</li>
                            <?php endif;?>
                            <?php if( !empty($this->dados->vagas) ):?>
                                <li class=""><span class="fa fa-car marg_right1"></span><?php echo $this->dados->vagas; ?> Vaga(s)</li>
                            <?php endif;?>
                            <?php if( !empty($this->dados->suites) ):?>
                                <li class=""><span class="fa fa-bath marg_right1"></span><?php echo $this->dados->suites; ?> Suíte(s)</li>
                            <?php endif;?>
                            <!-- <?php //if( !empty($this->dados->area_construida) ):?>
                                <li class="">Área Útil <?php //echo $this->dados->area_construida; ?>m²</li>
                            <?php //endif;?> -->
                            <?php if( !empty($this->dados->area_total) ):?>
                                <li class="">Área : <?php echo $this->dados->area_total; ?>m²</li>
                            <?php endif;?>
                            <?php if( !empty($this->dados->percentual_obra) ):?>
                                <li class="texto_maior">PERCENTUAL DA OBRA: <?php echo $this->dados->percentual_obra; ?>%</li>
                            <?php endif;?>

                            <?php if( !empty($this->dados->posicao_andar) ):?>
                                <li class=""><span class="fa fa-building marg_right1"></span>Andar: <?php echo $this->dados->posicao_andar; ?>
                            <?php endif; ?>
                            <?php if( !empty($this->dados->valor_condominio) ):?>
                                <li>Condomínio Aprox.: R$ <?php echo number_format($this->dados->valor_condominio, 2, ',', '.'); ?>
                            <?php endif; ?>
                            <!-- <?php //if( !empty($this->dados->valor_iptu) ):?>
                                <li>Valor do IPTU: R$ <?php //echo number_format($this->dados->valor_iptu, 2, ',', '.'); ?>
                            <?php //endif; ?>
 -->
                        </ul>
                        <div class="box_border_top">
                            <div class="box_valor">
                                <?php if( !empty($this->dados->valor_venda)): ?>
                                    <h2>Venda: R$ <?php echo number_format($this->dados->valor_venda, 2, ',', '.'); ?></h2>
                                <?php endif; ?>
                                <?php if( !empty($this->dados->valor_locacao)): ?>    
                                    <h2>Locação: R$ <?php echo number_format($this->dados->valor_locacao, 2, ',', '.'); ?></h2>
                                <?php endif; ?>
                            </div> 
                            <!--<a href="javascript:window.print()" class="btn_padrao_detalhes opacity">Imprimir Ficha do Imóvel</a>-->
                        </div>

                    </aside>
                    <aside id="imagem_principal">

                        <img id="image_principal" src="<?php echo ($this->dados->foto_destaque)? $this->dados->foto_destaque : 'src/foto-indisponivel.jpg'; ?>" alt="Foto principal do Imóvel" />
                        <div class="favorito_destaque pa fr marg_right-40 <?php echo Cadastro_FavCom::getInstance()->getComClass($this->dados->codigo); ?>" onclick="comparar(this, <?php echo $this->dados->codigo; ?>);" title="comparar"><span class="fa fa-check"></span></div>
                        <div class="favorito_destaque pa fr <?php echo Cadastro_FavCom::getInstance()->getFavClass($this->dados->codigo); ?>" onclick="favoritos(this, <?php echo $this->dados->codigo; ?>);" title="favoritos"><span class="fa fa-star"></span>

                        </div>

                    </aside>

                </section>
                <!--Galeria Fotos-->
                <section id="galeria">
                    <?php if( !empty($this->foto_imovel) || !empty($this->foto_empreendimento) ||
                      !empty($this->foto_plantas) || !empty($this->foto_perspectiva) ):
                      ?>
                      <ul id="nav_galeria">
                        <li>GALERIA DE IMAGENS</li>
                        <?php if(!empty($this->foto_imovel)): $ativo = 'aba_active'; ?>
                        <?php endif; ?>
                        <?php if(!empty($this->foto_empreendimento)): $ativo = ($ativo == '' )?  'aba_active':''; ?>
                            <li onClick="trocaGaleria(this.id)" id="2"><a id="aba02" href="javascript:trocaMenu_ativo('2')" class="opacity aba02 <?php echo $ativo; ?>">Fotos do Empreendimento </a></li>
                        <?php endif; ?>
                        <?php if(!empty($this->foto_plantas)): ?>
                            <!--li onClick="trocaGaleria(this.id)" id="3"><a id="aba03" href="javascript:trocaMenu_ativo('3')" class="opacity aba03">Imagens das Plantas</a></li-->
                        <?php endif; ?>
                        <?php if(!empty($this->foto_perspectiva)): ?>
                            <li onClick="trocaGaleria(this.id)" id="4"><a id="aba04" href="javascript:trocaMenu_ativo('4')" class="opacity aba04">Perspectiva Artística</a></li>
                        <?php endif; ?>
                    </ul>
                    <div class="galeira_borda">
                        <div id="gal1" class="comporta-galeria pr">
                            <div id="myScroller" class="thumb-scroller">
                                <ul class="ts-list">
                                    <?php foreach ($this->foto_imovel as $foto) : ?>
                                        <li>
                                          <a href="<?php echo $foto['foto']?>" data-lightbox-group="gallery1" title="<?php echo $foto['descricao']; ?>">
                                            <img src="<?php echo $foto['foto']?>" alt=""/>
                                        </a>            
                                    </li>
                                <?php endforeach; ?>

                            </ul>       
                        </div>
                        <div id="myScroller_tab" class="thumb-scroller">
                            <ul class="ts-list">
                                <?php foreach ($this->foto_imovel as $foto) : ?>
                                    <li>
                                      <a href="<?php echo $foto['foto']?>" data-lightbox-group="gallery12" title="<?php echo $foto['descricao']; ?>">
                                        <img src="<?php echo $foto['foto']?>" alt=""/>
                                    </a>                
                                </li>
                            <?php endforeach; ?>
                        </ul>       
                    </div>
                </div>
                <div id="gal2" class="comporta-galeria pr">
                    <div id="myScroller_02" class="thumb-scroller">
                        <ul class="ts-list">
                            <?php foreach ($this->foto_empreendimento as $foto) : ?>
                                <li>
                                  <a href="<?php echo $foto['foto']?>" data-lightbox-group="gallery2" title="<?php echo $foto['descricao']; ?>">
                                    <img src="<?php echo $foto['foto']?>" alt=""/>
                                </a>                
                            </li>
                        <?php endforeach; ?>   
                    </ul>       
                </div>
                <div id="myScroller_tab_02" class="thumb-scroller">
                    <ul class="ts-list">
                     <?php foreach ($this->foto_empreendimento as $foto) : ?>
                        <li>
                          <a href="<?php echo $foto['foto']?>" data-lightbox-group="gallery22" title="<?php echo $foto['descricao']; ?>">
                            <img src="<?php echo $foto['foto']?>" alt=""/>
                        </a>                
                    </li>
                <?php endforeach; ?>   
            </ul>       
        </div>
    </div>
    <div id="gal3" class="comporta-galeria pr">
        <div id="myScroller_03" class="thumb-scroller">
            <ul class="ts-list">
                <?php foreach ($this->foto_plantas as $foto) : ?>
                    <li>
                      <a href="<?php echo $foto['foto']?>" data-lightbox-group="gallery3" title="<?php echo $foto['descricao']; ?>">
                        <img src="<?php echo $foto['foto']?>" alt=""/>
                    </a>                
                </li>
            <?php endforeach; ?>  
        </ul>       
    </div>
    <div id="myScroller_tab_03" class="thumb-scroller">
        <ul class="ts-list">
            <?php foreach ($this->foto_plantas as $foto) : ?>
                <li>
                  <a href="<?php echo $foto['foto']?>" data-lightbox-group="gallery32" title="<?php echo $foto['descricao']; ?>">
                    <img src="<?php echo $foto['foto']?>" alt=""/>
                </a>                
            </li>
        <?php endforeach; ?>                  
    </ul>       
</div>
</div>
<div id="gal4" class="comporta-galeria pr">
    <div id="myScroller_04" class="thumb-scroller">
        <ul class="ts-list">
         <?php foreach ($this->foto_perspectiva as $foto) : ?>
            <li>
              <a href="<?php echo $foto['foto']?>" data-lightbox-group="gallery4" title="<?php echo $foto['descricao']; ?>">
                <img src="<?php echo $foto['foto']?>" alt=""/>
            </a>                
        </li>
    <?php endforeach; ?>                      
</ul>       
</div>
<div id="myScroller_tab_04" class="thumb-scroller">
    <ul class="ts-list">
        <?php foreach ($this->foto_perspectiva as $foto) : ?>
            <li>
              <a href="<?php echo $foto['foto']?>" data-lightbox-group="gallery42" title="<?php echo $foto['descricao']; ?>">
                <img src="<?php echo $foto['foto']?>" alt=""/>
            </a>                
        </li>
    <?php endforeach; ?>                       
</ul>       
</div>
</div>
</div>
<?php endif; ?>
</section>

<!-- info Mobile -->
<section id="info_imovel_mobile" class="">
    <div class="center radius4">
        <div class="box_img_mobileTablet"><a href="imoveis/imovelfoto/id/<?php echo $this->dados->codigo; ?>"><img src="<?php echo ($this->dados->foto_destaque)? $this->dados->foto_destaque : 'src/foto-indisponivel.jpg'; ?>" alt="Foto principal do Imóvel" /></a>
        </div>

        <!-- RC 23042018 -->
        <div class="div_info">
            <div class="btns_sociais">
                <div id="bts_likes">
                    <div class="addthis_native_toolbox"></div>
                    <div class="fb-share-button" data-href="" data-layout="button_count" style="margin-left: 3%;"></div>
                    <div class="whatsapp">
                        <a target="_blank" href='https://api.whatsapp.com/send?text=<?php echo $this->dados->bairro."+http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];  ?>' data-action="share/whatsapp/share"><span class="fa fa-whatsapp marg_left10"></span> Enviar<span class="noMobile"> via WhatsApp</span></a>
                    </div>

                </div>

                <div class="btn_favorece"><a href="favoritos">Favoritos</a></div>

                <div class="btn_comparar"><a href="comparar">Comparar</a></div>

            </div>
        </div>
        <!-- FIM RC 23042018 -->

        <h1 class="item_local"><?php //echo $this->dados->empreendimento; ?></h1>
        <h2 class="item_tipo bold"><?php echo $this->dados->categoria; ?></h2>
        <ul class="lista_info">
            <li class=""><?php echo $this->dados->bairro; ?> - <?php echo $this->dados->cidade; ?></li>
            <li class="">Ref. <?php echo $this->dados->codigo; ?></li>
            <?php if( !empty($this->dados->dormitorios) ):?>
                <li class=""><span class="fa fa-bed marg_right1"></span><?php echo $this->dados->dormitorios; ?> Dormitório(s)</li>
            <?php endif;?>
            <?php if( !empty($this->dados->vagas) ):?>
                <li class=""><span class="fa fa-car marg_right1"></span><?php echo $this->dados->vagas; ?> Vaga(s)</li>
            <?php endif;?>
            <?php if( !empty($this->dados->suites) ):?>
                <li class=""><span class="fa fa-bath marg_right1"></span><?php echo $this->dados->suites; ?> Suíte(s)</li>
            <?php endif;?>
            <?php if( !empty($this->dados->area_construida) ):?>
                <li class="">Área Construída: <?php echo $this->dados->area_construida; ?>m²</li>
            <?php endif;?>
            <?php if( !empty($this->dados->area_terreno) ):?>
                <li class="">Área Terreno: <?php echo $this->dados->area_terreno; ?>m²</li>
            <?php endif;?>
            <?php if( !empty($this->dados->percentual_obra) ):?>
                <li class="texto_maior">PERCENTUAL DA OBRA: <?php echo $this->dados->percentual_obra; ?>%</li>
            <?php endif;?>

            <?php if( !empty($this->dados->posicao_andar) ):?>
                <li class=""><span class="fa fa-building marg_right1"></span>Andar: <?php echo $this->dados->posicao_andar; ?>
            <?php endif; ?>
            <?php if( !empty($this->dados->valor_condominio) ):?>
                <li>Condomínio Aprox.: R$ <?php echo number_format($this->dados->valor_condominio, 2, ',', '.'); ?>
            <?php endif; ?>
            <?php if( !empty($this->dados->valor_iptu) ):?>
                <li>Valor do IPTU: R$ <?php echo number_format($this->dados->valor_iptu, 2, ',', '.'); ?>
            <?php endif; ?>
        </ul>
        <div class="box_valor_mobile box_top">
          <?php if( !empty($this->dados->valor_venda)): ?>
             <h1 class="valor">Venda: R$ <?php echo number_format($this->dados->valor_venda, 2, ',', '.'); ?></h1>
         <?php endif; ?>
         <?php if( !empty($this->dados->valor_locacao)): ?>
            <h1 class="valor">Locação: R$ <?php echo number_format($this->dados->valor_locacao, 2, ',', '.'); ?></h1>
        <?php endif; ?>
        <?php if( empty($this->dados->valor_venda) || empty($this->dados->valor_locacao)) : ?>
        <br>
    <?php endif; ?>
</div>             
<a href="javascript:window.print()" class="btn_padrao_detalhes bold radius4 opacity">IMPRIMIR FICHA DO IMÓVEL</a>
</div>
</section>



<!-- descrição / Contato -->
<article id="descricao_contato">
    <section id="descricao_imovel">
        <ul id="lista_nav_menu" class="noPrint">
            <li><a href="imoveis/imovelfoto/id/<?php echo $this->dados->codigo; ?>" class="radius4 active">GALERIA DE FOTOS</a></li>
            <li><a href="imoveis/imovelmapa/id/<?php echo $this->dados->codigo; ?>" class="radius4">MAPA DO IMÓVEL</a></li>
            <li><a href="imoveis/imovelvideo/id/<?php echo $this->dados->codigo; ?>" class="radius4">VÍDEO DO IMÓVEL</a></li>
        </ul>
        <h2 class="ttl">Descrição do Imóvel</h2>
        <div class="box_descricao pad marg_bot30">
         <?php echo $this->dados->descricao_web; ?>
     </div>
         <?php if ( $this->caracteristica ) :?>
            <h2 class="ttl dn_especial noPrint">Características </h2>
            <?php if ( $this->caracteristica  ) :?>
                <h3 class="sub_ttl">Características do Imóvel</h3>
                <div class="box_descricao border_bottom">
                    <ul class="lista_descricao pad">
                        <?php foreach ($this->caracteristica as $linha): ?>
                         <?php if($linha->label != 'andar do apto'): ?>
                             <li><?php echo $linha->label ?><?php echo ($linha->valor != 'Sim')?': '.$linha->valor:''; ?></li>
                         <?php endif; ?>
                     <?php endforeach; ?>
                 </ul>
             </div>
         <?php endif;?> 
     <?php if ( $this->caracteristica_cond ) :?>
        <h3 class="sub_ttl">Características do Condomínio</h3>
        <div class="box_descricao marg_bot30">
            <ul class="lista_descricao pad">
               <?php foreach ($this->caracteristica_cond as $linha): ?>
                   <?php if($linha->label !='frente' && $linha->label !='fundos'): ?>
                     <li><?php echo $linha->label ?><?php echo ($linha->valor != 'Sim')?': '.$linha->valor:''; ?></li>
                 <?php endif; ?>
                 <?php if($linha->label == 'frente'): ?>
                     <li><?php echo $linha->label ?><?php echo ($linha->valor)?': '.$linha->valor:''; ?> <span>m²</span></li>
                 <?php endif; ?>
                 <?php if($linha->label == 'fundos'): ?>
                     <li><?php echo $linha->label ?><?php echo ($linha->valor)?': '.$linha->valor:''; ?> <span>m²</span></li>
                 <?php endif; ?>
             <?php endforeach; ?>
         </ul>
     </div>
     <?php endif;?>
<?php endif;?>
<?php if ($this->dados->latitude && $this->dados->longitude ):?>
    <h3 class="ttl noMobile">Mapa do Imóvel</h3>
    <div class="box_mapa noMobile">
      <!--   <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3654.6600287496317!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce42ebeb9d5d15%3A0xd9a5fdb18e908054!2s<?php echo urlencode($this->endereco); ?>!5e0!3m2!1spt-BR!2sbr!4v1426786525209" width="100%" height="420" frameborder="0" style="border:0"></iframe>-->
      <div id="map" class="gmap load-gmap" style="height: 300px">
       <input type="hidden" class="gmaps-cor" value="#000000" />
       <input type="hidden" class="gmaps-km" value="600" />
       <input type="hidden" class="gmaps-icon" value="" />
       <input type="hidden" class="gmaps-zoom" value="15" />
       <ul><li><?php echo $this->dados->latitude; ?>||<?php echo $this->dados->longitude; ?>||circulo||</li></ul>
   </div>
   <div id="gmap-nearby" class="gmap-nearby">
     <h3 class="sub-titulo">Estabelecimentos próximos:</h3>
     <ul class="lista-estabelecimentos">
        <ul>
         <li><a href="#" rel="gym|weights"> <span class="off"></span> Academias</a></li>
         <li><a href="#" rel="bank|bank"><span class="off"></span> Bancos </a></li>
         <li><a href="#" rel="bar|bar"><span class="off"></span> Bares </a></li>
         <li><a href="#" rel="fire_station|firemen"><span class="off"></span> Bombeiros</a></li>
         <li><a href="#" rel="movie_theater|cinema"><span class="off"></span> Cinemas</a></li>
         <li><a href="#" rel="post_office|postal"><span class="off"></span> Correios</a></li>
         <li><a href="#" rel="school|school"><span class="off"></span> Escolas</a></li>
         <li><a href="#" rel="parking|parkinggarage"><span class="off"></span> Estacionamentos</a></li>
         <li><a href="#" rel="pharmacy|drugstore"><span class="off"></span> Farmácias</a></li>
         <li><a href="#" rel="hospital|firstaid"><span class="off"></span> Hospitais</a></li>
         <li><a href="#" rel="hotel|townhouse"><span class="off"></span> Hoteis</a></li>
         <li><a href="#" rel="doctor|medicine"><span class="off"></span> Médicos</a></li>
         <li><a href="#" rel="bakery|bread"><span class="off"></span> Padarias</a></li>
         <li><a href="#" rel="park|ferriswheel"><span class="off"></span> Parques</a></li>
         <li><a href="#" rel="police|police"><span class="off"></span> Polícia</a></li>
         <li><a href="#" rel="gas_station|fillingstation"><span class="off"></span> Postos de Gas.</a></li>
         <li><a href="#" rel="restaurant|restaurant"><span class="off"></span> Restaurantes</a></li>
         <li><a href="#" rel="shopping_mall|conference"><span class="off"></span> Shoppings</a></li>
         <li><a href="#" rel="taxi_stand|taxi"><span class="off"></span> Taxi</a></li>
         <li><a href="#" rel="grocery_or_supermarket|market"><span class="off"></span> Supermercados</a></li>
     </ul>
 </ul>
</div>
</div>
                    <?php /* 
                    <ul class="abas dn_especial noMobile noDesktop noPrint">
                        <li><a id="outro_menu01" href="javascript:trocaOutroConteudo('1')" class="active_aba">Mapa do Imóvel</a></li>
                        <li><a id="outro_menu02" href="javascript:trocaOutroConteudo('2')" class="noPrint noOnlyTablet">Vídeo do Imóvel</a></li>
                    </ul>
                    <div class="box_descricao dn_especial noMobile noDesktop noPrint">
                        <div id="outro_conteudo01" class="box_mapa">
                            <div id="map2" class="gmap load-gmap" style="height: 300px">
                                <input type="hidden" class="gmaps-cor" value="#000000" />
                                <input type="hidden" class="gmaps-km" value="600" />
                                <input type="hidden" class="gmaps-icon" value="" />
                                <input type="hidden" class="gmaps-zoom" value="15" />
                                <ul><li><?php echo $this->dados->latitude; ?>||<?php echo $this->dados->longitude; ?>||circulo||</li></ul>
                            </div>
                           <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3654.6600287496317!2d-46.53928400000002!3d-23.6523433!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce42ebeb9d5d15%3A0xd9a5fdb18e908054!2sR.+Jo%C3%A3o+Fernandes%2C+180+-+Jardim%2C+Santo+Andr%C3%A9+-+SP%2C+09090-740!5e0!3m2!1spt-BR!2sbr!4v1426786525209" width="100%" height="420" frameborder="0" style="border:0"></iframe>-->
                        </div>
                        <div id="outro_conteudo02" class="box_video noPrint"><iframe width="100%" height="420" src="https://www.youtube.com/embed/ZPCseR9kuSg" frameborder="0" allowfullscreen></iframe></div>
                    </div>
                    */ ?>
                    
                    <!-- Video do imóvel -->

                <?php endif; ?>
            </section>
            <section id="contato_imovel">
             <?php if( !empty($this->video_imovel->descricao)):?>
                <div class="box_video_desktop noMobile noOnlyTablet">
                 <?php 

                 if( strpos($this->video_imovel->video, 'embed') )
                 {

                    $link = $this->video_imovel->video;
                }else 
                {
                    $link = 'https://www.youtube.com/embed/'.$this->video_imovel->video;
                }

                ?>
                <iframe width="100%" height="277" src="<?php echo $link; ?>" frameborder="0" allowfullscreen></iframe>        
            </div>
        <?php endif; ?>


        <div class="box_contato_links dn">
            <h1 class="ttl">FALE CONOSCO</h1>
            <div class="box_border">
                <ul id="lista_contato">
                    <li class=""><span class="ico_fone marg_right5">Ligue:</span><strong>(11) 3061-0061</strong></li>
                    <li class=""><span class="ico_whats marg_right5">Whatsapp:</span> <strong>(11) 98833-5090</strong></li>
                    <li class=""><a href="contato/ligamos<?php echo $this->id ? "?ref={$this->id}":NULL;?>" class="ligamos_lightbox opacity"><span class="ico_fone"></span>Ligamos para você</a></li>
                    <li class=""><a href="contato/contatoemail<?php echo $this->id ? "?ref={$this->id}":NULL;?>" class="ligamos_lightbox opacity"><span class="ico_email"></span> Contato por e-mail</a></li>
                    <li class=""><a href="javascript: alert('Em breve!');" class="opacity"><span class="ico_atend"></span> Fale com um <strong>CORRETOR ONLINE</strong></a></li>
                </ul>    
            </div>
        </div>
        <!--forms desktop -->
        <div class="box_form_desk dinh">
            <div class="col_tablet col_mobile">
                <h1 class="ttl">TENHO INTERESSE</h1>
                <form action="contato/enviado" id="proposta" class="forms_detalhes" method="post">
                    <input type="hidden" value="Proposta" name="Assunto" />
                    <input type="hidden" value="<?php echo $this->dados->codigo?>" name="ref" />
                    <p class="texto">Preencha os campos abaixo e envie sua proposta:</p>
                    <div>
                        <label for="nome_prop" class="fl label_detal">Seu nome (*)</label>
                        <input type="text" id="Nome" class="input_detal" name="Nome" title=""  required="required">
                    </div>
                    <div>
                        <label for="emailseu_prop" class="fl label_detal">Seu e-mail (*)</label>
                        <input type="text" id="Email" class="input_detal" name="Email" title=""  required="required">
                    </div>
                    <div>
                        <label for="sua_proposta" class="fl label_detal">Sua proposta</label>
                        <textarea class="fl w100 textarea_detal" name="Proposta" id="sua_proposta" cols="3" rows="1"></textarea>
                    </div>
                    <div class="box_captcha_detal">
                        <div class="g-recaptcha" data-sitekey="6LeSM1sUAAAAAPj0pDQiXjin0d2D83TrvXSFT32W"></div>

                    </div>
                    <div>
                        <span class="txt_campos">*Campos obrigatórios</span>
                        <input type="submit" class="btn_enviar opacity" value="Enviar" name="">
                    </div> 
                </form>
            </div>
            <div class="col_tablet col_mobile">
                <h1 class="ttl">INDIQUE ESTE IMÓVEL</h1>
                <form action="contato/enviado" id="indique" class="forms_detalhes marg_bot30" method="post">
                  <input type="hidden" value="Indicação" name="Assunto" />
                  <input type="hidden" name="Ref" value="<?php echo "http://" . $server . $endereco; ?>">
                  <p class="texto">Indique este imóvel para um amigo ou parente:</p>
                  <div>
                    <label for="nome_indique" class="fl label_detal">Seu nome (*)</label>
                    <input type="text" id="nome_indique" class="input_detal" name="Nome" title="" required="required">
                </div>
                <div>
                    <label for="emailseu_indique" class="fl label_detal">Seu e-mail (*)</label>
                    <input type="text" id="emailseu_indique" class="input_detal" name="Email" title="" required="required">
                </div>
                <div>
                    <label for="email_indique" class="fl label_detal">E-mail Indicação (*)</label>
                    <input type="text" id="email_indique" class="input_detal" name="EmailIndicacao" required="required">
                </div>
                <div class="box_captcha_detal">

                    <div class="g-recaptcha" data-sitekey="6LeSM1sUAAAAAPj0pDQiXjin0d2D83TrvXSFT32W"></div>

                </div>
                <div>
                    <span class="txt_campos">*Campos obrigatórios</span>
                    <input type="submit" class="btn_enviar opacity" value="Enviar" name="">
                </div>    
            </form>
        </div>
    </div>    
</section>
</article>

<?php if ( $this->imoveis_semelhantes ) :?>
    <!-- Semelhantes -->

    <section id="imoveis_semelhantes">
        <div class="center">
            <h1 class="ttl2">IMÓVEIS SEMELHANTES</h1>
            <ul class="lista_imoveis_detal">
             <?php foreach ($this->imoveis_semelhantes as $result) :
               echo $this->partial('imovelLoop.php', array('result' => $result));                   
           endforeach;              
           ?>
       </ul>   
   </div>
</section>
<?php endif;?>
<!-- Últimos -->

<?php if ( $this->imoveis_ultimos ) :?>
    <section id="imoveis_visualizados">
        <div class="center">
            <h1 class="ttl2">ÚLTIMOS VISUALIZADOS</h1>
            <ul class="lista_imoveis_detal">
             <?php foreach ($this->imoveis_ultimos as $result) :
               echo $this->partial('imovelLoop.php', array('result' => $result));                   
           endforeach;              
           ?>
       </ul>   
   </div>
</section>
<?php endif;?>
<a href="javascript:window.history.go(-1)" class="btn_back marg_top30">VOLTAR PARA PÁGINA ANTERIOR</a>
</section>
</main>
<?php echo $this->render("footer.php"); ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCzxktEHYF6usW58y3BbO9yFhpLJyE6z5U&libraries=places"></script>
<script type="text/javascript" src="js/fullbiz.gmaps.js"></script>
<!-- Script troca galeria -->
<script type="text/javascript">
    function trocaGaleria(id){
        switch(id){
            case '1':
            $("#gal2").slideUp('fast');
            $("#gal3").slideUp('fast');
            $("#gal4").slideUp('fast');
            break;
            case '2':
            $("#gal1").slideUp('fast');
            $("#gal3").slideUp('fast');
            $("#gal4").slideUp('fast');
            break;
            case '3':
            $("#gal1").slideUp('fast');
            $("#gal2").slideUp('fast');
            $("#gal4").slideUp('fast');
            break;
            case '4':
            $("#gal1").slideUp('fast');
            $("#gal2").slideUp('fast');
            $("#gal3").slideUp('fast');
            break;
        }    
        $("#gal"+id).slideDown('fast');
    };
</script>
<!-- abas galeria -->
<script type="text/javascript">
    function trocaMenu_ativo(id){
        $("#aba01").removeClass("aba_active");
        $("#aba02").removeClass("aba_active");
        $("#aba03").removeClass("aba_active");
        $("#aba04").removeClass("aba_active");
        //
        $("#aba0"+id).addClass("aba_active");
    }
</script>
<script type="text/javascript">
    trocaOutroConteudo (1);
    function trocaOutroConteudo (id){
        $("#outro_conteudo01").hide();
        $("#outro_conteudo02").hide();
        //
        $("#outro_conteudo0"+id).fadeIn(600);
        //
        $("#outro_menu01").removeClass("active_aba");
        $("#outro_menu02").removeClass("active_aba");
        //
        $("#outro_menu0"+id).addClass("active_aba");
    }
</script>

<!-- Scripts Galeria de Imagens -->    
<script type="text/javascript" src="js/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.wt-lightbox.js"></script>
<script type="text/javascript" src="js/jquery.thumb-scroller.js"></script>    
<script type="text/javascript">
    $(window).load( 
        function() {
            //Scroll Galeria
            $("#myScroller, #myScroller_02, #myScroller_03, #myScroller_04").thumbScroller({
                responsive:true,
                orientation:'horizontal',
                numDisplay:8,
                slideWidth:120,
                slideHeight:70,
                slideMargin:8,
                slideBorder:0,
                padding:0,
                autoPlay:true,
                delay:4000,
                speed:1000,
                easing:'swing',
                control:'false',
                navButtons:'show', 
                playButton:false,
                captionEffect:'slide',
                captionAlign:'bottom',
                captionPosition:'inside',
                captionButton:false,
                captionHeight:'auto',
                continuous:true,
                shuffle:false,
                mousewheel:false,
                imagePosition:'fill',
                pauseOnHover:false,
                pauseOnInteraction:true,
                
            });
            $("#myScroller_tab, #myScroller_tab_02, #myScroller_tab_03, #myScroller_tab_04").thumbScroller({
                responsive:true,
                orientation:'horizontal',
                numDisplay:5,
                slideWidth:120,
                slideHeight:70,
                slideMargin:8,
                slideBorder:0,
                padding:0,
                autoPlay:true,
                delay:4000,
                speed:1000,
                easing:'swing',
                control:'false',
                navButtons:'show', 
                playButton:false,
                captionEffect:'slide',
                captionAlign:'bottom',
                captionPosition:'inside',
                captionButton:false,
                captionHeight:'auto',
                continuous:true,
                shuffle:false,
                mousewheel:false,
                imagePosition:'fill',
                pauseOnHover:false,
                pauseOnInteraction:true,
                
            });
            //Lightbox
            $("#myScroller a[data-lightbox-group='gallery1'], #myScroller_tab a[data-lightbox-group='gallery12'], #myScroller_02 a[data-lightbox-group='gallery2'], #myScroller_tab_02 a[data-lightbox-group='gallery22'], #myScroller_03 a[data-lightbox-group='gallery3'], #myScroller_tab_03 a[data-lightbox-group='gallery32'], #myScroller_04 a[data-lightbox-group='gallery4'], #myScroller_tab_04 a[data-lightbox-group='gallery42']").wtLightBox({
                responsive:true,
                autoPlay:false,
                delay:5000,
                speed:100,
                easing:'swing',
                navButtons:'mouseover',
                playButton:true,
                numberInfo:true,
                timer:true,
                captionPosition:'inside',
                captionButton:false,
                continuous:true,
                mousewheel:false,
                keyboard:true,
                swipe:true
            });
            
            trocaGaleria('1');
        });
    
    jQuery(function(){
       jQuery( '#indique' ).ajaxForm( {
         beforeSend: function() { 
          $('#indique input[type=submit]').attr({'value': 'ENVIANDO...', 'disabled':true});
      },
      success: function( data ) {
       if(data)
       {
           if(parseInt(data.error) > 0 )
           {
            $('#indique input[type=submit]').attr({'value': 'ENVIAR', 'disabled':false});
            alert(data.msg);
        }
        else
        {
            $('#indique input[type=submit]').attr({'value': 'ENVIADO', 'disabled':true});
            window.location = 'contato/emailenviado';
                            //alert(data.msg);                              
                        }
                    }
              }
          } );

   });

    jQuery(function(){
       jQuery( '#proposta' ).ajaxForm( {
         beforeSend: function() { 
          $('#proposta input[type=submit]').attr({'value': 'ENVIANDO...', 'disabled':true});
      },
      success: function( data ) {
       if(data)
       {
           if(parseInt(data.error) > 0 )
           {
            $('#proposta input[type=submit]').attr({'value': 'ENVIAR', 'disabled':false});
            alert(data.msg);
        }
        else
        {
            $('#proposta input[type=submit]').attr({'value': 'ENVIADO', 'disabled':true});
            window.location = 'contato/emailenviado';
                            //alert(data.msg);                              
                        }
                    }
              }
          } );

   });
    
    $('#image_principal').click(function(){
        $('a[data-lightbox-group=gallery1]')[0].click();
    });

</script>

<script type="text/javascript">
  var url_atual = location.href;
  $('.fb-share-button').attr('data-href', url_atual);
</script>