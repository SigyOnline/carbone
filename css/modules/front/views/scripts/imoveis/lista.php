<?php echo $this->render("header.php");
$dado = $this->dado;
	if ($_GET['status'] == 'VENDA')
	{
		$ch = 'checked="checked"';	
	}
	
	if($_GET['status'] == 'ALUGUEL')
	{
		
		$ch1 = 'checked="checked"';
	}
?>
<main class="pr border_top">
		<section class="center">
			<!-- resultado -->
			<article id="box_resultado">
                <!-- box nav -->
                <section id="nav_resultados">
	                <nav class="box_select noDesktop">
                        <div class="fake_select_resultado">
                            <select name="" id="" class="">
                                <option value="VISUALIZAR 12 POR PÁGINA">VISUALIZAR 12 POR PÁGINA</option>
                                <option value="VISUALIZAR 24 POR PÁGINA">VISUALIZAR 24 POR PÁGINA</option>
                                <option value="VISUALIZAR 36 POR PÁGINA">VISUALIZAR 36 POR PÁGINA</option>
                            </select>
                        </div>
                        <div class="fake_select_resultado">
                            <select name="" id="" class="">
                                <option value="ORDERNAR POR">ORDERNAR POR</option>
                                <option value="Menor Valor">Menor Valor</option>
                                <option value="Maior Valor">Maior Valor</option>
                                <option value="Bairro">Bairro</option>
                                <option value="Tipo">Tipo</option>
                                <option value="Metragem">Metragem</option>
                            </select>
                        </div>	
	                </nav>
                    <ul id="nav_ordernar" class="fl noOnlyTablet">
                        <li>ORDENAR POR</li>
                        <li><a href="#" class="opacity active">Menor Valor</a></li>
                        <li><a href="#" class="opacity">Maior Valor</a></li>
                        <li><a href="#" class="opacity">Bairro</a></li>
                        <li><a href="#" class="opacity">Tipo</a></li>
                        <li><a href="#" class="opacity">Metragem</a></li>
                    </ul>
	                <nav class="tipo_visualizacao">
	                	<a href="" class="btn_visualizacao"><span class="ico_box"></span>BOX</a>
                        <a href="" class="btn_visualizacao active"><span class="ico_lista"></span>LISTA</a>
	                </nav>
                </section>
                <!-- titulo -->
                <h1 class="pa noMobile ttl_internas">150 Resultado(s)</h1>
                <!-- no results -->
             	<?php if ( count($dado) == 0 ): ?>
				<section id="no_result">
<!--                     <div class="pa imagem_casal"><img src="src/imagem-casal.png" alt="Imagem referente a não retorno de sua pesquisa" /></div> -->
                    <div class="bg_faixa">
    					<div class="center">
    						<h1 class="bold">SUA PESQUISA NÃO TEVE RESULTADOS.</h1>
    						<p class="">Não encontramos imóveis com as características que você selecionou. <br/>Faça uma nova busca.</p>
    						<div class="comporta_links"><a href="./index.php"class="bold opacity">IR PARA HOME</a><a href="./contato.php"class="bold opacity">FALE CONOSCO </a></div>
    					</div>
                    </div>
				</section>
				
				<?php else:  ?>	
				
				<ul id="lista_box">
					<!-- item -->
					<?php foreach ($dado as $d ): ?>
                    <li class="imovel_destaque">
                        <img style="height:250px;" src="<?php echo (empty($d->foto_destaque))?'/imagens/ind.png':$d->foto_destaque; ?>" alt="Foto do imóvel em destaque" />
                        <div class="referencia_destaque pa">REF.: <?php echo $d->codigo; ?></div>
                        <div class="pos_info">
                            <h1><?php echo $d->bairro; ?> / <?php echo $d->cidade; ?></h1>
                            <h2><?php echo $d->categoria; ?></h2>
                        </div>
                        <div class="infos">
                            <h1 class="local"><?php echo $d->bairro; ?> / <?php echo $d->cidade; ?></h1>
                            <h2 class="tipo"><?php echo $d->categoria; ?></h2>
                            <span class="acabamento"></span>
                            <h2 class="itens"><?php echo $d->dormitorios; ?> Dorm(s)  | <?php echo $d->vagas; ?> Vaga(s) | <?php echo $d->suites; ?> Suíte(s)</h2>
                            <h2 class="valor">R$ <?php echo number_format($d->valor_venda,2,',','.'); ?></h2>
                            <a href="/imoveis/detalhe/id/<?php echo $d->codigo; ?>" class="">CONSULTE</a>
                        </div>    
                    </li>
              		<?php endforeach; ?>      
                    <!-- item -->
				</ul>
				<?php endif?>
				
				<nav id="paginacao">
					<div class="pag_number">
					<?php echo $dado; ?>
                    </div>
				</nav>
				
				<ul id="lista_linha">
					<!-- item -->
                    <li>
                        <div class="box_left">
                        	<a href="./detalhes-do-imovel.php">
                                <img src="src/foto-destaque.jpg" alt="Foto do Imóvel"/>
                                <div class="referencia_destaque pa">REF.: 4555</div>
                                <span class="efeito pa"></span>
                            </a>
						</div>
						<div class="box_right">
                            <h1 class="local">Itaim Bibi / SP</h1>
                            <h2 class="tipo">Apartamento</h2>
                            <span class="acabamento"></span>
                            <h2 class="itens">2 Dorm(s)  | 1 Vaga(s) | 2 Suíte(s)</h2>
                            <h2 class="valor">R$ 250.000.00</h2>
                            <a href="./detalhes-do-imovel.php" class="btn_padrao pa radius4 fnt_center">CONSULTE</a>
                        </div>
                    </li>
                    <!-- item -->
                    <li>
                        <div class="box_left">
                            <a href="./detalhes-do-imovel.php">
                                <img src="src/foto-destaque.jpg" alt="Foto do Imóvel"/>
                                <div class="referencia_destaque pa">REF.: 4555</div>
                                <span class="efeito pa"></span>
                            </a>
                        </div>
                        <div class="box_right">
                            <h1 class="local">Itaim Bibi / SP</h1>
                            <h2 class="tipo">Apartamento</h2>
                            <span class="acabamento"></span>
                            <h2 class="itens">2 Dorm(s)  | 1 Vaga(s) | 2 Suíte(s)</h2>
                            <h2 class="valor">R$ 250.000.00</h2>
                            <a href="./detalhes-do-imovel.php" class="btn_padrao pa radius4 fnt_center">CONSULTE</a>
                        </div>
                    </li>
                    <!-- item -->
                    <li>
                        <div class="box_left">
                            <a href="./detalhes-do-imovel.php">
                                <img src="src/foto-destaque.jpg" alt="Foto do Imóvel"/>
                                <div class="referencia_destaque pa">REF.: 4555</div>
                                <span class="efeito pa"></span>
                            </a>
                        </div>
                        <div class="box_right">
                            <h1 class="local">Itaim Bibi / SP</h1>
                            <h2 class="tipo">Apartamento</h2>
                            <span class="acabamento"></span>
                            <h2 class="itens">2 Dorm(s)  | 1 Vaga(s) | 2 Suíte(s)</h2>
                            <h2 class="valor">R$ 250.000.00</h2>
                            <a href="./detalhes-do-imovel.php" class="btn_padrao pa radius4 fnt_center">CONSULTE</a>
                        </div>
                    </li>
                    <!-- item -->
                    <li>
                        <div class="box_left">
                            <a href="./detalhes-do-imovel.php">
                                <img src="src/foto-destaque.jpg" alt="Foto do Imóvel"/>
                                <div class="referencia_destaque pa">REF.: 4555</div>
                                <span class="efeito pa"></span>
                            </a>
                        </div>
                        <div class="box_right">
                            <h1 class="local">Itaim Bibi / SP</h1>
                            <h2 class="tipo">Apartamento</h2>
                            <span class="acabamento"></span>
                            <h2 class="itens">2 Dorm(s)  | 1 Vaga(s) | 2 Suíte(s)</h2>
                            <h2 class="valor">R$ 250.000.00</h2>
                            <a href="./detalhes-do-imovel.php" class="btn_padrao pa radius4 fnt_center">CONSULTE</a>
                        </div>
                    </li>	
				</ul>
				<nav id="paginacao">
					<div class="pag_number">
						<div class="number_nav active">1</div>
						<div class="number_nav" onClick="atualizaimoveis(2)">2</div>
						<div class="number_nav" onClick="atualizaimoveis(3)">3</div>
						<div class="number_nav" onClick="atualizaimoveis(4)">4</div>
						<div class="number_nav" onClick="atualizaimoveis(5)">5</div>
                    </div>
				</nav>
    		</article>
    		<!-- Filtro -->
    		<aside id="box_filtros">
    		<form action="/imoveis" method="get" name="form1" id="form1">
                <div class="fake_select_resultado">
                    <select name="imopagina" id="imopagina" class="">
                        <option value="12" <?php echo ( $_GET['imopagina'] == "12" ? 'selected="selected"' : " " ); ?>>VISUALIZAR 12 POR PÁGINA</option>
                        <option value="24" <?php echo ( $_GET['imopagina'] == "24" ? 'selected="selected"' : " " ); ?>>VISUALIZAR 24 POR PÁGINA</option>
                        <option value="36" <?php echo ( $_GET['imopagina'] == "36" ? 'selected="selected"' : " " ); ?>>VISUALIZAR 36 POR PÁGINA</option>
                    </select>
                </div>
	            <div id="filtros">  
	                <h2 class="subttl_filtro">Status</h2>
	                <div class="item_filtros">
	                    <input <?php echo $ch; ?> type="radio" id="cod01" class="input_filtro" value="VENDA" name="status"/>
	                    <label for="cod01" class="label_filtro label_radio active">Venda</label>
	                </div>
	                <div class="item_filtros">
	                    <input <?php echo $ch1; ?> type="radio" id="cod03" class="input_filtro" value="ALUGUEL" name="status"/>
	                    <label for="cod03" class="label_filtro label_radio">Locação</label>
	                </div>
	                <h2 class="subttl_filtro">Fase da Obra</h2>
	              	<?php foreach ( $this->fase_obra as $fase ): ?>	
								<?php @$checked5 = (($fase->fase_obra == $_GET['fase_obra']))?'checked="checked"':'';
								 if (!empty($fase->fase_obra))
										{
								 ?>  
                    <div class="item_filtros">
                        <input <?php echo $checked5; ?> type="radio" id="codLançamentos<?php echo $fase->fase_obra; ?>" class="input_filtro" value="<?php echo $fase->fase_obra; ?>" name="fase_obra"/>
                        <label for="codLançamentos<?php echo $fase->fase_obra; ?>" class="label_filtro label_radio"><?php echo $fase->fase_obra; ?></label>
                    </div>
                <?php } endforeach; ?>    

	                <h2 class="subttl_filtro">Tipo de Imóvel</h2>
	             	<?php foreach ( $this->categoria as $cat ): ?>
								<?php @$checkedc = (in_array($cat->id_categoria, $_GET['categoria']))?'checked="checked"':'';
								 if (!empty($cat->id_categoria))
										{
								 ?>
	                <div class="item_filtros">
	                    <input <?php echo $checkedc; ?> name="categoria[]" type="checkbox" id="cod<?php echo $cat->categoria; ?>" class="input_filtro" value="<?php echo $cat->id_categoria; ?>"/>
	                    <label for="cod<?php echo $cat->categoria; ?>" class="label_filtro"><?php echo $cat->categoria; ?></label>
	                </div>
	               <?php } endforeach; ?> 
	               
                    <h2 class="subttl_filtro">Cidade</h2>
                    <select name="cidade" class="select_filtro cidadef">
                        <option value="" selected="selected">Selecione uma cidade</option>
                    <?php foreach ( $this->cidade as $c ): 
											$ci = str_replace('+',' ',$_GET['cidade']);
								?>
								<?php $s = ( $c->cidade == $ci ? 'selected="selected"' : " " ); ?>
                       <option value="<?php echo $c->cidade; ?>" <?php echo $s; ?>><?php echo $c->cidade; ?></option>
					<?php endforeach; ?>	
                    </select>
                    <h2 class="subttl_filtro">Bairro</h2>
                    <div class="item_filtros">
                        <h3 class="defalut_input">Selecione uma cidade</h3>
                    </div>    
                    <div class="box_bairros">
                    <?php foreach ( $this->bairro as $b ): ?>
								<?php @$checked4 = (in_array($b->bairro, $_GET['bairro']))?'checked="checked"':'';
								 if (!empty($b->bairro))
										{
								 ?>
                        <div class="item_filtros">
                            <input <?php echo $checked4; ?> type="checkbox" id="cod<?php echo $b->bairro; ?>" class="input_filtro" value="<?php echo $b->bairro; ?>" name="bairro[]"/>
                            <label for="cod<?php echo $b->bairro; ?>" class="label_filtro"><?php echo $b->bairro; ?></label>
                        </div>
                  <?php } endforeach; ?>       
                        
                    </div>
	                <h2 class="subttl_filtro">Característica do Imóvel</h2>
	                <div class="item_filtros">
	                    <input type="checkbox" id="alarme" class="input_filtro" value=""/>
	                    <label for="alarme" class="label_filtro">Alarme</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="piscina" class="input_filtro" value=""/>
	                    <label for="piscina" class="label_filtro">Piscina</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="vista_mar" class="input_filtro" value=""/>
	                    <label for="vista_mar" class="label_filtro">Vista para o Mar</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="mobiliado" class="input_filtro" value=""/>
	                    <label for="mobiliado" class="label_filtro">Mobiliado</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="tvcabo" class="input_filtro" value=""/>
	                    <label for="tvcabo" class="label_filtro">Tv a cabo</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="ediculo" class="input_filtro" value=""/>
	                    <label for="ediculo" class="label_filtro">Edículo</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="permuta" class="input_filtro" value=""/>
	                    <label for="permuta" class="label_filtro">Aceita Permuta</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="ar_condicionado" class="input_filtro" value=""/>
	                    <label for="ar_condicionado" class="label_filtro">Ar Condicionado</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="doc" class="input_filtro" value=""/>
	                    <label for="doc" class="label_filtro">Documentação Definitiva</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="promocao" class="input_filtro" value=""/>
	                    <label for="promocao" class="label_filtro">Promoção</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="exclusividade" class="input_filtro" value=""/>
	                    <label for="exclusividade" class="label_filtro">Exclusividade</label>
	                </div>
	                
	                <h2 class="subttl_filtro">Dormitórios</h2>
	                <div class="item_filtros">
	                <?php for ($i = 1; $i <= 5; $i++): ?>
								<?php @$checked1 = (in_array($i, $_GET['dormitorios']))?'checked="checked"':'';
								 if (!empty($i))
										{
								 ?>
	                    <input <?php echo $checked1; ?> type="checkbox" id="cod17" class="input_filtro" value="<?php echo $i; ?>" name="dormitorios[]"/>
	                    <label for="cod17" class="label_filtro"><?php echo $i; ?></label>
	                 <?php } endfor; ?>   
	                </div>
	                
	                <h2 class="subttl_filtro">Suítes</h2>
	                <div class="item_filtros">
	                <?php for ($y = 1; $y <= 5; $y++): ?>
								<?php @$checked2 = (in_array($y, $_GET['suites']))?'checked="checked"':'';
								 if (!empty($y))
										{
								 ?>
	                    <input <?php echo $checked2; ?> type="checkbox" id="cod22" class="input_filtro" value="<?php echo $y; ?>" name="suites[]"/>
	                    <label for="cod22" class="label_filtro"><?php echo $y; ?></label>
	                 <?php } endfor; ?>       
	                </div>
	                
	                <h2 class="subttl_filtro">Vagas</h2>
	                <div class="item_filtros">
	                <?php for ($j = 1; $j <= 5; $j++): ?>
								<?php @$checked3 = (in_array($j, $_GET['vagas']))?'checked="checked"':'';
								 if (!empty($j))
										{
								 ?>
	                    <input <?php echo $checked3; ?> type="checkbox" id="cod27" class="input_filtro" value="<?php echo $j; ?>" name="vagas[]"/>
	                    <label for="cod27" class="label_filtro"><?php echo $j; ?></label>
	                <?php } endfor; ?>           
	                </div>
	                
	                <h2 class="subttl_filtro">Metragem</h2>
	                <div class="item_filtros">
	                    <input type="checkbox" id="cod42" class="input_filtro" value="15a25" name="area_privativa[]" <?php echo (@in_array('15a25', $_GET['area_privativa']))?'checked="checked"':'' ?>/>
	                    <label for="cod42" class="label_filtro">15m - 25m</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="cod43" class="input_filtro" value="30a45" name="area_privativa[]" <?php echo (@in_array('30a45', $_GET['area_privativa']))?'checked="checked"':'' ?>/>
	                    <label for="cod43" class="label_filtro">30m - 45m</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="cod44" class="input_filtro" value="50a70" name="area_privativa[]" <?php echo (@in_array('50a70', $_GET['area_privativa']))?'checked="checked"':'' ?>/>
	                    <label for="cod44" class="label_filtro">50m - 70m</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="cod45" class="input_filtro" value="71a90" name="area_privativa[]" <?php echo (@in_array('71a90', $_GET['area_privativa']))?'checked="checked"':'' ?>/>
	                    <label for="cod45" class="label_filtro">71m - 90m</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="cod46" class="input_filtro" value="91a110" name="area_privativa[]" <?php echo (@in_array('91a110', $_GET['area_privativa']))?'checked="checked"':'' ?>/>
	                    <label for="cod46" class="label_filtro">91m - 110m</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="m111" class="input_filtro" value="111a135" name="area_privativa[]" <?php echo (@in_array('111a135', $_GET['area_privativa']))?'checked="checked"':'' ?>/>
	                    <label for="m111" class="label_filtro">111m - 135m</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="m145" class="input_filtro" value="145a160" name="area_privativa[]" <?php echo (@in_array('145a160', $_GET['area_privativa']))?'checked="checked"':'' ?>/>
	                    <label for="m145" class="label_filtro">145m - 160m</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="m165" class="input_filtro" value="165a190" name="area_privativa[]" <?php echo (@in_array('165a190', $_GET['area_privativa']))?'checked="checked"':'' ?>/>
	                    <label for="m165" class="label_filtro">165m - 190m</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="m195" class="input_filtro" value="195a200" name="area_privativa[]" <?php echo (@in_array('195a200', $_GET['area_privativa']))?'checked="checked"':'' ?>/>
	                    <label for="m195" class="label_filtro">195m - 200m</label>
	                </div>
	                <div class="item_filtros">
	                    <input type="checkbox" id="acima200" class="input_filtro" value="200" name="area_privativa[]" <?php echo (@in_array('200', $_GET['area_privativa']))?'checked="checked"':'' ?>/>
	                    <label for="acima200" class="label_filtro">Acima de 200m</label>
	                </div>
	                
                    <h2 class="subttl_filtro">Faixa de Valores (Venda)</h2>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod47" class="input_filtro" value="150000a250000" name="valor_venda[]" <?php echo (@in_array('150000a250000', $_GET['valor_venda']))?'checked="checked"':'' ?>/>
                        <label for="cod47" class="label_filtro">150.000,00 - 250.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod48" class="input_filtro" value="300000a350000" name="valor_venda[]" <?php echo (@in_array('300000a350000', $_GET['valor_venda']))?'checked="checked"':'' ?>/>
                        <label for="cod48" class="label_filtro">300.000,00 - 350.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod49" class="input_filtro" value="400000a450000" name="valor_venda[]" <?php echo (@in_array('400000a450000', $_GET['valor_venda']))?'checked="checked"':'' ?>/>
                        <label for="cod49" class="label_filtro">400.000,00 - 450.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod50" class="input_filtro" value="500000a550000" name="valor_venda[]" <?php echo (@in_array('500000a550000', $_GET['valor_venda']))?'checked="checked"':'' ?>/>
                        <label for="cod50" class="label_filtro">500.000,00 - 550.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod51" class="input_filtro" value="600000a650000" name="valor_venda[]" <?php echo (@in_array('600000a650000', $_GET['valor_venda']))?'checked="checked"':'' ?>/>
                        <label for="cod51" class="label_filtro">600.000,00 - 650.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod52" class="input_filtro" value="700000a750000" name="valor_venda[]" <?php echo (@in_array('700000a750000', $_GET['valor_venda']))?'checked="checked"':'' ?>/>
                        <label for="cod52" class="label_filtro">700.000,00 - 750.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod53" class="input_filtro" value="800000a850000" name="valor_venda[]" <?php echo (@in_array('800000a850000', $_GET['valor_venda']))?'checked="checked"':'' ?>/>
                        <label for="cod53" class="label_filtro">800.000,00 - 850.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod54" class="input_filtro" value="900000a950000" name="valor_venda[]" <?php echo (@in_array('900000a950000', $_GET['valor_venda']))?'checked="checked"':'' ?>/>
                        <label for="cod54" class="label_filtro">900.000,00 - 950.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod55" class="input_filtro" value="1000000a2000000" name="valor_venda[]" <?php echo (@in_array('1000000a2000000', $_GET['valor_venda']))?'checked="checked"':'' ?>/>
                        <label for="cod55" class="label_filtro">1.000.000,00 - 2.000.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod56" class="input_filtro" value="3000000a5000000" name="valor_venda[]" <?php echo (@in_array('3000000a5000000', $_GET['valor_venda']))?'checked="checked"':'' ?>/>
                        <label for="cod56" class="label_filtro">3.000.000,00 - 5.000.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod57" class="input_filtro" value="5000000" name="valor_venda[]" <?php echo (@in_array('5000000', $_GET['valor_venda']))?'checked="checked"':'' ?>/>
                        <label for="cod57" class="label_filtro">Acima de 5.000.000,00</label>
                    </div>
                    
                    <h2 class="subttl_filtro">Faixa de Valores (Locação)</h2>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod58" class="input_filtro" value="1000a2000" name="valor_locacao[]" <?php echo (@in_array('1000a2000', $_GET['valor_locacao']))?'checked="checked"':'' ?>/>
                        <label for="cod58" class="label_filtro">1.000,00 - 2.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod59" class="input_filtro" value="3000a4000" name="valor_locacao[]" <?php echo (@in_array('3000a4000', $_GET['valor_locacao']))?'checked="checked"':'' ?>/>
                        <label for="cod59" class="label_filtro">3.000,00 - 4.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod60" class="input_filtro" value="5000a6000" name="valor_locacao[]" <?php echo (@in_array('5000a6000', $_GET['valor_locacao']))?'checked="checked"':'' ?>/>
                        <label for="cod60" class="label_filtro">5.000,00 - 6.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod61" class="input_filtro" value="7000a8000" name="valor_locacao[]" <?php echo (@in_array('7000a8000', $_GET['valor_locacao']))?'checked="checked"':'' ?>/>
                        <label for="cod61" class="label_filtro">7.000,00 - 8.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod62" class="input_filtro" value="9000a10000" name="valor_locacao[]" <?php echo (@in_array('9000a10000', $_GET['valor_locacao']))?'checked="checked"':'' ?>/>
                        <label for="cod62" class="label_filtro">9.000,00 - 10.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod63" class="input_filtro" value="11000a12000" name="valor_locacao[]" <?php echo (@in_array('11000a12000', $_GET['valor_locacao']))?'checked="checked"':'' ?>/>
                        <label for="cod63" class="label_filtro">11.000,00 - 12.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod64" class="input_filtro" value="13000a14000" name="valor_locacao[]" <?php echo (@in_array('11000a12000', $_GET['valor_locacao']))?'checked="checked"':'' ?>/>
                        <label for="cod64" class="label_filtro">13.000,00 - 14.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod65" class="input_filtro" value="15000a19000" name="valor_locacao[]" <?php echo (@in_array('15000a19000', $_GET['valor_locacao']))?'checked="checked"':'' ?>/>
                        <label for="cod65" class="label_filtro">15.000,00 - 19.000,00</label>
                    </div>
                    <div class="item_filtros">
                        <input type="checkbox" id="cod66" class="input_filtro" value="19000" name="valor_locacao[]" <?php echo (@in_array('19000', $_GET['valor_locacao']))?'checked="checked"':'' ?>/>
                        <label for="cod66" class="label_filtro">Acima de 19.000,00</label>
                    </div>
	            </div>
	            
                <input type="reset" name="" value="LIMPAR FILTROS" class="btn_zerar btn_padrao"/>
    		</aside>
    		</form>		
		</section>
	</main>
<?php echo $this->render("footer.php"); ?>
<script type="text/javascript">
$(function(){
	    $("#form1").on("change", "input:checkbox,input:radio", function(){
	        $("#form1").submit();
	    });

	    $("#imopagina").change(function(){
	        $("#form1").submit();
	    });
	    
	    $(".cidadef").change(function(){
	        $("#form1").submit();
	    });
});

</script>
