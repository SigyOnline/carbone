<?php echo $this->render("header.php"); ?>
<div id="box_ttlMobile">
		<div class="center">
			<h1>FOTOS DO IMÓVEL</h1>
		</div>
	</div>
 
    <!--conteudo-->
	<article id="carousel" class="pad010">
		<div class="extern oh">
    		<ul id="box_carousel_01" class="content-slider">
                <!-- item -->
               <?php foreach ($this->foto_imovel as $foto) : ?> 
                <li class="">
                    <img src="<?php echo $foto['foto']?>" alt="Foto do imóvel em destaque" />
                </li>
                <?php endforeach; ?>
            </ul>
            <ul id="box_carousel_02" class="content-slider dn">
                <!-- item -->
             <?php foreach ($this->foto_empreendimento as $foto) : ?>   
                <li class="">
                    <img src="<?php echo $foto['foto']?>" alt="Foto do imóvel em destaque" />
                </li>
            <?php endforeach; ?>    
            </ul>
            <ul id="box_carousel_03" class="content-slider dn">
                <!-- item -->
            <?php foreach ($this->foto_plantas as $foto) : ?>
                <li class="">
                    <img src="<?php echo $foto['foto']?>" alt="Foto do imóvel em destaque" />
                </li>
            <?php endforeach; ?> 
                <!-- item -->
            </ul>
            <ul id="box_carousel_04" class="content-slider dn">
                <!-- item -->
            <?php foreach ($this->foto_perspectiva as $foto) : ?>      
                <li class="">
                    <img src="<?php echo $foto['foto']?>" alt="Foto do imóvel em destaque" />
                </li>
            <?php endforeach; ?>      
                <!-- item -->
            </ul>
        </div>
        <a href="javascript:window.history.go(-1)" class="btn_back">RETORNAR</a>
    </article>
<?php echo $this->render("footer.php"); ?>
<script src="js/jquery.lightSlider.js"></script>
<script>
	$(document).ready(function() {
    $('#box_carousel_01, #box_carousel_02, #box_carousel_03, #box_carousel_04').lightSlider({
        item:2,
        slideMove:1,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed:600,
        responsive : [
            {
                breakpoint:769,
                settings: {
                    item:2,
                    slideMove:1,
                    slideMargin:6,
                  }
            },
            {
                breakpoint:480,
                settings: {
                    item:1,
                    slideMove:1
                  }
            }
        ]
    });  
  });
</script>
<!-- Script troca galeria -->
<script src="/js/jquery.lightSlider.js"></script>
<script type="text/javascript">
    function trocaGaleria(val){
        switch(val){
            case '1':
                $("#box_carousel_02").slideUp('fast');
                $("#box_carousel_03").slideUp('fast');
                $("#box_carousel_04").slideUp('fast');
            break;
            case '2':
                $("#box_carousel_01").slideUp('fast');
                $("#box_carousel_03").slideUp('fast');
                $("#box_carousel_04").slideUp('fast');
            break;
            case '3':
                $("#box_carousel_01").slideUp('fast');
                $("#box_carousel_02").slideUp('fast');
                $("#box_carousel_04").slideUp('fast');
            break;
            case '4':
                $("#box_carousel_01").slideUp('fast');
                $("#box_carousel_02").slideUp('fast');
                $("#box_carousel_03").slideUp('fast');
            break;
        }    
        $("#box_carousel_0"+val).slideDown('fast');
    };
</script>
