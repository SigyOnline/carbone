<?php echo $this->render("header.php");?>
<main class="pr border_top">
		<section class="center">
			<!-- titulo -->
            <h1 class="pa noMobile ttl_internas">SG8 / Página Não Encontrada!</h1>
			<!-- no results -->
			<section id="no_result">
                <!-- <div class="pa imagem_casal"><img src="/src/imagem-casal.png" alt="Imagem referente a não retorno de sua pesquisa" /></div> -->
                <div class="bg_faixa">
                    <div class="center">
                        <h1 class="bold">DESCULPE, A PÁGINA NÃO EXISTE OU FOI REMOVIDA.</h1>
                        <p class="">Não conseguimos localizar a página pesquisada. Verifique se não digitou o endereço incorretamente selecione uma opção:</p>
                        <div class="comporta_links"><a href="/"class="bold opacity">IR PARA HOME</a><a href="/contato"class="bold opacity">FALE CONOSCO </a></div>
                    </div>
                </div>
            </section>	
		</section>
	</main>
<?php echo $this->render("footer.php");?>	