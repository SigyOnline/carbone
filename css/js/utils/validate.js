var Validate,
  bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

Validate = (function() {
  var checkbox, cnpj, cpf, date, of_age, radio, select, tinymce;

  function Validate() {
    this.validate_regex = bind(this.validate_regex, this);
    this.error_message = bind(this.error_message, this);
    this.clear_error = bind(this.clear_error, this);
    this.email = bind(this.email, this);
    this.text = bind(this.text, this);
    this.checkbox = bind(this.checkbox, this);
    this.message = [];
  }

  Validate.prototype.text = function(element, regex, age) {
    var value;
    this.clear_error(element);
    value = $(element).val();
    if (value === "" || (value == null)) {
      this.error_message(element, 'deve ser preenchido');
    }
    if (regex != null) {
      this.validate_regex(element, regex, value);
    }
    if (age != null) {
      return of_age(element, value, age);
    }
  };

  Validate.prototype.email = function(element) {
    return this.text(element, /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i);
  };

  cpf = function(element) {
    return Validate.text(element, /^\d{3}.\d{3}.\d{3}\-\d{2}$/);
  };

  cnpj = function(element) {
    return Validate.text(element, /^\d{2}.\d{3}.\d{3}\/\d{4}-\d{2}$/);
  };

  date = function(element, age) {
    return Validate.text(element, /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/, age);
  };

  of_age = function(element, value, age) {
    var birth, birthday, date_current;
    date = value.split("/");
    value = date[1] + "/" + date[0] + "/" + date[2];
    date_current = new Date();
    birth = new Date(value);
    birthday = Math.floor(Math.ceil(Math.abs(birth.getTime() - date_current.getTime()) / (1000 * 3600 * 24)) / 365.25);
    if (birthday < age) {
      return Validate.error_message(element, 'menor de idade');
    }
  };

  tinymce = function(element) {
    var content, id;
    content = tinymce.get(element).getContent({
      format: 'text'
    });
    if ($.trim(content) === '') {
      id = "#" + $('#' + element).prev('div').attr('id');
      return Validate.error_message(id, 'deve ser preenchido');
    }
  };

  select = function(element, regex) {
    var value;
    value = $(element + " option:selected").val();
    if (value === "" || (value == null)) {
      Validate.error_message(element, 'deve ser selecionado');
    }
    if (regex != null) {
      return Validate.validate_regex(element, regex, value);
    }
  };

  Validate.prototype.checkbox = function(element) {
    var value;
    value = $(element + " option:checked").val();
    if (value === "" || (value == null)) {
      Validate.error_message(element, 'deve ser marcado');
    }
//    if (regex != null) {
//      return Validate.validate_regex(element, regex, value);
//    }	  
  };

  radio = function(element) {};

  Validate.prototype.clear_error = function(element) {
    $(element).removeClass("error");
    return $("label.error").remove();
  };

  Validate.prototype.error_message = function(element, message) {
    var error_label;
    $(element).addClass('error');
    error_label = $("<label for='" + (element.split('#').pop()) + "' class='error'>").text(message);
    $(element).parent().append(error_label);
    return this.message.push({
      element: element,
      message: message
    });
  };

  Validate.prototype.validate_regex = function(element, regex, value) {
    if (!regex.test(value)) {
      return this.error_message(element, 'está em um formato inválido');
    }
  };

  return Validate;

})();
