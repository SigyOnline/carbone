﻿function initialize(lati, longi) {
    var latitude = lati;
    var longitude = longi;
    
    var latlng = new google.maps.LatLng(latitude, longitude);
    var myOptions = {
      zoom: 15,
	  scrollwheel: false,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("maps"), myOptions);
  
	// INSERINDO UM MARCADOR SIMPLES
	var marker = new google.maps.Marker({
	position: new google.maps.LatLng(latitude, longitude),
	map: map
	//,title: 'MARCADOR'        // Inserção de texto ao POINT
	,icon: '/js/google_maps/pointer.png'        // Para alterar a imagem de POINT do mapa
});
  
  // Função do API para criação de um circulo no mapa indicando a area do ponto que no nosso caso é oculto (pointer.png)
  var circle = new google.maps.Circle({
           center: new google.maps.LatLng(latitude, longitude),
           fillColor:"#255A6C",
           fillOpacity: 0.3,
           strokeColor:"#112935",
           strokeOpacity: 0.3,
           strokeWeight: 5,
           zIndex: 5,
           radius: 800 // Diametro do circulo em M
        });
	circle.setMap(map);

  /*
  // INSERINDO JANELA DE INFORMAÇÃO
  var infowindow = new google.maps.InfoWindow({
    content: 'O imóvel esta nessa região'
  });
  
  google.maps.event.addListener(marker, 'click', function(){
    // Janela de informação com HTML
    infowindow.open(map, marker);
  })
  */
}

// Mais funções adicionais do API da GOOGLE MAPS em: http://www.pjtecnologia.com.br/maps/author/maps/