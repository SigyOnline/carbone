<?php 
/* garante que será executado sem time out */
set_time_limit(0);

/* metodo usado durante o desenvolvimento */
function dump($array) {
    /* TESTE */
    echo "<pre>";
    print_r($array);
    echo "</pre>";
}

/**
 * Metodo Importador via RESTFull da VistaSoft
 * 
 * ETAPAS
 * 
 * Luiz Bueno - 29/03/2018
 ***************************************************************/


/**
 * Metodo para instaciar a matriz que vai sincronizar
 * os campos da tabela imovel do VistaSoft com 
 * os campos da tabela sg_imoveis da Sigy com o seguinte critério:
 * o indice do array e o campo do VistaSoft e 
 * o valor é o campo da tabela sg_imoveis 
 * caso não exista o valor significa que na tabela do cliente
 * essa informação não será tratada ou não tem relevância no
 * site do cliente Sigy/Vista
 * Luiz Bueno - 30/03/2018
 **************************************************************/
function inicializaArrayImoveis() {
    return array(
'Codigo'=>'codigo',
'DataCadastro'=>'data_cadastro',
'Endereco'=>'endereco',
'Bairro'=>'bairro_comercial',
'Cidade'=>'cidade',
'UF'=>'uf',
'CEP'=>'cep',
'Numero'=>'numero',
'Empreendimento'=>'empreendimento',
'Referencia'=>'referencia',
'Dormitorios'=>'dormitorios',
'Suites'=>'suites',
'ValorCondominio'=>'valor_condominio',
'ValorLocacao'=>'valor_locacao',
'ValorVenda'=>'valor_venda',
'Exclusivo'=>'exclusivo',
'Observacoes'=>'observacoes',
'Lancamento'=>'lancamento',
'ValorIptu'=>'valor_iptu',
'PosicaoAndar'=>'posicao_andar',
'Complemento'=>'complemento',
'Situacao'  => 'situacao',
'Bloco'=>'bloco',
'DataAtualizacao'=>'data_atualizacao',
'TipoImovel'=>'tipo_imovel',
'LocacaoAnual'=>'locacao_anual',
'LocacaoTemporada'=>'locacao_temporada',
'Venda'=>'venda',
'ExibirNoSite'=>'exibir_no_site',
'AptosEdificio'=>'aptos_edificio',
'AptosAndar'=>'aptos_andar',
'GaragemTipo'=>'garagem_tipo',
'Vagas'=>'vagas',
'AreaTotal'=>'area_total',
'AreaTerreno'=>'area_terreno',
'Status'=>'status',
'TituloSite'=>'titulo_site',
'Incompleto'=>'incompleto',
'EmDestaque'=>'em_destaque',
'AreaPrivativa' => 'area_util',
'DataEngrega'=>'data_entrega',
'EdificioResidencial'=>'edificio_residencial',
'TipoEndereco'=>'tipo_endereco',
'Finalidade'=>'finalidade',
'AreaConstruida'=>'area_construida',
'Salas'=>'salas',
'BairroComercial'=>'bairro',
'TipoOferta'=>'tipo_oferta',
'Categoria'=>'categoria',
'FotoDestaque'=>'foto_destaque',
'FotoDestaquePequena'=>'foto_destaque_pequena',
'Latitude'=>'latitude',
'Longitude'=>'longitude',
'Descricao'=>'descricao',
'DataHoraAtualizacao'=>'data_hora_atualizacao',
'DescricaoWeb' => 'descricao_web',
'Zona' => 'zona',
'AreaSite' => 'area_site',
'AceitaPermuta' => 'aceita_permuta',
'ImovelcomRenda' => 'imovelcom_renda',
'AdministradoraCondominio'  => 'administradora_condominio',      
'AnoConstrucao'  => 'ano_construcao', 
'AreaBoxPrivativa'  => 'area_box_privativa',        
'AreaBoxTotal'  => 'area_box_total',        
'BanheiroSocialQtd'  => 'banheiro_social_qtd',         
'Chave'  => 'chave',         
'ChaveNaAgencia'  => 'chave_na_agencia',         
'ClasseDoImovel'  => 'classe_do_imovel',         
'CodigoMalote'  => 'codigo_malote', 
'Construtora'  => 'construtora',         
'CorretorPrimeiroAge'   => 'corretor_primeiro_age',        
'DataDisponibilizacao'  => 'data_disponibilizacao',         
'DataEntrega'  => 'data_entrega',         
'DataFimAutorizacao'  => 'data_fim_autorizacao',         
'DataInicioAutorizacao'  => 'data_inicio_autorizacao',         
'DataLancamento'  => 'data_lancamento',         
'DataPlaca'  => 'data_placa',         
'DependenciadeEmpregada'  => 'dependenciade_empregada', 
'DimensoesTerreno'  => 'dimensoes_terreno',         
'Edificio'  => 'edificio',        
'EEmpreendimento'  => 'e_empreendimento',         
'EmitiuNotaFiscalComissao'  => 'emitiu_nota_fiscal_comissao',         
'EmpOrulo'  => 'emp_orulo',         
'ExclusivoCorretor'  => 'exclusivo_corretor',         
'ExibirComentarios'  => 'exibir_comentarios', 
'Garagem'  => 'garagem',         
'GaragemCoberta'  => 'garagem_coberta',         
'GaragemNumeroBox'  => 'garagem_numero_box',         
'Imediacoes'  => 'imediacoes',         
'Incorporadora'  => 'incorporadora',         
'InformacaoVenda'  => 'informacao_venda',         
'InicioObra'  => 'inicio_obra',         
'InscricaoMunicipal'  => 'inscricao_municipal',         
'Limpeza'  => 'limpeza',         
'LojasEdificio'  => 'lojas_edificio',         
'Lote'  => 'lote',         
'Matricula'  => 'matricula',  
'MercadoLivreTipoML'  => 'mercado_livre_tipo_m_l',         
'MesConstrucao'  => 'mes_construcao',         
'Midia'  => 'midia',         
'ObsLocacao'  => 'obs_locacao',         
'ObsVenda'  => 'obs_venda',         
'Ocupacao'  => 'ocupacao',         
'OLXFinalidadesPublicadas'  => 'o_l_x_finalidades_publicadas',         
'OnibusProximo'  => 'onibus_proximo',         
'Orientacao'  => 'orientacao',         
'Orulo'  => 'orulo', 
'PadraoConstrucao'  => 'padrao_construcao',         
'Pais'  => 'pais',         
'Pavimentos'  => 'pavimentos',         
'PercentualComissao'  => 'percentual_comissao',         
'Plantao'  => 'plantao',         
'PlantaoNoLocal'  => 'plantao_no_local',         
'PorteEstrutural'  => 'porte_estrutural',         
'PrazoDesocupacao'  => 'prazo_desocupacao',         
'Prestacao'  => 'prestacao', 
'ProjetoArquitetonico'  => 'projeto_arquitetonico',         
'Proposta'  => 'proposta',         
'PropostaLocacao'  => 'proposta_locacao',        
'Reajuste'  => 'reajuste',         
'Referencia'  => 'referencia',         
'Reformado'  => 'reformado',         
'Regiao'  => 'regiao',         
'ResponsavelReserva'  => 'responsavel_reserva',         
'SaldoDivida'  => 'saldo_divida',         
'Setor'  => 'setor',         
'SummerSale'  => 'summer_sale',         
'TemPlaca'   => 'tem_placa',        
'TextoAnuncio'  => 'texto_anuncio',         
'TotalComissao'  => 'total_comissao',         
'ValorACombinar'  => 'valor_a_combinar',         
'ValorComissao'  => 'valor_comissao', 
'ValorDiaria'  => 'valor_diaria',  
'ValorLimpeza'  => 'valor_limpeza',  
'ValorLivreProprietario'  => 'valor_livre_proprietario',  
'ValorLocacao'  => 'valor_locacao',  
'ValorM2'  => 'valor_m2',  
'Visita'  => 'visita',  
'VisitaAcompanhada'  => 'visita_acompanhada',  
'VivaRealDestaqueVivaReal'  => 'viva_real_destaque_viva_real',  
'WebEscritoriosDestaque'  => 'web_escritorios_destaque',  
'ZeladorNome'  => 'zelador_nome',  
'ZeladorTelefone'  => 'zelador_telefone',  
'CategoriaImovel'  => 'categoria_imovel',  
'CategoriaGrupo'  => 'categoria_grupo',  
'ImoCodigo'  => 'imo_codigo',  
'Moeda'  => 'moeda',  
'MoedaIndice'  => 'moeda_indice',  
'CodigoEmpresa'  => 'codigo_empresa',  
'CodigoEmp'  => 'codigo_emp',  
'CodigoEmpreendimento'  => 'codigo_empreendimento',  
'CodigoCategoria'  => 'codigo_categoria',  
'CodigoMoeda'  => 'codigo_moeda',  
'CodigoProprietario'  => 'codigo_proprietario',  
'Proprietario'  => 'proprietario',  
'FotoProprietario'  => 'foto_proprietario',  
'VideoDestaque'  => 'video_destaque',  
'VideoDestaqueTipo'  => 'video_destaque_tipo',  
'genciador'  => 'agenciador',  
'CodigoCorretor'  => 'codigo_corretor',  
'CodigoAgencia'  => 'codigo_agencia',  
'TotalBanheiros'  => 'total_banheiros'       
        
        );
}

/**
 * Normaliza o campo 
 * Com as seguintes regras:
 * 1 - Se Não for um objeto ou array.. retorna 
 *     o nome do campo da tabela sg_imoveis ou
 *     vazio se não for para importar
 * 2 - Se for um array.. retorno o array normatizado
 * Luiz Bueno - 31/03/2017
 ********************************************************/
function normalizeCampos($campo, $valor, $colImoveis) {
   /* inicializa o campo retorno */
    $campoRet = "";
    /**
     * SE NÃO É UM OBJETO NEM ARRAY - é um campo da tabela imoveis
     ************************************************************/
    if(!is_object($valor) && !is_array($valor)) {
        /* e.. ele tem um equivalência na tabela sg_imoveis */
        if(isset($colImoveis[$campo]) && !empty($colImoveis[$campo])) {
           /* então o campo retorno é o campo da tabela sg_imoveis */
           return $colImoveis[$campo];
       }
    /* se for um objeto */    
    } else {
        /* inicializa o array campos */
        $campos = array();
        /* varre o objeto */
        foreach ($valor as $chave => $conteudo) {
            /* estamos lidando com campo fotos */
            if (is_object($conteudo) || is_array($conteudo)) {
                $fotos = array();
                /* normaliza os nomes dos campos de cada foto */
                foreach ($conteudo as $nomeCampo => $conteudoCampo) {
                    $fotos[strtolower(camelCaseToSeparator($nomeCampo,"_"))] = $conteudoCampo;
                }
                /* troca o metodo camelCase por underline no nome do campo */
                $campos[strtolower(camelCaseToSeparator($chave,"_"))] = $fotos;
            /* se não.. é caracteristicas ou infra */    
            } else {
                /* troca o metodo camelCase por underline no nome do campo */
                $campos[strtolower(camelCaseToSeparator($chave,"_"))] = $conteudo;
            }
        }
        /* remonta  */
        return $campos;
    }
    /* chegou aqui.. campo não é para importar..retorna vazio */
    return "";
}



/* converte ou não para utfo */
function is_utf8($var) {
    /* testa para ver se tem código utf8 */
    $ok = mb_detect_encoding($var, 'UTF-8', true);
    /* se tiver.. */
    if ($ok) {
        /* decodifica */
        $var = utf8_decode($var);
        /* retorna decodificada */
        return $var;
    }
    /* chegou aqui.. */

    $ok = preg_match('%(?:
    [\xC2-\xDF][\x80-\xBF]              # non-overlong 2-byte
    |\xE0[\xA0-\xBF][\x80-\xBF]         # excluding overlongs
    |[\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
    |\xED[\x80-\x9F][\x80-\xBF]         # excluding surrogates
    |\xF0[\x90-\xBF][\x80-\xBF]{2}      # planes 1-3
    |[\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
    |\xF4[\x80-\x8F][\x80-\xBF]{2}      # plane 16
    )+%xs', $var);

    if ($ok) {
        $var = utf8_decode($var);
        return $var;
    }

    return $var;
}

/**
 * Metodo que recebe uma string em camelCase e 
 * separa com espaco entre os casos
 * @param $string - frase camelCase
 *        $separator - separador (default (espaço))
 **********************************************************/
function camelCaseToSeparator($string, $separator  = '_') {
    /* replace com expressão regular */
    return preg_replace('/(?<=[a-z])([A-Z])/', $separator . '$1', $string);
}


/**
 * Metodo para pegar a data/hora do banco de
 * dados (mais precisa)
 * caso não consiga retorna a do php mesmo
 * Luiz Bueno - 22/03/2018
 ***********************************************/
function getAgora() {
    /* inicializa retorno */
    $retorno = "";
    /* conecta o banco */
    $conn = conectaBanco();
        /* se consegui conectar */
        if ($conn) {
        /* monta a sql */
        $rs = mysqli_query($conn, "SELECT NOW() AS agora");
        /* se executou e tem o registro */
        if ($rs && mysqli_num_rows($rs) > 0) {
            /* pega o que veio */
            $row = mysqli_fetch_array($rs, MYSQLI_ASSOC);
            /* retorna o id */
            $retorno = $row['agora'];
        /* não encontrou. */    
        } else {
            /* retorna data hora php */
            $retorno = date('Y-m-d H:i:s');
        }
        /* fecha a conexão */
        mysqli_close($conn);
    /* se não */    
    } else {
        /* retorna data hora php */
        $retorno = date('Y-m-d H:i:s');
    }
    /* retorna o horário */
    return $retorno;

}


/**
 * Salva a categoria
 *
 * @return Id categoria ou false
 *******************************************/
function gravaCategoria($categoria) {

    /* inicializa variavel */
    $retorno = "";
    /* conecta o banco */
    $conn = conectaBanco();
    /* se consegui conectar.. */
    if($conn) {
        /* ajusta caracter */
        $categoria = utf8_decode($categoria);
        /* monta a sql */
        $sql = "select id_categoria from sg_categorias_temp where categoria = '$categoria'";
        /* monta a sql */
        $rs = mysqli_query($conn, $sql);
        /* se executou e tem o registro */
        if ($rs && mysqli_num_rows($rs) > 0) {
            /* pega o que veio */
            $row = mysqli_fetch_array($rs, MYSQLI_ASSOC);
            /* retorna o id */
            $retorno = $row['id_categoria'];
        /* não encontrou. */    
        } else {
            /* monta a sql */
            $sql = "insert into sg_categorias_temp set categoria = '$categoria'";
            /* monta a sql */
            $rs = mysqli_query($conn, $sql);
            /* se executou e tem o registro */
            if ($rs) {
                /* retorna o ultimo id */
                $retorno = mysqli_insert_id($conn);
            /* xi.. não deu certo */    
            } else {
                /* retorna falso */
                $retorno = false;
            }    
        }
        /* fecha o banco */
        mysqli_close($conn);
    /* se não */    
    } else {
        /* ecoa o erro */
        echo "Erro Gravando categoria : $categoria <br/>";
    }
    /* retorna o id ou false */
    return $retorno;
}

/**
 * Metodo que varre um array com as urls com chamada
 * curls para o REst vista para pegar todas informações
 * referente ao imovel passado pelo get
 * RETORNA erro ou um array com os dados dos imoveis
 * Luiz Bueno - 29/03/2018
 *********************************************************/
function pegaDadosImoveis($urls) {

    $imoveis = array();
    /* varre todo o array urls */
    foreach ($urls as $key => $value) {
        /* inicializa contador de tentativas */
        $tentativas = 0;
tentaDeNovo:
        /* instancia o curl */
        $ch = curl_init();
        /* set com o array de opões */
        curl_setopt_array($ch, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $value,
                CURLOPT_HTTPHEADER => array('Accept: application/json')
        ));
        /* executa e pega o retorno */
        $ret =  curl_exec($ch);
        /* se veio algum erro */
        if(curl_errno($ch)) {
            $tentativas++;
            if ($tentativas < 10) {
                /* retorna o erro */
                echo "erro Erro Executando pegaDadosImoveis " . curl_error($ch) . "<br/>";
                curl_close($ch);
                /* tenta de novo */
                goto tentaDeNovo;
            } else {
                /* encerra pois teve muitos problemas */
                return array("erro" => "Erro Executando pegaDadosImoveis " . curl_error($ch) . " depois de 10 tentativas <br/>");
            }

            /* se veio o status 200 encerra o loop */
            if (isset($ret->status) && ($ret->status == 200 || $ret->status == 200)) {
                $tentativas++;
                if ($tentativas < 10) {
                    /* retorna o erro */
                    echo "erro: STATUS 200/400 REST FULL <br/>";
                    curl_close($ch);
                    /* tenta de novo */
                    goto tentaDeNovo;
                } else {
                    /* encerra pois teve muitos problemas */
                    return array("erro" => "STATUS 200/400 REST FULL depois de 10 tentativas <br/>");
                }
            }
        }
        /* ecerra o curl */
        curl_close($ch);
        /* encerra o curls */
        /* pega a resposta decodificando o retorno json */
        $resp = json_decode($ret);
        /* se o retorno for status 400 */
        if (isset($resp->status) && $resp->status == 400) {
            /* retorna o erro */
            return array("erro" => "Status 400 Vista - Informe Suporte");
            /* aborta - só garantia */
            break;    
        }
        /* inclui em imoveis */
        $imoveis[] = $resp;
    }
    /*  chegou aqui.. retorna array com os imóveis */
    return $imoveis;
}


/**
 * destroi as tabelas temporárias
 * 
 * @return void
 *****************************************************/
function destroiTabelasTemporarias() {
    /* conecta o banco */
    $conn = conectaBanco();
    /* incondicionalmente tenta destruir as tabelas temporárias */
    $sql = "DROP TABLE sg_imoveis_temp";
    @mysqli_query($conn, $sql);
    $sql = "DROP TABLE sg_categorias_temp";
    @mysqli_query($conn, $sql);
    $sql = "DROP TABLE sg_caracteristica_imovel_temp";
    @mysqli_query($conn, $sql);
    $sql = "DROP TABLE sg_fotos_temp";
    @mysqli_query($conn, $sql);
    $sql = "DROP TABLE sg_imovel_videos_temp";
    @mysqli_query($conn, $sql);
    /* fecha a conexão */
    mysqli_close($conn);
}


/**
 * Conecta ao banco e cria a tabela temporária
 * 
 * @return conexão ou false se falhou
 *****************************************************/
function conectaBanco() {
    /* tenta estabelecer a conexão - inibe erro */
   $conn = @mysqli_connect('localhost', 'root', '','carbone');
    //$conn = @mysqli_connect('carbone_site.mysql.dbaas.com.br', 'carbone_site', 'C@rbone2018#','carbone_site');
    // /* BANCO OFICIAL */
    

    /* se ocorreu algum erro */
    if (mysqli_connect_errno()) {
        /* informa o usário */
        echo("Falha na conexão: " . mysqli_connect_error()) . "<br/>";
        /* retorna false */
        return false;
    }
    /* chegou aqui... retorna conexão */
    return $conn;
} 

/**
 * Le a tabela sg_imoveis e pega a ultima
 * data de importação
 *
 * NOTA: Pega da tabela sg_imoveis e não
 *       da tabela sg_imoveis_temp, pois
 *       ela ainda não foi criada e essa
 *       informação será a regra para 
 *       criar a tabela temporária
 *
 * return Ultima Data ou "";
 * Luiz Bueno - 02/04/2018
 **********************************************/
function pegaUltimaDataImportacao() {
    /* conecta o banco */
    $conn = conectaBanco();
    /* inicializa retorno */
    $retorno = "";
    /* monta a sql */
    $sql = "SELECT data_importacao FROM sg_imoveis ORDER BY data_importacao DESC limit 1";  
    /* executq */
    $rs = mysqli_query($conn, $sql);
    /* se executou e tem o registro */
    if ($rs && mysqli_num_rows($rs) > 0) {
        /* pega o que veio */
        $row = mysqli_fetch_array($rs, MYSQLI_ASSOC);
        /* retorna o id */
        $retorno = $row['data_importacao'];
    /* não encontrou. */    
    } else {
        /* retorna vazio */
        $retorno = "";
    }
    /* fecha a conexão */
    mysqli_close($conn);
    /* retorna o horário */
    return $retorno;
}


/**
 * metodo que trocas as tabela de importação
 * @return true se tudo ok, ou false se falhou
 ******************************************************/
function trocaTabelasImportacao() {
    /* conecta o banco */
    $conn = conectaBanco();

    $sql = "
        DROP TABLE IF EXISTS sg_imoveis_backup,
                             sg_categorias_backup,
                             sg_caracteristica_imovel_backup,
                             sg_imovel_videos_backup,
                             sg_fotos_backup;   
    ";
    /* tenta trocar a tabela temporária e se não conseguir  */
    if(!mysqli_query($conn, $sql)) {
        /* informa o erro.. */ 
        echo "Erro na tentativa de trocar tabela de importacao " . mysqli_error($conn) . "<br/>";
        /* retorna falso */
        return false;
    }

    /* passou.. então renomeia */
    $sql = "
        RENAME TABLE sg_imoveis                     TO sg_imoveis_backup,
                     sg_imoveis_temp                TO sg_imoveis,
                     sg_categorias                  TO sg_categorias_backup,
                     sg_categorias_temp             TO sg_categorias,
                     sg_caracteristica_imovel       TO sg_caracteristica_imovel_backup,
                     sg_caracteristica_imovel_temp  TO sg_caracteristica_imovel,
                     sg_imovel_videos               TO sg_imovel_videos_backup,
                     sg_imovel_videos_temp          TO sg_imovel_videos,
                     sg_fotos                       TO sg_fotos_backup,
                     sg_fotos_temp                  TO sg_fotos;
    ";

    /* tenta trocar a tabela temporária e se não conseguir  */
    if(!mysqli_query($conn, $sql)) {
        /* informa o erro.. */ 
        echo "Erro na tentativa de trocar tabela de importacao " . mysqli_error($conn) . "<br/>";
        /* retorna falso */
        return false;
    }
    /* fecha o banco */
    mysqli_close($conn);
    /* chegou aqui... tudo ok */
    return "";
}

/**
 * metodo que cria as tabelas temporária
 * seguindo os seguintes critérios
 * Se $tudo = true (não default) Cria a tabela vazia
 * Se $tudo = false (default) Cria uma cópia
 * @return true se tudo ok, ou o erro..
 ******************************************************/
function criaTabelasTemporias($tudo = false) {
    /* array para receber as sqls  */
    $sqlArray = array();
    /* incializa retorno */
    $retorno = "";
    /* andamento */
    echo "Criação da tabela - antes da destruição " . getAgora() . "<br/>";
    /* destroi incondicionalmente tabelas temporárias */
    destroiTabelasTemporarias();
    /* andamento */
    echo "Criação da tabela - depois da destruição " . getAgora() . "<br/>";
    /* se for do zero */
    if ($tudo) {
        /* cria as tabelas zeradas */
        $sqlArray = array(
                        "CREATE TABLE sg_imoveis_temp LIKE sg_imoveis",
                        "CREATE TABLE sg_categorias_temp LIKE sg_categorias",
                        "CREATE TABLE sg_caracteristica_imovel_temp LIKE sg_caracteristica_imovel",
                        "CREATE TABLE sg_fotos_temp LIKE sg_fotos",
                        "CREATE TABLE sg_imovel_videos_temp LIKE sg_imovel_videos"
                    );
    /* se não */    
    } else {
        /* cria uma cópia */
        $sqlArray = array(
                        "CREATE TABLE sg_imoveis_temp SELECT * FROM sg_imoveis",
                        "ALTER TABLE sg_imoveis_temp ADD PRIMARY KEY (codigo)",
                        "CREATE TABLE sg_categorias_temp SELECT * FROM sg_categorias",
                        "ALTER TABLE sg_categorias_temp ADD PRIMARY KEY (id_categoria)",
                        "ALTER TABLE sg_categorias_temp CHANGE COLUMN id_categoria id_categoria SMALLINT(3) NOT NULL AUTO_INCREMENT FIRST",
                        "CREATE TABLE sg_caracteristica_imovel_temp SELECT * FROM sg_caracteristica_imovel",
                        "ALTER TABLE sg_caracteristica_imovel_temp ADD PRIMARY KEY (id_caracteristica_imovel), ADD INDEX id_tipo_caracteristica_imovel (id_tipo_caracteristica_imovel)",
                        "ALTER TABLE sg_caracteristica_imovel_temp CHANGE COLUMN id_caracteristica_imovel id_caracteristica_imovel INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificação da Caracteristica' FIRST",
                        "CREATE TABLE sg_fotos_temp SELECT * FROM sg_fotos",
                        "ALTER TABLE sg_fotos_temp ADD PRIMARY KEY (id_foto), ADD INDEX id_imovel (id_imovel)",
                        "ALTER TABLE sg_fotos_temp CHANGE COLUMN id_foto id_foto INT(11) NOT NULL AUTO_INCREMENT FIRST",
                        "CREATE TABLE sg_imovel_videos_temp SELECT * FROM sg_imovel_videos",
                        "ALTER TABLE sg_imovel_videos_temp ADD PRIMARY KEY (id_imovel_videos), ADD INDEX id_imovel (id_imovel)",
                        "ALTER TABLE sg_imovel_videos_temp CHANGE COLUMN id_imovel_videos id_imovel_videos INT(11) NOT NULL AUTO_INCREMENT FIRST"
                    );

    }
    /* varre todo array */
    foreach ($sqlArray as $key => $value) {
        /* conecta o banco */
        $conn = conectaBanco();
        /* tenta criar a tabela temporária e se não conseguir  */
        if(!mysqli_query($conn, $value)) {
            /* informa o erro.. */ 
            $retorno.= "Erro na tentativa de criar as tabela temporárias " . mysqli_error($conn) . "<br/>";
        }
        /* fecha o banco */
        mysqli_close($conn);
    }
    /* andamento */
    echo "termino da criação das tabela de apoio " . getAgora() . "<br/>";
    /* chegou aqui... retorna vazio ou erro */
    return $retorno;
}

/**
 * Executa esclusão das tabelas 
 * sg_imoveis e tabelas complementares
 * return mensagem de erro ou vazio se tudo bem
 * Luiz Bueno - 03/08/2018
 **********************************************/
function deletaImoveisDaTabela($condicao) {
    /* estabelece a conexão */
    $conn = conectaBanco();
    /* executa a exclusão */
    $rs = mysqli_query($conn, "DELETE FROM sg_imoveis_temp WHERE codigo $condicao");
    /* Teve um erro */
    if(!$rs) {
        /* pega o erro */
        $erro = mysqli_error($conn);
        /* fecha a conexão */
        mysqli_close($conn);
        /* retorna um erro */
        return "Erro: Excluindo registro da tabela sg_imoveis_temp  $erro <br/>" . "DELETE FROM sg_imoveis_temp WHERE codigo $condicao" ;
    }
    $rs = mysqli_query($conn, "DELETE FROM sg_caracteristica_imovel_temp WHERE id_imovel $condicao");
    /* Teve um erro */
    if(!$rs) {
        /* pega o erro */
        $erro = mysqli_error($conn);
        /* fecha a conexão */
        mysqli_close($conn);
        /* retorna um erro */
        return "Erro: Excluindo registro da tabela sg_caracteristica_imovel_temp $erro";
    }
    $rs = mysqli_query($conn, "DELETE FROM sg_fotos_temp WHERE id_imovel $condicao");
    /* Teve um erro */
    if(!$rs) {
        /* pega o erro */
        $erro = mysqli_error($conn);
        /* fecha a conexão */
        mysqli_close($conn);
        /* retorna um erro */
        return "Erro: Excluindo registro da tabela sg_fotos_temp $erro";
    }
    $rs = mysqli_query($conn, "DELETE FROM sg_imovel_videos_temp WHERE id_imovel $condicao");
    /* Teve um erro */
    if(!$rs) {
        /* pega o erro */
        $erro = mysqli_error($conn);
        /* fecha a conexão */
        mysqli_close($conn);
        /* retorna um erro */
        return "Erro: Excluindo registro da tabela sg_imovel_videos_temp $erro";
    }
    /* chegou aqui.. então tudo bem retorna se erro (vazio) */
    return "";

}



/**
 * Metodo para excluir da tabela dos imoveis e ligadas os imoveis
 * que foram excluidos da tabela de imoveis da vista
 * @NOTA Metodo alterado por 
 * Luiz Bueno - 02/04/2018
 *********************************************************/
function removeImoveis($host, $chaveKey) {
    /* conecta o banco */
    $conn = conectaBanco();
    /* inicializa retorno */
    $retorno = "";
    /* executa consulta para pegar o todos de registro na tabela */
    $rs = mysqli_query($conn, "SELECT COUNT(*) as total FROM sg_imoveis_temp");
    /* Teve um erro */
    if(!$rs) {
        /* pega o erro */
        $erro = mysqli_error($conn);
        /* fecha a conexão */
        mysqli_close($conn);
        /* retorna um erro */
        return "Erro: Lendo a tabela sg_imoveis_temp $erro";
    }

    /*  passou então continua o processo */

    /* veio algum registro */
    if (mysqli_num_rows($rs) > 0) {
        /* pega o que veio */
        $row = mysqli_fetch_array($rs, MYSQLI_ASSOC);
        /* fecha a conexão */
        mysqli_close($conn);
        /* pega o total de registros */
        $totRegistros = $row['total'];
    /* Epa! não tem Registro ?! */
    } else {
        /* fecha a conexão */
        mysqli_close($conn);
        /* retorna um erro */
        return "Erro: Não foi encontrado nenhum registro na tabela sg_imoveis!! Erro Grave-Informe o suporte";
    }


    /* estabelece os Limites */
    $limite = 300;
    $linhas = ceil($totRegistros / $limite);
    $current = 0;
    $codigos = array();
    $codigosExcluidos = array();
    /* varre todas as linhas */
    while ($current  < $linhas) {
        /* pega a faixa */
        $offset = $current*$limite;
        /* estabelece a conexão */
        $conn = conectaBanco();
        /* executa consulta para pegar o todos de registro na tabela */
        $rs = mysqli_query($conn, "SELECT codigo FROM sg_imoveis_temp LIMIT $offset, $limite");
        /* epa! não executou */
        if(!$rs) {
            /* pega o erro */
            $erro = mysqli_error($conn);
            /* fecha a conexão */
            mysqli_close($conn);
            /* retorna um erro */
            return "Erro: Lendo a tabela sg_imoveis_temp $erro";
        }
        /* chegou aqui...varre todos os registros */
        while($row = mysqli_fetch_array($rs, MYSQLI_ASSOC)) {
            /* inclui no array */
            array_push($codigos, $row['codigo']);
        }
        /* fecha a conexão */
        mysqli_close($conn);
        /* implode o array $codigos */
        $imoveisCodigo = "[" . implode(", ", $codigos) . "]";
        /* executa a api rest via curl */
        $curll = curl_init();
        curl_setopt_array($curll, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL =>  $host .'/imoveis/removidos?key='. $chaveKey,
                CURLOPT_HTTPHEADER => array('Accept: application/json'),
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => 'imoveis='. $imoveisCodigo,

        ));
        /* pega o array de retorno */
        $resp = (array)json_decode(curl_exec($curll));
        /* se veio o status 200 encerra o loop */
        if (isset($resp->status) && $resp->status == 200 ) {
            /* força a linha com uma nova excessão */
            echo "não foi encotrado imoveis na pagina $offset <br/>";
            /* próximo */
            continue;
        } 
        if (isset($resp->status) && $resp->status == 400) {
            /* teve erro.. retorna o erro */
            return "Erro: 400 - Fale com suporte!";
        }

        /* varre o array .. */
        foreach($resp as $k => $v) {
            /* empurra para dentro do array dos imoveis excluidos */
            array_push($codigosExcluidos,  $v);
        }
        $codigos = array(); //Limpa array de codigos temporarios do select
        curl_close($curll);
        $current++;
    }
    /* se veio pelo menos 1 imovel para ser excluido */
    if (!empty($codigosExcluidos)) {
        /* cria a condição para exclusão */
        $condicao = " IN (" . implode(", ", $codigosExcluidos) . ")";
        /* tenta executar a exclusão e retorna vazio (tudo ok ou mensagem de erro) */
        return deletaImoveisDaTabela($condicao);
    /* não tem exclusão... */    
    } else {
        /* retorna vazio */
        return "";
    }
}

/* tenta ver se o imovel já está cadastrado.. */
function imovelCadastrado($imovel) {
    /* monta a sql */
    $sql = "select codigo from sg_imoveis_temp where codigo = '$imovel'";
    /* estabelece a conexão */
    $conn = conectaBanco();
    /* executa consulta para pegar o todos de registro na tabela */
    $rs = mysqli_query($conn, $sql);
    /* epa! não executou */
    if(!$rs) {
        /* pega o erro */
        $erro = mysqli_error($conn);
        /* fecha a conexão */
        mysqli_close($conn);
        /* retorna um erro */
        return "Erro: verificando se o imóvel existe => $erro";
    /* se passou.. */    
    } else {
        /* retorna se veio mais pelo menos 1 registro (como true) */
        if (mysqli_num_rows($rs) > 0) {
            /* encerra a conexão */
            mysqli_close($conn);
            /* informa que sim */
            return "sim";
        } else {
            /* encerra a conexão */
            mysqli_close($conn);
            /* informa que não */
            return "nao";
        }
    }
}


/**
 * Metodo para inserir um novo imovel
 * no arquivo temporário
 * return "" ou erro
 * Luiz Bueno - 30/03/2018
 ******************************************/
function insereNovoImovel($dados, $tudo = false) {
    /* se não for tudo */
    if (!$tudo) {
        /* ve se o imovel existe */
        $retorno = imovelCadastrado($dados['codigo']);
        /* se  veio.. */
        if ($retorno) {
            /* e o que veio é maior que 5 .. então é erro */
            if ( strlen($retorno) > 5) {
                /* é erro retorna o erro.. */
                return $retorno;
            /* se não */    
            } else {
                /* e o imovel existe */
                if ($retorno == 'sim') {
                    /* então temos que exclui-lo de todas as tabelas */
                    $retorno = deletaImoveisDaTabela(" = '" . $dados['codigo'] . "'");
                    /* teve erro.. */
                    if (!empty($retorno)) {
                        /* sai com erro */
                        return $retorno;
                    }
                }
            }
        }
    }


    /* chegou aqui... então é para incluir incodicionalmente */

    /* instacia variaveis de apoio */
    $complemento = array();
    $set = "";
    $retorno = "";
    /* varre dados */
    foreach ($dados as $key => $value) {
        /**
         * Se value for um objeto e não uma
         * string.. 
         *********************************/
        if (is_array($value) or is_object($value)) {
            /**
             * salva incluir nas tabela
             * complementares
             *********************************/
            $complemento[$key] = $value;
        /* caso contrário */    
        } else {
            /* se set não estiver vazia */
            if (!empty($set)) {
                /* inclui a virgula */
                $set.= ", ";
            }
            /* monta */
            $set.= $key . "= '$value'";
        }
    }
    /* salva o codigo do imovel */
    $codigoImovel = $dados['codigo'];
    /* monta a sql */
    $sql = "INSERT INTO sg_imoveis_temp SET $set";
    /* conecta o banco */
    $conn = conectaBanco();
    /* se consegui conectar.. */
    if($conn) {
        /* tenta inserir o imovel */
        $rs = mysqli_query($conn, $sql);
        /* se teve erro.. */
        if (!$rs) {
            /* retorna o erro */
            echo "Erro Gravando imovel : $sql <br/> " . mysqli_error($conn) . "<br/>";
            /* fecha o banco */
            mysqli_close($conn);
            /* cai fora */
            return "";
        }
        /* fecha o banco */
        mysqli_close($conn);
    /* se não */    
    } else {
        /* ecoa o erro */
        echo "Erro Gravando imovel : $sql <br/>";
        /* cai fora */
        return "";
    }

    /* chegou aqui.. vamos gravar as outras tabelas */

    /* conecta o banco */
    $conn = conectaBanco();
    /* se consegui conectar */
    if ($conn) {
        /* varre o array caracteristica */
        foreach ($complemento['caracteristicas'] as $key => $value) {
            /* monta a sql para gravar a caracteristica */
            $sql = "
                INSERT INTO sg_caracteristica_imovel_temp
                SET         id_imovel = '$codigoImovel',
                            id_tipo_caracteristica_imovel = '1',
                            label = '$key',
                            valor = '$value'
            ";
            /* tenta inserir a caracteristica */
            $rs = mysqli_query($conn, $sql);
            /* se teve erro.. */
            if (!$rs) {
                /* ecoa o erro */
                echo "Não conseguiu incluir a caracteristica $key " . mysqli_error($conn) . "<br/>";
                /*  vamos então para a próxima */
                continue;
            }
        }
        /* fecha o banco */
        mysqli_close($conn);
    /* se não */    
    } else {
        /* ecoa o erro */
        echo "Não conseguiu incluir as caracteristica do imovel $codigoImovel<br/>";
    }


    /* conecta o banco */
    $conn = conectaBanco();
    /* se consegui conectar */
    if ($conn) {
        /* varre o array Infra estrutura */
        foreach ($complemento['infraestrutura'] as $key => $value) {
            /* monta a sql para gravar a caracteristica */
            $sql = "
                INSERT INTO sg_caracteristica_imovel_temp
                SET         id_imovel = '$codigoImovel',
                            id_tipo_caracteristica_imovel = '2',
                            label = '$key',
                            valor = '$value'
            ";
            /* tenta inserir a caracteristica da infra */
            $rs = mysqli_query($conn, $sql);
            /* se teve erro.. */
            if (!$rs) {
                /* ecoa o erro */
                echo "Não conseguiu incluir a infra $key na sql: $sql <br/> " . mysqli_error($conn) . "<br/>";
                /* fecha o banco */
                mysqli_close($conn);
                /*  vamos então para a próxima */
                continue;
            }
        }
        /* fecha o banco */
        mysqli_close($conn);
    /* se não */    
    } else {
        /* ecoa o erro */
        echo "Não conseguiu incluir as infra do imovel $codigoImovel<br/>";
    }

    /* se veio as fotos */
    if (isset($complemento['foto'])) {

        /* conecta o banco */
        $conn = conectaBanco();
        /* se consegui conectar */
        if ($conn) {
            /* tenta gravar...e trata a exceção */
            try {
                /* inicializa sql */
                $sql = "";
                /* varre o array Foto */
                foreach ($complemento['foto'] as $key => $foto) {
                    /* se data for vazia */
                    if ($foto['data'] == '') {
                        $foto['data'] = '1900-01-01';
                    }

                    /* monta a sql */
                    $sql = "
                        INSERT INTO sg_fotos_temp
                        SET         id_imovel = ' $codigoImovel',
                                    data = '" . $foto['data'] . "',
                                    descricao = '" . str_replace("'","",$foto['descricao'])  . "',
                                    destaque = '" . $foto['destaque'] . "',
                                    foto = '" . $foto['foto'] . "',
                                    foto_pequena = '" . $foto['foto_pequena'] . "',
                                    tipo = '" . $foto['tipo'] . "'
                    ";
                    /* tenta inserir a caracteristica da infra */
                    $rs = mysqli_query($conn, $sql);
                    /* se teve erro.. */
                    if (!$rs) {
                        /* ecoa o erro */
                        echo "Não conseguiu incluir a foto " . mysqli_error($conn) . "<br/>";
                        /*  vamos então para a próxima */
                        continue;
                    }
                }
                /* fecha o banco */
                mysqli_close($conn);
            /* xi teve algum erro de excessao */     
            } catch (Exception $e) {
                /* informa o erro de excessão */
                echo "Erro INSERINDO FOTOS DO IMOVEL $sql : " . $e->getMessage();
            }
        /* se não */    
        } else {
            /* ecoa o erro */
            echo "Não conseguiu incluir as FOTOS do imovel $codigoImovel<br/>";
        }

    }

    /* se veio as fotos do empreendimento */
    if (isset($complemento['fotoempreendimento'])) {

        /**
         * vamos gravar as fotos do empreendimento 
         **********************************************/

        /* conecta o banco */
        $conn = conectaBanco();
        /* se consegui conectar */
        if ($conn) {


            /* tenta gravar...e trata a exceção */
            try {
                /* inicializa sql */
                $sql = "";
                /* varre o array Foto */
                foreach ($complemento['fotoempreendimento'] as $key => $foto) {
                    /* se data for vazia */
                    if ($foto['data'] == '') {
                        $foto['data'] = '1900-01-01';
                    }
                    /* monta a sql */
                    $sql = "
                        INSERT INTO sg_fotos_temp
                        SET         id_imovel = ' $codigoImovel',
                                    data = '" . $foto['data'] . "',
                                    descricao = '" . str_replace("'","",$foto['descricao'])  . "',
                                    destaque = '" . $foto['destaque'] . "',
                                    foto = '" . $foto['foto'] . "',
                                    foto_pequena = '" . $foto['foto_pequena'] . "',
                                    tipo = 'condominio'
                    ";
                    /* tenta inserir a caracteristica da infra */
                    $rs = mysqli_query($conn, $sql);
                    /* se teve erro.. */
                    if (!$rs) {
                        /* ecoa o erro */
                        echo "Não conseguiu incluir a foto empreendimento " . mysqli_error($conn) . "<br/>";
                        /*  vamos então para a próxima */
                        continue;
                    }
                }
                /* fecha o banco */
                mysqli_close($conn);
            /* xi teve algum erro de excessao */     
            } catch (Exception $e) {
                /* informa o erro de excessão */
                echo "Erro INSERINDO FOTOS DO CONDOMINIO IMOVEL $sql : " . $e->getMessage();
            }
        /* se não */    
        } else {
            /* ecoa o erro */
            echo "Não conseguiu incluir as FOTOS CONDOMINIO do imovel $codigoImovel<br/>";
        }
    } 
    // /* fecha o banco */
    // mysqli_close($conn);
    /* chegou aqui...então tudo foi gravado corretamente */
    return "";
}


/* Aqui a magiaca acontece */
function import($host, $chaveKey) {
    /* estamos começando */
    echo "STATUS ANDAMENTO IMPORTAÇÃO: <br/><br/>Inicio da Importação: " . getAgora() . "<br/>";
    /* instacia o array com os campos de sicronização */
    $colImoveis = inicializaArrayImoveis();
    /* pega a data/hora da importação */
    $agora =  getAgora();

    /* pega a data da ultima importação */
    $dataUltimaImportacao = pegaUltimaDataImportacao();


    // /* se a data da ultima importação e a de hoje for a mesma */
    // if (date("Y-m-d", strtotime($dataUltimaImportacao)) == date("Y-m-d", strtotime($agora))) {
    //     /* então informa e cai fora */
    //     echo "Importação já realizada na data de hoje <br/>";
    //     /* encerra */
    //     exit;
    // }

    /* inicializa variaveis da apoio */
    $importados = 0;
    /* bloco com tratamento de erro */
    try {

        /* cria as tabelas temporárias para receber os imóveis */    
        $retorno = criaTabelasTemporias(empty($dataUltimaImportacao));
        /* se veio algum erro */
        if(!empty($retorno)) {
           /* gera uma linha com erro de exceção */
           throw new Exception("Tentativa de Criar Tabelas Temporárias<br/> $retorno <br/>");
        }
        /* Se veio uma data de importação */
        if ( $dataUltimaImportacao != '') {
            /* andamento */
            echo "Removendo Imoveis Excluidos.." . getAgora() . "<br/>";
            /* tenta remover os imoveis excluido no Vista */
            $ret = removeImoveis($host, $chaveKey);
            if (!empty($ret)) {
                /* gera uma linha com erro de exceção */
                throw new Exception("Tentativa de Remoção de Imóveis<br/> $ret <br/>");
            }
            /* andamento */
            echo "Imoveis removidos " . getAgora() . "<br/>";
        }
        /* inicializa o curl */
        $curl = curl_init();
        /* executa para pegar a lista dos campos que vem do vista */
        curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $host.'/imoveis/listarcampos?key='.$chaveKey,
                CURLOPT_HTTPHEADER => array('Accept: application/json')
        ));
            
        /* pega o arry de campos */    
        $campo = (array)json_decode(curl_exec($curl));

// /* TESTE */
// dump($campo);
// exit;

        /* ecerra o curl */
        curl_close($curl);
        /* inicializa o array para as colunas */    
        $colunas = array();
        /* varre o que veio só para o array imoveis */
        foreach ($campo['imoveis'] as $index => $col) {
            /* esse campo do vista é tem na tabela sg_imoveis */
            if (isset($colImoveis)) {
                /* inclui então  */
                $colunas[] = $col;  
            } 
        }
        /* instancia o arreay campos e camposFotos para registro  */
        $campos['fields'] = array('Codigo');
        $campos['fields'][] = "Caracteristicas";
        $campos['fields'][] = "InfraEstrutura";
        $camposFotos['fields'] = $colunas;
        $camposFotos['fields'][] = array('Foto' => $campo['Foto']);
        $camposFotos['fields'][] = array('FotoEmpreendimento' => $campo['FotoEmpreendimento']);
        $camposFotos['fields'][] = array('Video' => $campo['Video']);
        $camposFotos['fields'][] = "Caracteristicas";
        $camposFotos['fields'][] = "InfraEstrutura";
        $camposFotos['fields'][] = array('Agencia' => array('Nome','DDD','Fone','Fone2'));
        /* inicializa as variaveis contadoras */
        $pagina = 1;
        $totalImportados = 1;

        /**
         * Se veio uma data de importação então
         * é Somente Atualização..
         * se não
         * É carregar do zero
         ****************************************/


        if ( $dataUltimaImportacao != '' ) {
            $pesquisar = array(
                    'fields' => $campos['fields'],
                    'filter' => array(
                            array('ExibirNoSite' => array('Sim','sim'),
                                    'DataHoraAtualizacao' => array(str_replace(' ', '+', $dataUltimaImportacao), str_replace(' ','+', $agora))
                            )
                    ),
                    'paginacao' => array(
                            'pagina'        => $pagina,
                            'quantidade'    => 50
                    ));
        } else {

            $pesquisar = array(
                    'fields' => $campos['fields'],
                    'filter' => array(
                            array('ExibirNoSite' => array('Sim','sim'))
                    ),
                    'paginacao' => array(
                            'pagina'        => $pagina,
                            'quantidade'    => 50
                    ));
        }

        /* zera contadores outros */
        $importados = 0;
        $tentativa = 0;

        /* pega todas as solicitações ate fim o erro */    
        while (true) {

tentaNovamente:            

            /**
             * inicializa o array urls que irá armazenar 
             * todas as inforações dos imóveis que vem
             * do Vista em cada bloco 
             ********************************************/
            $urls = array();

            /* instancia o curl */
            $curll = curl_init();
            /* set com o array de opões */
            curl_setopt_array($curll, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $host .'/imoveis/listar?&key='.$chaveKey.'&pesquisa='.json_encode($pesquisar),
                    CURLOPT_HTTPHEADER => array('Accept: application/json')
            ));
            /* executa e pega o retorno */
            $ret =  curl_exec($curll);

            /* se veio algum erro */
            if(curl_errno($curll)) {
                /* aciona tentativas */
                $tentativa++;
                /* se ainda não atigiu o numero maximo de tentativas */
                if($tentativa < 10) {
                    /* so informa */
                    echo "erro Erro carregando executando /imoveis/listar na pagina " . $pesquisar['paginacao']['pagina'] . ": " . curl_error($curll) . "<br/>";
                    /* encerra o curl */
                    curl_close($curll);
                    /* tenta novamente */
                    goto tentaNovamente;
                /* se não.. */    
                } else {
                    /* força a linha com uma nova excessão */
                    throw new Exception("Erro carregando executando /imoveis/listar na pagina " . $pesquisar['paginacao']['pagina'] . ": " . curl_error($curll));
                }
            }
            /* pega a resposta decodificando o retorno json */
            $resp = json_decode($ret);
            /* se veio o status 200 encerra o loop */
            if (isset($resp->status) && $resp->status == 200 ) {
                /* força a linha com uma nova excessão */
                echo "Fim de Arquivo <br/>";
                break;
            } 
            if (isset($resp->status) && $resp->status == 400) {
                /* força a linha com uma nova excessão */
                throw new Exception("Erro: 400 - Fale com suporte!");
            }

            /* monta a pesquisa para pegar os dados do imovel */
            $pesquisaD = array(
                    'fields' => $camposFotos['fields'],
                    'filter' => array(
                            array('ExibirNoSite' => array('Sim','sim'))
                    ),
            );
            /* Varre os imoveis que veio na pagina */

            foreach ($resp as $key => $value) {
                /* carrega o array com os imoveis da pagina retorno */    
                $urls[] = $host .'/imoveis/detalhes?&key='.$chaveKey.'&imovel='. $value->Codigo .'&pesquisa='.json_encode($pesquisaD);
            }
            // $idimovelteste = "74226";
            // $urls[] = $host .'/imoveis/detalhes?&key='.$chaveKey.'&imovel='. $idimovelteste .'&pesquisa='.json_encode($pesquisaD);

            /* andamento */
            echo "Pegando dados dos imóveis da pagina $pagina .." . getAgora() . "<br/>";
            /* carrega os detalhes dos imóveis passado por urls */ 
            $imoveis = pegaDadosImoveis($urls); 
            /* se veio um erro */
            if(isset($imoveis['erro'])) {
                /* gera uma linha com erro de exceção */
                throw new Exception($imoveis['erro']);
            }
            /* andamento */
            echo "Dados dos imóveis da pagina $pagina capturados .." . getAgora() . "<br/>";
            /* andamento */
            echo "<br/><br/>Iniciando Gravação dos imóveis capturados:<br/>";

            /* passou!! então.. */ 



            /* andamento */
            echo "Incluindo/Alterando lote da pagina $pagina .. " . getAgora()  . "<br/>";

            /* varre todos os imóveis dessa pagina */
            foreach ($imoveis as $imovel) {
                /* instancia o array para guardar todos os dados de importação */                        
                $dados = array();
                /* varre os dados de cada imovel */
                foreach ($imovel as $k => $v) {
                    /**
                     * NORMALIZAR O CAMPO
                     **********************************/
                    $coluna = normalizeCampos($k, $v, $colImoveis);
                    /* se não veio nada */
                    if (empty($coluna)) {
                        /* vamos para o próximo */
                        continue;
                    }
                    /* se o campo for um array */
                    if(is_array($coluna)) {
                        /* carrega direto */
                        $dados[strtolower($k)] = $coluna;
                    /* se for somente um campo */    
                    } else {
                        /* de acordo com a coluna */                            
                        switch ($coluna)  {
                            /* se for categoria */
                            case 'categoria':
                                /* inclui na coluna categoria o id da categora */
                                $dados[$coluna] = gravaCategoria($v);
                                /* encerra aqui */
                                break;
                            /* qual quer outra coluna */    
                            default:
                                /* se tiver valor e não for nulo */
                                if (!empty($v) && !is_null($v) ) {
                                    // /* é uma string */
                                    // if (is_string($v)) {
                                    //     /* retira apostrofe */
                                    //     $v = str_replace("'","",$v);
                                    // }
                                    /* so carrega */
                                    $dados[$coluna] = str_replace("'","",utf8_decode(str_replace("'","",$v)));
                                }
                                /* encerra */
                                break;
                        }

                    }

                    /* informa a data e hora da importação */    
                    $dados['data_importacao'] = $agora;  
                }
                /* tenta gravar com tratamento de exceção */    
                try {
                    /* insere e pega o retoro */
                    $retorno = insereNovoImovel($dados, empty($dataUltimaImportacao));
                    /* se voltou algo.. então teve erro */
                    if (!empty($retorno)) {
                        /* gera uma linha com erro de exceção */
                        throw new Exception($retorno);
                    }                            
                    /* totaliza os importados */
                    $importados++;

                /* caiu aqui.. teve erro */        
                } catch (Exception $e) {
                    /* se o erro for duplicação do imovel */
                    if ( $e->getCode() == 23000 ) {
                        /* informa somente */
                        echo "<br> Imóvel duplicado";
                    /* se não */        
                    } else {
                        /* encerra informando o erro */    
                        exit("<br>Falha \n\r". print_r($e->getMessage(),1));
                    }
                    /* se passou continua para o próximo imovel */
                    continue;
                }
            }

            /* andamento */
            echo "Lote da pagina $pagina incluido/alterado com sucesso .. " . getAgora()  . "<br/>";
            /* encerra o curl */
            curl_close($curll);
            /* incrementa contador de paginas */
            $pesquisar['paginacao']['pagina']++;
            $pagina = $pesquisar['paginacao']['pagina'];
        }
         
        /* chegou aqui.. deu tudo certo */

        /* finaliza e tenta trocar as tabelas atualizando o banco */
        $retorno = trocaTabelasImportacao();                
        /* se veio algum erro */ 
        if(!empty($retorno)) {
            /* gera uma linha com erro de exceção */
            throw new Exception($retorno);
        }

    /* caiu aqui.. erro de excessão */        
    } catch (Exception $e) {
        /* ecoa o erro de excessão */
        echo 'Exceção capturada: ' , $e->getMessage(), "<br/>" ;
    }
    //exit ( "<br>".date("d-m-Y H:i:s")."<br> $importados importado(s)" );

    /* informa o fim da importação */
    echo "<br/>Fim da importação: " . getAgora() .   " com $importados importado(s)";
    /* encerra o processo */
    exit;
}


/* instacia a variavel que recebe o endereço host e a chave */
$chaveKey = "082adf7ad2fb5913fc0d737e16deac1f";
$host = "http://carbonei-rest.vistahost.com.br";
/* start */
import($host, $chaveKey);



?>