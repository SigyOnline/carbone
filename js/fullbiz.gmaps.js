/* 
	Arquivo que concentra todas as funcionalidades baseadas no google maps V3  
*/

//declara variavais globais
var gLocalSearch;
var gMap;
var gInfoWindow;
var gPosition;
var gSelectedResults = [];
var gCurrentResults = [];
var gSearchForm;
var gBuscas = [];
var gMarkers = [];

var aCounter = 0;
var clickString = "";

var gIcon = new google.maps.MarkerImage(
	"http://google-maps-icons.googlecode.com/files/info.png",
	new google.maps.Size(32, 37),
	new google.maps.Point(0,0),
	new google.maps.Point(0, 7)
);


function start_maps() {
	//carrega mapas
	if ($("div.load-gmap").length>0) {
		$("div.load-gmap").each(function(){
			var cor = $(this).find("input.gmaps-cor").val();
			var km = $(this).find("input.gmaps-km").val();
			var icon = $(this).find("input.gmaps-icon").val();
			var zoom = $(this).find("input.gmaps-zoom").val();
			var src = $(this).find("input.gmaps-src").val(); if (src===null||src===undefined) src = "";
			var mapid = $(this).attr("id");
			var v = new Array();
			i = -1;
			$(this).find("ul li").each(function(){
				i++;
				var str = $(this).html()+'||'+cor+'||'+km+'||'+icon+'||'+src;
				v[i] = str.split("||");
			});
			$(this).html("Carregando mapa...");
			mapa_imovel(v,mapid,zoom);
		});
	}
}

//carrega o mapa do imóvel
function mapa_imovel(v,mapid,zoommap) {
	if ($("#"+mapid).length>0) {
		var position = new google.maps.LatLng(parseFloat(v[0][0]),parseFloat(v[0][1]));
		gPosition = position;
		var gOpts = {
		  zoom: parseInt(zoommap),
		  scrollwheel: false,
		  streetViewControl: true,
		  center: position,
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		gMap = new google.maps.Map(document.getElementById(mapid), gOpts);
		for (var i=0;i<v.length;i++) {
			mapa_imovel_marker(v[i]);
		}
		if ($("#gmap-nearby").length>0) {
			$("#gmap-nearby ul li a").click(function(){
				var procurar = $(this).attr("rel");
				if (!$(this).hasClass("active")) {
					var $a = $(this);
					if (!$a.hasClass("c")) {
						aCounter++;
						$a.addClass("c");
						$a.addClass("c-"+aCounter);
						var vp = procurar.split("|");
						var string = vp[0];
						$a.data("string",string);
						$a.data("c",aCounter);
						$a.addClass("string-"+string);
					}
					mapa_imovel_search(procurar);
					$a.addClass("active");
				} else {
					mapa_imovel_limpa_search(procurar)
					$(this).removeClass("active");
				}
				return false;
			})
		}
	}
}

//adiciona um ponto/marcador no mapa
function mapa_imovel_marker(v) {
	var marker_position = new google.maps.LatLng(parseFloat(v[0]),parseFloat(v[1]));
	if (v[2]=='circulo') {
		var gCircle = new google.maps.Circle({
		  map: gMap,
		  strokeColor: v[4],
		  strokeWeight: 2,
		  strokeOpacity: 1,
		  zIndex: 5000,
		  radius: eval(v[5]),
		  center: marker_position
		});
	}
	if (v[2]=='ponto') {
		if (v[6]) {
			var icone = 'img/design/gmaps_icon.png';
			if (v[7]!="") icone = v[7];
		} else {
			var icone = '';
		}
		var marker = new google.maps.Marker({
			position: marker_position, 
			map: gMap,
			icon: icone
		});
		if (v[3]!="") {
			var infowindow = new google.maps.InfoWindow({
				content: v[3]
			});
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(gMap,marker);
			});
		}
	}
}


function mapa_imovel_limpa_search(procurar) {
	var vp = procurar.split("|"); //explode string delimitada por |
	var string = vp[0]; //posicao 0 = tipo de estabelecimento
	var c = $("#gmap-nearby .string-"+string).data("c");
	if (!(gMarkers[c]===undefined)) {
		var amarkers = gMarkers[c];
		for (var i = 0; i < amarkers.length; i++) {
			amarkers[i].setMap(null);
		}
	}
}

function mapa_imovel_search(procurar) {
	var vp = procurar.split("|"); //explode string delimitada por |
	var string = vp[0]; //posicao 0 = tipo de estabelecimento
	clickString = string;
	var icone = vp[1]; //posicao 1 = icone do google
	//gIcon = new google.maps.MarkerImage("http://google-maps-icons.googlecode.com/files/"+icone+".png"); //cria gicon
	gIcon = new google.maps.MarkerImage("imagens/icones-mapas/"+icone+".png");
	var service = new google.maps.places.PlacesService(gMap);

	var c = $("#gmap-nearby .string-"+string).data("c");
	if (gBuscas[c]===undefined) {
		var request = {
			location: gPosition,
			radius: 1000,
			types: [string]
		};
		gInfoWindow = new google.maps.InfoWindow();
	} else {
		var request = gBuscas[c];
	}
	service.nearbySearch(request, gCallback);
	
}

function gCallback(results, status) {
	if (status == google.maps.places.PlacesServiceStatus.ZERO_RESULTS) { 
		//alert('Não foram encontrados locais deste tipo nas proximidades.');
	}
	if (status == google.maps.places.PlacesServiceStatus.OK) {
		gBuscas[clickString] = results;
		for (var i = 0; i < results.length; i++) {
			createMarker(results[i]);
		}
	}
	limpa_origem_search();
}

function createMarker(place) {
	var placeLoc = place.geometry.location;
	var marker = new google.maps.Marker({
		map: gMap,
		position: place.geometry.location,
		icon: gIcon
	});
	var c = $("#gmap-nearby a.string-"+clickString).data("c");
	if (gMarkers[c]===undefined) var gMarkers2 = []; else var gMarkers2 = gMarkers[c];
	gMarkers2.push(marker);
	gMarkers[c] = gMarkers2;
	google.maps.event.addListener(marker, 'click', function() {
		gInfoWindow.setContent(place.name);
		gInfoWindow.open(gMap, this);
	});
}


function unselectMarkers() {
	for (var i = 0; i < gCurrentResults.length; i++) {
		gCurrentResults[i].unselect();
	}
}

function limpa_origem_search(){
	var $obj = $("#map>div:first-child div:last");
	if ($obj.height()==19) $obj.remove();
}

  
$(document).ready(function(){
	start_maps();
});
//GSearch.setOnLoadCallback(start_maps);
