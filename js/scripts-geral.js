<!-- geral -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

<!--Script links externos-->
 <script type="text/javascript">
   $(document).ready(function(){
   $('a[rel=external]').attr('target','_blank');
  });
</script>

<!-- Script limpa conteudo do input no clique -->
<script type="text/javascript">
function apagaCampo(campo){
if (campo.value == campo.defaultValue) {
	campo.value = '';}
}
function preencheCampo(campo){
if (campo.value == "") {
	campo.value = campo.defaultValue;}
}
</script>

<!--script menu-->
<script type="text/javascript" src="js/jquery-latest.min.js"></script>
<script src="js/script.js"></script>

<!--busca principal-->
<script type="text/javascript" >
	$(document).ready(function()
	{
	$(".search").click(function()
	{
	var X=$(this).attr('id');

	if(X==1)
	{
	$(".submenu").fadeOut();
	$(this).attr('id', '0');	
	}
	else
	{

	$(".submenu").fadeIn();
	$(this).attr('id', '1');
	}
		
	});

	//Mouseup textarea false
	$(".submenu").mouseup(function()
	{
	return false
	});
	$(".search").mouseup(function()
	{
	return false
	});


	//Textarea without editing.
	$(document).mouseup(function()
	{
	$(".submenu").hide();
	$(".search").attr('id', '');
	});
		
	});
	
</script>

<!-- likebox -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-547fae404a810197" async="async"></script>


<!--[(gte IE 6)&(lte IE 8)] Usado para funcionar last-child -->
<script type="text/javascript" src="js/selectivizr.js"></script>

<!-- Script Busca -->
<script type="text/javascript" src="js/sigy.busca.js"></script>

<!-- lightboxs -->
<script type="text/javascript">
$(document).ready(function() {
	//seleciona os elementos a com atributo name="modal"
	$('a[name=modal]').click(function(e) {
	//cancela o comportamento padrão do link
	e.preventDefault();

	//armazena o atributo href do link
	var id = $(this).attr('href');

	//armazena a largura e a altura da tela
	var maskHeight = $(document).height();
	var maskWidth = $(window).width();

	//Define largura e altura do div#mask iguais ás dimensões da tela
	$('#mask').css({'width':maskWidth,'height':maskHeight});

	//efeito de transição
	$('#mask').fadeIn(600);
	$('#mask').fadeTo("slow",0.8);

	//armazena a largura e a altura da janela
	var winH = $(window).height();
	var winW = $(window).width();
	//centraliza na tela a janela popup
	$(id).css('top',  winH/2-$(id).height()/2);
	$(id).css('left', winW/2-$(id).width()/2);
	//efeito de transição
	$(id).fadeIn(600);
	});

	//se o botãoo fechar for clicado
	$('.window .close').click(function (e) {
	//cancela o comportamento padrão do link
	e.preventDefault();
	$('#mask, .window').hide();
	});

	//se div#mask for clicado
	$('#mask').click(function () {
	$(this).hide();
	$('.window').hide();
	});
	});
</script>

<!-- Fancybox -->
<script type="text/javascript" src="js/jquery.fancybox-1.3.4.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(" .ligamos_lightbox, .contato_lightbox, .sugestao_lightbox, .whatsapp_lightbox").fancybox({
            'transitionIn'	  : 'elastic',
            'transitionOut'	  : 'elastic',
            'margin'          : '0px',
            'padding'         : '0px',
            'showCloseButton' : false,
            'overlayColor'    : '#000',
            'titleShow'       : false,
            'scrolling'       : 'no',
            'onStart'         : function(){$('head').append('');}
        });
        $('.closeFancyBox').live('click', function(){$.fancybox.close()});
    });
</script>